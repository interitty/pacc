<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Pacc\Parser\ParserInterface;
use Interitty\Pacc\Tokens\Token;
use Interitty\Pacc\Tokens\Tokenizer;
use Interitty\Tokenizer\BaseTokenizerParser;
use Interitty\Tokenizer\Tokenizer as BaseTokenizer;

/**
 * Fills grammar from token stream
 *
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class GrammarParser extends BaseTokenizerParser implements ParserInterface
{
    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        $parser = new self();
        return $parser->parse($input);
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return BaseTokenizer
     */
    protected function createTokenizer(string $string): BaseTokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }
    // </editor-fold>

    /**
     * Syntax processor
     *
     * @return bool
     */
    protected function processSyntax(): bool
    {
        $reduced = false;
        if ($this->processToplevels() === true) {
            if ($this->processRules() === true) {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * Toplevels processor
     *
     * @return bool
     */
    protected function processToplevels(): bool
    {
        $reduced = false;
        if ($this->processToplevel() === true) {
            if ($this->processToplevels() === true) {
                $reduced = true;
            } else {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * OptionalReturnType processor
     *
     * @return bool
     */
    protected function processOptionalReturnType(): bool
    {
        $reduced = false;
        if ($this->currentTokenLexeme() === ':') {
            $this->next();
            if ($this->currentTokenType() === Token::TOKEN_ID) {
                $this->next();
                $reduced = true;
            } elseif ($this->currentTokenLexeme() === '?') {
                $this->next();
                if ($this->currentTokenType() === Token::TOKEN_ID) {
                    $this->next();
                    $reduced = true;
                }
            }
        } else {
            $reduced = true;
        }
        return $reduced;
    }

    /**
     * OptionalSeparator processor
     *
     * @return bool
     */
    protected function processOptionalSeparator(): bool
    {
        $reduced = false;
        if ($this->currentTokenLexeme() === ';') {
            $this->next();
            $reduced = true;
        } else {
            $reduced = true;
        }
        return $reduced;
    }

    /**
     * GarmmarName processor
     *
     * @return bool
     */
    protected function processGarmmarName(): bool
    {
        $reduced = false;
        if ($this->currentTokenLexeme() === 'grammar') {
            $this->next();
            if ($this->processBackslashSeparatedName() === true) {
                if ($this->processOptionalSeparator() === true) {
                    $reduced = true;
                }
            }
        }
        return $reduced;
    }

    /**
     * GrammarOptions processor
     *
     * @return bool
     */
    protected function processGrammarOptions(): bool
    {
        $reduced = false;
        if ($this->currentTokenLexeme() === '@') {
            $this->next();
            if ($this->processPeriodSeparatedName() === true) {
                if ($this->processOptionalReturnType() === true) {
                    if ($this->currentTokenLexeme() === '{') {
                        $this->next();
                        if ($this->currentTokenType() === Token::TOKEN_CODE) {
                            $this->next();
                            if ($this->currentTokenLexeme() === '}') {
                                $this->next();
                                if ($this->processOptionalSeparator() === true) {
                                    $reduced = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $reduced;
    }

    /**
     * Toplevel processor
     *
     * @return bool
     */
    protected function processToplevel(): bool
    {
        $reduced = false;
        if ($this->processGarmmarName() === true) {
            $reduced = true;
        } elseif ($this->processOptions() === true) {
            $reduced = true;
        } elseif ($this->currentTokenType() === Token::TOKEN_PHPDOC) {
            $this->next();
            $reduced = true;
        } elseif ($this->processGrammarOptions() === true) {
            $reduced = true;
        }
        return $reduced;
    }

    /**
     * PeriodSeparatedName processor
     *
     * @return bool
     */
    protected function processPeriodSeparatedName(): bool
    {
        $reduced = false;
        if ($this->currentTokenType() === Token::TOKEN_ID) {
            $this->next();
            if ($this->currentTokenLexeme() === '.') {
                $this->next();
                if ($this->processPeriodSeparatedName() === true) {
                    $reduced = true;
                }
            } else {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * BackslashSeparatedName processor
     *
     * @return bool
     */
    protected function processBackslashSeparatedName(): bool
    {
        $reduced = false;
        if ($this->currentTokenType() === Token::TOKEN_ID) {
            $this->next();
            if ($this->currentTokenLexeme() === '\\') {
                $this->next();
                if ($this->processBackslashSeparatedName() === true) {
                    $reduced = true;
                }
            } else {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * Options processor
     *
     * @return bool
     */
    protected function processOptions(): bool
    {
        $reduced = false;
        if ($this->currentTokenLexeme() === 'option') {
            $this->next();
            if ($this->processSingleOption() === true) {
                if ($this->processOptionalSeparator() === true) {
                    $reduced = true;
                }
            } elseif ($this->currentTokenLexeme() === '(') {
                $this->next();
                if ($this->processMoreOptions() === true) {
                    if ($this->currentTokenLexeme() === ')') {
                        $this->next();
                        if ($this->processOptionalSeparator() === true) {
                            $reduced = true;
                        }
                    }
                }
            }
        }
        return $reduced;
    }

    /**
     * MoreOptions processor
     *
     * @return bool
     */
    protected function processMoreOptions(): bool
    {
        $reduced = false;
        if ($this->processSingleOption() === true) {
            if ($this->processOptionalSeparator() === true) {
                if ($this->processMoreOptions() === true) {
                    $reduced = true;
                } else {
                    $reduced = true;
                }
            }
        }
        return $reduced;
    }

    /**
     * SingleOption processor
     *
     * @return bool
     */
    protected function processSingleOption(): bool
    {
        $reduced = false;
        if ($this->processPeriodSeparatedName() === true) {
            if ($this->currentTokenLexeme() === '=') {
                $this->next();
                if ($this->currentTokenType() === Token::TOKEN_STRING) {
                    $this->next();
                    $reduced = true;
                } elseif ($this->currentTokenLexeme() === '{') {
                    $this->next();
                    if ($this->currentTokenType() === Token::TOKEN_CODE) {
                        $this->next();
                        if ($this->currentTokenLexeme() === '}') {
                            $this->next();
                            $reduced = true;
                        }
                    }
                }
            }
        }
        return $reduced;
    }

    /**
     * Rules processor
     *
     * @return bool
     */
    protected function processRules(): bool
    {
        $reduced = false;
        if ($this->processRule() === true) {
            if ($this->processRules() === true) {
                $reduced = true;
            } else {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * Rule processor
     *
     * @return bool
     */
    protected function processRule(): bool
    {
        $reduced = false;
        if ($this->currentTokenType() === Token::TOKEN_ID) {
            $this->next();
            if ($this->currentTokenLexeme() === ':') {
                $this->next();
                if ($this->processExpressions() === true) {
                    if ($this->currentTokenLexeme() === ';') {
                        $this->next();
                        $reduced = true;
                    }
                }
            }
        }
        return $reduced;
    }

    /**
     * Expressions processor
     *
     * @return bool
     */
    protected function processExpressions(): bool
    {
        $reduced = false;
        if ($this->processExpression() === true) {
            if ($this->currentTokenLexeme() === '|') {
                $this->next();
                if ($this->processExpressions() === true) {
                    $reduced = true;
                }
            } else {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * Expression processor
     *
     * @return bool
     */
    protected function processExpression(): bool
    {
        $reduced = false;
        if ($this->processTermsOrNothing() === true) {
            if ($this->processOptionalReturnType() === true) {
                if ($this->currentTokenLexeme() === '{') {
                    $this->next();
                    if ($this->currentTokenType() === Token::TOKEN_CODE) {
                        $this->next();
                        if ($this->currentTokenLexeme() === '}') {
                            $this->next();
                            $reduced = true;
                        }
                    }
                } else {
                    $reduced = true;
                }
            }
        }
        return $reduced;
    }

    /**
     * TermsOrNothing processor
     *
     * @return bool
     */
    protected function processTermsOrNothing(): bool
    {
        $reduced = false;
        if ($this->processTerms() === true) {
            $reduced = true;
        } else {
            $reduced = true;
        }
        return $reduced;
    }

    /**
     * Terms processor
     *
     * @return bool
     */
    protected function processTerms(): bool
    {
        $reduced = false;
        if ($this->processTerm() === true) {
            if ($this->processTerms() === true) {
                $reduced = true;
            } else {
                $reduced = true;
            }
        }
        return $reduced;
    }

    /**
     * Term processor
     *
     * @return bool
     */
    protected function processTerm(): bool
    {
        $reduced = false;
        if ($this->currentTokenType() === Token::TOKEN_ID) {
            $this->next();
            $reduced = true;
        } elseif ($this->currentTokenType() === Token::TOKEN_STRING) {
            $this->next();
            $reduced = true;
        }
        return $reduced;
    }

    /**
     * Parse processor
     *
     * @return mixed
     */
    protected function processParse(): mixed
    {
        $status = $this->processSyntax();
        if ($status !== true) {
            throw new ParseException('Parse error');
        }
        return null;
    }
}
