grammar Interitty\Pacc\SimpleParser

/*<?php # this line is for basic syntax highlighting in PHP editors */

option
(
    algorithm = "LR";
    eol = "\n";
    extends = "Interitty\\Tokenizer\\BaseTokenizerParser";
    implements = "Interitty\\Pacc\\Parser\\ParserInterface";
    indentation = "    ";
    parse = "processParse";
    strictTypes = "1";
    terminalsPrefix = "Token::";
)

@header
{
    use Interitty\Pacc\Tokens\Token;
    use Interitty\Pacc\Tokens\Tokenizer;
    use Interitty\Tokenizer\Tokenizer as BaseTokenizer;
}

@inner
{
    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        $parser = new self();
        return $parser->parse($input);
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return BaseTokenizer
     */
    protected function createTokenizer(string $string): BaseTokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }
    // </editor-fold>
}

test
    : TOKEN_STRING
      {
          $$ = $1;
      }
    ;
