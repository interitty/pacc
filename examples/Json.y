grammar Interitty\Pacc\Json

/*<?php # this line is for basic syntax highlighting in PHP editors */

option
(
    algorithm = "LR";
    eol = "\n";
    indentation = "    ";
    implements = "Interitty\\Pacc\\Parser\\ParserInterface";
    parse = "processParse";
    strictTypes = "1";
    terminalsPrefix = "self::";
)

@header
{
use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Tokenizer\BaseParser;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Interitty\Utils\Arrays;
use Interitty\Utils\Strings;
use Interitty\Utils\Validators;
use stdClass;

use function array_flip;
use function array_merge;
use function assert;
use function chr;
use function explode;
use function hexdec;
use function implode;
use function in_array;
use function is_array;
use function is_numeric;
use function ltrim;
use function strtr;
use function var_export;

/**
 * JSON encoding and decoding; does not depend on any PHP module
 * @phpstan-type Token object{'type': ?string, 'lexeme': ?string}
 */
}

@inner
{
    /** All available token type constants */
    public const string TOKEN_STRING = 'string';
    public const string TOKEN_NUMBER = 'number';
    public const string TOKEN_SPECIAL = 'special';
    public const string TOKEN_KEYWORD = 'keyword';

    /** @phpstan-var array<self::TOKEN_*, string> keywords are detected according to their order in regexp */
    protected static array $patterns = [
        self::TOKEN_STRING => '~^"(([^"\\\\]|\\\\["\\\\/bfnrt]|\\\\u[0-9a-fA-F]{4})*)"~',
        self::TOKEN_NUMBER => '~^(-?(0|[1-9][0-9]*)(\\.[0-9]+([eE][+-]?[0-9]+)?)?)~',
        self::TOKEN_SPECIAL => '~^(\\{|:|,|\\}|\\[|\\])~',
        self::TOKEN_KEYWORD => '~^(true|false|null)~',
    ];

    /**
     * Conversion from escape sequences to characters
     * @var string[]
     */
    protected static array $escapes = [
        '\"' => '"',
        '\\\\' => '\\',
        '\b' => "\b",
        '\f' => "\f",
        '\n' => "\n",
        '\r' => "\r",
        '\t' => "\t",
    ];

    /** @var string JSON serialized data */
    protected string $json;

    /** @var bool Whether JSON objects should be returned as associative arrays */
    protected bool $associative = false;

    /** @phpstan-var Token Current token */
    protected object $token;

    /**
     * Decodes JSON serialized string
     *
     * @param string $json
     * @param bool $associative [OPTIONAL] whether JSON objects should be decoded as PHP arrays, not stdClass
     * @return mixed
     */
    public static function decode(string $json, bool $associative = false): mixed
    {
        $instance = new self();
        $instance->associative = $associative;
        return $instance->parse($json);
    }

    /**
     * Encodes PHP value to JSON
     *
     * @param mixed $data
     * @param bool $forceObject [OPTIONAL]
     * @return string
     */
    public static function encode(mixed $data, bool $forceObject = false): string
    {
        $encoded = 'null';
        if (($forceObject === false) && (Arrays::isList($data) === true)) {
            /** @var mixed[] $data */
            $return = [];
            foreach ($data as $value) {
                $return[] = self::encode($value, $forceObject);
            }
            $encoded = '[' . implode(',', $return) . ']';
        } elseif (Validators::is($data, 'array|iterable') === true) {
            /** @var mixed[] $data */
            $return = [];
            foreach ($data as $key => $value) {
                $return[] = self::encode((string) $key) . ':' . self::encode($value);
            }
            $encoded = '{' . implode(',', $return) . '}';
        } elseif (Validators::is($data, 'string') === true) {
            /** @var string $data */
            $encoded = '"' . strtr($data, array_flip(self::$escapes)) . '"';
        } elseif (Validators::is($data, 'numeric|bool') === true) {
            $encoded = var_export($data, true);
        }
        return $encoded;
    }

    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        return self::decode($input);
    }

    /**
     * Parses given JSON string
     *
     * @param string $json
     * @return mixed decoded JSON value
     */
    protected function parse(string $json): mixed
    {
        $this->json = ltrim($json);
        $this->next();

        try {
            $return = $this->processParse();
            return ($return === 0) ? null : $return;
        } catch (IllegalActionException $exception) {
            throw $exception->setMessage('Given text is not valid JSON string');
        }
    }

    /**
     * Decode JSON encoded string
     *
     * @param string $string
     * @return string
     */
    protected function stringDecode(string $string): string
    {
        $return = '';

        $iterator = 0;
        $fixedString = Strings::replace(strtr($string, self::$escapes), '#\\\\u([0-9a-fA-F]{4})#', "\xff\$1\xff");
        $strings = explode("\xff", $fixedString);
        unset($fixedString);

        foreach ($strings as $part) {
            if (($iterator & 1) !== 0) { // unicode escape sequence
                $char = hexdec($part);
                if ($char < 0x80) {
                    $return .= chr((int) $char);
                } elseif ($char < 0x0800) {
                    $return .= chr(($char >> 6) | 0xC0);
                    $return .= chr(($char & 0x3F) | 0x80);
                } elseif ($char < 0x10000) {
                    $return .= chr(($char >> 12) | 0xE0);
                    $return .= chr((($char >> 6) & 0x3F) | 0x80);
                    $return .= chr(($char & 0x3F) | 0x80);
                } elseif ($char < 0x200000) {
                    $return .= chr(($char >> 18) | 0xF0);
                    $return .= chr((($char >> 12) & 0x3F) | 0x80);
                    $return .= chr((($char >> 6) & 0x3F) | 0x80);
                    $return .= chr(($char & 0x3F) | 0x80);
                } else {
                    $return .= '?';
                }
            } else {
                $return .= $part;
            }

            ++$iterator;
        }

        return $return;
    }
}

/**
 * Current processor
 *
 * @phpstan-return Token
 */
@current
{
    $<object>$ = $this->token;
}

/**
 * CurrentTokenType processor
 */
@currentTokenType
{
    $<string|null>$ = $this->token->type;
}

/**
 * CurrentTokenLexeme processor
 */
@currentTokenLexeme
{
    $<string|null>$ = $this->token->lexeme;
}

/**
 * Next processor
 */
@next
{
    if (in_array($this->json, [null, ''], true)) {
        $this->token = (object) ['type' => null, 'lexeme' => null];
    } else {
        $status = false;
        foreach (self::$patterns as $type => $pattern) {
            $match = Strings::match($this->json, $pattern);
            if (is_array($match) === true) {
                /** @phpstan-var array{0: string, 1: string} $match */
                $this->token = (object) ['type' => $type, 'lexeme' => $match[1]];
                $this->json = ltrim(Strings::substring($this->json, Strings::length($match[0])));
                $status = true;
                break;
            }
        }

        if ($status !== true) {
            throw (new ParseException('Unexpected character ":character"'))->addData('character', $this->json);
        }
    }
}

value
    : TOKEN_STRING { $<string>$ = $this->stringDecode($<string>1); }
    | TOKEN_NUMBER {
        assert(is_numeric($<string>1) === true);
        $<int|float>$ = $<string>1 + 0;
    }
    | object {
        if ($this->associative === false) {
            $<stdClass>1 = (object) $<array>1;
        }
        $<array|stdClass>$ = $1;
    }
    | array { $<array>$ = $<array>1; }
    | 'true' { $<bool>$ = true; }
    | 'false' { $<bool>$ = false; }
    | 'null' { $<null>$ = null; }
    ;

object
    : '{' pairs '}' { $<stdClass>$ = (object) $2; }
    ;

pairs
    : pair { $<array>$ = [$<array#array{0: mixed, 1: mixed}>1[0] => $<array>1[1]]; }
    | pair ',' pairs { $<array>$ = array_merge([$<array#array{0: mixed, 1: mixed}>1[0] => $1[1]], $<array>3); }
    | /* nothing */ { $<array>$ = []; }
    ;

pair
    : TOKEN_STRING ':' value { $<array>$ = [$<string>1, $3]; }
    ;

array
    : '[' values ']' { $<array>$ = $<array>2; }
    ;

values
    : value { $<array>$ = [$1]; }
    | value ',' values { $<array>$ = array_merge([$1], $<array>3); }
    | /* nothing */ { $<array>$ = []; }
    ;
