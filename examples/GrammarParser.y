grammar Interitty\Pacc\GrammarParser

/*<?php # this line is for basic syntax highlighting in PHP editors */

option
(
    algorithm = "RD";
    eol = "\n";
    extends = "Interitty\\Tokenizer\\BaseTokenizerParser";
    implements = "Interitty\\Pacc\\Parser\\ParserInterface";
    indentation = "    ";
    parse = "processParse";
    strictTypes = "1";
    terminalsPrefix = "Token::";
)

@header
{
use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Pacc\Tokens\Token;
use Interitty\Pacc\Tokens\Tokenizer;
use Interitty\Tokenizer\BaseTokenizerParser;
use Interitty\Tokenizer\Tokenizer as BaseTokenizer;

/**
 * Fills grammar from token stream
 *
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
}

@inner
{
    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        $parser = new self();
        return $parser->parse($input);
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return BaseTokenizer
     */
    protected function createTokenizer(string $string): BaseTokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }
    // </editor-fold>
}

syntax : toplevels rules ;

toplevels
    : toplevel
    | toplevel toplevels
    ;

optionalReturnType
    : ':' TOKEN_ID
    | ':' '?' TOKEN_ID
    | /* nothing */
    ;

optionalSeparator
    : ';'
    |
    ;

garmmarName
    : 'grammar' backslashSeparatedName optionalSeparator
    ;

grammarOptions
    : '@' periodSeparatedName optionalReturnType '{' TOKEN_CODE '}' optionalSeparator
    ;

toplevel
    : garmmarName
    | options
    | TOKEN_PHPDOC
    | grammarOptions
    ;

periodSeparatedName
    : TOKEN_ID '.' periodSeparatedName
    | TOKEN_ID
    ;

backslashSeparatedName
    : TOKEN_ID '\\' backslashSeparatedName
    | TOKEN_ID
    ;

options
    : 'option' singleOption optionalSeparator
    | 'option' '(' moreOptions ')' optionalSeparator
    ;

moreOptions
    : singleOption optionalSeparator
    | singleOption optionalSeparator moreOptions
    ;

singleOption
    : periodSeparatedName '=' TOKEN_STRING
    | periodSeparatedName '=' '{' TOKEN_CODE '}'
    ;

rules
    : rule
    | rule rules
    ;

rule
    : TOKEN_ID ':' expressions ';'
    ;

expressions
    : expression
    | expression '|' expressions
    ;

expression
    : termsOrNothing optionalReturnType
    | termsOrNothing optionalReturnType '{' TOKEN_CODE '}'
    ;

termsOrNothing
    : /* nothing */
    | terms
    ;

terms
    : term
    | term terms
    ;

term
    : TOKEN_ID
    | TOKEN_STRING
    ;
