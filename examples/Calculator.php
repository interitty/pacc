<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use ArithmeticError;
use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Pacc\Parser\ParserInterface;
use Interitty\Tokenizer\BaseParser;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Interitty\Utils\Strings;

use function is_array;
use function is_string;

/**
 * Simple calculator implementation
 */
class Calculator extends BaseParser implements ParserInterface
{
    /** All available token type constants */
    public const string TOKEN_NUMBER = 'number';

    /** @var string|null */
    protected ?string $expression;

    /** @var string|null Current token*/
    protected ?string $token;

    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        $parser = new self();
        return $parser->processCalculate($input);
    }

    /**
     * Calculate processor
     *
     * @param string $expression
     * @return mixed
     */
    public function processCalculate(string $expression): mixed
    {
        try {
            $this->expression = $expression;
            $this->next();
            return $this->processParse();
        } catch (IllegalActionException $exception) {
            throw $exception->setMessage('Given text is not valid countable string');
        }
    }

    /** @var int[] */
    protected array $productionsEqual = [7 => 2];

    /** @var int[] */
    protected array $productionsLefts = [1 => 8, 8, 8, 8, 10, 10, 9, 9, 9];

    /** @var int[] */
    protected array $productionsLengths = [1 => 0, 1, 3, 3, 1, 3, 1, 3, 3];

    /** @var int[] */
    protected array $table = [
        0 => -1,
        1 => -1,
        2 => -1,
        3 => 4,
        4 => 5,
        8 => 1,
        9 => 2,
        10 => 3,
        11 => 0,
        12 => 6,
        13 => 7,
        22 => -2,
        23 => -2,
        24 => -2,
        28 => 8,
        29 => 9,
        33 => -7,
        34 => -7,
        35 => -7,
        39 => -7,
        40 => -7,
        44 => -5,
        45 => -5,
        46 => -5,
        50 => -5,
        51 => -5,
        56 => -1,
        57 => -1,
        58 => 13,
        59 => 14,
        60 => -1,
        63 => 10,
        64 => 11,
        65 => 12,
        69 => 4,
        70 => 5,
        75 => 15,
        76 => 3,
        80 => 4,
        81 => 5,
        86 => 16,
        87 => 3,
        91 => 4,
        92 => 5,
        98 => 17,
        102 => 4,
        103 => 5,
        109 => 18,
        111 => 19,
        112 => 20,
        115 => 21,
        122 => -2,
        123 => -2,
        126 => -2,
        127 => 22,
        128 => 23,
        133 => -7,
        134 => -7,
        137 => -7,
        138 => -7,
        139 => -7,
        144 => -5,
        145 => -5,
        148 => -5,
        149 => -5,
        150 => -5,
        155 => -1,
        156 => -1,
        157 => 13,
        158 => 14,
        159 => -1,
        162 => 24,
        163 => 11,
        164 => 12,
        165 => -3,
        166 => -3,
        167 => -3,
        171 => 8,
        172 => 9,
        176 => -4,
        177 => -4,
        178 => -4,
        182 => 8,
        183 => 9,
        187 => -8,
        188 => -8,
        189 => -8,
        193 => -8,
        194 => -8,
        198 => -9,
        199 => -9,
        200 => -9,
        204 => -9,
        205 => -9,
        212 => 13,
        213 => 14,
        218 => 25,
        219 => 12,
        223 => 13,
        224 => 14,
        229 => 26,
        230 => 12,
        231 => -6,
        232 => -6,
        233 => -6,
        237 => -6,
        238 => -6,
        245 => 13,
        246 => 14,
        252 => 27,
        256 => 13,
        257 => 14,
        263 => 28,
        265 => 19,
        266 => 20,
        269 => 29,
        276 => -3,
        277 => -3,
        280 => -3,
        281 => 22,
        282 => 23,
        287 => -4,
        288 => -4,
        291 => -4,
        292 => 22,
        293 => 23,
        298 => -8,
        299 => -8,
        302 => -8,
        303 => -8,
        304 => -8,
        309 => -9,
        310 => -9,
        313 => -9,
        314 => -9,
        315 => -9,
        320 => -6,
        321 => -6,
        324 => -6,
        325 => -6,
        326 => -6,
    ];

    /** @var int */
    protected int $tablePitch = 11;

    /** @var int[] */
    protected array $terminalsTypes = [self::TOKEN_NUMBER => 3];

    /** @var int[] */
    protected array $terminalsValues = ['+' => 1, '-' => 2, '(' => 4, ')' => 5, '*' => 6, '/' => 7];

    /**
     * Current processor
     *
     * @return string|null
     */
    protected function current(): ?string
    {
        $reduced = $this->token;
        return $reduced;
    }

    /**
     * CurrentTokenType processor
     *
     * @return string|null
     */
    protected function currentTokenType(): ?string
    {
        $reduced = null;
        if ((is_string($this->token) === true) && (is_array(Strings::match($this->token, '~^[0-9]+$~')) === true)) {
            $reduced = self::TOKEN_NUMBER;
        }
        return $reduced;
    }

    /**
     * CurrentTokenLexeme processor
     *
     * @return string|null
     */
    protected function currentTokenLexeme(): ?string
    {
        $reduced = $this->token;
        return $reduced;
    }

    /**
     * Next processor
     *
     * @return void
     */
    protected function next(): void
    {
        if (is_string($this->expression) === true) {
            $match = Strings::match($this->expression, '~^([0-9]+|\(|\)|\+|-|\*|/|\s)~');
            if (is_array($match) === true) {
                $this->token = $match[1];
                $this->expression = Strings::substring($this->expression, Strings::length($match[1]));
            } elseif ($this->expression === '') {
                $this->expression = null;
            } else {
                throw (new ParseException('Unexpected character ":character"'))
                ->addData('character', $this->expression);
            }
        }

        if ($this->expression === null) {
            $this->token = null;
        } elseif (Strings::trim((string) $this->token) === '') {
            $this->next();
        }
    }

    /**
     * Reduce 1 processor
     *
     * @return int
     */
    protected function processReduce1(): int
    {
        $reduced = 0;
        return $reduced;
    }

    /**
     * Reduce 2 processor
     *
     * @param array{1: int} $arg
     * @return int
     */
    protected function processReduce2(array $arg): int
    {
        $reduced = $arg[1];
        return $reduced;
    }

    /**
     * Reduce 3 processor
     *
     * @param array{1: int, 3: int} $arg
     * @return int
     */
    protected function processReduce3(array $arg): int
    {
        $reduced = $arg[1] + $arg[3];
        return $reduced;
    }

    /**
     * Reduce 4 processor
     *
     * @param array{1: int, 3: int} $arg
     * @return int
     */
    protected function processReduce4(array $arg): int
    {
        $reduced = $arg[1] - $arg[3];
        return $reduced;
    }

    /**
     * Reduce 5 processor
     *
     * @param array{1: string|int|float} $arg
     * @return int
     */
    protected function processReduce5(array $arg): int
    {
        $reduced = (int) $arg[1];
        return $reduced;
    }

    /**
     * Reduce 6 processor
     *
     * @param array{2: int} $arg
     * @return int
     */
    protected function processReduce6(array $arg): int
    {
        $reduced = $arg[2];
        return $reduced;
    }

    /**
     * Reduce 8 processor
     *
     * @param array{1: int, 3: int} $arg
     * @return int
     */
    protected function processReduce8(array $arg): int
    {
        $reduced = $arg[1] * $arg[3];
        return $reduced;
    }

    /**
     * Reduce 9 processor
     *
     * @param array{1: int, 3: int} $arg
     * @return int
     */
    protected function processReduce9(array $arg): int
    {
        try {
            $reduced = $arg[1] / $arg[3];
        } catch (ArithmeticError $exception) {
            throw new ParseException($exception->getMessage(), $exception->getCode(), $exception);
        }
        return $reduced;
    }
}
