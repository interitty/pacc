<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Interitty\Pacc\Parser\ParserInterface;
use Interitty\Pacc\Tokens\Token;
use Interitty\Pacc\Tokens\Tokenizer;
use Interitty\Tokenizer\BaseTokenizerParser;
use Interitty\Tokenizer\Tokenizer as BaseTokenizer;

class SimpleParser extends BaseTokenizerParser implements ParserInterface
{
    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        $parser = new self();
        return $parser->parse($input);
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return BaseTokenizer
     */
    protected function createTokenizer(string $string): BaseTokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }
    // </editor-fold>

    /** @var int[] */
    protected array $productionsEqual = [];

    /** @var int[] */
    protected array $productionsLefts = [1 => 2];

    /** @var int[] */
    protected array $productionsLengths = [1 => 1];

    /** @var int[] */
    protected array $table = [1 => 2, 2 => 1, 3 => 0, 6 => -1];

    /** @var int */
    protected int $tablePitch = 3;

    /** @var int[] */
    protected array $terminalsTypes = [Token::TOKEN_STRING => 1];

    /** @var int[] */
    protected array $terminalsValues = [];

    /**
     * Reduce 1 processor
     *
     * @param array{1: mixed} $arg
     * @return mixed
     */
    protected function processReduce1(array $arg): mixed
    {
        $reduced = $arg[1];
        return $reduced;
    }
}
