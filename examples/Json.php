<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Pacc\Parser\ParserInterface;
use Interitty\Tokenizer\BaseParser;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Interitty\Utils\Arrays;
use Interitty\Utils\Strings;
use Interitty\Utils\Validators;
use stdClass;

use function array_flip;
use function array_merge;
use function assert;
use function chr;
use function explode;
use function hexdec;
use function implode;
use function in_array;
use function is_array;
use function is_numeric;
use function ltrim;
use function strtr;
use function var_export;

/**
 * JSON encoding and decoding; does not depend on any PHP module
 * @phpstan-type Token object{'type': ?string, 'lexeme': ?string}
 */
class Json extends BaseParser implements ParserInterface
{
    /** All available token type constants */
    public const string TOKEN_STRING = 'string';
    public const string TOKEN_NUMBER = 'number';
    public const string TOKEN_SPECIAL = 'special';
    public const string TOKEN_KEYWORD = 'keyword';

    /** @phpstan-var array<self::TOKEN_*, string> keywords are detected according to their order in regexp */
    protected static array $patterns = [
        self::TOKEN_STRING => '~^"(([^"\\\\]|\\\\["\\\\/bfnrt]|\\\\u[0-9a-fA-F]{4})*)"~',
        self::TOKEN_NUMBER => '~^(-?(0|[1-9][0-9]*)(\\.[0-9]+([eE][+-]?[0-9]+)?)?)~',
        self::TOKEN_SPECIAL => '~^(\\{|:|,|\\}|\\[|\\])~',
        self::TOKEN_KEYWORD => '~^(true|false|null)~',
    ];

    /**
     * Conversion from escape sequences to characters
     * @var string[]
     */
    protected static array $escapes = [
        '\"' => '"',
        '\\\\' => '\\',
        '\b' => "\b",
        '\f' => "\f",
        '\n' => "\n",
        '\r' => "\r",
        '\t' => "\t",
    ];

    /** @var string JSON serialized data */
    protected string $json;

    /** @var bool Whether JSON objects should be returned as associative arrays */
    protected bool $associative = false;

    /** @phpstan-var Token Current token */
    protected object $token;

    /**
     * Decodes JSON serialized string
     *
     * @param string $json
     * @param bool $associative [OPTIONAL] whether JSON objects should be decoded as PHP arrays, not stdClass
     * @return mixed
     */
    public static function decode(string $json, bool $associative = false): mixed
    {
        $instance = new self();
        $instance->associative = $associative;
        return $instance->parse($json);
    }

    /**
     * Encodes PHP value to JSON
     *
     * @param mixed $data
     * @param bool $forceObject [OPTIONAL]
     * @return string
     */
    public static function encode(mixed $data, bool $forceObject = false): string
    {
        $encoded = 'null';
        if (($forceObject === false) && (Arrays::isList($data) === true)) {
            /** @var mixed[] $data */
            $return = [];
            foreach ($data as $value) {
                $return[] = self::encode($value, $forceObject);
            }
            $encoded = '[' . implode(',', $return) . ']';
        } elseif (Validators::is($data, 'array|iterable') === true) {
            /** @var mixed[] $data */
            $return = [];
            foreach ($data as $key => $value) {
                $return[] = self::encode((string) $key) . ':' . self::encode($value);
            }
            $encoded = '{' . implode(',', $return) . '}';
        } elseif (Validators::is($data, 'string') === true) {
            /** @var string $data */
            $encoded = '"' . strtr($data, array_flip(self::$escapes)) . '"';
        } elseif (Validators::is($data, 'numeric|bool') === true) {
            $encoded = var_export($data, true);
        }
        return $encoded;
    }

    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        return self::decode($input);
    }

    /**
     * Parses given JSON string
     *
     * @param string $json
     * @return mixed decoded JSON value
     */
    protected function parse(string $json): mixed
    {
        $this->json = ltrim($json);
        $this->next();

        try {
            $return = $this->processParse();
            return ($return === 0) ? null : $return;
        } catch (IllegalActionException $exception) {
            throw $exception->setMessage('Given text is not valid JSON string');
        }
    }

    /**
     * Decode JSON encoded string
     *
     * @param string $string
     * @return string
     */
    protected function stringDecode(string $string): string
    {
        $return = '';

        $iterator = 0;
        $fixedString = Strings::replace(strtr($string, self::$escapes), '#\\\\u([0-9a-fA-F]{4})#', "\xff\$1\xff");
        $strings = explode("\xff", $fixedString);
        unset($fixedString);

        foreach ($strings as $part) {
            if (($iterator & 1) !== 0) { // unicode escape sequence
                $char = hexdec($part);
                if ($char < 0x80) {
                    $return .= chr((int) $char);
                } elseif ($char < 0x0800) {
                    $return .= chr(($char >> 6) | 0xC0);
                    $return .= chr(($char & 0x3F) | 0x80);
                } elseif ($char < 0x10000) {
                    $return .= chr(($char >> 12) | 0xE0);
                    $return .= chr((($char >> 6) & 0x3F) | 0x80);
                    $return .= chr(($char & 0x3F) | 0x80);
                } elseif ($char < 0x200000) {
                    $return .= chr(($char >> 18) | 0xF0);
                    $return .= chr((($char >> 12) & 0x3F) | 0x80);
                    $return .= chr((($char >> 6) & 0x3F) | 0x80);
                    $return .= chr(($char & 0x3F) | 0x80);
                } else {
                    $return .= '?';
                }
            } else {
                $return .= $part;
            }

            ++$iterator;
        }

        return $return;
    }

    /** @var int[] */
    protected array $productionsEqual = [16 => 11];

    /** @var int[] */
    protected array $productionsLefts = [1 => 12, 12, 12, 12, 12, 12, 12, 13, 15, 15, 15, 16, 14, 17, 17, 17];

    /** @var int[] */
    protected array $productionsLengths = [1 => 1, 1, 1, 1, 1, 1, 1, 3, 1, 3, 0, 3, 3, 1, 3, 0];

    /** @var int[] */
    protected array $table = [
        1 => 4,
        2 => 5,
        3 => 6,
        4 => 7,
        5 => 8,
        6 => 9,
        10 => 10,
        12 => 1,
        13 => 2,
        14 => 3,
        18 => 0,
        36 => -3,
        54 => -4,
        72 => -1,
        90 => -2,
        108 => -5,
        126 => -6,
        144 => -7,
        163 => 13,
        169 => -11,
        177 => 11,
        178 => 12,
        181 => 18,
        182 => 19,
        183 => 20,
        184 => 21,
        185 => 22,
        186 => 23,
        190 => 24,
        191 => -16,
        192 => 14,
        193 => 15,
        194 => 16,
        197 => 17,
        205 => 25,
        223 => -9,
        224 => 26,
        243 => 27,
        260 => 28,
        263 => -14,
        278 => -3,
        281 => -3,
        296 => -4,
        299 => -4,
        317 => 29,
        332 => -1,
        335 => -1,
        350 => -2,
        353 => -2,
        368 => -5,
        371 => -5,
        386 => -6,
        389 => -6,
        404 => -7,
        407 => -7,
        415 => 13,
        421 => -11,
        429 => 30,
        430 => 12,
        433 => 18,
        434 => 19,
        435 => 20,
        436 => 21,
        437 => 22,
        438 => 23,
        442 => 24,
        443 => -16,
        444 => 14,
        445 => 15,
        446 => 16,
        449 => 31,
        450 => -8,
        469 => 13,
        475 => -11,
        483 => 32,
        484 => 12,
        487 => 36,
        488 => 37,
        489 => 38,
        490 => 39,
        491 => 40,
        492 => 41,
        496 => 42,
        498 => 33,
        499 => 34,
        500 => 35,
        505 => 18,
        506 => 19,
        507 => 20,
        508 => 21,
        509 => 22,
        510 => 23,
        514 => 24,
        515 => -16,
        516 => 14,
        517 => 15,
        518 => 16,
        521 => 43,
        522 => -13,
        547 => 44,
        569 => 45,
        583 => -10,
        601 => -12,
        602 => -12,
        619 => -3,
        620 => -3,
        637 => -4,
        638 => -4,
        655 => -1,
        656 => -1,
        673 => -2,
        674 => -2,
        691 => -5,
        692 => -5,
        709 => -6,
        710 => -6,
        727 => -7,
        728 => -7,
        739 => 13,
        745 => -11,
        753 => 46,
        754 => 12,
        757 => 18,
        758 => 19,
        759 => 20,
        760 => 21,
        761 => 22,
        762 => 23,
        766 => 24,
        767 => -16,
        768 => 14,
        769 => 15,
        770 => 16,
        773 => 47,
        785 => -15,
        800 => -8,
        803 => -8,
        818 => -13,
        821 => -13,
        835 => 48,
        857 => 49,
        871 => -8,
        872 => -8,
        889 => -13,
        890 => -13,
    ];

    /** @var int */
    protected int $tablePitch = 18;

    /** @var int[] */
    protected array $terminalsTypes = [self::TOKEN_STRING => 1, self::TOKEN_NUMBER => 2];

    /** @var int[] */
    protected array $terminalsValues = [
        'true' => 3,
        'false' => 4,
        'null' => 5,
        '{' => 6,
        '}' => 7,
        ',' => 8,
        ':' => 9,
        '[' => 10,
        ']' => 11,
    ];

    /**
     * Current processor
     *
     * @phpstan-return Token
     * @return object
     */
    protected function current(): object
    {
        $reduced = $this->token;
        return $reduced;
    }

    /**
     * CurrentTokenType processor
     *
     * @return string|null
     */
    protected function currentTokenType(): ?string
    {
        $reduced = $this->token->type;
        return $reduced;
    }

    /**
     * CurrentTokenLexeme processor
     *
     * @return string|null
     */
    protected function currentTokenLexeme(): ?string
    {
        $reduced = $this->token->lexeme;
        return $reduced;
    }

    /**
     * Next processor
     *
     * @return void
     */
    protected function next(): void
    {
        if (in_array($this->json, [null, ''], true)) {
            $this->token = (object) ['type' => null, 'lexeme' => null];
        } else {
            $status = false;
            foreach (self::$patterns as $type => $pattern) {
                $match = Strings::match($this->json, $pattern);
                if (is_array($match) === true) {
                    /** @phpstan-var array{0: string, 1: string} $match */
                    $this->token = (object) ['type' => $type, 'lexeme' => $match[1]];
                    $this->json = ltrim(Strings::substring($this->json, Strings::length($match[0])));
                    $status = true;
                    break;
                }
            }

            if ($status !== true) {
                throw (new ParseException('Unexpected character ":character"'))->addData('character', $this->json);
            }
        }
    }

    /**
     * Reduce 1 processor
     *
     * @param array{1: string} $arg
     * @return string
     */
    protected function processReduce1(array $arg): string
    {
        $reduced = $this->stringDecode($arg[1]);
        return $reduced;
    }

    /**
     * Reduce 2 processor
     *
     * @param array{1: string} $arg
     * @return int|float
     */
    protected function processReduce2(array $arg): int|float
    {
        assert(is_numeric($arg[1]) === true);
        $reduced = $arg[1] + 0;
        return $reduced;
    }

    /**
     * Reduce 3 processor
     *
     * @param array{1: stdclass|mixed[]} $arg
     * @return mixed[]|stdClass
     */
    protected function processReduce3(array $arg): array|stdClass
    {
        if ($this->associative === false) {
            $arg[1] = (object) $arg[1];
        }
        $reduced = $arg[1];
        return $reduced;
    }

    /**
     * Reduce 4 processor
     *
     * @param array{1: mixed[]} $arg
     * @return mixed[]
     */
    protected function processReduce4(array $arg): array
    {
        $reduced = $arg[1];
        return $reduced;
    }

    /**
     * Reduce 5 processor
     *
     * @return bool
     */
    protected function processReduce5(): bool
    {
        $reduced = true;
        return $reduced;
    }

    /**
     * Reduce 6 processor
     *
     * @return bool
     */
    protected function processReduce6(): bool
    {
        $reduced = false;
        return $reduced;
    }

    /**
     * Reduce 7 processor
     *
     * @return null
     */
    protected function processReduce7(): mixed
    {
        $reduced = null;
        return $reduced;
    }

    /**
     * Reduce 8 processor
     *
     * @param array{2: mixed} $arg
     * @return stdClass
     */
    protected function processReduce8(array $arg): stdClass
    {
        $reduced = (object) $arg[2];
        return $reduced;
    }

    /**
     * Reduce 9 processor
     *
     * @param array{1: array{0: mixed, 1: mixed}} $arg
     * @return mixed[]
     */
    protected function processReduce9(array $arg): array
    {
        $reduced = [$arg[1][0] => $arg[1][1]];
        return $reduced;
    }

    /**
     * Reduce 10 processor
     *
     * @param array{1: array{0: mixed, 1: mixed}, 3: mixed[]} $arg
     * @return mixed[]
     */
    protected function processReduce10(array $arg): array
    {
        $reduced = array_merge([$arg[1][0] => $arg[1][1]], $arg[3]);
        return $reduced;
    }

    /**
     * Reduce 11 processor
     *
     * @return mixed[]
     */
    protected function processReduce11(): array
    {
        $reduced = [];
        return $reduced;
    }

    /**
     * Reduce 12 processor
     *
     * @param array{1: string, 3: mixed} $arg
     * @return mixed[]
     */
    protected function processReduce12(array $arg): array
    {
        $reduced = [$arg[1], $arg[3]];
        return $reduced;
    }

    /**
     * Reduce 13 processor
     *
     * @param array{2: mixed[]} $arg
     * @return mixed[]
     */
    protected function processReduce13(array $arg): array
    {
        $reduced = $arg[2];
        return $reduced;
    }

    /**
     * Reduce 14 processor
     *
     * @param array{1: mixed} $arg
     * @return mixed[]
     */
    protected function processReduce14(array $arg): array
    {
        $reduced = [$arg[1]];
        return $reduced;
    }

    /**
     * Reduce 15 processor
     *
     * @param array{1: mixed, 3: mixed[]} $arg
     * @return mixed[]
     */
    protected function processReduce15(array $arg): array
    {
        $reduced = array_merge([$arg[1]], $arg[3]);
        return $reduced;
    }
}
