grammar Interitty\Pacc\Calculator

/*<?php # this line is for basic syntax highlighting in PHP editors */

option
(
    algorithm = "LR";
    eol = "\n";
    implements = "Interitty\\Pacc\\Parser\\ParserInterface";
    indentation = "    ";
    parse = "processParse";
    strictTypes = "1";
    terminalsPrefix = "self::";
)
@header
{
use ArithmeticError;
use Interitty\Tokenizer\BaseParser;
use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Interitty\Utils\Strings;

use function is_array;
use function is_string;

/**
 * Simple calculator implementation
 */
}

@inner
{
    /** All available token type constants */
    public const string TOKEN_NUMBER = 'number';

    /** @var string|null */
    protected ?string $expression;

    /** @var string|null Current token*/
    protected ?string $token;

    /**
     * @inheritDoc
     */
    public static function parseInput(string $input): mixed
    {
        $parser = new self();
        return $parser->processCalculate($input);
    }

    /**
     * Calculate processor
     *
     * @param string $expression
     * @return mixed
     */
    public function processCalculate(string $expression): mixed
    {
        try {
            $this->expression = $expression;
            $this->next();
            return $this->processParse();
        } catch (IllegalActionException $exception) {
            throw $exception->setMessage('Given text is not valid countable string');
        }
    }
}

/**
 * Current processor
 */
@current
{
    $<?string>$ = $this->token;
}

/**
 * CurrentTokenType processor
 */
@currentTokenType
{
    $<null>$ = null;
    if ((is_string($this->token) === true) && (is_array(Strings::match($this->token, '~^[0-9]+$~')) === true)) {
        $<string>$ = self::TOKEN_NUMBER;
    }
}

/**
 * CurrentTokenLexeme processor
 */
@currentTokenLexeme
{
    $<?string>$ = $this->token;
}

/**
 * Next processor
 */
@next
{
    if (is_string($this->expression) === true) {
        $match = Strings::match($this->expression, '~^([0-9]+|\(|\)|\+|-|\*|/|\s)~');
        if (is_array($match) === true) {
            $this->token = $match[1];
            $this->expression = Strings::substring($this->expression, Strings::length($match[1]));
        } elseif ($this->expression === '') {
            $this->expression = null;
        } else {
            throw (new ParseException('Unexpected character ":character"'))
                    ->addData('character', $this->expression);
        }
    }

    if ($this->expression === null) {
        $this->token = null;
    } elseif (Strings::trim((string) $this->token) === '') {
        $this->next();
    }
}

expression
    : /* nothing */ { $<int>$ = 0; }
    | component { $<int>$ = $<int>1; }
    | expression '+' component { $<int>$ = $<int>1 + $<int>3; }
    | expression '-' component { $<int>$ = $<int>1 - $<int>3; }
    ;

factor
    : TOKEN_NUMBER { $<int>$ = (int) $<string|int|float>1; }
    | '(' expression ')' { $<int>$ = $<int>2; }
    ;

component
    : factor { $<int>$ = $<int>1; }
    | component '*' factor { $<int>$ = $<int>1 * $<int>3; }
    | component '/' factor {
        try {
            $<int>$ = $<int>1 / $<int>3;
        } catch (ArithmeticError $exception) {
            throw new ParseException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
    ;
