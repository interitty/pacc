<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Interitty\Exceptions\Exceptions;
use Interitty\Iterators\Collection;
use InvalidArgumentException;
use Nette\NotSupportedException;
use Stringable;

use function assert;
use function gettype;
use function hash;
use function implode;
use function is_array;
use function is_object;
use function is_scalar;
use function is_string;
use function spl_object_hash;

use const PHP_EOL;

/**
 * @template CollectionValue
 * @extends Collection<CollectionValue>
 */
class SymbolCollection extends Collection implements EqualCheckableInterface
{
    /** @var string */
    protected static string $hashAlgorithm = 'md5';

    /**
     * Collection of type assertion helper
     *
     * @param string $expectedType
     * @return bool
     */
    public function assertCollectionOfType(string $expectedType): bool
    {
        $collectionType = $this->getType();
        if ($collectionType !== $expectedType) {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage('Bad type - expected Collection<:expectedType>, given Collection<:type>')
                    ->addData('expectedType', $expectedType)
                    ->addData('type', $collectionType);
        }
        return true;
    }

    // </editor-fold>
    /**
     * @inheritdoc
     */
    public function isEqual(EqualCheckableInterface $object): bool
    {
        $isEqual = (($object instanceof self) &&
            ($this->count() === $object->count())) &&
            ($this->isArrayEqual($object));

        return $isEqual;
    }

    /**
     * Arrays equal checker
     *
     * @phpstan-param SymbolCollection<CollectionValue> $right
     * @return bool
     */
    protected function isArrayEqual(SymbolCollection $right): bool
    {
        $isEqual = true;
        foreach ($this->data as $item) {
            if ($right->contains($item) === false) {
                $isEqual = false;
                break;
            }
        }
        return $isEqual;
    }

    /**
     * To string processor
     *
     * @return string
     */
    public function __toString(): string
    {
        $return = '{' . PHP_EOL;
        foreach ($this as $item) {
            if ((is_scalar($item) !== true) && (($item instanceof Stringable) !== true)) {
                throw Exceptions::extend(NotSupportedException::class)
                        ->setMessage('Not supported type ":type"')
                        ->addData('type', $this->getType());
            }
            $return .= '    ' . (string) $item . PHP_EOL;
        }
        $return .= '}' . PHP_EOL;
        return $return;
    }

    /**
     * Add item to collection data
     *
     * @phpstan-param CollectionValue $item
     * @param int|string|null $key [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function add($item, $key = null): static
    {
        $hash = $key !== null ? $key : $this->hash($item);
        $has = $this->has($hash);
        $equal = $this->tryEq($item);
        if (($has === false) && ($equal === null)) {
            parent::add($item, $hash);
        }

        return $this;
    }

    /**
     * Helper that add item to collection data or give equal
     *
     * @phpstan-param CollectionValue $item
     * @param int|string|null $key [OPTIONAL]
     * @phpstan-return CollectionValue Given object or that with equal content
     */
    public function addOrGetEqual(mixed $item, int|string|null $key = null): mixed
    {
        $equalItem = $this->getEqual($item, $key);
        if ($equalItem === null) {
            $equalItem = $item;
            $this->add($equalItem, $key);
        }
        return $equalItem;
    }

    /**
     * Get equal item from collection data
     *
     * @phpstan-param CollectionValue $item
     * @param int|string|null $key [OPTIONAL]
     * @phpstan-return CollectionValue|null Equal item or null
     */
    public function getEqual(mixed $item, int|string|null $key = null): mixed
    {
        $hash = $key !== null ? $key : $this->hash($item);
        $has = $this->has($hash);
        $equalKey = $has ? $hash : $this->tryEq($item);
        $equalItem = null;
        if (is_string($equalKey) === true) {
            $equalItem = $this->get($equalKey);
        }

        return $equalItem;
    }

    /**
     * Check if item is in collection data
     *
     * @param mixed $item
     * @return bool
     */
    public function contains(mixed $item): bool
    {
        $hash = $this->hash($item);
        $contains = ($this->has($hash) === true) || ($this->tryEq($item) !== null);
        return $contains;
    }

    /**
     * Check if all items from collection is in collection data
     *
     * @phpstan-param SymbolCollection<CollectionValue> $collection
     * @return bool
     */
    public function containsCollection(SymbolCollection $collection): bool
    {
        $contains = true;
        foreach ($collection as $item) {
            if ($this->contains($item) === false) {
                $contains = false;
                break;
            }
        }

        return $contains;
    }

    /**
     * Delete item from set
     *
     * @param mixed $item
     * @return bool
     */
    public function deleteItem(mixed $item): bool
    {
        $hash = $this->hash($item);
        $result = parent::delete($hash);
        if (($result === false) && (($hash = $this->tryEq($item)) !== null)) {
            $result = parent::delete($hash);
        }
        return $result;
    }

    /**
     * Get hash of given value
     *
     * @param mixed $value
     * @return string hash
     */
    protected function hash(mixed $value): string
    {
        if (is_array($value)) {
            $array = [];
            foreach ($value as $innerKey => $innerValue) {
                $array[] = hash(self::$hashAlgorithm, gettype($innerKey) . ':' . $innerKey) . $this->hash($innerValue);
            }
            $return = hash(self::$hashAlgorithm, implode(',', $array));
        } elseif (is_object($value)) {
            $return = spl_object_hash($value);
        } elseif (is_scalar($value)) {
            $return = hash(self::$hashAlgorithm, gettype($value) . ':' . ((string) $value));
        } else {
            throw Exceptions::extend(InvalidArgumentException::class)
                    ->setMessage('Unsupported value type ":type" for hash operation')
                    ->addData('type', gettype($value));
        }

        return $return;
    }

    /**
     * Tries if val instance of `EqualCheckableInterface`, then checks if some item in set is not equal
     *
     * @param mixed $val
     * @return string|null hash of equal item in set
     */
    protected function tryEq(mixed $val): ?string
    {
        $return = null;
        if ($val instanceof EqualCheckableInterface) {
            foreach ($this->data as $hash => $item) {
                assert($item instanceof EqualCheckableInterface);
                if ($val->isEqual($item)) {
                    $return = (string) $hash;
                    break;
                }
            }
        }

        return $return;
    }
}
