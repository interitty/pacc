<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

class NonTerminal extends Symbol
{
    /**
     * To string processor
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
