<?php

namespace Interitty\Pacc\Symbol;

interface EqualCheckableInterface
{
    /**
     * Is equal checker
     *
     * @param EqualCheckableInterface $object
     * @return bool
     */
    public function isEqual(EqualCheckableInterface $object): bool;
}
