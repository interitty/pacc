<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Interitty\Pacc\Parser\TokenizerParser;

/**
 * @phpstan-type GrammarOptionType array<'doc'|'type', string[]>
 * @phpstan-type GrammarOptionTypes array<'return'|int, GrammarOptionType>
 */
class Production implements EqualCheckableInterface
{
    /** All available return type constants */
    public const string RETURN_TYPE_MIXED = 'mixed';
    public const string RETURN_TYPE_NULL = 'null';
    public const string RETURN_TYPE_VOID = 'void';

    /** @var string|null */
    protected ?string $code;

    /** @var int */
    protected int $index;

    /** @var NonTerminal */
    protected NonTerminal $left;

    /** @var Symbol[] */
    protected array $right;

    /** @phpstan-var GrammarOptionTypes|null */
    protected ?array $types = null;

    /** @var int */
    protected int $rightCount = 0;

    /**
     * Constructor
     *
     * @param NonTerminal $left
     * @param Symbol[] $right
     * @param string|null $code [OPTIONAL]
     * @phpstan-param GrammarOptionTypes $types [OPTIONAL]
     * @return void
     */
    public function __construct(NonTerminal $left, array $right, ?string $code = null, ?array $types = null)
    {
        $this->setLeft($left);
        $this->setRight($right);
        $this->setCode($code);
        $this->setTypes($types);
    }

    /**
     * @inheritdoc
     */
    public function isEqual(EqualCheckableInterface $object): bool
    {
        $isEqual = (($object instanceof self) &&
            ($this->getRightCount() === $object->getRightCount()) &&
            ($this->getCode() === $object->getCode()) &&
            ($this->getTypes() === $object->getTypes()) &&
            ($this->getLeft()->isEqual($object->getLeft())) &&
            ($this->isCollectionEqual($this->getRight(), $object->getRight())));

        return $isEqual;
    }

    /**
     * Collection equal checker
     *
     * @param EqualCheckableInterface[] $left
     * @param EqualCheckableInterface[] $right
     * @return bool
     */
    protected function isCollectionEqual(array $left, array $right): bool
    {
        $isEqual = true;
        foreach ($left as $index => $leftSymbol) {
            if ($leftSymbol->isEqual($right[$index]) === false) {
                $isEqual = false;
                break;
            }
        }
        return $isEqual;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Code getter
     *
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * Code setter
     *
     * @param string|null $code
     * @return static Provides fluent interface
     */
    protected function setCode(?string $code = null): static
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Index getter
     *
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * Index setter
     *
     * @param int $index
     * @return static Provides fluent interface
     */
    public function setIndex(int $index): static
    {
        $this->index = $index;
        return $this;
    }

    /**
     * Left getter
     *
     * @return NonTerminal
     */
    public function getLeft(): NonTerminal
    {
        return $this->left;
    }

    /**
     * Left setter
     *
     * @param NonTerminal $left
     * @return static Provides fluent interface
     */
    protected function setLeft(NonTerminal $left): static
    {
        $this->left = $left;
        return $this;
    }

    /**
     * Left name getter
     *
     * @return string
     */
    public function getLeftName(): string
    {
        $leftName = $this->getLeft()->getName();
        return $leftName;
    }

    /**
     * Right symbol adder
     *
     * @param Symbol $right
     * @return static Provides fluent interface
     */
    protected function addRightSymbol(Symbol $right): static
    {
        $this->right[] = $right;
        $this->setRightCount($this->getRightCount() + 1);
        return $this;
    }

    /**
     * Right getter
     *
     * @return Symbol[]
     */
    public function getRight(): array
    {
        return $this->right;
    }

    /**
     * Right setter
     *
     * @param Symbol[] $right
     * @return static Provides fluent interface
     */
    protected function setRight(array $right): static
    {
        $this->right = [];
        $this->setRightCount(0);
        foreach ($right as $symbol) {
            $this->addRightSymbol($symbol);
        }
        return $this;
    }

    /**
     * Right count getter
     *
     * @return int
     */
    public function getRightCount(): int
    {
        return $this->rightCount;
    }

    /**
     * Right count setter
     *
     * @param int $rightCount
     * @return static Provides fluent interface
     */
    protected function setRightCount(int $rightCount): static
    {
        $this->rightCount = $rightCount;
        return $this;
    }

    /**
     * Types getter
     *
     * @phpstan-return GrammarOptionTypes
     */
    public function getTypes(): array
    {
        if ($this->types === null) {
            $code = $this->getCode();
            if ($code === null) {
                $types = [];
            } else {
                $types = TokenizerParser::processParseTypes($code);
                $this->setCode($code);
            }
            $this->setTypes($types);
        }
        /** @phpstan-var GrammarOptionTypes */
        return $this->types;
    }

    /**
     * Types setter
     *
     * @phpstan-param GrammarOptionTypes|null $types
     * @return static Provides fluent interface
     */
    protected function setTypes(?array $types): static
    {
        $this->types = $types;
        return $this;
    }

    // </editor-fold>
}
