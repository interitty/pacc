<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use function sprintf;

class Terminal extends Symbol
{
    /** @var string|null */
    protected ?string $type;

    /** @var string|null */
    protected ?string $value;

    /**
     * Constructor
     *
     * @param string $name
     * @param string|null $type [OPTIONAL]
     * @param string|null $value [OPTIONAL]
     * @return void
     */
    public function __construct(string $name, ?string $type = null, ?string $value = null)
    {
        parent::__construct($name);
        $this->setType($type);
        $this->setValue($value);
    }

    /**
     * @inheritdoc
     */
    public function isEqual(EqualCheckableInterface $object): bool
    {
        $isEqual = false;
        if (
            ($object instanceof self) &&
            ($object->getName() === $this->getName()) &&
            ($object->getType() === $this->getType()) &&
            ($object->getValue() === $this->getValue())
        ) {
            $isEqual = true;
        }

        return $isEqual;
    }

    /**
     * To string generator
     *
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('`%s`', $this->getName());
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Type getter
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Type setter
     *
     * @param string|null $type
     * @return static Provides fluent interface
     */
    protected function setType(?string $type): static
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Value getter
     *
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * Value setter
     *
     * @param string|null $value
     * @return static Provides fluent interface
     */
    protected function setValue(?string $value): static
    {
        $this->value = $value;
        return $this;
    }

    // </editor-fold>
}
