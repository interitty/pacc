<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use function get_class;

abstract class Symbol implements EqualCheckableInterface
{
    /** @phpstan-var SymbolCollection<int> */
    protected SymbolCollection $first;

    /** @phpstan-var SymbolCollection<int> */
    protected SymbolCollection $follow;

    /** @var int */
    protected int $index;

    /** @var string */
    protected string $name;

    /**
     * Constructor
     *
     * @param string $name
     * @return void
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }

    /**
     * To string generator
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @inheritdoc
     */
    public function isEqual(EqualCheckableInterface $object): bool
    {
        $isEqual = false;
        if (
            (get_class($object) === get_class($this)) &&
            ($object->getName() === $this->getName())
        ) {
            $isEqual = true;
        }
        return $isEqual;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * First getter
     *
     * @phpstan-return SymbolCollection<int>
     */
    public function getFirst(): SymbolCollection
    {
        return $this->first;
    }

    /**
     * First setter
     *
     * @phpstan-param SymbolCollection<int> $first
     * @return static Provides fluent interface
     */
    public function setFirst(SymbolCollection $first): static
    {
        $this->first = $first;
        return $this;
    }

    /**
     * Follow getter
     *
     * @phpstan-return SymbolCollection<int>
     */
    public function getFollow(): SymbolCollection
    {
        return $this->follow;
    }

    /**
     * Follow setter
     *
     * @phpstan-param SymbolCollection<int> $follow
     * @return static Provides fluent interface
     */
    public function setFollow(SymbolCollection $follow): static
    {
        $this->follow = $follow;
        return $this;
    }

    /**
     * Index getter
     *
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * Index setter
     *
     * @param int $index
     * @return static Provides fluent interface
     */
    public function setIndex(int $index): static
    {
        $this->index = $index;
        return $this;
    }

    /**
     * Name getter
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Name setter
     *
     * @param string $name
     * @return static Provides fluent interface
     */
    protected function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    // </editor-fold>
}
