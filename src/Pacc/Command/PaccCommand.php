<?php

declare(strict_types=1);

namespace Interitty\Pacc\Command;

use Interitty\Console\BaseCommand;
use Interitty\Pacc\Exceptions\PaccException;
use Interitty\Pacc\Generator\GeneratorInterface;
use Interitty\Pacc\Generator\LeftToRight\Generator as LRGenerator;
use Interitty\Pacc\Generator\RecursiveDescent\Generator as RDGenerator;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Parser\TokenizerParser;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use Interitty\Utils\FileSystem;
use Interitty\Utils\Strings;
use Symfony\Component\Console\Input\InputOption;
use Throwable;

use function file_exists;
use function is_string;
use function rtrim;

class PaccCommand extends BaseCommand
{
    /** All available algorithm constants */
    public const string ALGORITHM_LR = 'LR';
    public const string ALGORITHM_RD = 'RD';

    /** All available argument constants */
    public const string OPTION_ALGORITHM = 'algorithm';
    public const string OPTION_FORCE = 'force';
    public const string OPTION_INPUT = 'input';
    public const string OPTION_OUTPUT = 'output';

    /** @var string|null */
    protected static ?string $defaultName = 'pacc:generate';

    /** @phpstan-var array<string,string> */
    protected static array $algorithms = [
        self::ALGORITHM_LR => 'canonical LR(1)',
        self::ALGORITHM_RD => 'recursive descent',
    ];

    /**
     * @inheritdoc
     */
    protected function configure(): void
    {
        $this->setDescription(
            'Parser generator (currently generates recursive descent parser and canonical LR(1) parser) for PHP.'
        );
        $this->addOption(self::OPTION_ALGORITHM, 'a', InputOption::VALUE_OPTIONAL, 'algorithm for generated parser');
        $this->addOption(self::OPTION_FORCE, 'f', InputOption::VALUE_NONE, 'force output file overwrite if exists');
        $this->addOption(self::OPTION_INPUT, 'i', InputOption::VALUE_OPTIONAL, 'input file');
        $this->addOption(self::OPTION_OUTPUT, 'o', InputOption::VALUE_OPTIONAL, 'output file');
    }

    // <editor-fold defaultstate="collapsed" desc="Processors">
    /**
     * Algorithm processor
     *
     * @param Grammar $grammar
     * @return string One of self::ALGORITHM_* constants
     */
    protected function processAlgorithm(Grammar $grammar): string
    {
        $algorithm = $this->getInput()->getOption(self::OPTION_ALGORITHM);
        if (is_string($algorithm) === false) {
            $options = $grammar->getOptions();
            if ($options->hasOption(OptionsCollection::OPTION_ALGORITHM) === true) {
                $algorithm = $options->getOption(OptionsCollection::OPTION_ALGORITHM);
            } else {
                $algorithm = self::ALGORITHM_RD;
            }
        }
        return $algorithm;
    }

    /**
     * Exception processor
     *
     * @param Throwable $exception
     * @param string $input [OPTIONAL]
     * @return int One of the self::FAILURE, self::INVALID, self::SUCCESS constants
     */
    protected function processException(Throwable $exception, string $input = ''): int
    {
        $errorOutput = $this->getErrorOutput();
        $error = [$exception->getMessage()];
        if (($errorOutput->isVerbose() === true) && ($exception instanceof UnexpectedTokenException)) {
            /** @phpstan-var string[] $lines */
            $lines = Strings::split($input, '~\r?\n|\r~');
            $token = $exception->getToken();
            $line = $token->getLine();
            $error[] = ((string) $line) . ': ' . rtrim($lines[$line - 1]);
            $error[] = Strings::padLeft(' ', Strings::length((string) $line) + $token->getPosition() + 1) . '^';
        }
        if ($errorOutput->isDebug() === true) {
            $error[] = '';
            $error[] = $exception->getTraceAsString();
        }
        $this->writeError($error);
        return self::FAILURE;
    }

    /**
     * @inheritdoc
     */
    protected function processExecute(): int
    {
        $inputContent = $this->processInputContent();
        $generator = $this->createGenerator($inputContent);
        $this->processOutput($generator);
        $result = self::SUCCESS;
        return $result;
    }

    /**
     * Grammar processor
     *
     * @param string $input
     * @return Grammar
     * @throws PaccException
     */
    protected function processGrammar(string $input): Grammar
    {
        $parser = $this->createTokenizerParser();
        $parser->parse($input);
        $grammar = $parser->getGrammar();
        if ($grammar->hasName() !== true) {
            throw new PaccException('The "grammar" statement omitted');
        }
        $grammar->setOutput($this->getOutput());
        return $grammar;
    }

    /**
     * Input file processor
     *
     * @return string
     */
    protected function processInputFile(): string
    {
        $inputFile = $this->getInput()->getOption(self::OPTION_INPUT);
        if (is_string($inputFile) === false) {
            $inputFile = 'php://stdin';
        }
        return $inputFile;
    }

    /**
     * Input content processor
     *
     * @return string
     */
    protected function processInputContent(): string
    {
        $inputFile = $this->processInputFile();
        $inputContent = FileSystem::read($inputFile);
        return $inputContent;
    }

    /**
     * Output processor
     *
     * @param GeneratorInterface $generator
     * @return void
     * @throws PaccException
     */
    protected function processOutput(GeneratorInterface $generator): void
    {
        $input = $this->getInput();
        $outputFile = $input->getOption(self::OPTION_OUTPUT);
        if (is_string($outputFile) === true) {
            if ((file_exists($outputFile) === true) && ($input->getOption(self::OPTION_FORCE) !== true)) {
                throw new PaccException('Output file already exists. Use -f to force overwrite');
            }
            FileSystem::write($outputFile, $generator->getGenerated());
        } else {
            $this->getOutput()->write($generator->getGenerated());
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Generator factory
     *
     * @param string $input
     * @return GeneratorInterface
     * @throws PaccException
     */
    protected function createGenerator(string $input): GeneratorInterface
    {
        $grammar = $this->processGrammar($input);
        $algorithm = $this->processAlgorithm($grammar);
        if ($algorithm === self::ALGORITHM_LR) {
            $generator = $this->createLRGenerator($grammar);
        } elseif ($algorithm === self::ALGORITHM_RD) {
            $generator = $this->createRDGenerator($grammar);
        } else {
            throw (new PaccException('Unknown algorithm ":algorithm"'))->addData('algorithm', $algorithm);
        }
        return $generator;
    }

    /**
     * LRGenerator factory
     *
     * @param Grammar $grammar
     * @return LRGenerator
     */
    protected function createLRGenerator(Grammar $grammar): LRGenerator
    {
        $generator = new LRGenerator($grammar);
        return $generator;
    }

    /**
     * RDGenerator factory
     *
     * @param Grammar $grammar
     * @return RDGenerator
     */
    protected function createRDGenerator(Grammar $grammar): RDGenerator
    {
        $generator = new RDGenerator($grammar);
        return $generator;
    }

    /**
     * TokenizerParser factory
     *
     * @return TokenizerParser
     */
    protected function createTokenizerParser(): TokenizerParser
    {
        $tokenizerParser = new TokenizerParser();
        return $tokenizerParser;
    }

    // </editor-fold>
}
