<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator;

use Exception;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Utils\Arrays;
use Interitty\Utils\Strings;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\Visibility;

use function array_search;
use function explode;
use function implode;
use function str_contains;

/**
 * @phpstan-import-type GrammarOptionType from OptionsCollection
 * @phpstan-import-type GrammarOptionTypes from OptionsCollection
 */
class PhpGeneratorHelpers
{
    /** All available type constants */
    public const string TYPE_ARRAY = 'array';
    public const string TYPE_MIXED = 'mixed';
    public const string TYPE_NULL = 'null';
    public const string TYPE_VOID = 'void';

    /** All available visibility constants */
    public const string VISIBILITY_PRIVATE = Visibility::Private;
    public const string VISIBILITY_PROTECTED = Visibility::Protected;
    public const string VISIBILITY_PUBLIC = Visibility::Public;

    /**
     * Class extends processor
     *
     * @param BaseGenerator $generator
     * @param ClassType $class
     * @param string $extends
     * @return void
     * @throws InvalidStateException
     */
    public static function processClassExtends(BaseGenerator $generator, ClassType $class, string $extends): void
    {
        if ($extends !== '') {
            $namespace = $generator->getNamespace();
            if ($namespace->simplifyName($extends) !== $extends) {
                $namespace->addUse($extends);
            }
            $class->setExtends($extends);
        }
    }

    /**
     * Class implements processor
     *
     * @param BaseGenerator $generator
     * @param ClassType $class
     * @param string $implements
     * @return void
     * @throws InvalidStateException
     */
    public static function processClassImplements(BaseGenerator $generator, ClassType $class, string $implements): void
    {
        if ($implements !== '') {
            $namespace = $generator->getNamespace();
            $implements = explode(',', $implements);
            foreach ($implements as $key => $implement) {
                $implements[$key] = $implement = Strings::trim($implement);
                if ($namespace->simplifyName($implement) !== $implement) {
                    $namespace->addUse($implement);
                }
            }
            $class->setImplements($implements);
        }
    }

    /**
     * Method arguments processor
     *
     * @param BaseGenerator $generator
     * @param Method $method
     * @phpstan-param GrammarOptionTypes $types
     * @return void
     */
    public static function processMethodArguments(BaseGenerator $generator, Method $method, array $types): void
    {
        $comment = (string) $method->getComment();
        if (($types !== []) && (str_contains($comment, '@param') === false)) {
            $eol = $generator->getPrinter()->getEol();
            $parts = [];
            foreach ($types as $key => $typesData) {
                self::processMethodDetectPhpDoc($typesData);
                self::processNullReturnType($typesData['doc']);
                $parts[] = $key . ': ' . implode('|', $typesData['doc']);
            }
            $argumentComment = '@param array{' . implode(', ', $parts) . '} $arg';
            $method->addComment((str_contains($comment, '@') ? '' : $eol) . $argumentComment);
        }
    }

    /**
     * Method return type processor
     *
     * @param BaseGenerator $generator
     * @param Method $method
     * @phpstan-param GrammarOptionType|null $typesData
     * @return void
     */
    public static function processMethodReturnType(BaseGenerator $generator, Method $method, ?array $typesData): void
    {
        /** @var string|null $returnType */
        $returnType = $method->getReturnType();
        $comment = (string) $method->getComment();
        $types = self::processTypesData($generator, $method, $typesData);
        if ($types['type'] === []) {
            $types['type'] = [self::TYPE_MIXED];
        }
        $method->setReturnType($returnType === null ? implode('|', $types['type']) : $returnType);
        if (str_contains($comment, '@return') === false) {
            $eol = $generator->getPrinter()->getEol();
            $docType = '@return ' . ($types['doc'] === [] ? self::TYPE_MIXED : implode('|', $types['doc']));
            $method->addComment((str_contains($comment, '@') ? '' : $eol) . $docType);
        }
    }

    /**
     * Pick return types from given types processor
     *
     * @phpstan-param GrammarOptionTypes $types
     * @phpstan-return GrammarOptionType|null
     */
    public static function processPickReturnTypes(array &$types): ?array
    {
        /**
         * @phpstan-var GrammarOptionType|null $returnTypes
         * @phpstan-ignore parameterByRef.type
         */
        $returnTypes = Arrays::pick($types, 'return', null);
        return $returnTypes;
    }

    /**
     * Reduced return code processor
     *
     * @param BaseGenerator $generator
     * @param Method $method
     * @phpstan-param GrammarOptionTypes $types
     * @return void
     */
    public static function processReducedReturnCode(BaseGenerator $generator, Method $method, array $types): void
    {
        if ((isset($types['return']) === true) && (($types['return']['type'][0] ?? null) !== self::TYPE_VOID)) {
            $code = $method->getBody();
            if (str_contains($code, 'return') === false) {
                $code .= 'return $reduced;';
                $method->setBody($generator->getPrinter()->processFormatCode($code, 0));
            }
        }
    }

    /**
     * Method phpDoc detect from return type processor
     *
     * @phpstan-param GrammarOptionType $typesData
     * @return void
     */
    protected static function processMethodDetectPhpDoc(array &$typesData): void
    {
        if ($typesData['doc'] === []) {
            foreach ($typesData['type'] as $part) {
                $typePart = Strings::lower($part);
                $typesData['doc'][] = ($typePart === self::TYPE_ARRAY) ? 'mixed[]' : $typePart;
            }
        }
        if ($typesData['doc'] === []) {
            $typesData['doc'][] = self::TYPE_MIXED;
        }
    }

    /**
     * Null type processor
     *
     * @param string[] $types
     * @return string[]
     */
    protected static function processNullReturnType(array &$types): array
    {
        if (($key = array_search(self::TYPE_NULL, $types, true)) !== false) {
            unset($types[$key]);
            $types[] = self::TYPE_NULL;
        }
        return $types;
    }

    /**
     * Simplify type processor
     *
     * @param BaseGenerator $generator
     * @param string $type
     * @return string
     */
    protected static function processSimplifyType(BaseGenerator $generator, string $type): string
    {
        try {
            if (str_contains($type, '\\') === true) {
                $namespace = $generator->getNamespace();
                $namespace->addUse($type);
                $type = $namespace->simplifyType($type);
            }
            return $type;
        } catch (Exception $exception) {
            throw new InvalidArgumentException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * Types data processor
     *
     * @param BaseGenerator $generator
     * @param Method $method
     * @phpstan-param GrammarOptionType|null $typesData
     * @phpstan-return GrammarOptionType
     */
    protected static function processTypesData(BaseGenerator $generator, Method $method, ?array $typesData): array
    {
        $types = isset($typesData['type']) ? $typesData['type'] : [self::TYPE_VOID];
        $docs = isset($typesData['doc']) ? $typesData['doc'] : [];
        foreach ($types as $key => $part) {
            $part = self::processSimplifyType($generator, $part);
            $typePart = Strings::lower($part);
            if (($typesData['doc'] ?? []) === []) {
                $docs[] = ($typePart === self::TYPE_ARRAY) ? 'mixed[]' : $part;
            }
            if ($typePart === self::TYPE_NULL) {
                $method->setReturnNullable();
                unset($types[$key]);
            } elseif ($typePart === self::TYPE_MIXED) {
                unset($types[$key]);
            }
        }
        self::processNullReturnType($docs);
        return ['doc' => $docs, 'type' => $types];
    }
}
