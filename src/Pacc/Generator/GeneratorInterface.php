<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator;

use Interitty\Pacc\Grammar;
use Nette\InvalidStateException;

interface GeneratorInterface
{
    /**
     * Constructor
     *
     * @param Grammar $grammar
     * @return void
     */
    public function __construct(Grammar $grammar);

    /**
     * Singleton implementation of generated getter
     *
     * @return string
     * @throws InvalidStateException
     */
    public function getGenerated(): string;

    /**
     * Printer getter
     *
     * @return Printer
     */
    public function getPrinter(): Printer;
}
