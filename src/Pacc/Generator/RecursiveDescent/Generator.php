<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\RecursiveDescent;

use Interitty\Pacc\Generator\BaseGenerator;
use Interitty\Pacc\Generator\PhpGeneratorHelpers;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\Utils\Strings;
use Nette\PhpGenerator\ClassType;

use function assert;
use function count;
use function is_string;
use function key;
use function str_repeat;
use function var_export;

/**
 * @phpstan-type RecursiveType string|array<string, RecursiveType>
 */
class Generator extends BaseGenerator
{
    /**
     * @inheritdoc
     */
    protected function processInner(ClassType $class): void
    {
        $rulesTree = $this->processRulesTree();
        $firstRuleKey = (string) key($rulesTree);
        $this->processRules($class, $rulesTree);
        $this->processParse($class, $firstRuleKey);
        parent::processInner($class);
    }

    /**
     * Parse processor
     *
     * @param ClassType $class
     * @param string $firstRuleKey
     * @return void
     */
    protected function processParse(ClassType $class, string $firstRuleKey): void
    {
        $printer = $this->getPrinter();
        $eol = $printer->getEol();
        $parseMethodName = $this->getOption(OptionsCollection::OPTION_PARSE, 'processParse');

        $body = $printer->processFormatCode(
            '$status = $this->process' . Strings::firstUpper($firstRuleKey) . '();' . $eol
            . 'if ($status !== true) {' . $eol
            . '    throw new ParseException(\'Parse error\');' . $eol
            . '}' . $eol
            . 'return null;'
        );
        $class->addMethod($parseMethodName)
            ->setVisibility(PhpGeneratorHelpers::VISIBILITY_PROTECTED)
            ->setReturnType(PhpGeneratorHelpers::TYPE_MIXED)
            ->setComment('Parse processor' . $eol . $eol . '@return mixed')
            ->setBody($body);
    }

    /**
     * Rules processor
     *
     * @param ClassType $class
     * @phpstan-param array<string, RecursiveType> $rulesTree
     * @return void
     */
    protected function processRules(ClassType $class, array $rulesTree): void
    {
        $printer = $this->getPrinter();
        $eol = $printer->getEol();
        foreach ($rulesTree as $name => $ruleTree) {
            $body = $printer->processFormatCode(
                '$reduced = false;'
                . $eol . $this->phpizeRuleTree($ruleTree, 1, 2, $eol)
                . 'return $reduced;'
            );
            $class->addMethod('process' . Strings::firstUpper($name))
                ->setVisibility(PhpGeneratorHelpers::VISIBILITY_PROTECTED)
                ->setReturnType('bool')
                ->setComment(Strings::firstUpper($name) . ' processor' . $eol . $eol . '@return bool')
                ->setBody($body);
        }
    }

    /**
     * Rules tree processor
     *
     * @phpstan-return array<string, RecursiveType>
     */
    protected function processRulesTree(): array
    {
        $return = [];
        $productions = $this->getProductions();
        /** @var Production $production */
        foreach ($productions as $production) {
            $return = $this->processRulesTreeProduction($production, $return);
        }

        foreach ($return as &$rule) {
            $rule = $this->treeLifting($rule);
        }

        return $return;
    }

    /**
     * Rules tree production processor
     *
     * @param Production $production
     * @phpstan-param array<string, RecursiveType> $return
     * @phpstan-return array<string, RecursiveType>
     */
    protected function processRulesTreeProduction(Production $production, array $return): array
    {
        $leftName = $production->getLeftName();
        if (isset($return[$leftName]) === false) {
            $return[$leftName] = [];
        }
        $current = &$return[$leftName];

        foreach ($production->getRight() as $right) {
            $key = $this->processRulesTreeRight($right);
            /** @phpstan-var array<string, RecursiveType> $current */
            if (isset($current[$key]) === false) {
                $current[$key] = [];
            }
            $current = &$current[$key];
        }

        $current['$'] = '$reduced = true;';
        return $return;
    }

    /**
     * Rules tree right processor
     *
     * @param Symbol $right
     * @return string
     */
    protected function processRulesTreeRight(Symbol $right): string
    {
        if ($right instanceof NonTerminal) {
            $key = 'N:' . $right->getName();
        } else {
            assert($right instanceof Terminal);
            $symbolType = $right->getType();
            $symbolValue = $right->getValue();
            if ($symbolType !== null) {
                $key = 'T:' . $symbolType;
            } else {
                assert($symbolValue !== null);
                $key = 'S:' . $symbolValue;
            }
        }
        return $key;
    }

    /**
     * Optimizes rule tree
     *
     * @phpstan-param array<string, RecursiveType> $tree
     * @phpstan-return array<string, RecursiveType>
     */
    protected function treeLifting($tree): array|string
    {
        if (count($tree) === 1 && isset($tree['$'])) {
            $tree = $tree['$'];
        } else {
            if (isset($tree['$'])) {
                $item = $tree['$'];
                unset($tree['$']);
            }

            foreach ($tree as &$value) {
                $value = $this->treeLifting($value);
            }

            if (isset($item)) {
                $tree['$'] = $item;
            }
        }
        return $tree;
    }

    /**
     * Converts one rule tree to PHP
     *
     * @phpstan-param string|array<string, RecursiveType> $tree
     * @param int $index
     * @param int $level
     * @param string $eol
     * @return string
     */
    protected function phpizeRuleTree($tree, int $index, int $level, string $eol): string
    {
        if (is_string($tree)) {
            $ret = $this->getPrinter()->processFormatCode($tree, $level);
        } else {
            $ret = $this->phpizeRuleTreeArray($tree, $index, $level, $eol);
        }
        return $ret;
    }

    /**
     * Converts one array rule tree to PHP
     *
     * @phpstan-param array<string, RecursiveType> $tree
     * @param int $index
     * @param int $level
     * @param string $eol
     * @return string
     */
    protected function phpizeRuleTreeArray(array $tree, int $index, int $level, string $eol): string
    {
        $oneIndentation = $this->getPrinter()->getIndentation();
        $indentation = str_repeat($oneIndentation, $level);

        $first = true;
        $else = null;
        if (isset($tree['$'])) {
            $else = $tree['$'];
            unset($tree['$']);
        }

        $return = '';
        foreach ($tree as $key => $value) {
            $return .= $this->phpizeRuleTreeItem($key, $value, $index, $level, $eol, $first);
            $first = false;
        }

        if (($first === false) && ($else !== null)) {
            $return .= ' else {' . $eol;
            $return .= $this->phpizeRuleTree($else, 1, $level + 1, $eol);
            $return .= $indentation . '}';
        }
        $return .= $eol;

        return $return;
    }

    /**
     * Converts one array rule tree item to PHP
     *
     * @param string $key
     * @param string|mixed[] $value
     * @param int $index
     * @param int $level
     * @param string $eol
     * @param bool $first
     * @return string
     */
    protected function phpizeRuleTreeItem(string $key, $value, int $index, int $level, string $eol, bool $first): string
    {
        $oneIndentation = $this->getPrinter()->getIndentation();
        $indentation = str_repeat($oneIndentation, $level);
        $keyType = Strings::substring($key, 0, 1);
        $subkey = Strings::substring($key, 2);
        $return = ($first === true ? $indentation : ' else');
        if ($keyType === 'S') {
            $symbol = var_export($subkey, true);
            $return .= 'if ($this->currentTokenLexeme() === ' . $symbol . ') {' . $eol;
            $return .= $indentation . $oneIndentation . '$this->next();' . $eol;
        } elseif ($keyType === 'T') {
            $terminal = $this->getOption(OptionsCollection::OPTION_TERMINALS_PREFIX, 'self::') . $subkey;
            $return .= 'if ($this->currentTokenType() === ' . $terminal . ') {' . $eol;
            $return .= $indentation . $oneIndentation . '$this->next();' . $eol;
        } elseif ($keyType === 'N') {
            $nonTerminal = Strings::firstUpper($subkey);
            $return .= 'if ($this->process' . $nonTerminal . '() === true) {' . $eol;
        }
        $return .= $this->phpizeRuleTree($value, $index + 1, $level + 1, $eol);
        $return .= $indentation . '}';
        return $return;
    }
}
