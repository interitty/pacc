<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\GotosProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\ReduceProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\ShiftProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\TableCollection;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\SymbolCollection;

use function assert;

class TableProcessor extends BaseProcessor
{
    /** @var ReduceProcessor */
    protected ReduceProcessor $reduceProcessor;

    /** @var GotosProcessor */
    protected GotosProcessor $gotosProcessor;

    /** @var IndexesProcessor */
    protected IndexesProcessor $indexesProcessor;

    /** @var ShiftProcessor */
    protected ShiftProcessor $shiftProcessor;

    /** @var StatesProcessor */
    protected StatesProcessor $statesProcessor;

    /** @var TableCollection */
    protected TableCollection $tableCollection;

    /**
     * Constructor
     *
     * @param Grammar $grammar
     * @param IndexesProcessor $indexesProcessor
     * @param StatesProcessor $statesProcessor
     * @return void
     */
    public function __construct(Grammar $grammar, IndexesProcessor $indexesProcessor, StatesProcessor $statesProcessor)
    {
        parent::__construct($grammar);
        $this->setIndexesProcessor($indexesProcessor);
        $this->setStatesProcessor($statesProcessor);
    }

    /**
     * @inheritdoc
     */
    public function process(): void
    {
        $states = $this->getStates();
        /** @phpstan-var SymbolCollection<Item> $items */
        foreach ($states as $state => $items) {
            assert($items->assertCollectionOfType(Item::class));
            $this->getShiftProcessor()->process($items, $state);
            $this->getReduceProcessor()->process($items, $state);
            $this->getGotosProcessor()->process($items, $state);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * GotosProcessor factory
     *
     * @return GotosProcessor
     */
    protected function createGotosProcessor(): GotosProcessor
    {
        $grammar = $this->getGrammar();
        $indexesProcessor = $this->getIndexesProcessor();
        $statesProcessor = $this->getStatesProcessor();
        $tableCollection = $this->getTableCollection();
        $gotosProcessor = new GotosProcessor(
            $grammar,
            $indexesProcessor,
            $statesProcessor,
            $tableCollection
        );
        return $gotosProcessor;
    }

    /**
     * ReduceProcessor factory
     *
     * @return ReduceProcessor
     */
    protected function createReduceProcessor(): ReduceProcessor
    {
        $grammar = $this->getGrammar();
        $indexesProcessor = $this->getIndexesProcessor();
        $statesProcessor = $this->getStatesProcessor();
        $tableCollection = $this->getTableCollection();
        $reduceProcessor = new ReduceProcessor(
            $grammar,
            $indexesProcessor,
            $statesProcessor,
            $tableCollection
        );
        return $reduceProcessor;
    }

    /**
     * ShiftProcessor factory
     *
     * @return ShiftProcessor
     */
    protected function createShiftProcessor(): ShiftProcessor
    {
        $grammar = $this->getGrammar();
        $indexesProcessor = $this->getIndexesProcessor();
        $statesProcessor = $this->getStatesProcessor();
        $tableCollection = $this->getTableCollection();
        $shiftProcessor = new ShiftProcessor(
            $grammar,
            $indexesProcessor,
            $statesProcessor,
            $tableCollection
        );
        return $shiftProcessor;
    }

    /**
     * TableCollection factory
     *
     * @return TableCollection
     */
    protected function createTableCollection(): TableCollection
    {
        $tableCollection = new TableCollection();
        return $tableCollection;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * GotosProcessor getter
     *
     * @return GotosProcessor
     */
    protected function getGotosProcessor(): GotosProcessor
    {
        if (isset($this->gotosProcessor) === false) {
            $gotosProcessor = $this->createGotosProcessor();
            $this->setGotosProcessor($gotosProcessor);
        }
        return $this->gotosProcessor;
    }

    /**
     * GotosProcessor setter
     *
     * @param GotosProcessor $gotosProcessor
     * @return static Provides fluent interface
     */
    protected function setGotosProcessor(GotosProcessor $gotosProcessor): static
    {
        $this->gotosProcessor = $gotosProcessor;
        return $this;
    }

    /**
     * IndexesProcessor getter
     *
     * @return IndexesProcessor
     */
    protected function getIndexesProcessor(): IndexesProcessor
    {
        return $this->indexesProcessor;
    }

    /**
     * IndexesProcessor setter
     *
     * @param IndexesProcessor $indexesProcessor
     * @return static Provides fluent interface
     */
    protected function setIndexesProcessor(IndexesProcessor $indexesProcessor): static
    {
        $this->indexesProcessor = $indexesProcessor;
        return $this;
    }

    /**
     * ReduceProcessor getter
     *
     * @return ReduceProcessor
     */
    protected function getReduceProcessor(): ReduceProcessor
    {
        if (isset($this->reduceProcessor) === false) {
            $reduceProcessor = $this->createReduceProcessor();
            $this->setReduceProcessor($reduceProcessor);
        }
        return $this->reduceProcessor;
    }

    /**
     * ReduceProcessor setter
     *
     * @param ReduceProcessor $reduceProcessor
     * @return static Provides fluent interface
     */
    protected function setReduceProcessor(ReduceProcessor $reduceProcessor): static
    {
        $this->reduceProcessor = $reduceProcessor;
        return $this;
    }

    /**
     * ShiftProcessor getter
     *
     * @return ShiftProcessor
     */
    protected function getShiftProcessor(): ShiftProcessor
    {
        if (isset($this->shiftProcessor) === false) {
            $shiftProcessor = $this->createShiftProcessor();
            $this->setShiftProcessor($shiftProcessor);
        }
        return $this->shiftProcessor;
    }

    /**
     * ShiftProcessor setter
     *
     * @param ShiftProcessor $shiftProcessor
     * @return static Provides fluent interface
     */
    protected function setShiftProcessor(ShiftProcessor $shiftProcessor): static
    {
        $this->shiftProcessor = $shiftProcessor;
        return $this;
    }

    /**
     * States getter
     *
     * @phpstan-return array<SymbolCollection<Item>>
     */
    public function getStates(): array
    {
        $states = $this->getStatesProcessor()->getStates();
        return $states;
    }

    /**
     * StatesProcessor getter
     *
     * @return StatesProcessor
     */
    protected function getStatesProcessor(): StatesProcessor
    {
        return $this->statesProcessor;
    }

    /**
     * StatesProcessor setter
     *
     * @param StatesProcessor $statesProcessor
     * @return static Provides fluent interface
     */
    protected function setStatesProcessor(StatesProcessor $statesProcessor): static
    {
        $this->statesProcessor = $statesProcessor;
        return $this;
    }

    /**
     * Table getter
     *
     * @return int[]
     */
    public function getTable(): array
    {
        $table = $this->getTableCollection()->getTable();
        return $table;
    }

    /**
     * TableCollection getter
     *
     * @return TableCollection
     */
    protected function getTableCollection(): TableCollection
    {
        if (isset($this->tableCollection) === false) {
            $tableCollection = $this->createTableCollection();
            $this->setTableCollection($tableCollection);
        }
        return $this->tableCollection;
    }

    /**
     * TableCollection setter
     *
     * @param TableCollection $tableCollection
     * @return static Provides fluent interface
     */
    protected function setTableCollection(TableCollection $tableCollection): static
    {
        $this->tableCollection = $tableCollection;
        return $this;
    }

    // </editor-fold>
}
