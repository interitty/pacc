<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Terminal;

class FollowProcessor extends BaseProcessor
{
    /**
     * @inheritdoc
     */
    public function process(): void
    {
        $epsilonIndex = $this->getEpsilonIndex();
        $productions = $this->getProductions();

        $this->getStart()->getFollow()->add($this->getEnd()->getIndex());

        /** @var Production $production */
        foreach ($productions as $production) {
            $this->processProductionIndex($production, $epsilonIndex);
        }

        do {
            $done = true;
            /** @var Production $production */
            foreach ($productions as $production) {
                $done = $this->processSymbolFollows($production, $epsilonIndex);
            }
        } while ($done === false);
    }

    /**
     * Production index processor
     *
     * @param Production $production
     * @param int $epsilonIndex
     * @return void
     */
    protected function processProductionIndex(Production $production, int $epsilonIndex): void
    {
        $rights = $production->getRight();
        $rightsCount = $production->getRightCount() - 1;
        for ($index = 0; $index < $rightsCount; ++$index) {
            if ($rights[$index] instanceof Terminal) {
                continue;
            }
            foreach ($rights[$index + 1]->getFirst() as $nextIndex) {
                if ($nextIndex === $epsilonIndex) {
                    continue;
                }
                $rights[$index]->getFollow()->add($nextIndex);
            }
        }
    }

    /**
     * Symbol follows processor
     *
     * @param Production $production
     * @param int $epsilonIndex
     * @return bool
     */
    protected function processSymbolFollows(Production $production, int $epsilonIndex): bool
    {
        $done = true;
        $rights = $production->getRight();
        $rightsCount = $production->getRightCount();
        for ($index = 0; $index < $rightsCount; ++$index) {
            if ($rights[$index] instanceof Terminal) {
                continue;
            }
            $done = $this->processSymbolFollow($production, $epsilonIndex, $index);
        }
        return $done;
    }

    /**
     * Symbol follow processor
     *
     * @param Production $production
     * @param int $epsilonIndex
     * @param int $index
     * @return bool
     */
    protected function processSymbolFollow(Production $production, int $epsilonIndex, int $index): bool
    {
        $leftFollow = $production->getLeft()->getFollow();
        $rights = $production->getRight();
        $rightsCount = $production->getRightCount();

        $done = true;
        $emptyAfter = true;
        for ($nextIndex = $index + 1; $nextIndex < $rightsCount; ++$nextIndex) {
            if ($rights[$nextIndex]->getFirst()->contains($epsilonIndex) === false) {
                $emptyAfter = false;
                break;
            }
        }

        if (
            ($emptyAfter === true) &&
            ($rights[$index]->getFollow()->containsCollection($leftFollow) === false)
        ) {
            $rights[$index]->getFollow()->addCollection($leftFollow);
            $done = false;
        }
        return $done;
    }
}
