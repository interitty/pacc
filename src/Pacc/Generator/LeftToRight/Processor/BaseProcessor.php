<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseProcessor
{
    /** All available collection type constants */
    public const string COLLECTION_INTEGER = 'integer';

    /** @var Grammar */
    protected Grammar $grammar;

    /**
     * Constructor
     *
     * @param Grammar $grammar
     * @return void
     */
    public function __construct(Grammar $grammar)
    {
        $this->setGrammar($grammar);
    }

    /**
     * Internal logic of the processor
     *
     * @return void
     */
    abstract public function process(): void;

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Collection factory
     *
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     * @template CollectionType of object
     */
    protected function createCollection(string $type): SymbolCollection
    {
        /** @phpstan-var SymbolCollection<CollectionType> $collection */
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * Collection of integer factory
     *
     * @phpstan-return SymbolCollection<int>
     */
    protected function createCollectionInteger(): SymbolCollection
    {
        /** @phpstan-var SymbolCollection<int> $collection */
        $collection = new SymbolCollection(self::COLLECTION_INTEGER);
        return $collection;
    }

    /**
     * Item factory
     *
     * @param Production $production
     * @param int $dot
     * @param int $terminalIndex
     * @return Item
     */
    protected function createItem(Production $production, int $dot, int $terminalIndex): Item
    {
        $item = new Item($production, $dot, $terminalIndex);
        return $item;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Writes a message to the standard output
     *
     * @param string|iterable<string> $messages The message as an iterable of strings or a single string
     * @param bool $newLine Whether to add a newline
     * @param int $options A bitmask of OutputInterface::OUTPUT_* or OutputInterface::VERBOSITY_* constants
     * @return static Provides fluent interface
     */
    protected function write($messages, bool $newLine = true, int $options = 0): static
    {
        $output = $this->getGrammar()->getOutput();
        if ($output instanceof OutputInterface) {
            $output->write($messages, $newLine, $options);
        }
        return $this;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * End getter
     *
     * @return Terminal
     */
    protected function getEnd(): Terminal
    {
        $end = $this->getGrammar()->getEnd();
        return $end;
    }

    /**
     * End setter
     *
     * @param Terminal $end
     * @return static Provides fluent interface
     */
    protected function setEnd(Terminal $end): static
    {
        $this->getGrammar()->setEnd($end);
        return $this;
    }

    /**
     * Epsilon index getter
     *
     * @return int
     */
    protected function getEpsilonIndex(): int
    {
        $epsilonIndex = $this->getGrammar()->getEpsilon()->getIndex();
        return $epsilonIndex;
    }

    /**
     * Epsilon setter
     *
     * @param Terminal $epsilon
     * @return static Provides fluent interface
     */
    protected function setEpsilon(Terminal $epsilon): static
    {
        $this->getGrammar()->setEpsilon($epsilon);
        return $this;
    }

    /**
     * Grammar getter
     *
     * @return Grammar
     */
    protected function getGrammar(): Grammar
    {
        return $this->grammar;
    }

    /**
     * Grammar setter
     *
     * @param Grammar $grammar
     * @return static Provides fluent interface
     */
    protected function setGrammar(Grammar $grammar): static
    {
        $this->grammar = $grammar;
        return $this;
    }

    /**
     * NonTerminals getter
     *
     * @phpstan-return SymbolCollection<NonTerminal>
     */
    protected function getNonTerminals(): SymbolCollection
    {
        $nonTerminals = $this->getGrammar()->getNonTerminals();
        return $nonTerminals;
    }

    /**
     * Productions getter
     *
     * @phpstan-return SymbolCollection<Production>
     */
    protected function getProductions(): SymbolCollection
    {
        $productions = $this->getGrammar()->getProductions();
        return $productions;
    }

    /**
     * Start getter
     *
     * @return NonTerminal
     */
    protected function getStart(): NonTerminal
    {
        $start = $this->getGrammar()->getStart();
        return $start;
    }

    /**
     * Start setter
     *
     * @param NonTerminal $start
     * @return static Provides fluent interface
     */
    protected function setStart(NonTerminal $start): static
    {
        $this->getGrammar()->setStart($start);
        return $this;
    }

    /**
     * StartProduction getter
     *
     * @return Production
     */
    protected function getStartProduction(): Production
    {
        $startProduction = $this->getGrammar()->getStartProduction();
        return $startProduction;
    }

    /**
     * StartProduction setter
     *
     * @param Production $startProduction
     * @return static Provides fluent interface
     */
    protected function setStartProduction(Production $startProduction): static
    {
        $this->getGrammar()->setStartProduction($startProduction);
        return $this;
    }

    /**
     * Terminals getter
     *
     * @phpstan-return SymbolCollection<Terminal>
     */
    protected function getTerminals(): SymbolCollection
    {
        $terminals = $this->getGrammar()->getTerminals();
        return $terminals;
    }

    // </editor-fold>
}
