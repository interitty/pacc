<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Exceptions\BadItemException;
use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;

use function count;

class ReduceProcessor extends BaseTableProcessor
{
    /**
     * @inheritdoc
     */
    public function process(SymbolCollection $items, int $state): void
    {
        $startProduction = $this->getStartProduction();
        /** @var Item $item */
        foreach ($items as $item) {
            if (count($item->afterDot()) === 0) {
                $this->processReduces($item, $startProduction, $state);
            }
        }
    }

    /**
     * Reduces processor
     *
     * @param Item $item
     * @param Production $startProduction
     * @param int $state
     * @return void
     */
    protected function processReduces(Item $item, Production $startProduction, int $state): void
    {
        $tableCollection = $this->getTableCollection();
        $tableIndex = $state * $this->getTablePitch() + $item->getTerminalIndex();
        $tableItem = 0;
        if ($item->getProduction()->isEqual($startProduction) === false) {
            $this->processReducesConflict($item, $tableIndex);
            $tableItem = -$item->getProduction()->getIndex();
        }
        $tableCollection->setTableItem($tableIndex, $tableItem);
    }

    /**
     * ReducesConflict processor
     *
     * @param Item $item
     * @param int $tableIndex
     * @return void
     */
    protected function processReducesConflict(Item $item, int $tableIndex): void
    {
        $tableCollection = $this->getTableCollection();
        if ($tableCollection->hasTableItem($tableIndex) === true) {
            $tableItem = $tableCollection->getTableItem($tableIndex);
            if ($tableItem > 0) {
                throw new BadItemException('Shift-reduce conflict');
            } elseif ($tableItem < 0) {
                throw (new BadItemException('Reduce-reduce conflict: :item'))
                        ->addData('item', $item);
            } else {
                throw (new BadItemException('Accept-reduce conflict: :item'))
                        ->addData('item', $item);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * StartProduction getter
     *
     * @return Production
     */
    protected function getStartProduction(): Production
    {
        $startProduction = $this->getGrammar()->getStartProduction();
        return $startProduction;
    }

    // </editor-fold>
}
