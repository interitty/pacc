<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

class TableCollection
{
    /** @var int[] */
    protected array $table = [];

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Table getter
     *
     * @return int[]
     */
    public function getTable(): array
    {
        return $this->table;
    }

    /**
     * Table item getter
     *
     * @param int $index
     * @return int
     */
    public function getTableItem(int $index): int
    {
        return $this->table[$index];
    }

    /**
     * Table item checker
     *
     * @param int $index
     * @return bool
     */
    public function hasTableItem(int $index): bool
    {
        $hasTableItem = array_key_exists($index, $this->table);
        return $hasTableItem;
    }

    /**
     * Table item setter
     *
     * @param int $index
     * @param int $item
     * @return static Provides fluent interface
     */
    public function setTableItem(int $index, int $item): static
    {
        $this->table[$index] = $item;
        return $this;
    }

    // </editor-fold>
}
