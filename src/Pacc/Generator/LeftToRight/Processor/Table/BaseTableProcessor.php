<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Generator\LeftToRight\Jump;
use Interitty\Pacc\Generator\LeftToRight\Processor\IndexesProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\StatesProcessor;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;

use function assert;
use function is_int;

abstract class BaseTableProcessor
{
    /** @var Grammar */
    protected Grammar $grammar;

    /** @var IndexesProcessor */
    protected IndexesProcessor $indexesProcessor;

    /** @var StatesProcessor */
    protected StatesProcessor $statesProcessor;

    /** @var TableCollection */
    protected TableCollection $tableCollection;

    /**
     * Constructor
     *
     * @param Grammar $grammar
     * @param IndexesProcessor $indexesProcessor
     * @param StatesProcessor $statesProcessor
     * @param TableCollection $tableCollection
     * @return void
     */
    public function __construct(
        Grammar $grammar,
        IndexesProcessor $indexesProcessor,
        StatesProcessor $statesProcessor,
        TableCollection $tableCollection
    ) {
        $this->setGrammar($grammar);
        $this->setIndexesProcessor($indexesProcessor);
        $this->setStatesProcessor($statesProcessor);
        $this->setTableCollection($tableCollection);
    }

    /**
     * Internal logic of the processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @param int $state
     * @return void
     */
    abstract public function process(SymbolCollection $items, int $state): void;

    /**
     * Next state processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @param Symbol $symbol
     * @return int|null
     */
    protected function processNextState(SymbolCollection $items, Symbol $symbol): ?int
    {
        assert($items->assertCollectionOfType(Item::class));

        $jumps = $this->getJumps();
        $nextState = null;
        foreach ($jumps as $jump) {
            $nextState = $this->processNextStateJump($items, $jump, $symbol);
            if (is_int($nextState) === true) {
                break;
            }
        }

        return $nextState;
    }

    /**
     * Next state processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @param Jump $jump
     * @param Symbol $symbol
     * @return int|null
     */
    protected function processNextStateJump(SymbolCollection $items, Jump $jump, Symbol $symbol): ?int
    {
        $nextState = null;
        if (($jump->getItemFrom()->isEqual($items) === true) && ($jump->getSymbol()->isEqual($symbol) === true)) {
            foreach ($this->getStates() as $index => $state) {
                if ($jump->getItemTo()->isEqual($state)) {
                    $nextState = (int) $index;
                    break;
                }
            }
        }
        return $nextState;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Grammar getter
     *
     * @return Grammar
     */
    protected function getGrammar(): Grammar
    {
        return $this->grammar;
    }

    /**
     * Grammar setter
     *
     * @param Grammar $grammar
     * @return static Provides fluent interface
     */
    protected function setGrammar(Grammar $grammar): static
    {
        $this->grammar = $grammar;
        return $this;
    }

    /**
     * IndexesProcessor getter
     *
     * @return IndexesProcessor
     */
    protected function getIndexesProcessor(): IndexesProcessor
    {
        return $this->indexesProcessor;
    }

    /**
     * IndexesProcessor setter
     *
     * @param IndexesProcessor $indexesProcessor
     * @return static Provides fluent interface
     */
    protected function setIndexesProcessor(IndexesProcessor $indexesProcessor): static
    {
        $this->indexesProcessor = $indexesProcessor;
        return $this;
    }

    /**
     * Jumps getter
     *
     * @return Jump[]
     */
    protected function getJumps(): array
    {
        $jumps = $this->getStatesProcessor()->getJumps();
        return $jumps;
    }

    /**
     * States getter
     *
     * @phpstan-return array<SymbolCollection<Item>>
     */
    public function getStates(): array
    {
        $states = $this->getStatesProcessor()->getStates();
        return $states;
    }

    /**
     * StatesProcessor getter
     *
     * @return StatesProcessor
     */
    protected function getStatesProcessor(): StatesProcessor
    {
        return $this->statesProcessor;
    }

    /**
     * StatesProcessor setter
     *
     * @param StatesProcessor $statesProcessor
     * @return static Provides fluent interface
     */
    protected function setStatesProcessor(StatesProcessor $statesProcessor): static
    {
        $this->statesProcessor = $statesProcessor;
        return $this;
    }

    /**
     * TableCollection getter
     *
     * @return TableCollection
     */
    protected function getTableCollection(): TableCollection
    {
        return $this->tableCollection;
    }

    /**
     * TableCollection setter
     *
     * @param TableCollection $tableCollection
     * @return static Provides fluent interface
     */
    protected function setTableCollection(TableCollection $tableCollection): static
    {
        $this->tableCollection = $tableCollection;
        return $this;
    }

    /**
     * TablePitch getter
     *
     * @return int
     */
    protected function getTablePitch(): int
    {
        $tablePitch = $this->getIndexesProcessor()->getTablePitch();
        return $tablePitch;
    }

    // </editor-fold>
}
