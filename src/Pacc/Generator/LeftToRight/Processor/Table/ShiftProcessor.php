<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Exceptions\BadItemException;
use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;

use function current;

class ShiftProcessor extends BaseTableProcessor
{
    /**
     * @inheritdoc
     */
    public function process(SymbolCollection $items, int $state): void
    {
        $terminals = $this->getTerminals();
        /** @var Terminal $terminal */
        foreach ($terminals as $terminal) {
            $this->processShifts($items, $terminal, $state);
        }
    }

    /**
     * Shifts processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @param Terminal $terminal
     * @param int $state
     * @return void
     */
    protected function processShifts(SymbolCollection $items, Terminal $terminal, int $state): void
    {
        $doShift = false;
        /** @var Item $item */
        foreach ($items as $item) {
            $afterDot = current($item->afterDot());
            if (($afterDot !== false) && ($afterDot->isEqual($terminal))) {
                $doShift = true;
                break;
            }
        }

        if ($doShift === true) {
            $nextState = $this->processNextState($items, $terminal);
            if ($nextState === null) {
                throw new BadItemException('Cannot get next state for shift');
            }
            $tableCollection = $this->getTableCollection();
            $tableIndex = $state * $this->getTablePitch() + $terminal->getIndex();
            $tableCollection->setTableItem($tableIndex, $nextState);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Terminals getter
     *
     * @phpstan-return SymbolCollection<Terminal>
     */
    protected function getTerminals(): SymbolCollection
    {
        $terminals = $this->getGrammar()->getTerminals();
        return $terminals;
    }

    // </editor-fold>
}
