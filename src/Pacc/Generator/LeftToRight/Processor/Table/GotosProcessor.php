<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\SymbolCollection;

use function assert;
use function is_int;

class GotosProcessor extends BaseTableProcessor
{
    /**
     * @inheritdoc
     */
    public function process(SymbolCollection $items, int $state): void
    {
        $nonTerminals = $this->getNonTerminals();
        /** @var NonTerminal $nonTerminal */
        foreach ($nonTerminals as $nonTerminal) {
            $this->processGotos($items, $nonTerminal, $state);
        }
    }

    /**
     * Gotos processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @param NonTerminal $nonTerminal
     * @param int $state
     * @return void
     */
    protected function processGotos(SymbolCollection $items, NonTerminal $nonTerminal, int $state): void
    {
        assert($items->assertCollectionOfType(Item::class));
        $nextState = $this->processNextState($items, $nonTerminal);
        $tableCollection = $this->getTableCollection();
        if (is_int($nextState) === true) {
            $tableIndex = $state * $this->getTablePitch() + $nonTerminal->getIndex();
            $tableCollection->setTableItem($tableIndex, $nextState);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * NonTerminals getter
     *
     * @phpstan-return SymbolCollection<NonTerminal>
     */
    protected function getNonTerminals(): SymbolCollection
    {
        $nonTerminals = $this->getGrammar()->getNonTerminals();
        return $nonTerminals;
    }

    // </editor-fold>
}
