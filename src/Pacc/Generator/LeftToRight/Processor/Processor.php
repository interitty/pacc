<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Utils\Strings;
use Symfony\Component\Console\Output\OutputInterface;

use function get_class;

class Processor extends BaseProcessor
{
    /** @var FirstProcessor */
    protected FirstProcessor $firstProcessor;

    /** @var FollowProcessor */
    protected FollowProcessor $followProcessor;

    /** @var IndexesProcessor */
    protected IndexesProcessor $indexesProcessor;

    /** @var InitProcessor */
    protected InitProcessor $initProcessor;

    /** @var StatesProcessor */
    protected StatesProcessor $statesProcessor;

    /** @var TableProcessor */
    protected TableProcessor $tableProcessor;

    /**
     * @inheritdoc
     */
    public function process(): void
    {
        // order sensitive actions!
        $this->processRunProcessor($this->getInitProcessor());
        $this->processRunProcessor($this->getIndexesProcessor());
        $this->processRunProcessor($this->getFirstProcessor());
        $this->processRunProcessor($this->getFollowProcessor());
        $this->processRunProcessor($this->getStatesProcessor());
        $this->processRunProcessor($this->getTableProcessor());
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * FirstProcessor factory
     *
     * @return FirstProcessor
     */
    protected function createFirstProcessor(): FirstProcessor
    {
        $grammar = $this->getGrammar();
        $firstProcessor = new FirstProcessor($grammar);
        return $firstProcessor;
    }

    /**
     * FollowProcessor factory
     *
     * @return FollowProcessor
     */
    protected function createFollowProcessor(): FollowProcessor
    {
        $grammar = $this->getGrammar();
        $followProcessor = new FollowProcessor($grammar);
        return $followProcessor;
    }

    /**
     * IndexesProcessor factory
     *
     * @return IndexesProcessor
     */
    protected function createIndexesProcessor(): IndexesProcessor
    {
        $grammar = $this->getGrammar();
        $indexesProcessor = new IndexesProcessor($grammar);
        return $indexesProcessor;
    }

    /**
     * InitProcessor factory
     *
     * @return InitProcessor
     */
    protected function createInitProcessor(): InitProcessor
    {
        $grammar = $this->getGrammar();
        $initProcessor = new InitProcessor($grammar);
        return $initProcessor;
    }

    /**
     * StatesProcessor factory
     *
     * @return StatesProcessor
     */
    protected function createStatesProcessor(): StatesProcessor
    {
        $grammar = $this->getGrammar();
        $statesProcessor = new StatesProcessor($grammar);
        return $statesProcessor;
    }

    /**
     * TableProcessor factory
     *
     * @return TableProcessor
     */
    protected function createTableProcessor(): TableProcessor
    {
        $grammar = $this->getGrammar();
        $indexesProcessor = $this->getIndexesProcessor();
        $statesProcessor = $this->getStatesProcessor();
        $tableProcessor = new TableProcessor($grammar, $indexesProcessor, $statesProcessor);
        return $tableProcessor;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Run processor processor
     *
     * @param BaseProcessor $processor
     * @return static Provides fluent interface
     */
    protected function processRunProcessor(BaseProcessor $processor): static
    {
        $processorName = (string) Strings::after(get_class($processor), '\\', -1);
        $this->write($processorName, false, OutputInterface::VERBOSITY_VERBOSE);
        $processor->process();
        $this->write(' <info>Done</info>', true, OutputInterface::VERBOSITY_VERBOSE);
        return $this;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * FirstProcessor getter
     *
     * @return FirstProcessor
     */
    protected function getFirstProcessor(): FirstProcessor
    {
        if (isset($this->firstProcessor) === false) {
            $firstProcessor = $this->createFirstProcessor();
            $this->setFirstProcessor($firstProcessor);
        }
        return $this->firstProcessor;
    }

    /**
     * FirstProcessor setter
     *
     * @param FirstProcessor $firstProcessor
     * @return static Provides fluent interface
     */
    protected function setFirstProcessor(FirstProcessor $firstProcessor): static
    {
        $this->firstProcessor = $firstProcessor;
        return $this;
    }

    /**
     * FollowProcessor getter
     *
     * @return FollowProcessor
     */
    protected function getFollowProcessor(): FollowProcessor
    {
        if (isset($this->followProcessor) === false) {
            $followProcessor = $this->createFollowProcessor();
            $this->setFollowProcessor($followProcessor);
        }
        return $this->followProcessor;
    }

    /**
     * FollowProcessor setter
     *
     * @param FollowProcessor $followProcessor
     * @return static Provides fluent interface
     */
    protected function setFollowProcessor(FollowProcessor $followProcessor): static
    {
        $this->followProcessor = $followProcessor;
        return $this;
    }

    /**
     * IndexesProcessor getter
     *
     * @return IndexesProcessor
     */
    protected function getIndexesProcessor(): IndexesProcessor
    {
        if (isset($this->indexesProcessor) === false) {
            $indexesProcessor = $this->createIndexesProcessor();
            $this->setIndexesProcessor($indexesProcessor);
        }
        return $this->indexesProcessor;
    }

    /**
     * IndexesProcessor setter
     *
     * @param IndexesProcessor $indexesProcessor
     * @return static Provides fluent interface
     */
    protected function setIndexesProcessor(IndexesProcessor $indexesProcessor): static
    {
        $this->indexesProcessor = $indexesProcessor;
        return $this;
    }

    /**
     * InitProcessor getter
     *
     * @return InitProcessor
     */
    protected function getInitProcessor(): InitProcessor
    {
        if (isset($this->initProcessor) === false) {
            $initProcessor = $this->createInitProcessor();
            $this->setInitProcessor($initProcessor);
        }
        return $this->initProcessor;
    }

    /**
     * InitProcessor setter
     *
     * @param InitProcessor $initProcessor
     * @return static Provides fluent interface
     */
    protected function setInitProcessor(InitProcessor $initProcessor): static
    {
        $this->initProcessor = $initProcessor;
        return $this;
    }

    /**
     * StatesProcessor getter
     *
     * @return StatesProcessor
     */
    protected function getStatesProcessor(): StatesProcessor
    {
        if (isset($this->statesProcessor) === false) {
            $statesProcessor = $this->createStatesProcessor();
            $this->setStatesProcessor($statesProcessor);
        }
        return $this->statesProcessor;
    }

    /**
     * StatesProcessor setter
     *
     * @param StatesProcessor $statesProcessor
     * @return static Provides fluent interface
     */
    protected function setStatesProcessor(StatesProcessor $statesProcessor): static
    {
        $this->statesProcessor = $statesProcessor;
        return $this;
    }

    /**
     * Table getter
     *
     * @return int[]
     */
    public function getTable(): array
    {
        $table = $this->getTableProcessor()->getTable();
        return $table;
    }

    /**
     * TablePitch getter
     *
     * @return int
     */
    public function getTablePitch(): int
    {
        $tablePitch = $this->getIndexesProcessor()->getTablePitch();
        return $tablePitch;
    }

    /**
     * TableProcessor getter
     *
     * @return TableProcessor
     */
    protected function getTableProcessor(): TableProcessor
    {
        if (isset($this->tableProcessor) === false) {
            $tableProcessor = $this->createTableProcessor();
            $this->setTableProcessor($tableProcessor);
        }
        return $this->tableProcessor;
    }

    /**
     * TableProcessor setter
     *
     * @param TableProcessor $tableProcessor
     * @return static Provides fluent interface
     */
    protected function setTableProcessor(TableProcessor $tableProcessor): static
    {
        $this->tableProcessor = $tableProcessor;
        return $this;
    }

    // </editor-fold>
}
