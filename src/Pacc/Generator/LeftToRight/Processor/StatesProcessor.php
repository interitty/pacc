<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Generator\LeftToRight\Jump;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;

use function assert;
use function count;
use function current;
use function next;

class StatesProcessor extends BaseProcessor
{
    /** @var Jump[] */
    protected array $jumps = [];

    /** @phpstan-var array<SymbolCollection<Item>> */
    protected array $states = [];

    /**
     * @inheritdoc
     */
    public function process(): void
    {
        $this->processStates();
        $this->processJumps();
    }

    /**
     * Closure item processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @param Item $item
     * @param bool $done
     * @return bool
     */
    protected function processClosureItem(SymbolCollection $items, Item $item, bool $done): bool
    {
        $new = $this->createCollection(Item::class);
        $this->processProductions($new, $item);
        $newDone = $this->processNewItems($items, $new, $done);
        return $newDone;
    }

    /**
     * Closure items processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @phpstan-return SymbolCollection<Item>
     */
    protected function processClosureItems(SymbolCollection $items): SymbolCollection
    {
        assert($items->assertCollectionOfType(Item::class));
        do {
            $done = true;
            /** @var Item $item */
            foreach ($items as $item) {
                if ((current($item->afterDot()) instanceof NonTerminal) === true) {
                    $done = $this->processClosureItem($items, $item, $done);
                }
            }
        } while ($done === false);
        return $items;
    }

    /**
     * Item firsts processor
     *
     * @param Item $item
     * @phpstan-return SymbolCollection<int>
     */
    protected function processItemFirsts(Item $item): SymbolCollection
    {
        $afterDots = $item->afterDot();
        $firsts = $this->createCollectionInteger();
        $nextItem = next($afterDots);
        if ($nextItem instanceof Symbol) {
            $firsts->addCollection($nextItem->getFirst());
            $firsts->deleteItem($this->getEpsilonIndex());
        }
        if ($firsts->count() === 0) {
            $firsts->add($item->getTerminalIndex());
        }
        return $firsts;
    }

    /**
     * Jump processor
     *
     * @param Symbol $symbol
     * @param int $index
     * @return static Provides fluent interface
     */
    protected function processJump(Symbol $symbol, int $index): static
    {
        $stateItems = $this->getStateItems($index);
        assert($stateItems->assertCollectionOfType(Item::class));

        $jump = $this->processJumpItems($stateItems, $symbol);
        if ($jump->count() > 0) {
            $this->processJumpAdd($jump, $symbol, $index);
        }
        return $this;
    }

    /**
     * Jump add processor
     *
     * @phpstan-param SymbolCollection<Item> $jump
     * @param Symbol $symbol
     * @param int $index
     * @return void
     */
    protected function processJumpAdd(SymbolCollection $jump, Symbol $symbol, int $index): void
    {
        $alreadyIn = false;
        foreach ($this->getStates() as $state) {
            if ($state->isEqual($jump) === true) {
                $alreadyIn = true;
                $jump = $state;
                break;
            }
        }
        if ($alreadyIn === false) {
            $this->addStateItems($jump);
        }
        $this->addJump($this->createJump($this->getStateItems($index), $symbol, $jump));
    }

    /**
     * Jump items processor
     *
     * @phpstan-param SymbolCollection<Item> $stateItems
     * @param Symbol $symbol
     * @phpstan-return SymbolCollection<Item>
     */
    protected function processJumpItems(SymbolCollection $stateItems, Symbol $symbol): SymbolCollection
    {
        $items = $this->createCollection(Item::class);
        /** @var Item $item */
        foreach ($stateItems as $item) {
            $currentAfterDot = current($item->afterDot());
            if (($currentAfterDot instanceof Symbol) && ($currentAfterDot->isEqual($symbol) === true)) {
                $newItem = $this->createItem($item->getProduction(), $item->getDot() + 1, $item->getTerminalIndex());
                $items->add($newItem);
            }
        }
        $jump = $this->processClosureItems($items);
        return $jump;
    }

    /**
     * Jumps processor
     *
     * @return void
     */
    protected function processJumps(): void
    {
        $index = 0;
        $nonTerminals = $this->getNonTerminals();
        $terminals = $this->getTerminals();
        do {
            /** @var NonTerminal $nonTerminal */
            foreach ($nonTerminals as $nonTerminal) {
                $this->processJump($nonTerminal, $index);
            }
            /** @var Terminal $terminal */
            foreach ($terminals as $terminal) {
                $this->processJump($terminal, $index);
            }
            $index = $index + 1;
        } while ($index < count($this->getStates()));
    }

    /**
     * New items processor
     *
     * @phpstan-param SymbolCollection<Item> $items
     * @phpstan-param SymbolCollection<Item> $new
     * @param bool $done
     * @return bool
     */
    protected function processNewItems(SymbolCollection $items, SymbolCollection $new, bool $done): bool
    {
        if (($new->count() > 0) && ($items->containsCollection($new) === false)) {
            $items->addCollection($new);
            $done = false;
        }
        return $done;
    }

    /**
     * Prepare states processor
     *
     * @phpstan-return SymbolCollection<Item>
     */
    protected function processPrepareStates(): SymbolCollection
    {
        $item = $this->createItem($this->getStartProduction(), 0, $this->getEnd()->getIndex());
        $items = $this->createCollection(Item::class);
        $items->add($item);
        return $items;
    }

    /**
     * Production processor
     *
     * @phpstan-param SymbolCollection<int> $firsts
     * @phpstan-param SymbolCollection<Item> $new
     * @param Production $production
     * @return void
     */
    protected function processProduction(SymbolCollection $firsts, SymbolCollection $new, Production $production): void
    {
        /** @var int $terminalIndex */
        foreach ($firsts as $terminalIndex) {
            $item = $this->createItem($production, 0, $terminalIndex);
            $new->add($item);
        }
    }

    /**
     * Productions processor
     *
     * @phpstan-param SymbolCollection<Item> $new
     * @param Item $item
     * @return void
     */
    protected function processProductions(SymbolCollection $new, Item $item): void
    {
        $firsts = $this->processItemFirsts($item);
        $productions = $this->getProductions();
        /** @var Production $production */
        foreach ($productions as $production) {
            $currentAfterDot = current($item->afterDot());
            if (($currentAfterDot instanceof Symbol) && ($currentAfterDot->isEqual($production->getLeft()) === true)) {
                $this->processProduction($firsts, $new, $production);
            }
        }
    }

    /**
     * States processor
     *
     * @return void
     */
    protected function processStates(): void
    {
        $items = $this->processPrepareStates();
        $states = [$this->processClosureItems($items)];
        $this->setStates($states);
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Jump factory
     *
     * @phpstan-param SymbolCollection<Item> $itemFrom
     * @param Symbol $symbol
     * @phpstan-param SymbolCollection<Item> $itemTo
     * @return Jump
     */
    protected function createJump(SymbolCollection $itemFrom, Symbol $symbol, SymbolCollection $itemTo): Jump
    {
        $jump = new Jump($itemFrom, $symbol, $itemTo);
        return $jump;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Jump adder
     *
     * @param Jump $jump
     * @return static Provides fluent interface
     */
    protected function addJump(Jump $jump): static
    {
        $this->jumps[] = $jump;
        return $this;
    }

    /**
     * Jumps getter
     *
     * @return Jump[]
     */
    public function getJumps(): array
    {
        return $this->jumps;
    }

    /**
     * Jumps setter
     *
     * @param Jump[] $jumps
     * @return static Provides fluent interface
     */
    protected function setJumps(array $jumps): static
    {
        $this->jumps = [];
        foreach ($jumps as $jump) {
            $this->addJump($jump);
        }
        return $this;
    }

    /**
     * State items adder
     *
     * @phpstan-param SymbolCollection<Item> $stateItems
     * @return static Provides fluent interface
     */
    protected function addStateItems(SymbolCollection $stateItems): static
    {
        $this->states[] = $stateItems;
        return $this;
    }

    /**
     * State items getter
     *
     * @param int $state
     * @phpstan-return SymbolCollection<Item>
     */
    public function getStateItems(int $state): SymbolCollection
    {
        $stateItems = $this->states[$state];
        return $stateItems;
    }

    /**
     * States getter
     *
     * @phpstan-return array<SymbolCollection<Item>>
     */
    public function getStates(): array
    {
        return $this->states;
    }

    /**
     * States setter
     *
     * @phpstan-param array<SymbolCollection<Item>> $states
     * @return static Provides fluent interface
     */
    protected function setStates(array $states): static
    {
        $this->states = [];
        /** @phpstan-var SymbolCollection<Item> $stateItems */
        foreach ($states as $stateItems) {
            $this->addStateItems($stateItems);
        }
        return $this;
    }

    // </editor-fold>
}
