<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Terminal;

class IndexesProcessor extends BaseProcessor
{
    /** @var int Max symbol index (for table pitch) */
    protected int $tablePitch;

    /**
     * @inheritdoc
     */
    public function process(): void
    {
        $this->processSymbolsIndex();
        $this->processProductionsIndex();
    }

    /**
     * Symbols index procesor
     *
     * @return void
     */
    protected function processSymbolsIndex(): void
    {
        $index = 1;
        $terminals = $this->getTerminals();
        $nonTerminals = $this->getNonTerminals();

        /** @var Terminal $terminal */
        foreach ($terminals as $terminal) {
            $terminal->setIndex($index++);
            $firstCollection = $this->createCollectionInteger();
            $firstCollection->add($terminal->getIndex());
            $terminal->setFirst($firstCollection);
        }
        $terminals->add($this->getEnd());

        /** @var NonTerminal $nonTerminal */
        foreach ($nonTerminals as $nonTerminal) {
            $nonTerminal->setFirst($this->createCollectionInteger());
            $nonTerminal->setFollow($this->createCollectionInteger());
            $nonTerminal->setIndex($index++);
        }

        $this->setTablePitch($index - 1);
    }

    /**
     * Productions index processor
     *
     * @return void
     */
    protected function processProductionsIndex(): void
    {
        $productionIndex = 1;
        $productions = $this->getProductions();
        /** @var Production $production */
        foreach ($productions as $production) {
            $production->setIndex($productionIndex++);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * TablePitch getter
     *
     * @return int
     */
    public function getTablePitch(): int
    {
        return $this->tablePitch;
    }

    /**
     * TablePitch setter
     *
     * @param int $tablePitch
     * @return static Provides fluent interface
     */
    protected function setTablePitch(int $tablePitch): static
    {
        $this->tablePitch = $tablePitch;
        return $this;
    }

    // </editor-fold>
}
