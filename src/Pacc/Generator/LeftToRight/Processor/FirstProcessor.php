<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;

class FirstProcessor extends BaseProcessor
{
    /**
     * @inheritdoc
     */
    public function process(): void
    {
        $epsilonIndex = $this->getEpsilonIndex();
        $productions = $this->getProductions();

        $this->processAddFirstEpsilonIndex($productions, $epsilonIndex);

        do {
            $done = true;
            /** @var Production $production */
            foreach ($productions as $production) {
                $done = $this->processProductionIndex($production, $epsilonIndex, $done);
            }
        } while ($done === false);
    }

    /**
     * Add epsilon index to the all firsts that have something on the right
     *
     * @phpstan-param SymbolCollection<Production> $productions
     * @param int $epsilonIndex
     * @return void
     */
    protected function processAddFirstEpsilonIndex(SymbolCollection $productions, int $epsilonIndex): void
    {
        /** @var Production $production */
        foreach ($productions as $production) {
            if ($production->getRightCount() === 0) {
                $production->getLeft()->getFirst()->add($epsilonIndex);
            }
        }
    }

    /**
     * Production index processor
     *
     * @param Production $production
     * @param int $epsilonIndex
     * @param bool $done
     * @return bool
     */
    protected function processProductionIndex(Production $production, int $epsilonIndex, bool $done): bool
    {
        /** @var Symbol $rightSymbol */
        foreach ($production->getRight() as $rightSymbol) {
            $rightFirstSymbol = $rightSymbol->getFirst();
            $leftFirstProduction = $production->getLeft()->getFirst();

            foreach ($rightFirstSymbol as $index) {
                if (
                    ($index !== $epsilonIndex) &&
                    ($leftFirstProduction->contains($index) === false)
                ) {
                    $leftFirstProduction->add($index);
                    $done = false;
                }
            }

            if ($rightFirstSymbol->contains($epsilonIndex) === false) {
                break;
            }
        }
        return $done;
    }
}
