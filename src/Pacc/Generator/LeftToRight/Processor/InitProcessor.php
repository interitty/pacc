<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\Terminal;

class InitProcessor extends BaseProcessor
{
    /**
     * @inheritdoc
     */
    public function process(): void
    {
        $newStart = $this->createNonTerminal('$start');
        $this->setStartProduction($this->createProduction($newStart, [$this->getStart()]));
        $this->getNonTerminals()->add($newStart);
        $this->setStart($newStart);

        $this->setEpsilon($this->createTerminal('$epsilon')
                ->setIndex(-1));

        $endCollection = $this->createCollectionInteger();
        $endCollection->add(0);
        $this->setEnd($this->createTerminal('$end')
                ->setIndex(0)
                ->setFirst($endCollection));
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    /**
     * Production factory
     *
     * @param NonTerminal $left
     * @param Symbol[] $right
     * @param string|null $code [OPTIONAL]
     * @return Production
     */
    protected function createProduction(NonTerminal $left, array $right, string $code = null): Production
    {
        $production = new Production($left, $right, $code);
        return $production;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    protected function createTerminal(string $name, ?string $type = null, ?string $value = null): Terminal
    {
        $terminal = new Terminal($name, $type, $value);
        return $terminal;
    }

    // </editor-fold>
}
