<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight;

use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;

class Jump
{
    /** @phpstan-var SymbolCollection<Item> Begining state */
    protected SymbolCollection $itemFrom;

    /** @phpstan-var SymbolCollection<Item> Ending state */
    protected SymbolCollection $itemTo;

    /** @phpstan-var Symbol */
    protected Symbol $symbol;

    /**
     * Constructor
     *
     * @phpstan-param SymbolCollection<Item> $itemFrom
     * @param Symbol $symbol
     * @phpstan-param SymbolCollection<Item> $itemTo
     * @return void
     */
    public function __construct(SymbolCollection $itemFrom, Symbol $symbol, SymbolCollection $itemTo)
    {
        $this->setItemFrom($itemFrom);
        $this->setSymbol($symbol);
        $this->setItemTo($itemTo);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Item from getter
     *
     * @phpstan-return SymbolCollection<Item>
     */
    public function getItemFrom(): SymbolCollection
    {
        return $this->itemFrom;
    }

    /**
     * Item from setter
     *
     * @phpstan-param SymbolCollection<Item> $itemFrom
     * @return static Provides fluent interface
     */
    protected function setItemFrom(SymbolCollection $itemFrom): static
    {
        $this->itemFrom = $itemFrom;
        return $this;
    }

    /**
     * Item to getter
     *
     * @phpstan-return SymbolCollection<Item>
     */
    public function getItemTo(): SymbolCollection
    {
        return $this->itemTo;
    }

    /**
     * Item to setter
     *
     * @phpstan-param SymbolCollection<Item> $itemTo
     * @return static Provides fluent interface
     */
    protected function setItemTo(SymbolCollection $itemTo): static
    {
        $this->itemTo = $itemTo;
        return $this;
    }

    /**
     * Symbol getter
     *
     * @return Symbol
     */
    public function getSymbol(): Symbol
    {
        return $this->symbol;
    }

    /**
     * Symbol setter
     *
     * @param Symbol $symbol
     * @return static Provides fluent interface
     */
    protected function setSymbol(Symbol $symbol): static
    {
        $this->symbol = $symbol;
        return $this;
    }

    // </editor-fold>
}
