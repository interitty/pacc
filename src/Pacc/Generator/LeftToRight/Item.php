<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight;

use Interitty\Pacc\Symbol\EqualCheckableInterface;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;

use function array_slice;
use function implode;

class Item implements EqualCheckableInterface
{
    /** @var int */
    protected int $dot = 0;

    /** @var Production */
    protected Production $production;

    /** @var int */
    protected int $terminalIndex;

    /**
     * Constructor
     *
     * @param Production $production
     * @param int $dot
     * @param int $terminalIndex
     * @return void
     */
    public function __construct(Production $production, int $dot, int $terminalIndex)
    {
        $this->setProduction($production);
        $this->setDot($dot);
        $this->setTerminalIndex($terminalIndex);
    }

    /**
     * Before dot getter
     *
     * @return Symbol[]
     */
    public function beforeDot(): array
    {
        return array_slice($this->getProduction()->getRight(), 0, $this->getDot());
    }

    /**
     * After dot getter
     *
     * @return Symbol[]
     */
    public function afterDot(): array
    {
        return array_slice($this->getProduction()->getRight(), $this->getDot());
    }

    /**
     * @inheritdoc
     */
    public function isEqual(EqualCheckableInterface $object): bool
    {
        $isEqual = false;
        if (
            ($object instanceof self) &&
            ($this->getTerminalIndex() === $object->getTerminalIndex()) &&
            ($this->getDot() === $object->getDot()) &&
            ($this->getProduction()->isEqual($object->getProduction()) === true)
        ) {
            $isEqual = true;
        }

        return $isEqual;
    }

    /**
     * To string processor
     *
     * @return string
     */
    public function __toString(): string
    {
        $ret = '[' . $this->getProduction()->getLeftName() . ' -> ';

        $beforeSyms = [];
        foreach ($this->beforeDot() as $symbol) {
            $beforeSyms[] = (string) $symbol;
        }
        $ret .= implode(' ', $beforeSyms);

        $ret .= ' . ';

        $afterSyms = [];
        foreach ($this->afterDot() as $symbol) {
            $afterSyms[] = (string) $symbol;
        }
        $ret .= implode(' ', $afterSyms);

        $ret .= ', ' . $this->getTerminalIndex() . ']';
        return $ret;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Dot getter
     *
     * @return int
     */
    public function getDot(): int
    {
        return $this->dot;
    }

    /**
     * Dot setter
     *
     * @param int $dot
     * @return static Provides fluent interface
     */
    protected function setDot(int $dot): static
    {
        $this->dot = $dot;
        return $this;
    }

    /**
     * Production getter
     *
     * @return Production
     */
    public function getProduction(): Production
    {
        return $this->production;
    }

    /**
     * Production setter
     *
     * @param Production $production
     * @return static Provides fluent interface
     */
    protected function setProduction(Production $production): static
    {
        $this->production = $production;
        return $this;
    }

    /**
     * TerminalIndex getter
     *
     * @return int
     */
    public function getTerminalIndex(): int
    {
        return $this->terminalIndex;
    }

    /**
     * TerminalIndex setter
     *
     * @param int $terminalIndex
     * @return static Provides fluent interface
     */
    protected function setTerminalIndex(int $terminalIndex): static
    {
        $this->terminalIndex = $terminalIndex;
        return $this;
    }

    // </editor-fold>
}
