<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight;

use Interitty\Pacc\Generator\BaseGenerator;
use Interitty\Pacc\Generator\LeftToRight\Processor\Processor;
use Interitty\Pacc\Generator\PhpGeneratorHelpers;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Parser\TokenizerParser;
use Interitty\Pacc\Symbol\Production;
use Interitty\Tokenizer\BaseParser;
use Interitty\Utils\Arrays;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\Property;

use function hash;
use function implode;
use function ksort;
use function rtrim;

class Generator extends BaseGenerator
{
    /** All available Default option constants */
    public const string DEFAULT_OPTION_PARSE = 'processParse';
    public const string DEFAULT_PRODUCTION_CODE = '$$ = $1;';

    /** @var string */
    protected static string $hashAlgorithm = 'md5';

    /** @var Processor */
    protected Processor $processor;

    /**
     * @inheritdoc
     */
    public function __construct(Grammar $grammar)
    {
        parent::__construct($grammar);
        $this->getProcessor()->process();
    }

    /**
     * @inheritdoc
     */
    protected function processInner(ClassType $class): void
    {
        $this->processProductions($class);
        $this->processTables($class);
        $this->processTerminals($class);

        $parseMethodName = $this->getOption(OptionsCollection::OPTION_PARSE, self::DEFAULT_OPTION_PARSE);
        if ($parseMethodName !== self::DEFAULT_OPTION_PARSE) {
            $this->processParseMethod($class, $parseMethodName);
        }

        parent::processInner($class);
    }

    /**
     * Parse method processor
     *
     * @param ClassType $class
     * @param string $parseMethodName
     * @return void
     */
    protected function processParseMethod(ClassType $class, string $parseMethodName): void
    {
        $eol = $this->getPrinter()->getEol();

        $class->addMethod($parseMethodName)
            ->setVisibility(PhpGeneratorHelpers::VISIBILITY_PROTECTED)
            ->setReturnType(PhpGeneratorHelpers::TYPE_MIXED)
            ->setComment('Alias of ' . self::DEFAULT_OPTION_PARSE . '()' . $eol . $eol . '@return mixed')
            ->setBody('$result = $this->' . self::DEFAULT_OPTION_PARSE . '();' . $eol
                . 'return $result;');
    }

    /**
     * Productions processor
     *
     * @param ClassType $class
     * @return void
     */
    protected function processProductions(ClassType $class): void
    {
        $productionsLengths = [];
        $productionsLefts = [];
        $productionsEqal = [];
        $productionsEqalMap = [];
        /** @var Production $production */
        foreach ($this->getProductions() as $production) {
            $productionHash = $this->processProductionHash($production);
            $productionIndex = $production->getIndex();
            $productionsLengths[$productionIndex] = $production->getRightCount();
            $productionsLefts[$productionIndex] = $production->getLeft()->getIndex();
            if (isset($productionsEqalMap[$productionHash]) === true) {
                $productionsEqal[$productionIndex] = $productionsEqalMap[$productionHash];
            } else {
                $this->processProductionMethod($class, $production);
                $productionsEqalMap[$productionHash] = $productionIndex;
            }
        }
        $arrayType = PhpGeneratorHelpers::TYPE_ARRAY;
        $this->processProtectedProperty($class, 'productionsEqual', $productionsEqal, $arrayType, '@var int[]');
        $this->processProtectedProperty($class, 'productionsLefts', $productionsLefts, $arrayType, '@var int[]');
        $this->processProtectedProperty($class, 'productionsLengths', $productionsLengths, $arrayType, '@var int[]');
    }

    /**
     * Production hash processor
     *
     * @param Production $production
     * @return string
     */
    protected function processProductionHash(Production $production): string
    {
        $productionCode = $production->getCode() ?? self::DEFAULT_PRODUCTION_CODE;
        $types = $production->getTypes();
        $typesString = implode('|', Arrays::flatten($types));
        $productionHash = hash(self::$hashAlgorithm, $productionCode . $typesString);
        return $productionHash;
    }

    /**
     * Production method processor
     *
     * @param ClassType $class
     * @param Production $production
     * @return void
     */
    protected function processProductionMethod(ClassType $class, Production $production): void
    {
        $productionIndex = $production->getIndex();
        $types = $production->getTypes();
        $productionCode = $production->getCode();
        if ($productionCode === null) {
            $productionCode = self::DEFAULT_PRODUCTION_CODE;
            $types = TokenizerParser::processParseTypes($productionCode);
        }
        $method = $class->addMethod('processReduce' . $productionIndex);
        $method->setComment('Reduce ' . $productionIndex . ' processor');
        $method->setBody($this->getPrinter()->processFormatCode(rtrim($productionCode)));
        $method->setVisibility(PhpGeneratorHelpers::VISIBILITY_PROTECTED);
        PhpGeneratorHelpers::processReducedReturnCode($this, $method, $types);
        $returnTypes = PhpGeneratorHelpers::processPickReturnTypes($types);
        if ($types !== []) {
            $method->addParameter('arg')->setType('array');
        }
        PhpGeneratorHelpers::processMethodArguments($this, $method, $types);
        PhpGeneratorHelpers::processMethodReturnType($this, $method, $returnTypes);
    }

    /**
     * Protected properrty processor
     *
     * @param ClassType $class
     * @param string $name
     * @param mixed $value
     * @param string|null $type
     * @param string $phpDoc
     * @return Property
     */
    protected function processProtectedProperty(
        ClassType $class,
        string $name,
        mixed $value,
        ?string $type,
        string $phpDoc
    ): Property {
        $property = $class->addProperty($name, $value)
            ->setVisibility(PhpGeneratorHelpers::VISIBILITY_PROTECTED)
            ->setType($type)
            ->setComment($phpDoc);
        return $property;
    }

    /**
     * Tables processor
     *
     * @param ClassType $class
     * @return void
     */
    protected function processTables(ClassType $class): void
    {
        $processor = $this->getProcessor();
        $table = $processor->getTable();
        $tablePitch = $processor->getTablePitch();
        ksort($table);
        $this->processProtectedProperty($class, 'table', $table, PhpGeneratorHelpers::TYPE_ARRAY, '@var int[]');
        $this->processProtectedProperty($class, 'tablePitch', $tablePitch, 'int', '@var int');
    }

    /**
     * Terminals processor
     *
     * @param ClassType $class
     * @return void
     */
    protected function processTerminals(ClassType $class): void
    {
        $terminals = $this->getTerminals();
        $terminalsPrefix = $this->getOption(OptionsCollection::OPTION_TERMINALS_PREFIX, 'self::');

        $terminalsTypes = [];
        $terminalsValues = [];
        foreach ($terminals as $terminal) {
            $terminalIndex = $terminal->getIndex();
            $terminalType = $terminal->getType();
            $terminalValue = $terminal->getValue();

            if ($terminalType !== null) {
                $terminalsTypes[] = $this->createLiteral($terminalsPrefix . $terminalType . ' => ' . $terminalIndex);
            } elseif ($terminalValue !== null) {
                $terminalsValues[$terminalValue] = $terminalIndex;
            }
        }

        $arrayType = PhpGeneratorHelpers::TYPE_ARRAY;
        $this->processProtectedProperty($class, 'terminalsTypes', $terminalsTypes, $arrayType, '@var int[]');
        $this->processProtectedProperty($class, 'terminalsValues', $terminalsValues, $arrayType, '@var int[]');
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Literal factory
     *
     * @param string $value
     * @return Literal
     */
    protected function createLiteral(string $value): Literal
    {
        $phpLiteral = new Literal($value);
        return $phpLiteral;
    }

    /**
     * Processor factroy
     *
     * @return Processor
     */
    protected function createProcessor(): Processor
    {
        $grammar = $this->getGrammar();
        $processor = new Processor($grammar);
        return $processor;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * @inheritdoc
     */
    protected function getOption(string $name, string $default = null): string
    {
        if ($name === OptionsCollection::OPTION_EXTENDS) {
            $default = BaseParser::class;
        }
        $option = parent::getOption($name, $default);
        return $option;
    }

    /**
     * Processor getter
     *
     * @return Processor
     */
    protected function getProcessor(): Processor
    {
        if (isset($this->processor) === false) {
            $processor = $this->createProcessor();
            $this->setProcessor($processor);
        }
        return $this->processor;
    }

    /**
     * Processor setter
     *
     * @param Processor $processor
     * @return static Provides fluent interface
     */
    protected function setProcessor(Processor $processor): static
    {
        $this->processor = $processor;
        return $this;
    }

    // </editor-fold>
}
