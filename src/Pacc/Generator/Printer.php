<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator;

use Interitty\Utils\Arrays;
use Interitty\Utils\Strings;
use Nette\InvalidStateException;
use Nette\PhpGenerator\ClassLike;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\EnumType;
use Nette\PhpGenerator\InterfaceType;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\Printer as NettePrinter;
use Nette\PhpGenerator\TraitType;

use function array_key_exists;
use function rtrim;
use function str_repeat;
use function substr_replace;

use const PHP_EOL;

class Printer extends NettePrinter
{
    /** @var string */
    protected string $eol = PHP_EOL;

    /** @var string[] */
    protected array $footers = [];

    /** @var string[] */
    protected array $headers = [];

    /** @var string */
    public string $indentation = '    ';

    /** @var string[] */
    protected array $inners = [];

    /** @var int */
    public int $linesBetweenMethods = 1;

    /**
     * @inheritdoc
     */
    public function printClass(
        ClassType|InterfaceType|TraitType|EnumType $class,
        ?PhpNamespace $namespace = null
    ): string {
        $eol = $this->getEol();
        $classCode = parent::printClass($class, $namespace);
        $code = $this->getHeader($class)
            . substr_replace($classCode, $this->getInner($class), (Strings::indexOf($classCode, '{') ?? 0) + 1, 0)
            . $this->getFooter($class);
        return Strings::normalize($code) . $eol;
    }

    /**
     * @inheritdoc
     */
    public function printNamespace(PhpNamespace $namespace): string
    {
        $eol = $this->getEol();
        $original = parent::printNamespace($namespace);
        $fixed = Strings::replace($original, '~(use(?! function)(?:.*))\n(use function)~', '$1' . $eol . $eol . '$2');
        $output = Strings::replace($fixed, '~(use(?! const)(?:.*))\n(use const)~', '$1' . $eol . $eol . '$2');
        return $output;
    }

    /**
     * Format code processor
     *
     * @param string $code
     * @param int $level [OPTIONAL]
     * @return string
     */
    public function processFormatCode(string $code, int $level = 0): string
    {
        $eol = $this->getEol();
        $oneIndentation = $this->getIndentation();
        $formatCode = '';
        /** @phpstan-var string[] $lines */
        $lines = Strings::split($code, "/\r\n|\n|\r/");
        foreach ($lines as $line) {
            $formatCode .= $this->processFormatCodeLine(Strings::trim($line), $oneIndentation, $eol, $level);
        }

        return $formatCode;
    }

    /**
     * Format code line processor
     *
     * @param string $line
     * @param string $oneIndentation
     * @param string $eol
     * @param int $level
     * @return string
     */
    protected function processFormatCodeLine(string $line, string $oneIndentation, string $eol, int &$level): string
    {
        $isCodeBlockOpen = static function (string $line, string $lastCharacter, string $start, string $end): bool {
            return (Strings::indexOf($line, $start) === null) && ($lastCharacter === $end);
        };

        $lastCharacter = Strings::substring($line, -1);
        if (
            ($isCodeBlockOpen($line, $lastCharacter, '{', '}') === true) ||
            ($isCodeBlockOpen($line, $lastCharacter, '(', ')') === true) ||
            ($isCodeBlockOpen($line, $lastCharacter, '[', ']') === true) ||
            ((Strings::indexOf($line, '} else') !== null) || (Strings::indexOf($line, '} catch') !== null)) ||
            (Arrays::contains([') {', '];', ');'], $line) === true)
        ) {
            $level = $level - 1;
        }
        $indentation = str_repeat($oneIndentation, $level);
        $lineCode = str_replace('$$', '$reduced', Strings::replace($line, '~\$(\d+)~', '$arg[$1]'));
        $formatCode = $indentation . $lineCode . $eol;
        if (Arrays::contains(['{', '(', '['], $lastCharacter) === true) {
            $level = $level + 1;
        }
        return $formatCode;
    }

    /**
     * Header processor
     *
     * @param PhpNamespace $namespace
     * @param string $header
     * @return string
     * @throws InvalidStateException
     */
    protected function processHeader(PhpNamespace $namespace, string $header): string
    {
        $eol = $this->getEol();
        $headerCode = '';
        $pattern = '~use (?:(?:(?<type>const|function) )?(?<use>(?:[^; ])+))(?: as (?P<as>(?:[^;])+))?;~';
        /** @phpstan-var string[] $lines */
        $lines = Strings::split($header, "/\r\n|\n|\r/");
        foreach ($lines as $line) {
            $match = Strings::match($line, $pattern);
            if ($match === null) {
                $headerCode .= $line . $eol;
            } else {
                $type = match ($match['type']) {
                    'const' => $namespace::NameConstant,
                    'function' => $namespace::NameFunction,
                    default => $namespace::NameNormal,
                };
                $namespace->addUse($match['use'], isset($match['as']) ? $match['as'] : null, $type);
            }
        }
        return rtrim($headerCode);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Eol getter
     *
     * @return string
     */
    public function getEol(): string
    {
        return $this->eol;
    }

    /**
     * Eol setter
     *
     * @param string $eol
     * @return static Provides fluent interface
     */
    public function setEol(string $eol): static
    {
        $this->eol = $eol;
        return $this;
    }

    /**
     * Footer getter
     *
     * @param ClassLike $class
     * @return string
     */
    public function getFooter(ClassLike $class): string
    {
        $className = $class->getName();
        $footer = '';
        if ($this->hasFooter($class) === true) {
            $footer = $this->footers[$className];
            if ($footer !== '') {
                $eol = $this->getEol();
                $footer = $eol . $footer;
            }
        }
        return $footer;
    }

    /**
     * Footer checker
     *
     * @param ClassLike $class
     * @return bool
     */
    public function hasFooter(ClassLike $class): bool
    {
        $className = (string) $class->getName();
        $hasFooter = array_key_exists($className, $this->footers);
        return $hasFooter;
    }

    /**
     * Footer setter
     *
     * @param ClassLike $class
     * @param string $footer
     * @return static Provides fluent interface
     */
    public function setFooter(ClassLike $class, string $footer): static
    {
        $className = (string) $class->getName();
        $this->footers[$className] = $footer;
        return $this;
    }

    /**
     * Header getter
     *
     * @param ClassLike $class
     * @return string
     */
    public function getHeader(ClassLike $class): string
    {
        $className = (string) $class->getName();
        $header = '';
        if ($this->hasHeader($class) === true) {
            $header = $this->headers[$className];
            if ($header !== '') {
                $eol = $this->getEol();
                $header = $eol . $header . $eol;
            }
        }
        return $header;
    }

    /**
     * Header checker
     *
     * @param ClassLike $class
     * @return bool
     */
    public function hasHeader(ClassLike $class): bool
    {
        $className = (string) $class->getName();
        $hasHeader = array_key_exists($className, $this->headers);
        return $hasHeader;
    }

    /**
     * Header setter
     *
     * @param PhpNamespace $namespace
     * @param ClassLike $class
     * @param string $header
     * @return static Provides fluent interface
     * @throws InvalidStateException
     */
    public function setHeader(PhpNamespace $namespace, ClassLike $class, string $header): static
    {
        $className = (string) $class->getName();
        $this->headers[$className] = $this->processHeader($namespace, $header);
        return $this;
    }

    /**
     * Indentation getter
     *
     * @return string
     */
    public function getIndentation(): string
    {
        return $this->indentation;
    }

    /**
     * Indentation setter
     *
     * @param string $indentation
     * @return static Provides fluent interface
     */
    public function setIndentation(string $indentation): static
    {
        $this->indentation = $indentation;
        return $this;
    }

    /**
     * Inner getter
     *
     * @param ClassLike $class
     * @return string
     */
    public function getInner(ClassLike $class): string
    {
        $className = (string) $class->getName();
        $inner = '';
        if ($this->hasInner($class) === true) {
            $inner = $this->inners[$className];
            if ($inner !== '') {
                $eol = $this->getEol();
                $inner = $eol . $inner . $eol;
            }
        }
        return $inner;
    }

    /**
     * Inner checker
     *
     * @param ClassLike $class
     * @return bool
     */
    public function hasInner(ClassLike $class): bool
    {
        $className = (string) $class->getName();
        $hasInner = array_key_exists($className, $this->inners);
        return $hasInner;
    }

    /**
     * Inner setter
     *
     * @param ClassLike $class
     * @param string $inner
     * @return static Provides fluent interface
     */
    public function setInner(ClassLike $class, string $inner): static
    {
        $className = (string) $class->getName();
        $this->inners[$className] = $inner;
        return $this;
    }

    // </editor-fold>
}
