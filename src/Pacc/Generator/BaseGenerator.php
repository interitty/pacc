<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator;

use Interitty\Pacc\Grammar;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Nette\InvalidStateException;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Helpers;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;

use function array_keys;
use function in_array;
use function ltrim;

use const PHP_EOL;

abstract class BaseGenerator implements GeneratorInterface
{
    /** @var string Generated string */
    protected string $generated;

    /** @var Grammar */
    protected Grammar $grammar;

    /** @var string[] */
    protected static array $internalMethods = [
        OptionsCollection::GRAMMAR_OPTION_FOOTER,
        OptionsCollection::GRAMMAR_OPTION_HEADER,
        OptionsCollection::GRAMMAR_OPTION_INNER,
    ];

    /** @var PhpNamespace */
    protected PhpNamespace $namespace;

    /** @var PhpFile */
    protected PhpFile $phpFile;

    /** @var Printer */
    protected Printer $printer;

    /**
     * @inheritdoc
     */
    public function __construct(Grammar $grammar)
    {
        $this->setGrammar($grammar);
    }

    /**
     * Generated processor
     *
     * @return string
     * @throws InvalidStateException
     */
    protected function processGenerated(): string
    {
        $extends = $this->getOption(OptionsCollection::OPTION_EXTENDS, '');
        $implements = $this->getOption(OptionsCollection::OPTION_IMPLEMENTS, '');
        $phpFile = $this->getPhpFile();
        $printer = $this->getPrinter();
        $grammarName = $this->getGrammarName();
        $namespace = $phpFile->addNamespace(Helpers::extractNamespace($grammarName));
        $this->setNamespace($namespace);
        $class = $phpFile->addClass($grammarName);
        PhpGeneratorHelpers::processClassExtends($this, $class, $extends);
        PhpGeneratorHelpers::processClassImplements($this, $class, $implements);
        $header = $this->getGrammarOption(OptionsCollection::GRAMMAR_OPTION_HEADER, '');
        $printer->setHeader($namespace, $class, $header);
        $this->processGrammarOptionMethods($class);
        $this->processInner($class);
        $printer->setFooter($class, $this->getGrammarOption(OptionsCollection::GRAMMAR_OPTION_FOOTER, ''));

        $generated = $printer->printFile($phpFile);
        return $generated;
    }

    /**
     * Grammar option method processor
     *
     * @param ClassType $class
     * @param string $name
     * @return Method
     * @throws InvalidStateException
     */
    protected function processGrammarOptionMethod(ClassType $class, string $name): Method
    {
        $printer = $this->getPrinter();
        $options = $this->getGrammarOptionsCollection();
        $types = $options->getGrammarOptionTypes($name);
        $method = $class->addMethod($name);
        $method->setComment(Helpers::unformatDocComment((string) $options->getGrammarOptionPhpDoc($name)));
        $method->setBody($printer->processFormatCode($this->getGrammarOption($name), 0));
        $method->setVisibility(PhpGeneratorHelpers::VISIBILITY_PROTECTED);
        PhpGeneratorHelpers::processReducedReturnCode($this, $method, $types);
        $returnTypes = PhpGeneratorHelpers::processPickReturnTypes($types);
        PhpGeneratorHelpers::processMethodArguments($this, $method, $types);
        PhpGeneratorHelpers::processMethodReturnType($this, $method, $returnTypes);
        return $method;
    }

    /**
     * Grammar option methods processor
     *
     * @param ClassType $class
     * @return void
     * @throws InvalidStateException
     */
    protected function processGrammarOptionMethods(ClassType $class): void
    {
        foreach (array_keys($this->getGrammarOptionsCollection()->getGrammarOptions()) as $name) {
            if (in_array($name, self::$internalMethods, true) === false) {
                $this->processGrammarOptionMethod($class, $name);
            }
        }
    }

    /**
     * Inner processor
     *
     * @param ClassType $class
     * @return void
     */
    protected function processInner(ClassType $class): void
    {
        $inner = $this->getGrammarOption(OptionsCollection::GRAMMAR_OPTION_INNER, '');
        $this->getPrinter()->setInner($class, $inner);
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * PhpFile factory
     *
     * @return PhpFile
     */
    protected function createPhpFile(): PhpFile
    {
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes((bool) $this->getOption(OptionsCollection::OPTION_STRICT_TYPES, '0'));
        return $phpFile;
    }

    /**
     * Printer factory
     *
     * @return Printer
     */
    protected function createPrinter(): Printer
    {
        $printer = new Printer();
        $printer->setEol($this->getOption(OptionsCollection::OPTION_EOL, PHP_EOL));
        $printer->setIndentation($this->getOption(OptionsCollection::OPTION_INDENTATION, '    '));
        return $printer;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * @inheritdoc
     */
    public function getGenerated(): string
    {
        if (isset($this->generated) === false) {
            $generated = $this->processGenerated();
            $this->generated = $generated;
        }

        return $this->generated;
    }

    /**
     * Grammar getter
     *
     * @return Grammar
     */
    protected function getGrammar(): Grammar
    {
        return $this->grammar;
    }

    /**
     * Grammar setter
     *
     * @param Grammar $grammar
     * @return static Provides fluent interface
     */
    protected function setGrammar(Grammar $grammar): static
    {
        $this->grammar = $grammar;
        return $this;
    }

    /**
     * Grammar name getter
     *
     * @return string
     */
    protected function getGrammarName(): string
    {
        $grammarName = ltrim($this->getGrammar()->getName(), '\\');
        return $grammarName;
    }

    /**
     * GrammarOption getter
     *
     * @param string $name
     * @param string $default [OPTIONAL]
     * @return string
     */
    protected function getGrammarOption(string $name, string $default = null): string
    {
        $option = $this->getGrammarOptionsCollection()->getGrammarOption($name, $default);
        return $option;
    }

    /**
     * GrammarOption checker
     *
     * @param string $name
     * @return bool
     */
    protected function hasGrammarOption(string $name): bool
    {
        $hasOption = $this->getGrammarOptionsCollection()->hasGrammarOption($name);
        return $hasOption;
    }

    /**
     * Namespace getter
     *
     * @return PhpNamespace
     */
    public function getNamespace(): PhpNamespace
    {
        return $this->namespace;
    }

    /**
     * Namespace setter
     *
     * @param PhpNamespace $namespace
     * @return static Provides fluent interface
     */
    protected function setNamespace(PhpNamespace $namespace): static
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * Option getter
     *
     * @param string $name
     * @param string|null $default [OPTIONAL]
     * @return string
     */
    protected function getOption(string $name, string $default = null): string
    {
        $option = $this->getGrammarOptionsCollection()->getOption($name, $default);
        return $option;
    }

    /**
     * Option checker
     *
     * @param string $name
     * @return bool
     */
    protected function hasOption(string $name): bool
    {
        $hasOption = $this->getGrammarOptionsCollection()->hasOption($name);
        return $hasOption;
    }

    /**
     * Grammar OptionsCollection getter
     *
     * @return OptionsCollection
     */
    protected function getGrammarOptionsCollection(): OptionsCollection
    {
        $optionsCollection = $this->getGrammar()->getOptions();
        return $optionsCollection;
    }

    /**
     * PhpFile getter
     *
     * @return PhpFile
     */
    protected function getPhpFile(): PhpFile
    {
        if (isset($this->phpFile) === false) {
            $phpFile = $this->createPhpFile();
            $this->setPhpFile($phpFile);
        }
        return $this->phpFile;
    }

    /**
     * PhpFile setter
     *
     * @param PhpFile $phpFile
     * @return static Provides fluent interface
     */
    protected function setPhpFile(PhpFile $phpFile): static
    {
        $this->phpFile = $phpFile;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPrinter(): Printer
    {
        if (isset($this->printer) === false) {
            $printer = $this->createPrinter();
            $this->setPrinter($printer);
        }
        return $this->printer;
    }

    /**
     * Printer setter
     *
     * @param Printer $printer
     * @return static Provides fluent interface
     */
    protected function setPrinter(Printer $printer): static
    {
        $this->printer = $printer;
        return $this;
    }

    /**
     * Productions getter
     *
     * @phpstan-return SymbolCollection<Production>
     */
    protected function getProductions(): SymbolCollection
    {
        $productions = $this->getGrammar()->getProductions();
        return $productions;
    }

    /**
     * Terminals getter
     *
     * @phpstan-return SymbolCollection<Terminal>
     */
    protected function getTerminals(): SymbolCollection
    {
        $terminals = $this->getGrammar()->getTerminals();
        return $terminals;
    }

    // </editor-fold>
}
