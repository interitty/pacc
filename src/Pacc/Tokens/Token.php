<?php

declare(strict_types=1);

namespace Interitty\Pacc\Tokens;

use Interitty\Tokenizer\Token as BaseToken;
use Interitty\Utils\Strings;

use function stripcslashes;

class Token extends BaseToken
{
    /** All available token type constants */
    public const string TOKEN_BAD = 'bad';
    public const string TOKEN_CODE = 'code';
    public const string TOKEN_COMMENT = 'comment';
    public const string TOKEN_ID = 'id';
    public const string TOKEN_PHPDOC = 'phpdoc';
    public const string TOKEN_SPECIAL = 'special';
    public const string TOKEN_STRING = 'string';
    public const string TOKEN_WHITESPACE = 'whitespace';

    /**
     * @inheritdoc
     */
    protected function processValue(): string
    {
        $value = $this->getLexeme();
        if ($this->getType() === self::TOKEN_STRING) {
            $lastIndex = Strings::length($value) - 1;
            while (($value[0] === '"' || $value[0] === '\'') && ($value[0] === $value[$lastIndex])) {
                $value = Strings::substring($value, 1, Strings::length($value) - 2);
                $lastIndex = Strings::length($value) - 1;
            }
            $value = stripcslashes($value);
        }
        return $value;
    }
}
