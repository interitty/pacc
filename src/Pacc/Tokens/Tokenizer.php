<?php

declare(strict_types=1);

namespace Interitty\Pacc\Tokens;

use Interitty\Pacc\Exceptions\ParseException;
use Interitty\Tokenizer\Token as BaseToken;
use Interitty\Tokenizer\Tokenizer as BaseTokenizer;
use Interitty\Utils\Strings;

use function strpos;
use function substr_count;

class Tokenizer extends BaseTokenizer
{
    /** @var string[] Mapping from token regexes to token classes */
    public array $map = [
        Token::TOKEN_WHITESPACE => '~^(\s+)~Ss',
        Token::TOKEN_ID => '~^([a-zA-Z][a-zA-Z0-9_]*)~S',
        Token::TOKEN_STRING => '~^(\'(?:\\\'|[^\'])*\'|"(?:\\"|[^"])*"|`(?:\\`|[^`])*`)~SU',
        Token::TOKEN_SPECIAL => '~^(@|\\\\|\\.|=|\(|\)|:|\||\{|\}|;|\?)~S',
        Token::TOKEN_PHPDOC => '~^(/\*\*.*\*/)~SUs',
        Token::TOKEN_COMMENT => '~^(/\*.*\*/)~SUs',
        Token::TOKEN_BAD => '~^(.)~Ss',
    ];

    /** @var string[] */
    protected array $skippedTokenTypes = [
        Token::TOKEN_COMMENT,
        Token::TOKEN_WHITESPACE,
    ];

    /**
     * @inheritdoc
     */
    protected function processTokenLexeme(BaseToken $token): string
    {
        $lexeme = parent::processTokenLexeme($token);
        if (($token->getType() === Token::TOKEN_SPECIAL) && ($lexeme === '{')) {
            $lexeme .= $this->processBlockCodeLexeme();
        }
        return $lexeme;
    }

    /**
     * Block of code lexeme processor
     *
     * @return string
     */
    protected function processBlockCodeLexeme(): string
    {
        $string = $this->getString();
        $offset = 0;
        do {
            $rbrace = strpos($string, '}', $offset);
            if ($rbrace === false) {
                throw new ParseException('Unable to find terminal curly bracket');
            }
            $offset = $rbrace + 1;
            $code = Strings::substring($string, 0, $offset);
            $regex = '~
                 "((?<!\\\\)\\\\"|[^"])*$|
                 "((?<!\\\\)\\\\"|[^"])*"|
                \'((?<!\\\\)\\\\\'|[^\'])*\'|
                \'((?<!\\\\)\\\\\'|[^\'])*$
                ~x';
            $test = Strings::replace($code, $regex, '');
        } while (substr_count($test, '{') !== substr_count($test, '}'));

        $code = Strings::substring($code, 1, Strings::length($code) - 2);
        $this->addToken($this->createToken(Token::TOKEN_CODE, $code, 1));
        return $code;
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * @inheritdoc
     */
    protected function createToken(string $type, string $lexeme = '', int $increasePosition = 0): BaseToken
    {
        $line = $this->getLine();
        $position = $this->getPosition() + $increasePosition;
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    // </editor-fold>
}
