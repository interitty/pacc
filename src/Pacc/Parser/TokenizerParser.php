<?php

declare(strict_types=1);

namespace Interitty\Pacc\Parser;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\Pacc\Tokens\Token;
use Interitty\Utils\Arrays;
use Interitty\Utils\Strings;

use function array_key_exists;
use function ltrim;
use function ord;
use function preg_quote;

/**
 * @phpstan-type GrammarOptionSubType array<'type', string>
 * @phpstan-type GrammarOptionType array<'doc'|'type', string[]>
 * @phpstan-type GrammarOptionTypes array<'return'|int, GrammarOptionType>
 */
class TokenizerParser extends BaseGrammarParser
{
    /** All available regular expression pattern constants */
    public const string TYPES_PATTERN = '~\$(?><(?P<type>(?>(?>[^<>]+)?(?><(?>[^<>]|(?1))*>)?)+)?>)?(?P<name>\$|\d+)~';
    public const string SUBTYPES_PATTERN = '~(?J)(?>(?<type>(?>[^<>|]+)?(?><(?>[^<>]|(?1))*>)+)|(?<type>[^<>|]+))~';

    /**
     * @inheritdoc
     */
    protected function processParse(): mixed
    {
        $grammar = $this->getGrammar();
        while (true) {
            $phpDoc = $this->processPhpDoc();
            if ($this->check(Token::TOKEN_ID, 'grammar')) {
                $this->next();
                $grammar->setName($this->separatedName('\\'));
            } elseif ($this->check(Token::TOKEN_ID, 'option')) {
                $this->processOptions();
            } elseif ($this->check(Token::TOKEN_SPECIAL, '@')) {
                $this->processGrammarOptions($phpDoc);
            } elseif ($this->check(Token::TOKEN_SPECIAL, ';')) {
                $this->next();
            } else {
                break;
            }
        }
        $this->processRules();
        return null;
    }

    /**
     * Code processor
     *
     * @return string
     */
    protected function processCode(): string
    {
        $this->expect(Token::TOKEN_SPECIAL, '{');
        $this->next();

        $this->expect(Token::TOKEN_CODE);
        $code = Strings::trim($this->currentTokenLexeme(), "\n\r");
        $this->next();

        $this->expect(Token::TOKEN_SPECIAL, '}');
        $this->next();

        return $code;
    }

    /**
     * Php doc processor
     *
     * @return string|null
     */
    protected function processPhpDoc(): ?string
    {
        $phpDoc = null;
        if ($this->check(Token::TOKEN_PHPDOC)) {
            $phpDoc = $this->currentTokenLexeme();
            $this->next();
        }
        return $phpDoc;
    }

    /**
     * Grammar options processor
     *
     * @param string|null $phpDoc
     * @return void
     */
    protected function processGrammarOptions(?string $phpDoc): void
    {
        $this->next();
        $name = $this->separatedName('.');
        $code = $this->processCode();
        $this->getGrammar()->getOptions()->setGrammarOption($name, $code, $phpDoc);
    }

    /**
     * Options processor
     *
     * @return void
     */
    protected function processOptions(): void
    {
        $this->next();
        do {
            if ($this->check(Token::TOKEN_SPECIAL, '(')) {
                $this->next();
            }

            $this->processSingleOption();

            if ($this->check(Token::TOKEN_SPECIAL, ';')) {
                $this->next();
            }
            if ($this->check(Token::TOKEN_SPECIAL, ')')) {
                $this->next();
                break;
            }
        } while ($this->check(Token::TOKEN_END) !== true);
    }

    /**
     * Types parse processor
     *
     * @param string $code
     * @phpstan-return GrammarOptionTypes
     */
    public static function processParseTypes(string &$code): array
    {
        $matches = Strings::matchAll($code, self::TYPES_PATTERN);
        $types = [];
        /** @var array{name: string, type?: string} $match */
        foreach ($matches as $match) {
            $name = $match['name'] === '$' ? 'return' : (int) $match['name'];
            if (array_key_exists($name, $types) === false) {
                $types[$name] = ['doc' => [], 'type' => []];
            }
            if (isset($match['type']) === true) {
                $code = Strings::replace($code, '~<' . preg_quote($match['type']) . '>~');
                $doc = (string) Strings::after($match['type'], '#');
                $type = ($doc === '') ? $match['type'] : (string) Strings::before($match['type'], '#');
                $types[$name] = self::processReduceTypes($types[$name], $type, $doc);
            }
        }
        return $types;
    }

    /**
     * Types reduce processor
     *
     * @phpstan-param GrammarOptionType $types
     * @param string $type
     * @param string $doc
     * @phpstan-return GrammarOptionType
     */
    protected static function processReduceTypes(array $types, string $type, string $doc): array
    {
        foreach (Strings::matchAll($type, self::SUBTYPES_PATTERN) as $subType) {
            /** @phpstan-var GrammarOptionSubType $subType */
            $returnType = ltrim($subType['type'], '?');
            if (($returnType !== $subType['type']) && (Arrays::contains($types['type'], 'null') === false)) {
                $types['type'][] = 'null';
            }
            if (($returnType !== '') && (Arrays::contains($types['type'], $returnType) === false)) {
                $types['type'][] = $returnType;
            }
        }
        foreach (Strings::matchAll($doc, self::SUBTYPES_PATTERN) as $subDoc) {
            if (($subDoc['type'] !== '') && (Arrays::contains($types['doc'], $subDoc['type']) === false)) {
                $types['doc'][] = $subDoc['type'];
            }
        }
        /** @phpstan-var GrammarOptionType $types */
        return $types;
    }

    /**
     * Rule processor
     *
     * @param NonTerminal $name
     * @return void
     */
    protected function processRule(NonTerminal $name): void
    {
        $grammar = $this->getGrammar();
        do {
            $terms = $this->processTerms();
            $code = null;
            if ($this->check(Token::TOKEN_SPECIAL, '{')) {
                $code = $this->processCode();
            }
            $grammar->createProduction($name, $terms, $code);
        } while ($this->check(Token::TOKEN_SPECIAL, '|'));
    }

    /**
     * Rules processor
     *
     * @return void
     */
    protected function processRules(): void
    {
        $grammar = $this->getGrammar();

        while ($this->check(Token::TOKEN_END) !== true) {
            $this->expect(Token::TOKEN_ID);

            $name = $grammar->createNonTerminal($this->currentTokenLexeme());
            if ($grammar->hasStart() === false) {
                $grammar->setStart($name);
            }

            $this->next();
            $this->expect(Token::TOKEN_SPECIAL, ':');
            $this->processRule($name);

            $this->expect(Token::TOKEN_SPECIAL, ';');
            $this->next();
        }
    }

    /**
     * Single option processor
     *
     * @return void
     */
    protected function processSingleOption(): void
    {
        $name = $this->separatedName('.');
        $this->expect(Token::TOKEN_SPECIAL, '=');

        $this->next();
        $optionValue = $this->currentTokenLexeme();
        if ($this->check(Token::TOKEN_STRING)) {
            $this->next();
        } else {
            $this->expect(Token::TOKEN_SPECIAL, '{');
            $optionValue = $this->processCode();
        }

        $this->getGrammar()->getOptions()->setOption($name, $optionValue);
    }

    /**
     * Terms processor
     *
     * @phpstan-return array<Terminal|NonTerminal>
     */
    protected function processTerms(): array
    {
        $terms = [];
        $grammar = $this->getGrammar();
        do {
            $this->next();
            $tokenValue = $this->currentTokenLexeme();
            if (($this->check(Token::TOKEN_ID)) && (ord($tokenValue[0]) >= 65 && ord($tokenValue[0]) <= 90)) {
                // A-Z
                $terms[] = $grammar->createTerminal($tokenValue, $tokenValue, null);
            } elseif ($this->check(Token::TOKEN_ID)) {
                $terms[] = $grammar->createNonTerminal($tokenValue);
            } elseif ($this->check(Token::TOKEN_STRING)) {
                $terms[] = $grammar->createTerminal($tokenValue, null, $tokenValue);
            }
        } while (($this->check(Token::TOKEN_ID)) || ($this->check(Token::TOKEN_STRING)));
        return $terms;
    }
}
