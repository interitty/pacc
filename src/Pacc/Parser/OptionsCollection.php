<?php

declare(strict_types=1);

namespace Interitty\Pacc\Parser;

use Interitty\Exceptions\Exceptions;
use LogicException;

use function array_key_exists;
use function is_array;
use function is_string;

/**
 * @phpstan-type GrammarOption array{
 *      code: string,
 *      phpDoc: string|null,
 *      types: GrammarOptionTypes,
 *  }
 * @phpstan-type GrammarOptionType array<'doc'|'type', string[]>
 * @phpstan-type GrammarOptionTypes array<'return'|int, GrammarOptionType>
 */
class OptionsCollection
{
    /** All available grammar option constants */
    public const string GRAMMAR_OPTION_FOOTER = 'footer';
    public const string GRAMMAR_OPTION_HEADER = 'header';
    public const string GRAMMAR_OPTION_INNER = 'inner';

    /** All available option constants */
    public const string OPTION_ALGORITHM = 'algorithm';
    public const string OPTION_EOL = 'eol';
    public const string OPTION_EXTENDS = 'extends';
    public const string OPTION_IMPLEMENTS = 'implements';
    public const string OPTION_INDENTATION = 'indentation';
    public const string OPTION_PARSE = 'parse';
    public const string OPTION_STRICT_TYPES = 'strictTypes';
    public const string OPTION_TERMINALS_PREFIX = 'terminalsPrefix';

    /** @phpstan-var GrammarOption[] */
    protected array $grammarOptions = [];

    /** @var string[] */
    protected $options = [];

    /**
     * Constructor
     *
     * @param string[] $options [OPTIONAL]
     * @phpstan-param GrammarOption[] $grammarOptions [OPTIONAL]
     * @return void
     */
    public function __construct(array $options = [], array $grammarOptions = [])
    {
        $this->setOptions($options);
        $this->setGrammarOptions($grammarOptions);
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * GrammarOption getter
     *
     * @param string $name One of OptionsCollection::GRAMMAR_OPTION_* constants
     * @param string $default [OPTIONAL]
     * @return string
     */
    public function getGrammarOption(string $name, string $default = null): string
    {
        if ($this->hasGrammarOption($name) === true) {
            $grammarOption = $this->grammarOptions[$name]['code'];
        } elseif (is_string($default) === true) {
            $grammarOption = $default;
        } else {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('GrammarOption ":option" was not set')
                    ->addData('option', $name);
        }
        return $grammarOption;
    }

    /**
     * GrammarOption php doc getter
     *
     * @param string $name One of OptionsCollection::GRAMMAR_OPTION_* constants
     * @param string $default [OPTIONAL]
     * @return string|null
     */
    public function getGrammarOptionPhpDoc(string $name, string $default = null): ?string
    {
        if ($this->hasGrammarOption($name) === true) {
            $phpDoc = $this->grammarOptions[$name]['phpDoc'];
        } elseif (is_string($default) === true) {
            $phpDoc = $default;
        } else {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('GrammarOption ":option" was not set')
                    ->addData('option', $name);
        }
        return $phpDoc;
    }

    /**
     * GrammarOption types getter
     *
     * @param string $name One of OptionsCollection::GRAMMAR_OPTION_* constants
     * @phpstan-param GrammarOptionTypes $default [OPTIONAL]
     * @phpstan-return GrammarOptionTypes
     */
    public function getGrammarOptionTypes(string $name, array $default = null): array
    {
        if ($this->hasGrammarOption($name) === true) {
            $types = $this->grammarOptions[$name]['types'];
        } elseif (is_array($default) === true) {
            $types = $default;
        } else {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('GrammarOption ":option" was not set')
                    ->addData('option', $name);
        }
        return $types;
    }

    /**
     * GrammarOption checker
     *
     * @param string $name One of OptionsCollection::GRAMMAR_OPTION_* constants
     * @return bool
     */
    public function hasGrammarOption(string $name): bool
    {
        $hasGrammarOption = array_key_exists($name, $this->grammarOptions);
        return $hasGrammarOption;
    }

    /**
     * GrammarOption setter
     *
     * @param string $name One of OptionsCollection::GRAMMAR_OPTION_* constants
     * @param string $code
     * @param string|null $phpDoc [OPTIONAL]
     * @phpstan-param GrammarOptionTypes $types [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function setGrammarOption(string $name, string $code, ?string $phpDoc = null, array $types = null): static
    {
        if ($types === null) {
            $types = TokenizerParser::processParseTypes($code);
        }
        $this->grammarOptions[$name] = [
            'code' => $code,
            'phpDoc' => $phpDoc,
            'types' => $types,
        ];
        return $this;
    }

    /**
     * GrammarOptions getter
     *
     * @phpstan-return GrammarOption[]
     */
    public function getGrammarOptions(): array
    {
        return $this->grammarOptions;
    }

    /**
     * GrammarOptions setter
     *
     * @phpstan-param GrammarOption[] $grammarOptions
     * @return static Provides fluent interface
     */
    public function setGrammarOptions(array $grammarOptions): static
    {
        $this->grammarOptions = [];
        foreach ($grammarOptions as $name => $value) {
            $this->setGrammarOption($name, $value['code'], $value['phpDoc'], $value['types']);
        }
        return $this;
    }

    /**
     * Option getter
     *
     * @param string $name One of OptionsCollection::OPTION_* constants
     * @param string|null $default [OPTIONAL]
     * @return string
     */
    public function getOption(string $name, string $default = null): string
    {
        if ($this->hasOption($name) === true) {
            $option = $this->options[$name];
        } elseif (is_string($default) === true) {
            $option = $default;
        } else {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('Option ":option" was not set')
                    ->addData('option', $name);
        }
        return $option;
    }

    /**
     * Option checker
     *
     * @param string $name One of OptionsCollection::OPTION_* constants
     * @return bool
     */
    public function hasOption(string $name): bool
    {
        $hasOption = array_key_exists($name, $this->options);
        return $hasOption;
    }

    /**
     * Option setter
     *
     * @param string $name One of OptionsCollection::OPTION_* constants
     * @param string $value
     * @return static Provides fluent interface
     */
    public function setOption(string $name, string $value): static
    {
        $this->options[$name] = $value;
        return $this;
    }

    /**
     * Options getter
     *
     * @return string[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Options setter
     *
     * @param string[] $options
     * @return static Provides fluent interface
     */
    public function setOptions(array $options): static
    {
        $this->options = [];
        foreach ($options as $name => $value) {
            $this->setOption($name, $value);
        }
        return $this;
    }

    // </editor-fold>
}
