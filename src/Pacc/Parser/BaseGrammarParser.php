<?php

declare(strict_types=1);

namespace Interitty\Pacc\Parser;

use Interitty\Pacc\Grammar;
use Interitty\Pacc\Tokens\Token;
use Interitty\Pacc\Tokens\Tokenizer;
use Interitty\Tokenizer\BaseTokenizerParser;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use Interitty\Tokenizer\Tokenizer as BaseTokenizer;

abstract class BaseGrammarParser extends BaseTokenizerParser
{
    /** @var Grammar */
    protected Grammar $grammar;

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Grammar factory
     *
     * @return Grammar
     */
    protected function createGrammar(): Grammar
    {
        $grammar = new Grammar();
        return $grammar;
    }

    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return BaseTokenizer
     */
    protected function createTokenizer(string $string): BaseTokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Separated name getter
     *
     * @param string $separator
     * @return string
     */
    protected function separatedName(string $separator): string
    {
        $name = '';
        $previousType = null;

        $currentValue = $this->currentTokenLexeme();

        while (
            (($this->check(Token::TOKEN_SPECIAL, $separator)) || ($this->check(Token::TOKEN_ID))) &&
            (($previousType === null) || ($this->currentTokenType() !== $previousType)) &&
            (($previousType !== null) || ($currentValue !== $separator))
        ) {
            $name .= $currentValue;
            $previousType = $this->currentTokenType();

            $this->next();
            $currentValue = $this->currentTokenLexeme();
        }

        if (($previousType === null) || ($previousType !== Token::TOKEN_ID)) {
            throw new UnexpectedTokenException($this->current());
        }

        return $name;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Grammar getter
     *
     * @return Grammar
     */
    public function getGrammar(): Grammar
    {
        if (isset($this->grammar) === false) {
            $grammar = $this->createGrammar();
            $this->setGrammar($grammar);
        }
        return $this->grammar;
    }

    /**
     * Grammar setter
     *
     * @param Grammar $grammar
     * @return static Provides fluent interface
     */
    protected function setGrammar(Grammar $grammar): static
    {
        $this->grammar = $grammar;
        return $this;
    }

    // </editor-fold>
}
