<?php

namespace Interitty\Pacc\Parser;

interface ParserInterface
{
    /**
     * Parse given input processor
     *
     * @param string $input
     * @return mixed
     */
    public static function parseInput(string $input): mixed;
}
