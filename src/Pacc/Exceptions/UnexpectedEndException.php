<?php

declare(strict_types=1);

namespace Interitty\Pacc\Exceptions;

use Throwable;

class UnexpectedEndException extends PaccException
{
    /**
     * Constructor
     *
     * @param Throwable|null $previous [OPTIONAL]
     * @return void
     */
    public function __construct(?Throwable $previous = null)
    {
        parent::__construct('Unexpected end.', 0, $previous);
    }
}
