<?php

declare(strict_types=1);

namespace Interitty\Pacc\Exceptions;

use Interitty\Exceptions\ExtendedExceptionInterface;
use Interitty\Exceptions\ExtendedExceptionTrait;
use LogicException;

class PaccException extends LogicException implements ExtendedExceptionInterface
{
    use ExtendedExceptionTrait;
}
