<?php

declare(strict_types=1);

namespace Interitty\Pacc\Exceptions;

use Interitty\Pacc\Tokens\Token;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use Throwable;

class BadIdentifierException extends UnexpectedTokenException
{
    /**
     * Constructor
     *
     * @param Token $token
     * @param Throwable|null $previous [OPTIONAL]
     * @return void
     */
    public function __construct(Token $token, ?Throwable $previous = null)
    {
        parent::__construct($token, $previous);
        $this->setMessage('Bad identifier ":identifier" on line :line at position :position');
        $this->setToken($token);
        $this->setData([
            'identifier' => $token->getValue(),
            'line' => $token->getLine(),
            'position' => $token->getPosition(),
        ]);
    }
}
