<?php

declare(strict_types=1);

namespace Interitty\Pacc\Exceptions;

class BadItemException extends PaccException
{
}
