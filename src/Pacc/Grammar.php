<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Interitty\Exceptions\Exceptions;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\Utils\Validators;
use LogicException;
use Symfony\Component\Console\Output\OutputInterface;

use function assert;

/**
 * @phpstan-import-type GrammarOption from OptionsCollection
 * @phpstan-import-type GrammarOptionTypes from OptionsCollection
 */
class Grammar
{
    /** @var Terminal */
    protected Terminal $end;

    /** @var Terminal */
    protected Terminal $epsilon;

    /** @var string */
    protected string $name;

    /** @phpstan-var SymbolCollection<NonTerminal> */
    protected SymbolCollection $nonTerminals;

    /** @var OptionsCollection */
    protected OptionsCollection $options;

    /** @var OutputInterface|null */
    protected ?OutputInterface $output = null;

    /** @phpstan-var SymbolCollection<Production> */
    protected SymbolCollection $productions;

    /** @var NonTerminal */
    protected NonTerminal $start;

    /** @var Production */
    protected Production $startProduction;

    /** @phpstan-var SymbolCollection<Terminal> */
    protected SymbolCollection $terminals;

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Collection factory
     *
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     * @template CollectionType of object
     */
    protected function createCollection(string $type): SymbolCollection
    {
        /** @var SymbolCollection<CollectionType> $collection */
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    public function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        $equalNonTerminal = $this->getNonTerminals()->addOrGetEqual($nonTerminal);
        return $equalNonTerminal;
    }

    /**
     * Options collection factory
     *
     * @param string[] $options [OPTIONAL]
     * @phpstan-param GrammarOption[] $grammarOptions [OPTIONAL]
     * @return OptionsCollection
     */
    protected function createOptionsCollection(array $options = [], array $grammarOptions = []): OptionsCollection
    {
        $optionsCollection = new OptionsCollection($options, $grammarOptions);
        return $optionsCollection;
    }

    /**
     * Production factory
     *
     * @param NonTerminal $left
     * @param Symbol[] $right
     * @param string|null $code
     * @phpstan-param GrammarOptionTypes|null $types [OPTIONAL]
     * @return Production
     */
    public function createProduction(NonTerminal $left, array $right, ?string $code, ?array $types = null): Production
    {
        $production = new Production($left, $right, $code, $types);
        $equalProduction = $this->getProductions()->addOrGetEqual($production);
        return $equalProduction;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    public function createTerminal(string $name, ?string $type = null, ?string $value = null): Terminal
    {
        $terminal = new Terminal($name, $type, $value);
        $equalTerminal = $this->getTerminals()->addOrGetEqual($terminal);
        return $equalTerminal;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * End getter
     *
     * @return Terminal
     */
    public function getEnd(): Terminal
    {
        return $this->end;
    }

    /**
     * End setter
     *
     * @param Terminal $end
     * @return static Provides fluent interface
     */
    public function setEnd(Terminal $end): static
    {
        $this->end = $end;
        return $this;
    }

    /**
     * Epsilon getter
     *
     * @return Terminal
     */
    public function getEpsilon(): Terminal
    {
        return $this->epsilon;
    }

    /**
     * Epsilon setter
     *
     * @param Terminal $epsilon
     * @return static Provides fluent interface
     */
    public function setEpsilon(Terminal $epsilon): static
    {
        $this->epsilon = $epsilon;
        return $this;
    }

    /**
     * Name getter
     *
     * @return string
     */
    public function getName(): string
    {
        if ($this->hasName() !== true) {
            throw Exceptions::extend(LogicException::class)->setMessage('Missing parser name');
        }
        return $this->name;
    }

    /**
     * Name checker
     *
     * @return bool
     */
    public function hasName(): bool
    {
        $hasName = isset($this->name);
        return $hasName;
    }

    /**
     * Name setter
     *
     * @param string $name
     * @return static Provides fluent interface
     */
    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * NonTerminals getter
     *
     * @return SymbolCollection<NonTerminal>
     */
    public function getNonTerminals(): SymbolCollection
    {
        if (isset($this->nonTerminals) === false) {
            $nonTerminals = $this->createCollection(NonTerminal::class);
            $this->setNonTerminals($nonTerminals);
        }
        return $this->nonTerminals;
    }

    /**
     * NonTerminals setter
     *
     * @param SymbolCollection<NonTerminal> $nonTerminals
     * @return static Provides fluent interface
     */
    public function setNonTerminals(SymbolCollection $nonTerminals): static
    {
        assert($nonTerminals->assertCollectionOfType(NonTerminal::class));
        $this->nonTerminals = $nonTerminals;
        return $this;
    }

    /**
     * Options getter
     *
     * @return OptionsCollection
     */
    public function getOptions(): OptionsCollection
    {
        if (isset($this->options) === false) {
            $options = $this->createOptionsCollection();
            $this->setOptions($options);
        }
        return $this->options;
    }

    /**
     * Options setter
     *
     * @param OptionsCollection $options
     * @return static Provides fluent interface
     */
    public function setOptions(OptionsCollection $options): static
    {
        $this->options = $options;
        return $this;
    }

    /**
     * Output getter
     *
     * @return OutputInterface|null
     */
    public function getOutput(): ?OutputInterface
    {
        return $this->output;
    }

    /**
     * Output setter
     *
     * @param OutputInterface $output
     * @return static Provides fluent interface
     */
    public function setOutput(OutputInterface $output): static
    {
        assert(Validators::check($this->output, 'null', 'output before set'));
        $this->output = $output;
        return $this;
    }

    /**
     * Productions getter
     *
     * @return SymbolCollection<Production>
     */
    public function getProductions(): SymbolCollection
    {
        if (isset($this->productions) === false) {
            $productions = $this->createCollection(Production::class);
            $this->setProductions($productions);
        }
        return $this->productions;
    }

    /**
     * Productions setter
     *
     * @param SymbolCollection<Production> $productions
     * @return static Provides fluent interface
     */
    public function setProductions(SymbolCollection $productions): static
    {
        assert($productions->assertCollectionOfType(Production::class));
        $this->productions = $productions;
        return $this;
    }

    /**
     * Start getter
     *
     * @return NonTerminal
     */
    public function getStart(): NonTerminal
    {
        if ($this->hasStart() !== true) {
            throw Exceptions::extend(LogicException::class)->setMessage('Missing start symbol');
        }
        return $this->start;
    }

    /**
     * Start checker
     *
     * @return bool
     */
    public function hasStart(): bool
    {
        $hasStart = isset($this->start);
        return $hasStart;
    }

    /**
     * Start setter
     *
     * @param NonTerminal $start
     * @return static Provides fluent interface
     */
    public function setStart(NonTerminal $start): static
    {
        $this->start = $start;
        return $this;
    }

    /**
     * StartProduction getter
     *
     * @return Production
     */
    public function getStartProduction(): Production
    {
        return $this->startProduction;
    }

    /**
     * StartProduction setter
     *
     * @param Production $startProduction
     * @return static Provides fluent interface
     */
    public function setStartProduction(Production $startProduction): static
    {
        $equalStartProduction = $this->getProductions()->getEqual($startProduction);
        $this->startProduction = $equalStartProduction ?? $startProduction;
        return $this;
    }

    /**
     * Terminals getter
     *
     * @phpstan-return SymbolCollection<Terminal>
     */
    public function getTerminals(): SymbolCollection
    {
        if (isset($this->terminals) === false) {
            $terminals = $this->createCollection(Terminal::class);
            $this->setTerminals($terminals);
        }
        return $this->terminals;
    }

    /**
     * Terminals setter
     *
     * @phpstan-param SymbolCollection<Terminal> $terminals
     * @return static Provides fluent interface
     */
    public function setTerminals(SymbolCollection $terminals): static
    {
        assert($terminals->assertCollectionOfType(Terminal::class));
        $this->terminals = $terminals;
        return $this;
    }

    // </editor-fold>
}
