<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Exception;
use Interitty\Console\Application;
use Interitty\Pacc\Command\PaccCommand;
use LogicException;

use function file_exists;
use function fwrite;
use function version_compare;

use const PHP_BINARY;
use const PHP_EOL;
use const PHP_VERSION;
use const STDERR;

// <editor-fold defaultstate="collapsed" desc="Libs">
if (version_compare('8.3.0', PHP_VERSION, '>')) {
    fwrite(STDERR, 'This version of Pacc is supported on PHP 8.3 or higher' . PHP_EOL);
    fwrite(STDERR, 'You are using PHP ' . PHP_VERSION . ' (' . PHP_BINARY . ').' . PHP_EOL);
    die(1);
}

$composerFile = null;
foreach ([__DIR__ . '/../../../autoload.php', __DIR__ . '/../vendor/autoload.php'] as $file) {
    if (file_exists($file) === true) {
        $composerFile = $file;
        break;
    }
}
unset($file);

if ($composerFile === null) {
    fwrite(STDERR, 'You need to set up the project dependencies using Composer:' . PHP_EOL);
    fwrite(STDERR, PHP_EOL);
    fwrite(STDERR, '    composer install' . PHP_EOL);
    fwrite(STDERR, PHP_EOL);
    fwrite(STDERR, 'You can learn all about Composer on https://getcomposer.org/.' . PHP_EOL);
    die(1);
}

require $composerFile;
// </editor-fold>

try {
    $command = new PaccCommand('pacc');
    $application = new Application();
    $application->add($command);
    $application->setDefaultCommand((string) $command->getName(), true);
    $application->run();
} catch (Exception $exception) {
    throw new LogicException($exception->getMessage(), $exception->getCode(), $exception);
}
