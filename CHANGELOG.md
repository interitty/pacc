# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.9] - 2025-02-25 ##

### Fixed ###

- Add missing PHP 8.3 static return types for mock methods
- Optimization of reduce methods, removal of excess of unused start production

## [1.0.8] - 2025-01-21 ##

### Changed ###

- PHP 8.3 property static types
- Upgrade dependent packages
- Upgrade license to 2025

## [1.0.7] - 2024-09-01 ##

### Changed ###

- Upgrade dependent packages

### Fixed ###

- Phpstan extension.neon of composer/pcre missing by dg/composer-cleaner

## [1.0.6] - 2024-05-19 ##

### Changed ###

- Update SECURITY key
- Upgrade dependent packages

## [1.0.5] - 2023-12-29 ##

### Changed ###

- Increase minimal PHP to 8.3
- Update security contacts
- Upgrade dependent packages
- Upgrade license to 2024

## [1.0.4] - 2023-03-12 ##

### Changed ###

- Update dependencies to newer version

### Fixed ###

- Minor changes due to new version of code checker

## [1.0.3] - 2023-01-05 ##

### Changed ###

- Upgrade dependent packages

### Fixed ###

- Replace deprecated `Nette\PhpGenerator\ClassType::VISIBILITY_PROTECTED`
- Replace deprecated `Nette\PhpGenerator\Nette\PhpGenerator\PhpNamespace::NAME_CONSTANT`
- Replace deprecated `Nette\PhpGenerator\Nette\PhpGenerator\PhpNamespace::NAME_FUNCTION`
- Replace deprecated `Nette\PhpGenerator\PhpLiteral`
- Replace deprecated `Nette\Utils\Validators::isList()`

## [1.0.2] - 2023-01-05 ##

### Added ###

- Examples exception handling and tests
- Support for `implements` in generated output
- Unified `ParserInterface`

### Changed ###

- Increase minimal PHP to 8.2
- Upgrade dependent packages
- Upgrade license to 2023
- Whitespace support to Json example

### Fixed ###

- Missing exception handling in examples
- Remove unnecessary dependency on `Symfony/Console`

## [1.0.1] - 2022-09-15 ##

### Changed ###

- Upgrade dependent packages

## [1.0.0] - 2022-08-26 ##

### Added ###

- composer.json support
- custom methods support
- CHANGELOG, LICENSE, README, and SECURITY
- enable `strict_types`
- generated return type support
- nette generator support
- options collection
- phpdoc to each method
- phpdoc support
- return type support compatible with yacc format
- strict data type where it is possible
- tests
- unified symbols with new `EqualCheckableInterface`

### Changed ###

- alphabetically sorted methods in classes
- collections refactoring
- examples refactoring
- exception messages unification
- generators refactoring
- moving classes to namespaces
- optimized generated output
- rename `Set` to `Collection`
- rename classes `Pacc[ClassName]` to `[ClassName]`
- replace PHP 3 syntax `array()` by `[]`
- replace C syntax for non-public methods and properties (prefixed by `_`)
- replace inner `Tokenizer` by derivated package `Interitty\Tokenizer`
- replace native `trim` by `Interitty\Utils\Strings\trim`
- short variable names to self-described one
- using getters, setters, and factories

### Fixed ###

- calling an undeclared property
- composer autoloader
- `eval` in string token replaced
