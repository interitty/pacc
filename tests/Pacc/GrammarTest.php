<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Output\OutputInterface;

use function array_merge;

/**
 * @coversDefaultClass Interitty\Pacc\Grammar
 */
class GrammarTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of End getter/setter implementation
     *
     * @return void
     * @covers ::getEnd
     * @covers ::setEnd
     */
    public function testGetSetEnd(): void
    {
        $end = $this->createTerminalMock();
        $this->processTestGetSet(Grammar::class, 'end', $end);
    }

    /**
     * Tester of Epsilon getter/setter implementation
     *
     * @return void
     * @covers ::getEpsilon
     * @covers ::setEpsilon
     */
    public function testGetSetEpsilon(): void
    {
        $epsilon = $this->createTerminalMock();
        $this->processTestGetSet(Grammar::class, 'epsilon', $epsilon);
    }

    /**
     * Tester of getName exception implementation
     *
     * @return void
     * @covers ::getName
     * @group negative
     */
    public function testGetNameException(): void
    {
        $grammar = $this->createGrammarMock();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Missing parser name');
        $this->expectExceptionData();
        $grammar->getName();
    }

    /**
     * Tester of Output getter/setter implementation
     *
     * @return void
     * @covers ::getOutput
     * @covers ::setOutput
     */
    public function testGetSetOutput(): void
    {
        $output = $this->createOutputMock();
        $this->processTestGetSet(Grammar::class, 'output', $output);
    }

    /**
     * Tester of Start getter/setter implementation
     *
     * @return void
     * @covers ::getStart
     * @covers ::setStart
     */
    public function testGetSetStart(): void
    {
        $start = $this->createNonTerminalMock();
        $this->processTestGetSet(Grammar::class, 'start', $start);
    }

    /**
     * Tester of getStart exception implementation
     *
     * @return void
     * @covers ::getStart
     * @group negative
     */
    public function testGetStartException(): void
    {
        $grammar = $this->createGrammarMock();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Missing start symbol');
        $this->expectExceptionData();
        $grammar->getStart();
    }

    /**
     * Tester of StartProduction getter/setter implementation
     *
     * @return void
     * @covers ::getStartProduction
     * @covers ::setStartProduction
     */
    public function testGetSetStartProduction(): void
    {
        $startProduction = $this->createProductionMock();
        $grammar = $this->createGrammarMock();
        $productions = $grammar->getProductions();
        static::assertSame($grammar, $this->callNonPublicMethod($grammar, 'setStartProduction', [$startProduction]));
        static::assertSame($startProduction, $this->callNonPublicMethod($grammar, 'getStartProduction'));
        self::assertFalse($productions->contains($startProduction));
    }

    /**
     * Tester of StartProduction getter/setter for equal already exists implementation
     *
     * @return void
     * @covers ::getStartProduction
     * @covers ::setStartProduction
     */
    public function testGetSetStartProductionEqual(): void
    {
        $startProduction = $this->createProductionMock();
        $grammar = $this->createGrammarMock();
        $productions = $grammar->getProductions()->add($startProduction);
        static::assertSame($grammar, $this->callNonPublicMethod($grammar, 'setStartProduction', [$startProduction]));
        static::assertSame($startProduction, $this->callNonPublicMethod($grammar, 'getStartProduction'));
        self::assertTrue($productions->contains($startProduction));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * Output mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OutputInterface&MockObject
     */
    protected function createOutputMock(array $methods = []): OutputInterface&MockObject
    {
        $mockMethods = array_merge($methods, [
            'write',
            'writeln',
            'setVerbosity',
            'getVerbosity',
            'isQuiet',
            'isVerbose',
            'isVeryVerbose',
            'isDebug',
            'setDecorated',
            'isDecorated',
            'setFormatter',
            'getFormatter',
        ]);
        $mock = $this->createPartialMock(OutputInterface::class, $mockMethods);
        return $mock;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
