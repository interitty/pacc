<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Symbol\Production
 */
class ProductionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of Index getter/setter implementation
     *
     * @return void
     * @covers ::getIndex
     * @covers ::setIndex
     */
    public function testGetSetIndex(): void
    {
        $index = 1;
        $this->processTestGetSet(Production::class, 'index', $index);
    }

    // </editor-fold>
}
