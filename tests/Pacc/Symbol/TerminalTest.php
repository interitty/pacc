<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Symbol\Terminal
 */
class TerminalTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of ToString implementation
     *
     * @return void
     * @covers ::__toString
     * @group integration
     */
    public function testToString(): void
    {
        $name = 'name';
        $type = 'type';
        $value = 'value';
        $terminal = $this->createTerminal($name, $type, $value);
        self::assertSame('`' . $name . '`', (string) $terminal);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    protected function createTerminal(string $name, ?string $type = null, ?string $value = null): Terminal
    {
        $terminal = new Terminal($name, $type, $value);
        return $terminal;
    }

    // </editor-fold>
}
