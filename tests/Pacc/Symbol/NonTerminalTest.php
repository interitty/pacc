<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Symbol\NonTerminal
 */
class NonTerminalTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of ToString implementation
     *
     * @return void
     * @covers ::__toString
     * @group integration
     */
    public function testToString(): void
    {
        $name = 'name';
        $nonTerminal = $this->createNonTerminal($name);
        self::assertSame($name, (string) $nonTerminal);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    // </editor-fold>
}
