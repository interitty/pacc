<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use InvalidArgumentException;
use Nette\NotSupportedException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Symbol\SymbolCollection
 */
class SymbolCollectionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of SymbolCollection for work with equal implementation
     *
     * @return void
     * @group integration
     * @covers ::__toString
     * @covers ::add
     * @covers ::contains
     * @covers ::containsCollection
     * @covers ::deleteItem
     * @covers ::get
     * @covers ::isArrayEqual
     * @covers ::isEqual
     */
    public function testEqual(): void
    {
        $terminal = $this->createTerminal('test');
        $equalTerminal = $this->createTerminal('test');
        $symbols = $this->createSymbolCollection(Terminal::class);
        $equalSymbols = $this->createSymbolCollection(Terminal::class);

        self::assertFalse($symbols->contains($terminal));
        $symbols->add($terminal);
        self::assertTrue($symbols->contains($terminal));
        self::assertFalse($equalSymbols->containsCollection($symbols));

        $equalSymbols->add($terminal);
        self::assertTrue($equalSymbols->contains($equalTerminal));
        self::assertTrue($symbols->isEqual($equalSymbols));
        self::assertSame((string) $symbols, (string) $equalSymbols);
        self::assertTrue($equalSymbols->containsCollection($symbols));

        $equalSymbols->deleteItem($terminal);
        self::assertFalse($equalSymbols->contains($terminal));
        self::assertFalse($equalSymbols->isEqual($symbols));

        $symbols->deleteItem($equalTerminal);
        self::assertFalse($symbols->contains($equalTerminal));
        self::assertTrue($symbols->isEqual($equalSymbols));
        self::assertSame((string) $symbols, (string) $equalSymbols);
    }

    /**
     * Tester of SymbolCollection for work with nonEqual implementation
     *
     * @return void
     * @group negative
     * @covers ::add
     * @covers ::contains
     * @covers ::delete
     * @covers ::get
     * @covers ::isArrayEqual
     * @covers ::isEqual
     */
    public function testNonEqual(): void
    {
        $key = 'nonEqualTerminal';
        $terminal = $this->createTerminal('test');
        $nonEqualTerminal = $this->createTerminal('nonEqual');
        $symbols = $this->createSymbolCollection(Terminal::class);
        $equalSymbols = $this->createSymbolCollection(Terminal::class);

        self::assertFalse($symbols->contains($terminal));
        $symbols->add($terminal);
        self::assertTrue($symbols->contains($terminal));

        $equalSymbols->add($nonEqualTerminal, $key);
        self::assertFalse($equalSymbols->isEqual($symbols));
        self::assertSame($nonEqualTerminal, $equalSymbols->get($key));
        $equalSymbols->delete($key);
        self::assertFalse($equalSymbols->contains($nonEqualTerminal));
    }

    /**
     * Tester of __toString for exception handle implementation
     *
     * @return void
     * @group negative
     * @covers ::__toString
     */
    public function testToStringException(): void
    {
        $production = $this->createProductionMock();
        $collection = $this->createSymbolCollection(Production::class);
        $collection->add($production);

        $this->expectException(NotSupportedException::class);
        $this->expectExceptionMessage('Not supported type ":type"');
        $this->expectExceptionData(['type' => Production::class]);
        $collection->__toString();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of assertCollectionOfType implementation
     *
     * @return void
     * @covers ::assertCollectionOfType
     */
    public function testAssertCollectionOfType(): void
    {
        $type = 'Symbol';
        $symbolCollection = $this->createSymbolCollectionMock(['getType']);
        $symbolCollection->expects(self::once())
            ->method('getType')
            ->willReturn($type);

        self::assertTrue($symbolCollection->assertCollectionOfType($type));
    }

    /**
     * Tester of assertCollectionOfType exception implementation
     *
     * @return void
     * @covers ::assertCollectionOfType
     * @group negative
     */
    public function testAssertCollectionOfTypeException(): void
    {
        $expectedType = 'test';
        $type = 'Symbol';
        $symbolCollection = $this->createSymbolCollectionMock(['getType']);
        $symbolCollection->expects(self::once())
            ->method('getType')
            ->willReturn($type);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Bad type - expected Collection<:expectedType>, given Collection<:type>');
        $this->expectExceptionData(['expectedType' => $expectedType, 'type' => $type]);
        $symbolCollection->assertCollectionOfType($expectedType);
    }

    /**
     * Dataprovider for hash
     *
     * @phpstan-return Generator<string, array<mixed>>
     */
    public function hashDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="bool">
        yield 'bool' => [true];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="integer">
        yield 'integer' => [42];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="float">
        yield 'float' => [4.2];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="string">
        yield 'string' => ['string'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="object">
        yield 'object' => [(object) 'object'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="array">
        yield 'array' => [['string' => 'string', 'object' => (object) 'object']];
        // </editor-fold>
    }

    /**
     * Tester of hash implementation
     *
     * @param mixed $value
     * @return void
     * @dataProvider hashDataProvider
     * @covers ::hash
     */
    public function testHash($value): void
    {
        $symbolCollection = $this->createSymbolCollectionMock();
        $firstHash = $this->callNonPublicMethod($symbolCollection, 'hash', [$value]);
        $secondHash = $this->callNonPublicMethod($symbolCollection, 'hash', [$value]);
        self::assertSame($firstHash, $secondHash);
    }

    /**
     * Tester of hash implementation
     *
     * @return void
     * @covers ::hash
     * @group negative
     */
    public function testHashUnsupported(): void
    {
        $symbolCollection = $this->createSymbolCollectionMock();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unsupported value type ":type" for hash operation');
        $this->expectExceptionData(['type' => 'NULL']);
        $this->callNonPublicMethod($symbolCollection, 'hash', [null]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     * @template CollectionType of object
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        /** @var SymbolCollection<CollectionType> $symbolCollection */
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    /**
     * SymbolCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return SymbolCollection<Symbol>&MockObject
     */
    protected function createSymbolCollectionMock(array $methods = []): SymbolCollection&MockObject
    {
        $mock = $this->createPartialMock(SymbolCollection::class, $methods);
        return $mock;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @return Terminal
     */
    protected function createTerminal(string $name): Terminal
    {
        $terminal = new Terminal($name);
        return $terminal;
    }

    // </editor-fold>
}
