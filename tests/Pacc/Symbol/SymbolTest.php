<?php

declare(strict_types=1);

namespace Interitty\Pacc\Symbol;

use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Symbol\Symbol
 */
class SymbolTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of First getter/setter implementation
     *
     * @return void
     * @covers ::getFirst
     * @covers ::setFirst
     */
    public function testGetSetFirst(): void
    {
        $first = $this->createSymbolCollectionMock();
        $this->processTestGetSet(Symbol::class, 'first', $first);
    }

    /**
     * Tester of Follow getter/setter implementation
     *
     * @return void
     * @covers ::getFollow
     * @covers ::setFollow
     */
    public function testGetSetFollow(): void
    {
        $follow = $this->createSymbolCollectionMock();
        $this->processTestGetSet(Symbol::class, 'follow', $follow);
    }

    /**
     * Tester of Index getter/setter implementation
     *
     * @return void
     * @covers ::getIndex
     * @covers ::setIndex
     */
    public function testGetSetIndex(): void
    {
        $index = 123;
        $this->processTestGetSet(Symbol::class, 'index', $index);
    }

    /**
     * Tester of __toString implementation
     *
     * @return void
     * @covers ::__toString
     */
    public function testToString(): void
    {
        $name = 'name';
        $symbol = $this->createSymbolMock();
        $this->callNonPublicMethod($symbol, 'setName', [$name]);
        self::assertSame($name, (string) $symbol);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Symbol mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Symbol&MockObject
     */
    protected function createSymbolMock(array $methods = []): Symbol&MockObject
    {
        $mock = $this->createPartialMock(Symbol::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return SymbolCollection<Symbol>&MockObject
     */
    protected function createSymbolCollectionMock(array $methods = []): SymbolCollection&MockObject
    {
        $mock = $this->createPartialMock(SymbolCollection::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
