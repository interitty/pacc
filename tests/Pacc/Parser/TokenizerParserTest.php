<?php

declare(strict_types=1);

namespace Interitty\Pacc\Parser;

use Generator;
use Interitty\Pacc\Generator\LeftToRight\Generator as LRGenerator;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\Pacc\Tokens\Token;
use Interitty\Pacc\Tokens\Tokenizer;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;

use function array_shift;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Pacc\Parser\TokenizerParser
 * @phpstan-import-type GrammarOptionTypes from OptionsCollection
 */
class TokenizerParserTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Data provider for grammarName test
     *
     * @phpstan-return Generator<string, array{0: string, 1: string}>
     */
    public function grammarNameDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="grammar test">
        yield 'grammar test' => ['grammar test', 'test'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="grammar test\test">
        yield 'grammar test\\test' => ['grammar test\\test', 'test\\test'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="grammar test;">
        yield 'grammar test;' => ['grammar test;', 'test'];
        // </editor-fold>
    }

    /**
     * Tester of grammar name parse implementation
     *
     * @param string $string
     * @param string $expectedGrammarName
     * @return void
     * @dataProvider grammarNameDataProvider
     * @group integration
     */
    public function testGrammarName(string $string, string $expectedGrammarName): void
    {
        $grammar = $this->processParse($string);

        self::assertSame($expectedGrammarName, $grammar->getName());
    }

    /**
     * Data provider for processGrammarOptions tester
     *
     * @phpstan-return Generator<string, array{0: string, 1: mixed[], 2: GrammarOptionTypes}>
     */
    public function processGrammarOptionsDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="one line">
        yield 'one line' => ['@block {}', ['block' => ''], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="new line">
        yield 'new line' => ['@block' . PHP_EOL . '{}', ['block' => ''], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="multi line">
        yield 'multi line' => ['@block {' . PHP_EOL . 'test' . PHP_EOL . '}', ['block' => 'test'], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="return types">
        yield 'return types' => [
            '@block {$<?int#int|null>$ = $<?int>1;}',
            ['block' => '$$ = $1;'],
            [
                'return' => ['doc' => ['int', 'null'], 'type' => ['null', 'int']],
                1 => ['doc' => [], 'type' => ['null', 'int']],
            ],
        ];
        // </editor-fold>
    }

    /**
     * Tester of grammarOptions parse implementation
     *
     * @param string $string
     * @param mixed[] $expectedOptions
     * @phpstan-param GrammarOptionTypes $types
     * @return void
     * @dataProvider processGrammarOptionsDataProvider
     * @group integration
     */
    public function testProcessGramarOptions(string $string, array $expectedOptions, array $types): void
    {
        $phpDoc = '/** phpdoc */';
        $grammar = $this->processParse($phpDoc . PHP_EOL . $string);

        $options = $grammar->getOptions()->getGrammarOptions();
        foreach ($expectedOptions as $name => $value) {
            self::assertArrayHasKey($name, $options);
            self::assertSame($value, $options[$name]['code']);
            self::assertSame($phpDoc, $options[$name]['phpDoc']);
            self::assertSame($types, $options[$name]['types']);
        }
        self::assertSameSize($expectedOptions, $options);
    }

    /**
     * Data provider for processOptions tester
     *
     * @phpstan-return Generator<string, array{0: string, 1: mixed[]}>
     */
    public function processOptionsDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="one line">
        yield 'one line' => ['option eol = "\n";', ['eol' => "\n"]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="one line block">
        yield 'one line block' => [
            'option' . PHP_EOL .
            '(' . PHP_EOL .
            '    indentation = "    ";' . PHP_EOL .
            ')',
            ['indentation' => "    "],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="multiline with code block">
        yield 'multiline with code block' => [
            'option (' . PHP_EOL .
            '    parse = "processParse";' . PHP_EOL .
            '    algorithm = {' . PHP_EOL . 'LR' . PHP_EOL . '}' . PHP_EOL .
            ')',
            ['parse' => "processParse", 'algorithm' => "LR"],
        ];
        // </editor-fold>
    }

    /**
     * Tester of options parse implementation
     *
     * @param string $string
     * @param mixed[] $expectedOptions
     * @return void
     * @dataProvider processOptionsDataProvider
     * @group integration
     */
    public function testProcesOptions(string $string, array $expectedOptions): void
    {
        $grammar = $this->processParse($string);
        $options = $grammar->getOptions()->getOptions();

        foreach ($expectedOptions as $name => $value) {
            self::assertArrayHasKey($name, $options);
            self::assertSame($value, $options[$name]);
        }
        self::assertSameSize($expectedOptions, $options);
    }

    /**
     * Data provider for processReduceTypes tester
     *
     * @phpstan-return Generator<string, array{0: string, 1: string, 2: GrammarOptionTypes}>
     */
    public function processParseTypesDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="DEFAULT_PRODUCTION_CODE">
        yield 'DEFAULT_PRODUCTION_CODE' => [LRGenerator::DEFAULT_PRODUCTION_CODE, '$$ = $1;', [
                'return' => ['doc' => [], 'type' => []], '1' => ['doc' => [], 'type' => []],
        ]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="second argument">
        yield 'second argument' => ['$$ = $2;', '$$ = $2;', [
                'return' => ['doc' => [], 'type' => []], '2' => ['doc' => [], 'type' => []],
        ]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="multiple arguments">
        yield 'multiple arguments' => ['var_dump($2 === $3);', 'var_dump($2 === $3);', [
                '2' => ['doc' => [], 'type' => []], '3' => ['doc' => [], 'type' => []],
        ]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="no return">
        yield 'no return' => ['echo $3;', 'echo $3;', ['3' => ['doc' => [], 'type' => []]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="no types">
        yield 'no types' => ['echo "no types";', 'echo "no types";', []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="no code">
        yield 'no code' => ['', '', []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="empty types">
        yield 'empty types' => ['echo $<>1;', 'echo $1;', ['1' => ['doc' => [], 'type' => []]]];
        // </editor-fold>
    }

    /**
     * Data provider for processReduceTypes with types tester
     *
     * @phpstan-return Generator<string, array{0: string, 1: string, 2: GrammarOptionTypes}>
     */
    public function processParseTypesWithTypesDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="return type">
        yield 'return types' => ['$<int>$ = 42;', '$$ = 42;', ['return' => ['doc' => [], 'type' => ['int']]]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="return nullable type">
        yield 'return nullable types' => ['$<?int>$ = is_int($1) ? $1 : null;', '$$ = is_int($1) ? $1 : null;', [
                'return' => ['doc' => [], 'type' => ['null', 'int']], '1' => ['doc' => [], 'type' => []]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="multiple types">
        yield 'multiple types' => [
            '$$ = is_object($1) ? $<object>1 : (object) $<array>1;',
            '$$ = is_object($1) ? $1 : (object) $1;', [
                'return' => ['doc' => [], 'type' => []], '1' => ['doc' => [], 'type' => ['object', 'array']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="phpdoc">
        yield 'phpdoc' => ['$<array#int[]>$ = [1, 2, 3];', '$$ = [1, 2, 3];', [
                'return' => ['doc' => ['int[]'], 'type' => ['array']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="multiple phpdocs">
        yield 'multiple phpdocs' => [
            'var_dump(is_object($1) ? $<object#object{int[]}>1 : (object) $<array#array<int>>1);',
            'var_dump(is_object($1) ? $1 : (object) $1);', [
                '1' => ['doc' => ['object{int[]}', 'array<int>'], 'type' => ['object', 'array']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="prevent duplicity">
        yield 'prevent duplicity' => ['$<array#int[]>1 !== (array) $<array#int[]>1;', '$1 !== (array) $1;', [
                '1' => ['doc' => ['int[]'], 'type' => ['array']]],
        ];
        // </editor-fold>
    }

    /**
     * Tester of parse types processor implementation
     *
     * @param string $code
     * @param string $expectedCode
     * @phpstan-param GrammarOptionTypes $expectedTypes
     * @return void
     * @dataProvider processParseTypesDataProvider
     * @dataProvider processParseTypesWithTypesDataProvider
     * @covers ::processParseTypes
     * @group integration
     */
    public function testProcessParseTypes(string $code, string $expectedCode, array $expectedTypes): void
    {
        $types = TokenizerParser::processParseTypes($code);
        self::assertSame($expectedTypes, $types);
        self::assertSame($expectedCode, $code);
    }

    /**
     * Tester of phpDoc processor implementation
     *
     * @return void
     * @covers ::processParse
     * @covers ::processPhpDoc
     * @group integration
     */
    public function testProcessPhpDoc(): void
    {
        $phpDoc = '/** test */';
        $string = '@test {}';
        $grammar = $this->processParse($phpDoc . PHP_EOL . $string);
        $options = $grammar->getOptions()->getGrammarOptions();

        self::assertArrayHasKey('test', $options);
        self::assertSame($phpDoc, $options['test']['phpDoc']);
    }

    /**
     * Data provider for processRulesProductions tester
     *
     * @phpstan-return Generator<string, array{
     *      0: string,
     *      1: array{
     *          array{
     *              'name': string,
     *              'right': string[],
     *              'types'?: GrammarOptionTypes
     *          }
     *      }
     *  }>
     */
    public function processRulesProductionsDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="value : literal | \'empty\';">
        yield 'value : literal | \'empty\';' => [
            'value : literal | \'empty\';',
            [['name' => 'value', 'right' => ['literal']], ['name' => 'value', 'right' => ['empty']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="value : literal | literal; literal: \'empty\'|\'empty\';">
        yield 'value : literal | literal; literal: \'empty\'|\'empty\';' => [
            'value : literal | literal; literal: \'empty\'|\'empty\';',
            [['name' => 'value', 'right' => ['literal']], ['name' => 'literal', 'right' => ['empty']]],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="value : TOKEN_STRING {$$ = (string) $<int>1;};">
        yield 'value : TOKEN_STRING {$$ = (string) $<int>1;};' => [
            'value : TOKEN_STRING {$$ = (string) $<int>1;} | TOKEN_STRING {$$ = (string) $<int>1;};',
            [[
                'name' => 'value',
                'right' => ['TOKEN_STRING'],
                'types' => ['return' => ['doc' => [], 'type' => []], '1' => ['doc' => [], 'type' => ['int']]],
                ]],
        ];
        // </editor-fold>
    }

    /**
     * Tester of rules productions parse implementation
     *
     * @param string $string
     * @phpstan-param array{
     *      array{
     *          'name': string,
     *          'right': string[],
     *          'types'?: GrammarOptionTypes
     *      }
     *  } $expectedProductions
     * @return void
     * @dataProvider processRulesProductionsDataProvider
     * @group integration
     */
    public function testProcessRulesProductions(string $string, array $expectedProductions)
    {
        $grammar = $this->processParse($string);
        $productions = $grammar->getProductions();

        self::assertSameSize($expectedProductions, $productions);
        /** @var Production $production */
        foreach ($productions as $production) {
            $expectedProduction = array_shift($expectedProductions);
            self::assertIsArray($expectedProduction);
            $expectedTypes = isset($expectedProduction['types']) ? $expectedProduction['types'] : [];
            self::assertSame($expectedProduction['name'], $production->getLeftName());
            self::assertSame($expectedTypes, $production->getTypes());
            self::assertSameSize($expectedProduction['right'], $production->getRight());
            foreach ($production->getRight() as $right) {
                $expectedRightName = array_shift($expectedProduction['right']);
                self::assertSame($expectedRightName, $right->getName());
            }
        }
    }

    /**
     * Data provider for processRulesTerminals tester
     *
     * @phpstan-return Generator<string, array{0: string, 1: mixed[], 2: mixed[]}>
     */
    public function processRulesTerminalsDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="value : literal | \'empty\';">
        yield 'value : literal | \'empty\';' => [
            'value : literal | \'empty\';', ['value', 'literal'], ['empty'],
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="value : literal | literal; literal: \'empty\'|\'empty\';">
        yield 'value : literal | literal; literal: \'empty\'|\'empty\';' => [
            'value : literal | literal; literal: \'empty\'|\'empty\';', ['value', 'literal'], ['empty'],
        ];
        // </editor-fold>
    }

    /**
     * Tester of rules productions parse implementation
     *
     * @param string $string
     * @param mixed[] $expectedNonTerminals
     * @param mixed[] $expectedTerminals
     * @return void
     * @dataProvider processRulesTerminalsDataProvider
     * @group integration
     */
    public function testProcessRulesTerminals(string $string, array $expectedNonTerminals, array $expectedTerminals)
    {
        $grammar = $this->processParse($string);
        $terminals = $grammar->getTerminals();
        $nonTerminals = $grammar->getNonTerminals();

        self::assertSameSize($expectedTerminals, $terminals);
        /** @var Terminal $terminal */
        foreach ($terminals as $terminal) {
            $expectedTerminal = array_shift($expectedTerminals);
            self::assertSame($expectedTerminal, $terminal->getName());
        }

        self::assertSameSize($expectedNonTerminals, $nonTerminals);
        /** @var NonTerminal $nonTerminal */
        foreach ($nonTerminals as $nonTerminal) {
            $expectedNonTerminal = array_shift($expectedNonTerminals);
            self::assertSame($expectedNonTerminal, $nonTerminal->getName());
        }
    }

    /**
     * Tester of separated name exception implementation
     *
     * @return void
     * @covers ::separatedName
     * @group negative
     */
    public function testSeparatedNameException(): void
    {
        $string = 'grammar $';
        $this->expectException(UnexpectedTokenException::class);
        $this->expectExceptionMessage('Unexpected token ":token" of type ":type" on line :line at position :position');
        $this->expectExceptionData([
            'token' => '$',
            'type' => Token::TOKEN_BAD,
            'line' => 1,
            'position' => 9,
        ]);

        $this->processParse($string);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of tokenizer getter/setter implementation
     *
     * @return void
     * @covers ::getTokenizer
     * @covers ::setTokenizer
     */
    public function testGetSetTokenizer(): void
    {
        $tokenizer = $this->createTokenizer('');
        $this->processTestGetSet(TokenizerParser::class, 'tokenizer', $tokenizer);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Process parse helper
     *
     * @param string $string
     * @return Grammar
     * @covers ::processParse
     */
    protected function processParse(string $string): Grammar
    {
        $parser = new TokenizerParser();
        $tokenizer = $this->createTokenizer($string);
        $this->callNonPublicMethod($parser, 'setTokenizer', [$tokenizer]);
        $this->callNonPublicMethod($parser, 'processParse');
        $grammar = $this->callNonPublicMethod($parser, 'getGrammar');
        return $grammar;
    }

    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return Tokenizer
     */
    protected function createTokenizer(string $string): Tokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }

    // </editor-fold>
}
