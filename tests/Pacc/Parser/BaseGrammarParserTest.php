<?php

declare(strict_types=1);

namespace Interitty\Pacc\Parser;

use Interitty\Pacc\Tokens\Tokenizer;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Parser\BaseGrammarParser
 */
class BaseGrammarParserTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of tokenizer factory implementation
     *
     * @param string $string
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::createTokenizer
     * @group integration
     */
    public function testCreateTokenizer(string $string): void
    {
        $tokenizerParser = $this->createBaseGrammarParserMock([]);
        $tokenizer = $this->callNonPublicMethod($tokenizerParser, 'createTokenizer', [$string]);
        self::assertInstanceOf(Tokenizer::class, $tokenizer);
        self::assertSame($string, $this->callNonPublicMethod($tokenizer, 'getString'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseGrammarParser mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return BaseGrammarParser&MockObject
     */
    protected function createBaseGrammarParserMock(array $methods = []): BaseGrammarParser&MockObject
    {
        $mock = $this->createPartialMock(BaseGrammarParser::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
