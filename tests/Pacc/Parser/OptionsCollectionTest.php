<?php

declare(strict_types=1);

namespace Interitty\Pacc\Parser;

use Interitty\PhpUnit\BaseTestCase;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Pacc\Parser\OptionsCollection
 */
class OptionsCollectionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of getGrammarOption exception implementation
     *
     * @return void
     * @group negative
     * @covers ::getGrammarOption
     */
    public function testGetGrammarOptionException(): void
    {
        $option = 'current';
        $collection = $this->createOptionsCollection();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('GrammarOption ":option" was not set');
        $this->expectExceptionData(['option' => $option]);
        $collection->getGrammarOption($option);
    }

    /**
     * Tester of getGrammarOptionPhpDoc exception implementation
     *
     * @return void
     * @group negative
     * @covers ::getGrammarOptionPhpDoc
     */
    public function testGetGrammarOptionPhpDocException(): void
    {
        $option = 'current';
        $collection = $this->createOptionsCollection();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('GrammarOption ":option" was not set');
        $this->expectExceptionData(['option' => $option]);
        $collection->getGrammarOptionPhpDoc($option);
    }

    /**
     * Tester of getGrammarOptionTypes exception implementation
     *
     * @return void
     * @group negative
     * @covers ::getGrammarOptionTypes
     */
    public function testGetGrammarOptionTypesException(): void
    {
        $option = 'current';
        $collection = $this->createOptionsCollection();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('GrammarOption ":option" was not set');
        $this->expectExceptionData(['option' => $option]);
        $collection->getGrammarOptionTypes($option);
    }

    /**
     * Tester of getOption exception implementation
     *
     * @return void
     * @group negative
     * @covers ::getOption
     */
    public function testGetOptionException(): void
    {
        $option = OptionsCollection::OPTION_INDENTATION;
        $collection = $this->createOptionsCollection();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Option ":option" was not set');
        $this->expectExceptionData(['option' => $option]);
        $collection->getOption($option);
    }

    /**
     * Tester of grammarOptions implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getGrammarOption
     * @covers ::getGrammarOptionPhpDoc
     * @covers ::getGrammarOptionTypes
     * @covers ::getGrammarOptions
     * @covers ::hasGrammarOption
     * @covers ::setGrammarOption
     * @covers ::setGrammarOptions
     */
    public function testGrammarOptions(): void
    {
        $current = 'current';
        $currentOptionCode = 'return \'currentCode\'';
        $currentOptionPhpDoc = '/** */';
        $currentOptionTypes = ['return' => ['doc' => [], 'type' => ['string']]];
        $currentOption = [
            'code' => $currentOptionCode,
            'phpDoc' => $currentOptionPhpDoc,
            'types' => $currentOptionTypes,
        ];
        $options = [$current => $currentOption];
        $collection = $this->createOptionsCollection([], $options);
        self::assertTrue($collection->hasGrammarOption($current));
        self::assertSame($currentOptionCode, $collection->getGrammarOption($current));
        self::assertSame($currentOptionPhpDoc, $collection->getGrammarOptionPhpDoc($current));
        self::assertSame($currentOptionTypes, $collection->getGrammarOptionTypes($current));
    }

    /**
     * Tester of grammarOptions defaults implementation
     *
     * @return void
     * @group integration
     * @covers ::getGrammarOption
     * @covers ::getGrammarOptionPhpDoc
     * @covers ::getGrammarOptionTypes
     * @covers ::hasGrammarOption
     */
    public function testGrammarOptionsDefaults(): void
    {
        $current = 'current';
        $defaultCode = 'return \'currentCode\'';
        $defaultPhpDoc = '/** */';
        $defaultTypes = ['return' => ['doc' => [], 'type' => ['string']]];
        $collection = $this->createOptionsCollection();
        self::assertFalse($collection->hasGrammarOption($current));
        self::assertSame($defaultCode, $collection->getGrammarOption($current, $defaultCode));
        self::assertSame($defaultPhpDoc, $collection->getGrammarOptionPhpDoc($current, $defaultPhpDoc));
        self::assertSame($defaultTypes, $collection->getGrammarOptionTypes($current, $defaultTypes));
    }

    /**
     * Tester of options implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getOption
     * @covers ::getOptions
     * @covers ::hasOption
     * @covers ::setOption
     * @covers ::setOptions
     */
    public function testOptions(): void
    {
        $indentation = '    ';
        $options = [OptionsCollection::OPTION_EOL => PHP_EOL];
        $collection = $this->createOptionsCollection($options);
        self::assertSame($options, $collection->getOptions());
        self::assertTrue($collection->hasOption(OptionsCollection::OPTION_EOL));
        self::assertSame(PHP_EOL, $collection->getOption(OptionsCollection::OPTION_EOL));
        self::assertFalse($collection->hasOption(OptionsCollection::OPTION_INDENTATION));
        self::assertSame($collection, $collection->setOption(OptionsCollection::OPTION_INDENTATION, $indentation));
        self::assertTrue($collection->hasOption(OptionsCollection::OPTION_INDENTATION));
        self::assertSame($indentation, $collection->getOption(OptionsCollection::OPTION_INDENTATION));
    }

    /**
     * Tester of options defaults implementation
     *
     * @return void
     * @group integration
     * @covers ::getOption
     * @covers ::hasOption
     */
    public function testOptionsDefaults(): void
    {
        $default = '  ';
        $collection = $this->createOptionsCollection();
        self::assertFalse($collection->hasOption(OptionsCollection::OPTION_INDENTATION));
        self::assertSame($default, $collection->getOption(OptionsCollection::OPTION_INDENTATION, $default));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * OptionsCollection factory
     *
     * @param string[] $options [OPTIONAL]
     * @param array[] $grammarOptions [OPTIONAL] Each option contain key `code`, `phpDoc` and `returnType`
     * @return OptionsCollection
     * @phpstan-param array{
     *      code: string,
     *      phpDoc: string|null,
     *      types: array<'return'|int, array<'doc'|'type', string[]>>
     *  }[] $grammarOptions
     */
    protected function createOptionsCollection(array $options = [], array $grammarOptions = []): OptionsCollection
    {
        $symbolCollection = new OptionsCollection($options, $grammarOptions);
        return $symbolCollection;
    }

    /**
     * OptionsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OptionsCollection&MockObject
     */
    protected function createOptionsCollectionMock(array $methods = []): OptionsCollection&MockObject
    {
        $mock = $this->createPartialMock(OptionsCollection::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
