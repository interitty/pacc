<?php

declare(strict_types=1);

namespace Interitty\Pacc\Tokens;

use Interitty\Pacc\Exceptions\ParseException;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Tokens\Tokenizer
 */
class TokenizerTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * BlockCodeLexemeException processor implementation
     *
     * @return void
     * @covers ::processBlockCodeLexeme
     * @group negative
     */
    public function testProcessBlockCodeLexemeException(): void
    {
        $string = '@next {';
        $tokenizer = $this->createTokenizer($string);
        $typeToken = $tokenizer->next();
        self::assertSame('@', $typeToken->getValue());
        $nameToken = $tokenizer->next();
        self::assertSame('next', $nameToken->getValue());

        $this->expectException(ParseException::class);
        $this->expectExceptionMessage('Unable to find terminal curly bracket');
        $this->expectExceptionData();
        $tokenizer->next();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return Tokenizer
     */
    protected function createTokenizer(string $string): Tokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }

    // </editor-fold>
}
