<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator;

use Interitty\PhpUnit\BaseTestCase;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpNamespace;
use PHPUnit\Framework\MockObject\MockObject;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\Printer
 */
class PrinterTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of printClass implementation
     *
     * @return void
     * @covers ::printClass
     * @group integration
     */
    public function testPrintClass(): void
    {
        $code = 'class Test' . PHP_EOL
            . '{' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType('Test');
        $namespace = $this->createNamespace('Interitty\\Pacc');
        $printer = $this->createPrinterMock();
        self::assertSame($code, $printer->printClass($class, $namespace));
    }

    /**
     * Tester of printClass extended implementation
     *
     * @return void
     * @covers ::getFooter
     * @covers ::hasFooter
     * @covers ::setFooter
     * @covers ::getHeader
     * @covers ::hasHeader
     * @covers ::setHeader
     * @covers ::getInner
     * @covers ::hasInner
     * @covers ::setInner
     * @covers ::printClass
     * @group integration
     */
    public function testPrintClassExtended(): void
    {
        $code = '// <editor-fold desc="Generated content">' . PHP_EOL
            . 'class Test' . PHP_EOL
            . '{' . PHP_EOL
            . '// Content of this class is generated' . PHP_EOL
            . PHP_EOL
            . '}' . PHP_EOL
            . PHP_EOL
            . '// </editor-fold>' . PHP_EOL;
        $class = $this->createClassType('Test');
        $namespace = $this->createNamespace('Interitty\\Pacc');
        $printer = $this->createPrinterMock();
        $printer->setHeader($namespace, $class, '// <editor-fold desc="Generated content">');
        $printer->setInner($class, '// Content of this class is generated');
        $printer->setFooter($class, '// </editor-fold>');
        self::assertSame($code, $printer->printClass($class, $namespace));
    }

    /**
     * Tester of printNamespace implementation
     *
     * @return void
     * @covers ::printNamespace
     * @group integration
     */
    public function testPrintNamespace(): void
    {
        $code = 'namespace Interitty\Pacc;' . PHP_EOL
            . PHP_EOL
            . 'use Interitty\Pacc\Generator\Printer;' . PHP_EOL
            . PHP_EOL
            . 'use function is_bool;' . PHP_EOL
            . PHP_EOL
            . 'use const PHP_EOL;' . PHP_EOL
            . PHP_EOL;
        $namespace = $this->createNamespace('Interitty\\Pacc');
        $namespace->addUse(Printer::class);
        $namespace->addUseFunction('is_bool');
        $namespace->addUseConstant('PHP_EOL');
        $printer = $this->createPrinterMock();
        $printer->omitEmptyNamespaces = false;
        self::assertSame($code, $printer->printNamespace($namespace));
    }

    /**
     * Tester of processFormatCode for code blocks implementation
     *
     * @return void
     * @covers ::processFormatCode
     * @covers ::processFormatCodeLine
     * @group integration
     */
    public function testProcessFormatCodeBlocks(): void
    {
        $code = 'if { ' . PHP_EOL
            . '  test(' . PHP_EOL
            . '         $test' . PHP_EOL
            . '      );' . PHP_EOL
            . '	} else { ' . PHP_EOL
            . '	  $test = [ ' . PHP_EOL
            . ']; ' . PHP_EOL
            . '       }';
        $formatedCode = '    if {' . PHP_EOL
            . '        test(' . PHP_EOL
            . '            $test' . PHP_EOL
            . '        );' . PHP_EOL
            . '    } else {' . PHP_EOL
            . '        $test = [' . PHP_EOL
            . '        ];' . PHP_EOL
            . '    }' . PHP_EOL;

        $printer = $this->createPrinterMock();
        self::assertSame($formatedCode, $this->callNonPublicMethod($printer, 'processFormatCode', [$code, 1]));
    }

    /**
     * Tester of processFormatCode for functions implementation
     *
     * @return void
     * @covers ::processFormatCode
     * @covers ::processFormatCodeLine
     * @group integration
     */
    public function testProcessFormatCodeFunctions(): void
    {
        $code = 'function test(array $arg) { ' . PHP_EOL
            . 'return $$ = $3;' . PHP_EOL
            . '}';
        $formatedCode = '        function test(array $arg) {' . PHP_EOL
            . '            return $reduced = $arg[3];' . PHP_EOL
            . '        }' . PHP_EOL;

        $printer = $this->createPrinterMock();
        self::assertSame($formatedCode, $this->callNonPublicMethod($printer, 'processFormatCode', [$code, 2]));
    }

    /**
     * Tester of processHeader extended implementation
     *
     * @return void
     * @covers ::processHeader
     * @group integration
     */
    public function testProcessHeader(): void
    {
        $code = '/** Generated content */';
        $header = 'use Interitty\Pacc\Generator\Printer;' . PHP_EOL
            . 'use function is_bool;' . PHP_EOL
            . 'use const PHP_EOL;' . PHP_EOL
            . $code;
        $namespace = $this->createNamespace('Interitty\\Pacc');
        self::assertEmpty($namespace->getUses());
        $printer = $this->createPrinterMock();
        self::assertSame($code, $this->callNonPublicMethod($printer, 'processHeader', [$namespace, $header]));
        self::assertContains('Interitty\Pacc\Generator\Printer', $namespace->getUses($namespace::NameNormal));
        self::assertContains('is_bool', $namespace->getUses($namespace::NameFunction));
        self::assertContains('PHP_EOL', $namespace->getUses($namespace::NameConstant));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of getEol and setEol implementation
     *
     * @return void
     * @covers ::getEol
     * @covers ::setEol
     */
    public function testGetSetEol(): void
    {
        $eol = PHP_EOL;
        $this->processTestGetSet(Printer::class, 'eol', $eol);
    }

    /**
     * Tester of getIndentation and setIndentation implementation
     *
     * @return void
     * @covers ::getIndentation
     * @covers ::setIndentation
     */
    public function testGetSetIndentation(): void
    {
        $indentation = '    ';
        $this->processTestGetSet(Printer::class, 'indentation', $indentation);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * ClassType factory
     *
     * @param string|null $name [OPTIONAL]
     * @param PhpNamespace|null $namespace [OPTIONAL]
     * @return ClassType
     */
    protected function createClassType(string $name = null, PhpNamespace $namespace = null): ClassType
    {
        $classType = new ClassType($name, $namespace);
        return $classType;
    }

    /**
     * Namespace factory
     *
     * @param string $name
     * @return PhpNamespace
     */
    protected function createNamespace(string $name): PhpNamespace
    {
        $namespace = new PhpNamespace($name);
        return $namespace;
    }

    /**
     * Literal factory
     *
     * @param string $value
     * @return Literal
     */
    protected function createLiteral(string $value): Literal
    {
        $literal = new Literal($value);
        return $literal;
    }

    /**
     * Printer mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Printer&MockObject
     */
    protected function createPrinterMock(array $methods = []): Printer&MockObject
    {
        $mock = $this->createPartialMock(Printer::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
