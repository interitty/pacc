<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator;

use Interitty\Pacc\Grammar;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Parser\ParserInterface;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\BaseTokenizerParser;
use Nette\InvalidArgumentException;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;
use PHPUnit\Framework\MockObject\MockObject;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\BaseGenerator
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class BaseGeneratorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of constructor implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getGrammar
     * @covers ::setGrammar
     */
    public function testConstructor(): void
    {
        $grammar = $this->createGrammarMock();
        $baseGenerator = $this->createBaseGeneratorMock();
        $baseGenerator->__construct($grammar);
        self::assertSame($grammar, $this->callNonPublicMethod($baseGenerator, 'getGrammar'));
    }

    /**
     * Tester of createPhpFile implementation
     *
     * @return void
     * @covers ::createPhpFile
     * @group integration
     */
    public function testCreatePhpFile(): void
    {
        $generator = $this->createBaseGeneratorMock();
        $firstPhpFile = $this->callNonPublicMethod($generator, 'createPhpFile');
        self::assertFalse($firstPhpFile->hasStrictTypes());

        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->getOptions()->setOption(OptionsCollection::OPTION_STRICT_TYPES, '1');
        $secondPhpFile = $this->callNonPublicMethod($generator, 'createPhpFile');
        self::assertTrue($secondPhpFile->hasStrictTypes());
        self::assertNotSame($firstPhpFile, $secondPhpFile);
    }

    /**
     * Tester of createPrinter implementation
     *
     * @return void
     * @covers ::createPrinter
     * @group integration
     */
    public function testCreatePrinter(): void
    {
        $generator = $this->createBaseGeneratorMock();
        $firstPrinter = $this->callNonPublicMethod($generator, 'createPrinter');
        self::assertSame(PHP_EOL, $firstPrinter->getEol());
        self::assertSame('    ', $firstPrinter->getIndentation());

        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->getOptions()->setOption(OptionsCollection::OPTION_EOL, '');
        $grammar->getOptions()->setOption(OptionsCollection::OPTION_INDENTATION, '  ');

        $secondPrinter = $this->callNonPublicMethod($generator, 'createPrinter');
        self::assertSame('', $secondPrinter->getEol());
        self::assertSame('  ', $secondPrinter->getIndentation());
        self::assertNotSame($firstPrinter, $secondPrinter);
    }

    /**
     * Tester of processGenerated implementation
     *
     * @return void
     * @covers ::processGenerated
     * @group integration
     */
    public function testGenerated(): void
    {
        $code = '<?php' . PHP_EOL
            . PHP_EOL
            . 'namespace Interitty\Pacc;' . PHP_EOL
            . PHP_EOL
            . 'class TestParser' . PHP_EOL
            . '{' . PHP_EOL
            . '}' . PHP_EOL;
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setName('Interitty\Pacc\TestParser');
        self::assertSame($code, $this->callNonPublicMethod($generator, 'processGenerated'));
    }

    /**
     * Tester of processGenerated extends implementation
     *
     * @return void
     * @covers ::processGenerated
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers::processClassExtends
     * @group integration
     */
    public function testGeneratedExtends(): void
    {
        $code = '<?php' . PHP_EOL
            . PHP_EOL
            . 'namespace Interitty\Pacc;' . PHP_EOL
            . PHP_EOL
            . 'use ' . BaseTokenizerParser::class . ';' . PHP_EOL
            . PHP_EOL
            . 'class TestParser extends BaseTokenizerParser' . PHP_EOL
            . '{' . PHP_EOL
            . '}' . PHP_EOL;
        /* @var $generator BaseGenerator */
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setName('Interitty\Pacc\TestParser');
        $grammar->getOptions()->setOption(OptionsCollection::OPTION_EXTENDS, BaseTokenizerParser::class);
        self::assertSame($code, $this->callNonPublicMethod($generator, 'processGenerated'));
    }

    /**
     * Tester of processGenerated with grammar options implementation
     *
     * @return void
     * @covers ::processGenerated
     * @group integration
     */
    public function testGeneratedGrammarOptions(): void
    {
        $code = '<?php' . PHP_EOL
            . PHP_EOL
            . 'namespace Interitty\Pacc;' . PHP_EOL
            . PHP_EOL
            . '// <editor-fold desc="Generated content">' . PHP_EOL
            . 'class TestParser' . PHP_EOL
            . '{' . PHP_EOL
            . '// Content of this class is generated' . PHP_EOL
            . PHP_EOL
            . '}' . PHP_EOL
            . PHP_EOL
            . '// </editor-fold>' . PHP_EOL;
        /* @var $generator BaseGenerator */
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setName('Interitty\Pacc\TestParser');
        $grammar->getOptions()
            ->setGrammarOption(OptionsCollection::GRAMMAR_OPTION_HEADER, '// <editor-fold desc="Generated content">')
            ->setGrammarOption(OptionsCollection::GRAMMAR_OPTION_INNER, '// Content of this class is generated')
            ->setGrammarOption(OptionsCollection::GRAMMAR_OPTION_FOOTER, '// </editor-fold>');
        self::assertSame($code, $this->callNonPublicMethod($generator, 'processGenerated'));
    }

    /**
     * Tester of processGenerated implements implementation
     *
     * @return void
     * @covers ::processGenerated
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers::processClassImplements
     * @group integration
     */
    public function testGeneratedImplements(): void
    {
        $code = '<?php' . PHP_EOL
            . PHP_EOL
            . 'namespace Interitty\Pacc;' . PHP_EOL
            . PHP_EOL
            . 'use ' . ParserInterface::class . ';' . PHP_EOL
            . PHP_EOL
            . 'class TestParser implements ParserInterface' . PHP_EOL
            . '{' . PHP_EOL
            . '}' . PHP_EOL;
        /* @var $generator BaseGenerator */
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setName('Interitty\Pacc\TestParser');
        $grammar->getOptions()->setOption(OptionsCollection::OPTION_IMPLEMENTS, ParserInterface::class);
        self::assertSame($code, $this->callNonPublicMethod($generator, 'processGenerated'));
    }

    /**
     * Tester of getProductions implementation
     *
     * @return void
     * @covers ::getProductions
     * @group integration
     */
    public function testGetProductions(): void
    {
        $productions = $this->createSymbolCollection(Production::class);
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setProductions($productions);
        self::assertSame($productions, $this->callNonPublicMethod($generator, 'getProductions'));
    }

    /**
     * Tester of getTerminals implementation
     *
     * @return void
     * @covers ::getTerminals
     * @group integration
     */
    public function testGetTerminals(): void
    {
        $terminals = $this->createSymbolCollection(Terminal::class);
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setTerminals($terminals);
        self::assertSame($terminals, $this->callNonPublicMethod($generator, 'getTerminals'));
    }

    /**
     * Tester of processGrammarOptionMethod implementation
     *
     * @return void
     * @covers ::processGrammarOptionMethod
     * @covers ::getGrammarOption
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers
     * @group integration
     */
    public function testProcessGrammarOptionMethod(): void
    {
        $code = '/**' . PHP_EOL
            . ' * GeneratedMethod' . PHP_EOL
            . ' *' . PHP_EOL
            . ' * @return mixed' . PHP_EOL
            . ' */' . PHP_EOL
            . 'protected function processTest(): mixed' . PHP_EOL
            . '{' . PHP_EOL
            . '    $reduced = $this->getMixed();' . PHP_EOL
            . '    return $reduced;' . PHP_EOL
            . '}' . PHP_EOL;
        $phpDoc = '    /** GeneratedMethod */';
        $name = 'processTest';
        $namespace = $this->createNamespace('Interitty\Pacc');
        $class = $this->createClassType('Test', $namespace);
        $generator = $this->createBaseGeneratorMock();
        $this->callNonPublicMethod($generator, 'setNamespace', [$namespace]);
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->getOptions()->setGrammarOption($name, '$<mixed>$ = $this->getMixed();', $phpDoc);
        $method = $this->callNonPublicMethod($generator, 'processGrammarOptionMethod', [$class, $name]);
        self::assertSame($code, $this->createPrinterMock()->printMethod($method, $namespace));
    }

    /**
     * Tester of processGrammarOptionMethod exception handling implementation
     *
     * @return void
     * @covers ::processGrammarOptionMethod
     * @covers ::getGrammarOption
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers
     * @group negative
     */
    public function testProcessGrammarOptionMethodException(): void
    {
        $name = 'processTest';
        $namespace = $this->createNamespace('Interitty\Pacc');
        $class = $this->createClassType('Test', $namespace);
        $generator = $this->createBaseGeneratorMock();
        $this->callNonPublicMethod($generator, 'setNamespace', [$namespace]);
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->getOptions()->setGrammarOption($name, '$<\\>$');

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Value \'\\\' is not valid class/function/constant name.');
        $this->callNonPublicMethod($generator, 'processGrammarOptionMethod', [$class, $name]);
    }

    /**
     * Tester of processGrammarOptionMethod returnType implementation
     *
     * @return void
     * @covers ::processGrammarOptionMethod
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers
     * @group integration
     */
    public function testProcessGrammarOptionMethodReturnType(): void
    {
        $code = '/**' . PHP_EOL
            . ' * @return Test' . PHP_EOL
            . ' */' . PHP_EOL
            . 'protected function processTest(): Test' . PHP_EOL
            . '{' . PHP_EOL
            . '    $reduced = new Test();' . PHP_EOL
            . '    return $reduced;' . PHP_EOL
            . '}' . PHP_EOL;
        $name = 'processTest';
        $namespace = $this->createNamespace('Interitty\\Pacc');
        $class = $this->createClassType('Test', $namespace);
        $generator = $this->createBaseGeneratorMock();
        $this->callNonPublicMethod($generator, 'setNamespace', [$namespace]);
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->getOptions()->setGrammarOption($name, '$<Interitty\\Test>$ = new Test();');
        $method = $this->callNonPublicMethod($generator, 'processGrammarOptionMethod', [$class, $name]);
        self::assertSame($code, $this->createPrinterMock()->printMethod($method, $namespace));
        self::assertContains('Interitty\\Test', $namespace->getUses());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of getGenerated implementation
     *
     * @return void
     * @covers ::getGenerated
     */
    public function testGetGenerated(): void
    {
        $generated = 'generated';
        $generator = $this->createBaseGeneratorMock(['processGenerated']);
        $generator->expects(self::once())
            ->method('processGenerated')
            ->willReturn($generated);
        self::assertSame($generated, $generator->getGenerated());
        self::assertSame($generated, $generator->getGenerated());
    }

    /**
     * Tester of getGrammarName implementation
     *
     * @return void
     * @covers ::getGrammarName
     */
    public function testGetGrammarName(): void
    {
        $name = 'Interitty\\Pacc\\NameTest';
        $fullName = '\\' . $name;
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setName($fullName);
        self::assertSame($name, $this->callNonPublicMethod($generator, 'getGrammarName'));
    }

    /**
     * Tester of hasGrammarOption implementation
     *
     * @return void
     * @covers ::hasGrammarOption
     */
    public function testHasGrammarOption(): void
    {
        $name = 'test';
        $options = $this->createOptionsCollectionMock(['hasGrammarOption']);
        $generator = $this->createBaseGeneratorMock(['getGrammarOptionsCollection']);
        $generator->expects(self::once())
            ->method('getGrammarOptionsCollection')
            ->willReturn($options);
        $options->expects(self::once())
            ->method('hasGrammarOption')
            ->with(self::equalTo($name))
            ->willReturn(true);
        self::assertTrue($this->callNonPublicMethod($generator, 'hasGrammarOption', [$name]));
    }

    /**
     * Tester of getOption implementation
     *
     * @return void
     * @covers ::getOption
     */
    public function testGetOption(): void
    {
        $name = 'test';
        $default = 'default';
        $option = 'option';
        $options = $this->createOptionsCollectionMock(['getOption']);
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setOptions($options);
        $options->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo($name), self::equalTo($default))
            ->willReturn($option);
        self::assertSame($option, $this->callNonPublicMethod($generator, 'getOption', [$name, $default]));
    }

    /**
     * Tester of hasOption implementation
     *
     * @return void
     * @covers ::hasOption
     */
    public function testHasOption(): void
    {
        $name = 'test';
        $options = $this->createOptionsCollectionMock(['hasOption']);
        $generator = $this->createBaseGeneratorMock();
        $grammar = $this->callNonPublicMethod($generator, 'getGrammar');
        $grammar->setOptions($options);
        $options->expects(self::once())
            ->method('hasOption')
            ->with(self::equalTo($name))
            ->willReturn(true);
        self::assertTrue($this->callNonPublicMethod($generator, 'hasOption', [$name]));
    }

    /**
     * Tester of getGrammarOptionsCollection implementation
     *
     * @return void
     * @covers ::getGrammarOptionsCollection
     */
    public function testGetGrammarOptionsCollection(): void
    {
        $options = $this->createOptionsCollectionMock();
        $grammar = $this->createGrammarMock(['getOptions']);
        $grammar->expects(self::once())
            ->method('getOptions')
            ->willReturn($options);
        $generator = $this->createBaseGeneratorMock(['getGrammar']);
        $generator->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        self::assertSame($options, $this->callNonPublicMethod($generator, 'getGrammarOptionsCollection'));
    }

    /**
     * Tester of phpFile getter/setter implementation
     *
     * @return void
     * @covers ::getPhpFile
     * @covers ::setPhpFile
     */
    public function testGetSetPhpFile(): void
    {
        $phpFile = $this->createPhpFile();
        $this->processTestGetSet(BaseGenerator::class, 'phpFile', $phpFile);
    }

    /**
     * Tester of getPhpFile factory implementation
     *
     * @return void
     * @covers ::getPhpFile
     */
    public function testGetPhpFileFactory(): void
    {
        $phpFile = $this->createPhpFile();
        $generator = $this->createBaseGeneratorMock(['createPhpFile']);
        $generator->expects(self::once())
            ->method('createPhpFile')
            ->willReturn($phpFile);
        self::assertSame($phpFile, $this->callNonPublicMethod($generator, 'getPhpFile'));
    }

    /**
     * Tester of printer getter/setter implementation
     *
     * @return void
     * @covers ::getPrinter
     * @covers ::setPrinter
     */
    public function testGetSetPrinter(): void
    {
        $printer = $this->createPrinterMock();
        $this->processTestGetSet(BaseGenerator::class, 'printer', $printer);
    }

    /**
     * Tester of getPrinter factory implementation
     *
     * @return void
     * @covers ::getPrinter
     */
    public function testGetPrinterFactory(): void
    {
        $printer = $this->createPrinterMock();
        $generator = $this->createPartialMock(BaseGenerator::class, ['createPrinter']);
        $generator->expects(self::once())
            ->method('createPrinter')
            ->willReturn($printer);
        self::assertSame($printer, $this->callNonPublicMethod($generator, 'getPrinter'));
    }

    /**
     * Tester of processGrammarOptionMethods implementation
     *
     * @return void
     * @covers ::processGrammarOptionMethods
     */
    public function testProcesGramarOptionMethods(): void
    {
        $name = 'Test';
        $grammarOptions = [
            OptionsCollection::GRAMMAR_OPTION_FOOTER => [],
            OptionsCollection::GRAMMAR_OPTION_HEADER => [],
            OptionsCollection::GRAMMAR_OPTION_INNER => [],
            $name => [],
        ];
        $namespace = $this->createNamespace('Interitty\\Pacc');
        $class = $this->createClassType('Test', $namespace);
        $options = $this->createOptionsCollectionMock(['getGrammarOptions']);
        $options->expects(self::once())->method('getGrammarOptions')->willReturn($grammarOptions);
        $generator = $this->createBaseGeneratorMock(['getGrammarOptionsCollection', 'processGrammarOptionMethod']);
        $generator->expects(self::once())->method('getGrammarOptionsCollection')->willReturn($options);
        $generator->expects(self::once())
            ->method('processGrammarOptionMethod')
            ->with(self::equalTo($class), self::equalTo($name))
            ->willReturn($class->addMethod($name));
        $this->callNonPublicMethod($generator, 'processGrammarOptionMethods', [$class]);
    }

    /**
     * Tester of processInner implementation
     *
     * @return void
     * @covers ::processInner
     */
    public function testProcessInner(): void
    {
        $inner = 'inner';
        $printer = $this->createPrinterMock(['setInner']);
        $class = $this->createClassType();
        $generator = $this->createBaseGeneratorMock(['getPrinter', 'getGrammarOption']);
        $generator->expects(self::once())
            ->method('getPrinter')
            ->willReturn($printer);
        $generator->expects(self::once())
            ->method('getGrammarOption')
            ->with(self::equalTo(OptionsCollection::GRAMMAR_OPTION_INNER), self::equalTo(''))
            ->willReturn($inner);
        $printer->expects(self::once())
            ->method('setInner')
            ->with(self::equalTo($class), self::equalTo($inner));
        $this->callNonPublicMethod($generator, 'processInner', [$class]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseGenerator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return BaseGenerator&MockObject
     */
    protected function createBaseGeneratorMock(array $methods = []): BaseGenerator&MockObject
    {
        $grammar = $this->createGrammarMock();
        $printer = $this->createPrinterMock();
        $mock = $this->createPartialMock(BaseGenerator::class, $methods);
        $this->callNonPublicMethod($mock, 'setPrinter', [$printer]);
        $this->callNonPublicMethod($mock, 'setGrammar', [$grammar]);
        return $mock;
    }

    /**
     * ClassType factory
     *
     * @param string|null $name [OPTIONAL]
     * @param PhpNamespace|null $namespace [OPTIONAL]
     * @return ClassType
     */
    protected function createClassType(string $name = null, PhpNamespace $namespace = null): ClassType
    {
        $classType = new ClassType($name, $namespace);
        return $classType;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * Namespace factory
     *
     * @param string $name
     * @return PhpNamespace
     */
    protected function createNamespace(string $name): PhpNamespace
    {
        $namespace = new PhpNamespace($name);
        return $namespace;
    }

    /**
     * OptionsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OptionsCollection&MockObject
     */
    protected function createOptionsCollectionMock(array $methods = []): OptionsCollection&MockObject
    {
        $mock = $this->createPartialMock(OptionsCollection::class, $methods);
        return $mock;
    }

    /**
     * PhpFile factory
     *
     * @return PhpFile
     */
    protected function createPhpFile(): PhpFile
    {
        $phpFile = new PhpFile();
        return $phpFile;
    }

    /**
     * Printer mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Printer&MockObject
     */
    protected function createPrinterMock(array $methods = []): Printer&MockObject
    {
        $mock = $this->createPartialMock(Printer::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     * @template CollectionType of object
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        /** @var SymbolCollection<CollectionType> $symbolCollection */
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    // </editor-fold>
}
