<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight;

use Generator;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

use function array_merge;
use function count;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Item
 */
class ItemTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of afterDot and beforeDot implementation
     *
     * @return void
     * @group integration
     * @covers ::afterDot
     * @covers ::beforeDot
     */
    public function testAfterBeforeDot(): void
    {
        $beforeDot = [
            $this->createSymbolMock(),
            $this->createSymbolMock(),
        ];
        $afterDot = [
            $this->createSymbolMock(),
            $this->createSymbolMock(),
            $this->createSymbolMock(),
        ];
        $right = array_merge($beforeDot, $afterDot);
        $dot = count($beforeDot);
        $production = $this->createProductionMock(['getRight']);
        $production->expects(self::exactly(2))
            ->method('getRight')
            ->willReturn($right);
        $terminalIndex = 2;
        $item = $this->createItem($production, $dot, $terminalIndex);
        self::assertSame($beforeDot, $item->beforeDot());
        self::assertSame($afterDot, $item->afterDot());
    }

    /**
     * DataProvider for isEqua test
     *
     * @phpstan-return Generator<string, array{0: int, 1: int, 2: bool, 3: int, 4: bool}>
     */
    public function isEqualDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="equal">
        yield 'equal' => [1, 2, true, 1, true];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="non equal terminalIndex">
        yield 'non equal terminalIndex' => [1, 0, true, 0, false];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="non equal dot">
        yield 'non equal dot' => [0, 2, true, 0, false];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="non equal production">
        yield 'non equal production' => [1, 2, false, 1, false];
        // </editor-fold>
    }

    /**
     * Tester of isEqual implementation
     *
     * @param int $dot
     * @param int $terminalIndex
     * @param bool $isEqual
     * @param int $isEqualCount
     * @param bool $result
     * @return void
     * @group integration
     * @dataProvider isEqualDataProvider
     * @covers ::isEqual
     */
    public function testIsEqual(int $dot, int $terminalIndex, bool $isEqual, int $isEqualCount, bool $result): void
    {
        $equalProduction = $this->createProductionMock();
        $equalDot = 1;
        $equalTerminalIndex = 2;

        $production = $this->createProductionMock(['isEqual']);
        $production->expects(self::exactly($isEqualCount))
            ->method('isEqual')
            ->with(self::equalTo($equalProduction))
            ->willReturn($isEqual);

        $item = $this->createItem($production, $dot, $terminalIndex);
        $equalItem = $this->createItem($equalProduction, $equalDot, $equalTerminalIndex);

        self::assertSame($result, $item->isEqual($equalItem));
    }

    /**
     * Tester of item implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getDot
     * @covers ::setDot
     * @covers ::getProduction
     * @covers ::setProduction
     * @covers ::getTerminalIndex
     * @covers ::setTerminalIndex
     */
    public function testItem(): void
    {
        $production = $this->createProductionMock();
        $dot = 1;
        $terminalIndex = 2;
        $item = $this->createItem($production, $dot, $terminalIndex);
        self::assertSame($production, $item->getProduction());
        self::assertSame($dot, $item->getDot());
        self::assertSame($terminalIndex, $item->getTerminalIndex());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of __toString implementation
     *
     * @return void
     * @covers ::__toString
     */
    public function testToString()
    {
        $beforeDot = [
            $this->createNonTerminal('Before1'),
            $this->createNonTerminal('Before2'),
        ];
        $afterDot = [
            $this->createNonTerminal('After1'),
        ];
        $production = $this->createProductionMock(['getLeftName']);
        $production->expects(self::once())->method('getLeftName')->willReturn('leftName');
        $item = $this->createItemMock(['getProduction', 'beforeDot', 'afterDot', 'getTerminalIndex']);
        $item->expects(self::once())->method('getProduction')->willReturn($production);
        $item->expects(self::once())->method('beforeDot')->willReturn($beforeDot);
        $item->expects(self::once())->method('afterDot')->willReturn($afterDot);
        $item->expects(self::once())->method('getTerminalIndex')->willReturn(42);

        $string = '[leftName -> Before1 Before2 . After1, 42]';
        self::assertSame($string, (string) $item);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Item factory
     *
     * @param Production $production
     * @param int $dot
     * @param int $terminalIndex
     * @return Item
     */
    protected function createItem(Production $production, int $dot, int $terminalIndex): Item
    {
        $item = new Item($production, $dot, $terminalIndex);
        return $item;
    }

    /**
     * Item mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Item&MockObject
     */
    protected function createItemMock(array $methods = []): Item&MockObject
    {
        $mock = $this->createPartialMock(Item::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * Symbol mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Symbol&MockObject
     */
    protected function createSymbolMock(array $methods = []): Symbol&MockObject
    {
        $mock = $this->createPartialMock(Symbol::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
