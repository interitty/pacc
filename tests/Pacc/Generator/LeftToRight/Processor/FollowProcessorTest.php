<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\FollowProcessor
 */
class FollowProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of process implementation
     *
     * @return void
     * @group integration
     * @covers ::process
     */
    public function testProcess(): void
    {
        $end = $this->createTerminal('end')->setIndex(2);
        $follow = $this->createSymbolCollectionInteger();
        $production = $this->createProductionMock();
        $productions = $this->createSymbolCollection(Production::class)->add($production);
        $start = $this->createNonTerminal('start')->setFollow($follow);
        $followProcessor = $this->createFollowProcessorMock([
            'getEnd', 'getEpsilonIndex', 'getProductions', 'getStart', 'processProductionIndex', 'processSymbolFollows',
        ]);
        $followProcessor->expects(self::once())->method('getEnd')->willReturn($end);
        $followProcessor->expects(self::once())->method('getEpsilonIndex')->willReturn(1);
        $followProcessor->expects(self::once())->method('getProductions')->willReturn($productions);
        $followProcessor->expects(self::once())->method('getStart')->willReturn($start);

        $followProcessor->expects(self::once())
            ->method('processProductionIndex')
            ->with(self::equalTo($production), self::equalTo(1));
        $followProcessor->expects(self::once())
            ->method('processSymbolFollows')
            ->with(self::equalTo($production), self::equalTo(1))
            ->willReturn(true);

        $followProcessor->process();
    }

    /**
     * Tester of processProductionIndex implementation
     *
     * @return void
     * @group integration
     * @covers ::processProductionIndex
     */
    public function testProcessProductionIndex(): void
    {
        $epsilonIndex = 1;
        $first = $this->createSymbolCollectionInteger()->add($epsilonIndex)->add(2);
        /** @var SymbolCollection<int>&MockObject $follow */
        $follow = $this->createSymbolCollectionMock(['add']);
        $followProcessor = $this->createFollowProcessorMock();
        $nonTerminal = $this->createNonTerminal('nonTerminal')->setFollow($follow);
        $production = $this->createProductionMock();
        $terminal = $this->createTerminal('terminal')->setFirst($first);
        $rights = [
            0 => $nonTerminal,
            1 => $terminal,
            2 => $terminal,
        ];
        $this->callNonPublicMethod($production, 'setRight', [$rights]);
        $follow->expects(self::once())->method('add')->with(self::equalTo(2));

        $this->callNonPublicMethod($followProcessor, 'processProductionIndex', [$production, $epsilonIndex]);
    }

    /**
     * Tester of processSymbolFollows implementation
     *
     * @return void
     * @group integration
     * @covers ::processSymbolFollows
     */
    public function testProcessSymbolFollows(): void
    {
        $epsilonIndex = 1;
        $production = $this->createProductionMock();
        $rights = [
            0 => $this->createNonTerminal('nonTerminal'),
            1 => $this->createTerminal('terminal'),
        ];
        $this->callNonPublicMethod($production, 'setRight', [$rights]);
        $followProcessor = $this->createFollowProcessorMock(['processSymbolFollow']);
        $followProcessor->expects(self::once())
            ->method('processSymbolFollow')
            ->with(self::equalTo($production), self::equalTo($epsilonIndex), self::equalTo(0))
            ->willReturn(true);
        self::assertTrue($this->callNonPublicMethod($followProcessor, 'processSymbolFollows', [
                $production,
                $epsilonIndex,
        ]));
    }

    /**
     * Tester of processSymbolFollow for empty first implementation
     *
     * @return void
     * @group integration
     * @covers ::processSymbolFollow
     */
    public function testProcessSymbolFollowEmpty(): void
    {
        $epsilonIndex = 1;
        $first = $this->createSymbolCollectionInteger();
        $follow = $this->createSymbolCollectionInteger();
        $index = 0;
        $nonTerminal = $this->createNonTerminal('nonTerminal')->setFollow($follow);
        $production = $this->createProductionMock();
        $terminal = $this->createTerminal('terminal')->setFirst($first);
        $rights = [
            0 => $nonTerminal,
            1 => $terminal,
        ];
        $this->callNonPublicMethod($production, 'setLeft', [$nonTerminal]);
        $this->callNonPublicMethod($production, 'setRight', [$rights]);
        $followProcessor = $this->createFollowProcessorMock();
        self::assertTrue($this->callNonPublicMethod($followProcessor, 'processSymbolFollow', [
                $production,
                $epsilonIndex,
                $index,
        ]));
    }

    /**
     * Tester of processSymbolFollow for not empty first implementation
     *
     * @return void
     * @group integration
     * @covers ::processSymbolFollow
     */
    public function testProcessSymbolFollowNotEmpty(): void
    {
        $epsilonIndex = 0;
        $first = $this->createSymbolCollectionInteger()->add($epsilonIndex);
        $follow = $this->createSymbolCollectionInteger();
        $leftFollow = $this->createSymbolCollectionInteger()->add(3);
        $nonTerminal = $this->createNonTerminal('nonTerminal')->setFollow($leftFollow);
        $production = $this->createProductionMock();
        $terminal = $this->createTerminal('terminal')->setFirst($first)->setFollow($follow);
        $rights = [
            0 => $nonTerminal,
            1 => $terminal,
        ];
        $this->callNonPublicMethod($production, 'setLeft', [$nonTerminal]);
        $this->callNonPublicMethod($production, 'setRight', [$rights]);
        $followProcessor = $this->createFollowProcessorMock();
        self::assertFalse($this->callNonPublicMethod($followProcessor, 'processSymbolFollow', [
                $production,
                $epsilonIndex,
                1,
        ]));
        self::assertTrue($leftFollow->contains(3));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FollowProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FollowProcessor&MockObject
     */
    protected function createFollowProcessorMock(array $methods = []): FollowProcessor&MockObject
    {
        $mock = $this->createPartialMock(FollowProcessor::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * Symbol collection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * Symbol collection of integers factory
     *
     * @return SymbolCollection<int>
     */
    protected function createSymbolCollectionInteger(): SymbolCollection
    {
        /** @var SymbolCollection<int> $collection */
        $collection = $this->createSymbolCollection(FollowProcessor::COLLECTION_INTEGER);
        return $collection;
    }

    /**
     * SymbolCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return SymbolCollection<mixed>&MockObject
     */
    protected function createSymbolCollectionMock(array $methods = []): SymbolCollection&MockObject
    {
        $mock = $this->createPartialMock(SymbolCollection::class, $methods);
        return $mock;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    protected function createTerminal(string $name, ?string $type = null, ?string $value = null)
    {
        $terminal = new Terminal($name, $type, $value);
        return $terminal;
    }

    // </editor-fold>
}
