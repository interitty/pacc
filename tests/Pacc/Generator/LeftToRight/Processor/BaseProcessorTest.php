<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Output\OutputInterface;

use function array_merge;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\BaseProcessor
 */
class BaseProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of constructor implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getGrammar
     * @covers ::setGrammar
     */
    public function testConstructor(): void
    {
        $grammar = $this->createGrammarMock();
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($grammar, $this->callNonPublicMethod($baseProcessor, 'getGrammar'));
    }

    /**
     * Tester of createCollection implementation
     *
     * @return void
     * @group integration
     * @covers ::createCollection
     */
    public function testCreateCollection(): void
    {
        $type = 'type';
        $baseProcessor = $this->createBaseProcessorMock();
        $collection = $this->callNonPublicMethod($baseProcessor, 'createCollection', [$type]);
        self::assertSame($type, $collection->getType());
    }

    /**
     * Tester of createCollectionInteger implementation
     *
     * @return void
     * @group integration
     * @covers ::createCollectionInteger
     */
    public function testCreateCollectionInteger(): void
    {
        $baseProcessor = $this->createBaseProcessorMock();
        $collection = $this->callNonPublicMethod($baseProcessor, 'createCollectionInteger');
        self::assertSame(BaseProcessor::COLLECTION_INTEGER, $collection->getType());
    }

    /**
     * Tester of createItem implementation
     *
     * @return void
     * @group integration
     * @covers ::createItem
     */
    public function testCreateItem(): void
    {
        $production = $this->createProductionMock();
        $dot = 1;
        $terminalIndex = 2;
        $baseProcessor = $this->createBaseProcessorMock();
        $item = $this->callNonPublicMethod($baseProcessor, 'createItem', [$production, $dot, $terminalIndex]);
        self::assertSame($production, $item->getProduction());
        self::assertSame($dot, $item->getDot());
        self::assertSame($terminalIndex, $item->getTerminalIndex());
    }

    /**
     * Tester of getEnd and setEnd implementation
     *
     * @return void
     * @group integration
     * @covers ::getEnd
     * @covers ::setEnd
     */
    public function testGetSetEnd(): void
    {
        $terminal = $this->createTerminalMock();
        $grammar = $this->createGrammarMock();
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($baseProcessor, $this->callNonPublicMethod($baseProcessor, 'setEnd', [$terminal]));
        self::assertSame($terminal, $this->callNonPublicMethod($baseProcessor, 'getEnd'));
    }

    /**
     * Tester of getEpsilonIndex and setEpsilon implementation
     *
     * @return void
     * @group integration
     * @covers ::getEpsilonIndex
     * @covers ::setEpsilon
     */
    public function testGetSetEpsilon(): void
    {
        $epsilonIndex = 1;
        $terminal = $this->createTerminalMock(['getIndex']);
        $terminal->expects(self::once())
            ->method('getIndex')
            ->willReturn($epsilonIndex);
        $grammar = $this->createGrammarMock();
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($baseProcessor, $this->callNonPublicMethod($baseProcessor, 'setEpsilon', [$terminal]));
        self::assertSame($epsilonIndex, $this->callNonPublicMethod($baseProcessor, 'getEpsilonIndex'));
    }

    /**
     * Tester of getProductions implementation
     *
     * @return void
     * @group integration
     * @covers ::getProductions
     */
    public function testGetProductions(): void
    {
        $productions = $this->createSymbolCollection(Production::class);
        $grammar = $this->createGrammarMock(['getProductions']);
        $grammar->expects(self::once())
            ->method('getProductions')
            ->willReturn($productions);
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($productions, $this->callNonPublicMethod($baseProcessor, 'getProductions'));
    }

    /**
     * Tester of getNonTerminals implementation
     *
     * @return void
     * @group integration
     * @covers ::getNonTerminals
     */
    public function testGetNonTerminals(): void
    {
        $nonTerminals = $this->createSymbolCollection(NonTerminal::class);
        $grammar = $this->createGrammarMock(['getNonTerminals']);
        $grammar->expects(self::once())
            ->method('getNonTerminals')
            ->willReturn($nonTerminals);
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($nonTerminals, $this->callNonPublicMethod($baseProcessor, 'getNonTerminals'));
    }

    /**
     * Tester of getStart and setStart implementation
     *
     * @return void
     * @group integration
     * @covers ::getStart
     * @covers ::setStart
     */
    public function testGetSetStart(): void
    {
        $nonTerminal = $this->createNonTerminalMock();
        $grammar = $this->createGrammarMock();
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($baseProcessor, $this->callNonPublicMethod($baseProcessor, 'setStart', [$nonTerminal]));
        self::assertSame($nonTerminal, $this->callNonPublicMethod($baseProcessor, 'getStart'));
    }

    /**
     * Tester of getStartProduction and setStartProduction implementation
     *
     * @return void
     * @group integration
     * @covers ::getStartProduction
     * @covers ::setStartProduction
     */
    public function testGetSetStartProduction(): void
    {
        $production = $this->createProductionMock();
        $grammar = $this->createGrammarMock();
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        $this->callNonPublicMethod($baseProcessor, 'setStartProduction', [$production]);
        self::assertSame($production, $this->callNonPublicMethod($baseProcessor, 'getStartProduction'));
    }

    /**
     * Tester of getTerminals implementation
     *
     * @return void
     * @group integration
     * @covers ::getTerminals
     */
    public function testGetTerminals(): void
    {
        $terminals = $this->createSymbolCollection(Terminal::class);
        $grammar = $this->createGrammarMock(['getTerminals']);
        $grammar->expects(self::once())
            ->method('getTerminals')
            ->willReturn($terminals);
        $baseProcessor = $this->createBaseProcessorMock();
        $this->callNonPublicMethod($baseProcessor, '__construct', [$grammar]);
        self::assertSame($terminals, $this->callNonPublicMethod($baseProcessor, 'getTerminals'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of write implementation
     *
     * @return void
     * @covers ::write
     */
    public function testWrite(): void
    {
        $messages = 'test';
        $newLine = true;
        $options = OutputInterface::VERBOSITY_VERBOSE;
        $output = $this->createOutputMock();
        $output->expects(self::once())
            ->method('write')
            ->with(self::equalTo($messages), self::equalTo($newLine), self::equalTo($options));
        $grammar = $this->createGrammarMock(['getOutput']);
        $grammar->expects(self::once())
            ->method('getOutput')
            ->willReturn($output);
        $processor = $this->createBaseProcessorMock(['getGrammar']);
        $processor->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        $this->callNonPublicMethod($processor, 'write', [$messages, $newLine, $options]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return BaseProcessor&MockObject
     */
    protected function createBaseProcessorMock(array $methods = []): BaseProcessor&MockObject
    {
        $mockMethods = array_merge($methods, ['process']);
        $mock = $this->createPartialMock(BaseProcessor::class, $mockMethods);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * Output mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OutputInterface&MockObject
     */
    protected function createOutputMock(array $methods = []): OutputInterface&MockObject
    {
        $mockMethods = array_merge($methods, [
            'write',
            'writeln',
            'setVerbosity',
            'getVerbosity',
            'isQuiet',
            'isVerbose',
            'isVeryVerbose',
            'isDebug',
            'setDecorated',
            'isDecorated',
            'setFormatter',
            'getFormatter',
        ]);
        $mock = $this->createPartialMock(OutputInterface::class, $mockMethods);
        return $mock;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     * @template CollectionType of object
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        /** @var SymbolCollection<CollectionType> $symbolCollection */
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
