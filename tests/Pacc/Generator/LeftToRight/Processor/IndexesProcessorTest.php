<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\IndexesProcessor
 */
class IndexesProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of processSymbolsIndex for empty collections implementation
     *
     * @return void
     * @group integration
     * @covers ::processSymbolsIndex
     */
    public function testProcessSymbolsIndexEmpty(): void
    {
        $end = $this->createTerminal('end');
        $terminals = $this->createSymbolCollection(Terminal::class);
        $nonTerminals = $this->createSymbolCollection(NonTerminal::class);
        $indexesProcessor = $this->createIndexesProcessorMock(
            ['getEnd', 'getNonTerminals', 'setTablePitch', 'getTerminals']
        );
        $indexesProcessor->expects(self::once())->method('getEnd')->willReturn($end);
        $indexesProcessor->expects(self::once())->method('getNonTerminals')->willReturn($nonTerminals);
        $indexesProcessor->expects(self::once())->method('getTerminals')->willReturn($terminals);
        $indexesProcessor->expects(self::once())->method('setTablePitch')->with(self::equalTo(0));
        $this->callNonPublicMethod($indexesProcessor, 'processSymbolsIndex');
    }

    /**
     * Tester of processSymbolsIndex for NonTerminals collections implementation
     *
     * @return void
     * @group integration
     * @covers ::processSymbolsIndex
     */
    public function testProcessSymbolsIndexNonTerminals(): void
    {
        $end = $this->createTerminal('end');
        $nonTerminal = $this->createNonTerminal('nonTerminal');
        $terminals = $this->createSymbolCollection(Terminal::class);
        $nonTerminals = $this->createSymbolCollection(NonTerminal::class);
        $nonTerminals->addCollection([$nonTerminal]);
        $indexesProcessor = $this->createIndexesProcessorMock(
            ['getEnd', 'getNonTerminals', 'setTablePitch', 'getTerminals']
        );
        $indexesProcessor->expects(self::once())->method('getEnd')->willReturn($end);
        $indexesProcessor->expects(self::once())->method('getNonTerminals')->willReturn($nonTerminals);
        $indexesProcessor->expects(self::once())->method('getTerminals')->willReturn($terminals);
        $indexesProcessor->expects(self::once())->method('setTablePitch')->with(self::equalTo($nonTerminals->count()));
        $this->callNonPublicMethod($indexesProcessor, 'processSymbolsIndex');

        $first = $nonTerminal->getFirst();
        $follow = $nonTerminal->getFirst();
        self::assertEquals(1, $nonTerminal->getIndex());
        self::assertSame(IndexesProcessor::COLLECTION_INTEGER, $first->getType());
        self::assertSame(IndexesProcessor::COLLECTION_INTEGER, $follow->getType());
    }

    /**
     * Tester of processSymbolsIndex for Terminals collections implementation
     *
     * @return void
     * @group integration
     * @covers ::processSymbolsIndex
     */
    public function testProcessSymbolsIndexTerminals(): void
    {
        $end = $this->createTerminal('end');
        $terminal = $this->createTerminal('terminal');
        $terminals = $this->createSymbolCollection(Terminal::class);
        $terminals->addCollection([$terminal]);
        $nonTerminals = $this->createSymbolCollection(NonTerminal::class);
        $indexesProcessor = $this->createIndexesProcessorMock(
            ['getEnd', 'getNonTerminals', 'setTablePitch', 'getTerminals']
        );
        $indexesProcessor->expects(self::once())->method('getEnd')->willReturn($end);
        $indexesProcessor->expects(self::once())->method('getNonTerminals')->willReturn($nonTerminals);
        $indexesProcessor->expects(self::once())->method('getTerminals')->willReturn($terminals);
        $indexesProcessor->expects(self::once())->method('setTablePitch')->with(self::equalTo($terminals->count()));
        $this->callNonPublicMethod($indexesProcessor, 'processSymbolsIndex');

        $index = $terminal->getIndex();
        $first = $terminal->getFirst();
        self::assertEquals(1, $index);
        self::assertSame(IndexesProcessor::COLLECTION_INTEGER, $first->getType());
        self::assertTrue($first->contains($index));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of getTablePitch setTablePich implementation
     *
     * @return void
     * @covers ::getTablePitch
     * @covers ::setTablePitch
     */
    public function testGetSetTablePitch(): void
    {
        $tablePitch = 1;
        $this->processTestGetSet(IndexesProcessor::class, 'tablePitch', $tablePitch);
    }

    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $indexesProcessor = $this->createIndexesProcessorMock(['processSymbolsIndex', 'processProductionsIndex']);
        $indexesProcessor->expects(self::once())
            ->method('processSymbolsIndex');
        $indexesProcessor->expects(self::once())
            ->method('processProductionsIndex');
        $indexesProcessor->process();
    }

    /**
     * Tester of processProductionsIndex implementation
     *
     * @return void
     * @covers ::processProductionsIndex
     */
    public function testProcessProductionsIndex(): void
    {
        $productionOne = $this->createProductionMock(['setIndex']);
        $productionOne->expects(self::once())
            ->method('setIndex')
            ->with(self::equalTo(1));
        $productionTwo = $this->createProductionMock(['setIndex']);
        $productionTwo->expects(self::once())
            ->method('setIndex')
            ->with(self::equalTo(2));
        $productions = $this->createSymbolCollection(Production::class);
        $this->setNonPublicPropertyValue($productions, 'data', [$productionOne, $productionTwo]);
        $indexesProcessor = $this->createIndexesProcessorMock(['getProductions']);
        $indexesProcessor->expects(self::once())
            ->method('getProductions')
            ->willReturn($productions);
        $this->callNonPublicMethod($indexesProcessor, 'processProductionsIndex');
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * IndexesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return IndexesProcessor&MockObject
     */
    protected function createIndexesProcessorMock(array $methods = []): IndexesProcessor&MockObject
    {
        $mock = $this->createPartialMock(IndexesProcessor::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * Symbol collection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    protected function createTerminal(string $name, ?string $type = null, ?string $value = null)
    {
        $terminal = new Terminal($name, $type, $value);
        return $terminal;
    }

    // </editor-fold>
}
