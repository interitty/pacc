<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Generator;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\FirstProcessor
 */
class FirstProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $epsilonIndex = 1;
        $production = $this->createProductionMock();
        $productions = $this->createSymbolCollectionMock();
        $this->setNonPublicPropertyValue($productions, 'data', [$production]);
        $firstProcesor = $this->createFirstProcessorMock(
            ['getEpsilonIndex', 'getProductions', 'processAddFirstEpsilonIndex', 'processProductionIndex']
        );
        $firstProcesor->expects(self::once())->method('getEpsilonIndex')->willReturn($epsilonIndex);
        $firstProcesor->expects(self::once())->method('getProductions')->willReturn($productions);
        $firstProcesor->expects(self::once())
            ->method('processAddFirstEpsilonIndex')
            ->with(self::equalTo($productions), self::equalTo($epsilonIndex));
        $firstProcesor->expects(self::once())
            ->method('processProductionIndex')
            ->with(self::equalTo($production), self::equalTo($epsilonIndex))
            ->willReturn(true);

        $firstProcesor->process();
    }

    /**
     * Tester of processAddFirstEpsilonIndex implementation
     *
     * @return void
     * @covers ::processAddFirstEpsilonIndex
     */
    public function testProcessAddFirstEpsilonIndex(): void
    {
        $epsilonIndex = 1;
        $nonTerminals = $this->createSymbolCollectionMock(['add']);
        $nonTerminals->expects(self::once())
            ->method('add')
            ->with(self::equalTo($epsilonIndex));
        $nonTerminal = $this->createNonTerminalMock(['getFirst']);
        $nonTerminal->expects(self::once())
            ->method('getFirst')
            ->willReturn($nonTerminals);
        $production = $this->createProductionMock(['getRightCount', 'getLeft']);
        $production->expects(self::once())
            ->method('getRightCount')
            ->willReturn(0);
        $production->expects(self::once())
            ->method('getLeft')
            ->willReturn($nonTerminal);
        $productions = $this->createSymbolCollectionMock();
        $this->setNonPublicPropertyValue($productions, 'data', [$production]);

        $firstProcesor = $this->createFirstProcessorMock();
        $this->callNonPublicMethod($firstProcesor, 'processAddFirstEpsilonIndex', [$productions, $epsilonIndex]);
    }

    /**
     * Dataprovider for processProductionIndex test
     *
     * @phpstan-return Generator<string, array{0: int[], 1: int[], 2: int, 3: bool}>
     */
    public function processProductionIndexDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Contain epsilon in left">
        yield 'Contain epsilon in left' => [[1], [1], 1, true];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Contain epsilon in right">
        yield 'Contain epsilon in right' => [[], [1], 1, true];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Missing epsilon in right">
        yield 'Missing epsilon in right' => [[], [2], 1, false];
        // </editor-fold>
    }

    /**
     * Tester of processProductionIndex implementation
     *
     * @param int[] $left
     * @param int[] $right
     * @param int $epsilonIndex
     * @param bool $result
     * @return void
     * @dataProvider processProductionIndexDataProvider
     * @covers ::processProductionIndex
     */
    public function testProcessProductionIndex(array $left, array $right, int $epsilonIndex, bool $result): void
    {
        $firstProcesor = $this->createFirstProcessorMock();
        $leftFirstProduction = $this->createSymbolCollectionMock();
        $leftSymbol = $this->createNonTerminalMock();
        $production = $this->createProductionMock();
        $rightFirstSymbol = $this->createSymbolCollectionMock();
        $rightSymbol = $this->createNonTerminalMock();

        $this->setNonPublicPropertyValue($leftFirstProduction, 'data', $left);
        $this->setNonPublicPropertyValue($leftFirstProduction, 'type', FirstProcessor::COLLECTION_INTEGER);
        $this->setNonPublicPropertyValue($leftSymbol, 'first', $leftFirstProduction);
        $this->setNonPublicPropertyValue($production, 'left', $leftSymbol);
        $this->setNonPublicPropertyValue($production, 'right', [$rightSymbol]);
        $this->setNonPublicPropertyValue($rightFirstSymbol, 'data', $right);
        $this->setNonPublicPropertyValue($rightSymbol, 'first', $rightFirstSymbol);

        $done = $this->callNonPublicMethod(
            $firstProcesor,
            'processProductionIndex',
            [$production, $epsilonIndex, true]
        );
        self::assertSame($result, $done);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FirstProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FirstProcessor&MockObject
     */
    protected function createFirstProcessorMock(array $methods = []): FirstProcessor&MockObject
    {
        $mock = $this->createPartialMock(FirstProcessor::class, $methods);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return SymbolCollection<Production|NonTerminal|Terminal>&MockObject
     */
    protected function createSymbolCollectionMock(array $methods = []): SymbolCollection&MockObject
    {
        $mock = $this->createPartialMock(SymbolCollection::class, $methods);
        return $mock;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
