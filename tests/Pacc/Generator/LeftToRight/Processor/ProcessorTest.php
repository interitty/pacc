<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Grammar;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Utils\Strings;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Output\OutputInterface as Output;

use function get_class;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\Processor
 */
class ProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $init = $this->createInitProcessorMock();
        $indexes = $this->createIndexesProcessorMock();
        $first = $this->createFirstProcessorMock();
        $follow = $this->createFollowProcessorMock();
        $states = $this->createStatesProcessorMock();
        $table = $this->createTableProcessorMock();
        $processor = $this->createProcessorMock(['getGrammar', 'processRunProcessor']);
        $this->setNonPublicPropertyValue($processor, 'initProcessor', $init);
        $this->setNonPublicPropertyValue($processor, 'indexesProcessor', $indexes);
        $this->setNonPublicPropertyValue($processor, 'firstProcessor', $first);
        $this->setNonPublicPropertyValue($processor, 'followProcessor', $follow);
        $this->setNonPublicPropertyValue($processor, 'statesProcessor', $states);
        $this->setNonPublicPropertyValue($processor, 'tableProcessor', $table);
        $consecutive = static fn(BaseProcessor $baseProcessor) => match ([$baseProcessor]) { // @phpstan-ignore-line
                [$init], [$indexes], [$first], [$follow], [$states], [$table] => $processor
        };
        $processor->expects(self::exactly(6))->method('processRunProcessor')->willReturnCallback($consecutive);
        $processor->process();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of createFirstProcessor implementation
     *
     * @return void
     * @covers ::createFirstProcessor
     */
    public function testCreateFirstProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $processor = $this->createProcessorMock(['getGrammar']);
        $processor->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $firstProcessor = $this->callNonPublicMethod($processor, 'createFirstProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($firstProcessor, 'getGrammar'));
    }

    /**
     * Tester of createFollowProcessor implementation
     *
     * @return void
     * @covers ::createFollowProcessor
     */
    public function testCreateFollowProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $processor = $this->createProcessorMock(['getGrammar']);
        $processor->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $followProcessor = $this->callNonPublicMethod($processor, 'createFollowProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($followProcessor, 'getGrammar'));
    }

    /**
     * Tester of createIndexesProcessor implementation
     *
     * @return void
     * @covers ::createIndexesProcessor
     */
    public function testCreateIndexesProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $processor = $this->createProcessorMock(['getGrammar']);
        $processor->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $indexesProcessor = $this->callNonPublicMethod($processor, 'createIndexesProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($indexesProcessor, 'getGrammar'));
    }

    /**
     * Tester of createInitProcessor implementation
     *
     * @return void
     * @covers ::createInitProcessor
     */
    public function testCreateInitProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $processor = $this->createProcessorMock(['getGrammar']);
        $processor->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $initProcessor = $this->callNonPublicMethod($processor, 'createInitProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($initProcessor, 'getGrammar'));
    }

    /**
     * Tester of createStatesProcessor implementation
     *
     * @return void
     * @covers ::createStatesProcessor
     */
    public function testCreateStatesProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $processor = $this->createProcessorMock(['getGrammar']);
        $processor->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $statesProcessor = $this->callNonPublicMethod($processor, 'createStatesProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($statesProcessor, 'getGrammar'));
    }

    /**
     * Tester of createTableProcessor implementation
     *
     * @return void
     * @covers ::createTableProcessor
     */
    public function testCreateTableProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $indexesProcessor = $this->createIndexesProcessorMock();
        $statesProcessor = $this->createStatesProcessorMock();
        $processor = $this->createProcessorMock(['getGrammar', 'getIndexesProcessor', 'getStatesProcessor']);
        $processor->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $processor->expects(self::once())->method('getIndexesProcessor')->willReturn($indexesProcessor);
        $processor->expects(self::once())->method('getStatesProcessor')->willReturn($statesProcessor);
        $tableProcessor = $this->callNonPublicMethod($processor, 'createTableProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($tableProcessor, 'getGrammar'));
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($tableProcessor, 'getIndexesProcessor'));
        self::assertSame($statesProcessor, $this->callNonPublicMethod($tableProcessor, 'getStatesProcessor'));
    }

    /**
     * Tester of getFirstProcessor and setFirstProcessor implementation
     *
     * @return void
     * @covers ::getFirstProcessor
     * @covers ::setFirstProcessor
     */
    public function testGetSetFirstProcessor(): void
    {
        $firstProcessor = $this->createFirstProcessorMock();
        $this->processTestGetSet(Processor::class, 'firstProcessor', $firstProcessor);
    }

    /**
     * Tester of getFirstProcessor factory implementation
     *
     * @return void
     * @covers ::getFirstProcessor
     */
    public function testGetFirstProcessorFactory(): void
    {
        $firstProcessor = $this->createFirstProcessorMock();
        $processor = $this->createProcessorMock(['createFirstProcessor']);
        $processor->expects(self::once())
            ->method('createFirstProcessor')
            ->willReturn($firstProcessor);
        self::assertSame($firstProcessor, $this->callNonPublicMethod($processor, 'getFirstProcessor'));
    }

    /**
     * Tester of getFollowProcessor and setFollowProcessor implementation
     *
     * @return void
     * @covers ::getFollowProcessor
     * @covers ::setFollowProcessor
     */
    public function testGetSetFollowProcessor(): void
    {
        $followProcessor = $this->createFollowProcessorMock();
        $this->processTestGetSet(Processor::class, 'followProcessor', $followProcessor);
    }

    /**
     * Tester of getFollowProcessor factory implementation
     *
     * @return void
     * @covers ::getFollowProcessor
     */
    public function testGetFollowProcessorFactory(): void
    {
        $followProcessor = $this->createFollowProcessorMock();
        $processor = $this->createProcessorMock(['createFollowProcessor']);
        $processor->expects(self::once())
            ->method('createFollowProcessor')
            ->willReturn($followProcessor);
        self::assertSame($followProcessor, $this->callNonPublicMethod($processor, 'getFollowProcessor'));
    }

    /**
     * Tester of getIndexesProcessor and setIndexesProcessor implementation
     *
     * @return void
     * @covers ::getIndexesProcessor
     * @covers ::setIndexesProcessor
     */
    public function testGetSetIndexesProcessor(): void
    {
        $indexesProcessor = $this->createIndexesProcessorMock();
        $this->processTestGetSet(Processor::class, 'indexesProcessor', $indexesProcessor);
    }

    /**
     * Tester of getIndexesProcessor factory implementation
     *
     * @return void
     * @covers ::getIndexesProcessor
     */
    public function testGetIndexesProcessorFactory(): void
    {
        $indexesProcessor = $this->createIndexesProcessorMock();
        $processor = $this->createProcessorMock(['createIndexesProcessor']);
        $processor->expects(self::once())
            ->method('createIndexesProcessor')
            ->willReturn($indexesProcessor);
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($processor, 'getIndexesProcessor'));
    }

    /**
     * Tester of getInitProcessor and setInitProcessor implementation
     *
     * @return void
     * @covers ::getInitProcessor
     * @covers ::setInitProcessor
     */
    public function testGetSetInitProcessor(): void
    {
        $initProcessor = $this->createInitProcessorMock();
        $this->processTestGetSet(Processor::class, 'initProcessor', $initProcessor);
    }

    /**
     * Tester of getInitProcessor factory implementation
     *
     * @return void
     * @covers ::getInitProcessor
     */
    public function testGetInitProcessorFactory(): void
    {
        $initProcessor = $this->createInitProcessorMock();
        $processor = $this->createProcessorMock(['createInitProcessor']);
        $processor->expects(self::once())
            ->method('createInitProcessor')
            ->willReturn($initProcessor);
        self::assertSame($initProcessor, $this->callNonPublicMethod($processor, 'getInitProcessor'));
    }

    /**
     * Tester of getStatesProcessor and setStatesProcessor implementation
     *
     * @return void
     * @covers ::getStatesProcessor
     * @covers ::setStatesProcessor
     */
    public function testGetSetStatesProcessor(): void
    {
        $statesProcessor = $this->createStatesProcessorMock();
        $this->processTestGetSet(Processor::class, 'statesProcessor', $statesProcessor);
    }

    /**
     * Tester of getStatesProcessor factory implementation
     *
     * @return void
     * @covers ::getStatesProcessor
     */
    public function testGetStatesProcessorFactory(): void
    {
        $statesProcessor = $this->createStatesProcessorMock();
        $processor = $this->createProcessorMock(['createStatesProcessor']);
        $processor->expects(self::once())
            ->method('createStatesProcessor')
            ->willReturn($statesProcessor);
        self::assertSame($statesProcessor, $this->callNonPublicMethod($processor, 'getStatesProcessor'));
    }

    /**
     * Tester of getTable implementation
     *
     * @return void
     * @covers ::getTable
     */
    public function testGetTable(): void
    {
        $table = [1, 2, 3];
        $tableProcessor = $this->createTableProcessorMock(['getTable']);
        $tableProcessor->expects(self::once())->method('getTable')->willReturn($table);
        $processor = $this->createProcessorMock(['getTableProcessor']);
        $processor->expects(self::once())->method('getTableProcessor')->willReturn($tableProcessor);
        self::assertSame($table, $processor->getTable());
    }

    /**
     * Tester of getTablePitch implementation
     *
     * @return void
     * @covers ::getTablePitch
     */
    public function testGetTablePitch(): void
    {
        $tablePitch = 1;
        $indexesProcessor = $this->createIndexesProcessorMock(['getTablePitch']);
        $indexesProcessor->expects(self::once())->method('getTablePitch')->willReturn($tablePitch);
        $processor = $this->createProcessorMock(['getIndexesProcessor']);
        $processor->expects(self::once())->method('getIndexesProcessor')->willReturn($indexesProcessor);
        self::assertSame($tablePitch, $processor->getTablePitch());
    }

    /**
     * Tester of getTableProcessor and setTableProcessor implementation
     *
     * @return void
     * @covers ::getTableProcessor
     * @covers ::setTableProcessor
     */
    public function testGetSetTableProcessor(): void
    {
        $tableProcessor = $this->createTableProcessorMock();
        $this->processTestGetSet(Processor::class, 'tableProcessor', $tableProcessor);
    }

    /**
     * Tester of getTableProcessor factory implementation
     *
     * @return void
     * @covers ::getTableProcessor
     */
    public function testGetTableProcessorFactory(): void
    {
        $tableProcessor = $this->createTableProcessorMock();
        $processor = $this->createProcessorMock(['createTableProcessor']);
        $processor->expects(self::once())
            ->method('createTableProcessor')
            ->willReturn($tableProcessor);
        self::assertSame($tableProcessor, $this->callNonPublicMethod($processor, 'getTableProcessor'));
    }

    /**
     * Tester of processRunProcessor implementation
     *
     * @return void
     * @covers ::processRunProcessor
     */
    public function testProcessRunProcessor(): void
    {
        $initProcessor = $this->createInitProcessorMock(['process']);
        $initProcessor->expects(self::once())
            ->method('process');
        $processorName = (string) Strings::after(get_class($initProcessor), '\\', -1);
        $processor = $this->createProcessorMock(['write']);
        $consecutive = static fn($messages, bool $newLine = true, int $options = 0)
            => match ([$messages, $newLine, $options]) { // @phpstan-ignore-line
                [$processorName, false, Output::VERBOSITY_VERBOSE] => $processor,
                [' <info>Done</info>', true, Output::VERBOSITY_VERBOSE] => $processor,
            };
        $processor->expects(self::exactly(2))->method('write')->willReturnCallback($consecutive);
        $this->callNonPublicMethod($processor, 'processRunProcessor', [$initProcessor]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FirstProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FirstProcessor&MockObject
     */
    protected function createFirstProcessorMock(array $methods = []): FirstProcessor&MockObject
    {
        $mock = $this->createPartialMock(FirstProcessor::class, $methods);
        return $mock;
    }

    /**
     * FollowProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FollowProcessor&MockObject
     */
    protected function createFollowProcessorMock(array $methods = []): FollowProcessor&MockObject
    {
        $mock = $this->createPartialMock(FollowProcessor::class, $methods);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * IndexesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return IndexesProcessor&MockObject
     */
    protected function createIndexesProcessorMock(array $methods = []): IndexesProcessor&MockObject
    {
        $mock = $this->createPartialMock(IndexesProcessor::class, $methods);
        return $mock;
    }

    /**
     * InitProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return InitProcessor&MockObject
     */
    protected function createInitProcessorMock(array $methods = []): InitProcessor&MockObject
    {
        $mock = $this->createPartialMock(InitProcessor::class, $methods);
        return $mock;
    }

    /**
     * Processor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Processor&MockObject
     */
    protected function createProcessorMock(array $methods = []): Processor&MockObject
    {
        $mock = $this->createPartialMock(Processor::class, $methods);
        return $mock;
    }

    /**
     * StatesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StatesProcessor&MockObject
     */
    protected function createStatesProcessorMock(array $methods = []): StatesProcessor&MockObject
    {
        $mock = $this->createPartialMock(StatesProcessor::class, $methods);
        return $mock;
    }

    /**
     * TableProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableProcessor&MockObject
     */
    protected function createTableProcessorMock(array $methods = []): TableProcessor&MockObject
    {
        $mock = $this->createPartialMock(TableProcessor::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
