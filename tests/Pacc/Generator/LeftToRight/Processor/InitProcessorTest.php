<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\InitProcessor
 */
class InitProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of createNonTerminal implementation
     *
     * @return void
     * @group integration
     * @covers ::createNonTerminal
     */
    public function testCreateNonTerminal(): void
    {
        $name = 'name';
        $initProcessor = $this->createInitProcessorMock();
        $nonTerminal = $this->callNonPublicMethod($initProcessor, 'createNonTerminal', [$name]);
        self::assertSame($name, $nonTerminal->getName());
    }

    /**
     * Tester of createProduction implementation
     *
     * @return void
     * @group integration
     * @covers ::createProduction
     */
    public function testCreateProduction(): void
    {
        $left = $this->createNonTerminalMock();
        $right = [];
        $code = 'code';
        $initProcessor = $this->createInitProcessorMock();
        $production = $this->callNonPublicMethod($initProcessor, 'createProduction', [$left, $right, $code]);
        self::assertSame($left, $production->getLeft());
        self::assertSame($right, $production->getRight());
        self::assertSame($code, $production->getCode());
    }

    /**
     * Tester of createTerminal implementation
     *
     * @return void
     * @group integration
     * @covers ::createTerminal
     */
    public function testCreateTerminal(): void
    {
        $name = 'name';
        $type = 'type';
        $value = 'value';
        $initProcessor = $this->createInitProcessorMock();
        $terminal = $this->callNonPublicMethod($initProcessor, 'createTerminal', [$name, $type, $value]);
        self::assertSame($name, $terminal->getName());
        self::assertSame($type, $terminal->getType());
        self::assertSame($value, $terminal->getValue());
    }

    /**
     * Tester of process implementation
     *
     * @return void
     * @group integration
     * @covers ::process
     */
    public function testProcess(): void
    {
        $start = $this->createNonTerminalMock();
        $processor = $this->createInitProcessorMock();
        $this->callNonPublicMethod($processor, 'setStart', [$start]);

        $processor->process();

        $newStart = $this->callNonPublicMethod($processor, 'getStart');
        $startProduction = $this->callNonPublicMethod($processor, 'getStartProduction');
        self::assertSame($newStart, $startProduction->getLeft());
        self::assertSame([$start], $startProduction->getRight());
        self::assertTrue($this->callNonPublicMethod($processor, 'getNonTerminals')->contains($newStart));
        self::assertSame('$start', $newStart->getName());

        $epsilon = $this->callNonPublicMethod($processor, 'getGrammar')->getEpsilon();
        self::assertSame('$epsilon', $epsilon->getName());
        self::assertSame(-1, $epsilon->getIndex());

        $end = $this->callNonPublicMethod($processor, 'getEnd');
        self::assertSame('$end', $end->getName());
        self::assertSame(0, $end->getIndex());
        $endCollection = $end->getFirst();
        self::assertSame(InitProcessor::COLLECTION_INTEGER, $endCollection->getType());
        self::assertTrue($endCollection->contains(0));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * InitProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return InitProcessor&MockObject
     */
    protected function createInitProcessorMock(array $methods = []): InitProcessor&MockObject
    {
        $grammar = $this->createGrammarMock();
        $mock = $this->createPartialMock(InitProcessor::class, $methods);
        $this->callNonPublicMethod($mock, '__construct', [$grammar]);
        return $mock;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
