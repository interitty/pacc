<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Generator\LeftToRight\Jump;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

use function current;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\StatesProcessor
 */
class StatesProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of createJump implementation
     *
     * @return void
     * @group integration
     * @covers ::createJump
     */
    public function testCreateJump(): void
    {
        $itemFrom = $this->createSymbolCollection(Item::class);
        $itemTo = $this->createSymbolCollection(Item::class);
        $symbol = $this->createSymbolMock();
        $processor = $this->createStatesProcessorMock();
        $jump = $this->callNonPublicMethod($processor, 'createJump', [$itemFrom, $symbol, $itemTo]);
        self::assertSame($itemFrom, $jump->getItemFrom());
        self::assertSame($itemTo, $jump->getItemTo());
        self::assertSame($symbol, $jump->getSymbol());
    }

    /**
     * Tester of addJumps, getJumps and setJumps implementation
     *
     * @return void
     * @group integration
     * @covers ::addJump
     * @covers ::getJumps
     * @covers ::setJumps
     */
    public function testGetSetJump(): void
    {
        $jumps = [
            $this->createJumpMock(),
            $this->createJumpMock(),
        ];
        $processor = $this->createStatesProcessorMock();
        self::assertEmpty($processor->getJumps());
        self::assertSame($processor, $this->callNonPublicMethod($processor, 'setJumps', [$jumps]));
        self::assertSame($jumps, $processor->getJumps());
    }

    /**
     * Tester of addStateItem, getStateItem, getStates and setStates implementation
     *
     * @return void
     * @group integration
     * @covers ::addStateItems
     * @covers ::getStateItems
     * @covers ::getStates
     * @covers ::setStates
     */
    public function testGetSetStates(): void
    {
        $firstStateItems = $this->createSymbolCollection(Item::class);
        $secondStateItems = $this->createSymbolCollection(Item::class);
        $states = [0 => $firstStateItems, 1 => $secondStateItems];
        $processor = $this->createStatesProcessorMock();
        self::assertEmpty($processor->getStates());
        self::assertSame($processor, $this->callNonPublicMethod($processor, 'setStates', [$states]));
        self::assertSame($states, $processor->getStates());
        self::assertSame($firstStateItems, $processor->getStateItems(0));
        self::assertSame($secondStateItems, $processor->getStateItems(1));
    }

    /**
     * Tester of processClosureItem implementation
     *
     * @return void
     * @group integration
     * @covers ::processClosureItem
     */
    public function testProcessClosureItem(): void
    {
        $new = $this->createSymbolCollection(Item::class);
        $item = $this->createItemMock();
        $items = $this->createSymbolCollection(Item::class);
        $processor = $this->createStatesProcessorMock(
            ['createCollection', 'processNewItems', 'processProductions']
        );
        $processor->expects(self::once())
            ->method('createCollection')
            ->with(self::equalTo(Item::class))
            ->willReturn($new);
        $processor->expects(self::once())
            ->method('processProductions')
            ->with(self::equalTo($new), self::equalTo($item));
        $processor->expects(self::once())
            ->method('processNewItems')
            ->with(self::equalTo($items), self::equalTo($new), self::equalTo(true))
            ->willReturn(true);
        $result = $this->callNonPublicMethod($processor, 'processClosureItem', [$items, $item, true]);
        self::assertTrue($result);
    }

    /**
     * Tester of processClosureItems implementation
     *
     * @return void
     * @group integration
     * @covers ::processClosureItems
     */
    public function testProcessClosureItems(): void
    {
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())->method('afterDot')->willReturn([$this->createNonTerminalMock()]);
        $items = $this->createSymbolCollection(Item::class)->add($item);
        $processor = $this->createStatesProcessorMock(
            ['processClosureItem']
        );
        $processor->expects(self::once())
            ->method('processClosureItem')
            ->with(self::equalTo($items), self::equalTo($item), self::equalTo(true))
            ->willReturn(true);
        self::assertSame($items, $this->callNonPublicMethod($processor, 'processClosureItems', [$items]));
    }

    /**
     * Tester of processClosureItems for empty items implementation
     *
     * @return void
     * @group integration
     * @covers ::processClosureItems
     */
    public function testProcessClosureItemsEmpty(): void
    {
        $items = $this->createSymbolCollectionMock(['assertCollectionOfType']);
        $items->expects(self::once())->method('assertCollectionOfType')->willReturn(true);
        $processor = $this->createStatesProcessorMock([]);
        self::assertSame($items, $this->callNonPublicMethod($processor, 'processClosureItems', [$items]));
    }

    /**
     * Tester of processJumpAdd for jump not in implementation
     *
     * @return void
     * @group integration
     * @covers ::processJumpAdd
     */
    public function testProcessJumpAdd(): void
    {
        $index = 1;
        $stateItems = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER)->add(1);
        $jump = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER)->add(2);
        $symbol = $this->createSymbolMock();
        $states = [
            0 => $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER)->add(0),
            $index => $stateItems,
        ];
        $processor = $this->createStatesProcessorMock();
        $this->setNonPublicPropertyValue($processor, 'states', $states);
        $this->callNonPublicMethod($processor, 'processJumpAdd', [$jump, $symbol, $index]);
        self::assertSame($jump, $processor->getStateItems(2));
        $newJump = current($processor->getJumps());
        self::assertInstanceOf(Jump::class, $newJump);
        self::assertSame($stateItems, $newJump->getItemFrom());
        self::assertSame($symbol, $newJump->getSymbol());
        self::assertSame($jump, $newJump->getItemTo());
    }

    /**
     * Tester of processJumpAdd for jump already in implementation
     *
     * @return void
     * @group integration
     * @covers ::processJumpAdd
     */
    public function testProcessJumpAddAlreadyIn(): void
    {
        $index = 1;
        $stateItems = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER)->add(1);
        $jump = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER)->add(1);
        $symbol = $this->createSymbolMock();
        $states = [
            0 => $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER)->add(0),
            $index => $stateItems,
        ];
        $processor = $this->createStatesProcessorMock();
        $this->setNonPublicPropertyValue($processor, 'states', $states);
        $this->callNonPublicMethod($processor, 'processJumpAdd', [$jump, $symbol, $index]);
        self::assertArrayNotHasKey(2, $this->getNonPublicPropertyValue($processor, 'states'));
        $newJump = current($processor->getJumps());
        self::assertInstanceOf(Jump::class, $newJump);
        self::assertSame($stateItems, $newJump->getItemFrom());
        self::assertSame($symbol, $newJump->getSymbol());
        self::assertSame($stateItems, $newJump->getItemTo());
    }

    /**
     * Tester of processJumpItems implementation
     *
     * @return void
     * @group integration
     * @covers ::processJumpItems
     */
    public function testProcessJumpItems(): void
    {
        $currentAfterDot = $this->createSymbolMock(['isEqual']);
        $production = $this->createProductionMock();
        $item = $this->createItemMock(['afterDot']);
        $item->__construct($production, 2, 4);
        $item->expects(self::once())->method('afterDot')->willReturn([$currentAfterDot]);
        $items = $this->createSymbolCollectionMock(['add']);
        $jump = $this->createSymbolCollection(Item::class);
        $newItem = $this->createItemMock();
        $stateItems = $this->createSymbolCollection(Item::class)->add($item);
        $symbol = $this->createSymbolMock();
        $processor = $this->createStatesProcessorMock(['createCollection', 'createItem', 'processClosureItems']);
        $processor->expects(self::once())->method('createCollection')->willReturn($items);
        $currentAfterDot->expects(self::once())->method('isEqual')->willReturn(true);
        $processor->expects(self::once())
            ->method('createItem')
            ->with(self::equalTo($production), self::equalTo(3), self::equalTo(4))
            ->willReturn($newItem);
        $items->expects(self::once())->method('add')->with(self::equalTo($newItem));
        $processor->expects(self::once())->method('processClosureItems')->willReturn($jump);
        $this->callNonPublicMethod($processor, 'processJumpItems', [$stateItems, $symbol]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $processor = $this->createStatesProcessorMock(['processStates', 'processJumps']);
        $processor->expects(self::once())->method('processStates');
        $processor->expects(self::once())->method('processJumps');
        $processor->process();
    }

    /**
     * Tester of processItemFirsts implementation
     *
     * @return void
     * @covers ::processItemFirsts
     */
    public function testProcessItemFirsts(): void
    {
        $epsilonIndex = 2;
        $firstAfterDot = $this->createSymbolMock();
        $firsts = $this->createSymbolCollectionMock(['add', 'addCollection', 'deleteItem']);
        $item = $this->createItemMock(['afterDot', 'getTerminalIndex']);
        $secondAfterDotFirst = $this->createSymbolCollectionMock();
        $secondAfterDot = $this->createSymbolMock(['getFirst']);
        $secondAfterDot->expects(self::once())->method('getFirst')->willReturn($secondAfterDotFirst);
        $terminalIndex = 1;
        $processor = $this->createStatesProcessorMock(['createCollectionInteger', 'getEpsilonIndex']);
        $processor->expects(self::once())->method('getEpsilonIndex')->willReturn($epsilonIndex);
        $processor->expects(self::once())
            ->method('createCollectionInteger')
            ->willReturn($firsts);
        $item->expects(self::once())->method('afterDot')->willReturn([$firstAfterDot, $secondAfterDot]);
        $firsts->expects(self::once())->method('addCollection')->with(self::equalTo($secondAfterDotFirst));
        $firsts->expects(self::once())->method('deleteItem')->with(self::equalTo($epsilonIndex));
        $item->expects(self::once())->method('getTerminalIndex')->willReturn($terminalIndex);
        $firsts->expects(self::once())->method('add')->with(self::equalTo($terminalIndex));
        self::assertSame($firsts, $this->callNonPublicMethod($processor, 'processItemFirsts', [$item]));
    }

    /**
     * Tester of processItemFirsts for empty afterDots implementation
     *
     * @return void
     * @covers ::processItemFirsts
     */
    public function testProcessItemFirstsEmptyAfterDots(): void
    {
        $firsts = $this->createSymbolCollectionMock(['add']);
        $item = $this->createItemMock(['afterDot', 'getTerminalIndex']);
        $terminalIndex = 1;
        $processor = $this->createStatesProcessorMock(['createCollectionInteger']);
        $processor->expects(self::once())
            ->method('createCollectionInteger')
            ->willReturn($firsts);
        $item->expects(self::once())
            ->method('afterDot')
            ->willReturn([]);
        $item->expects(self::once())
            ->method('getTerminalIndex')
            ->willReturn($terminalIndex);
        $firsts->expects(self::once())
            ->method('add')
            ->with(self::equalTo($terminalIndex));
        self::assertSame($firsts, $this->callNonPublicMethod($processor, 'processItemFirsts', [$item]));
    }

    /**
     * Tester of processJump for empty stateItems implementation
     *
     * @return void
     * @covers ::processJump
     */
    public function testProcessJumpEmptyStateItems(): void
    {
        $index = 1;
        $jump = $this->createSymbolCollectionMock(['count']);
        $jump->expects(self::once())->method('count')->willReturn(0);
        $stateItems = $this->createSymbolCollection(Item::class);
        $symbol = $this->createSymbolMock();
        $processor = $this->createStatesProcessorMock(['getStateItems', 'processJumpItems', 'processJumpAdd']);
        $processor->expects(self::once())
            ->method('getStateItems')
            ->with(self::equalTo($index))
            ->willReturn($stateItems);
        $processor->expects(self::once())
            ->method('processJumpItems')
            ->with(self::equalTo($stateItems), self::equalTo($symbol))
            ->willReturn($jump);
        $processor->expects(self::never())
            ->method('processJumpAdd');
        $this->callNonPublicMethod($processor, 'processJump', [$symbol, $index]);
    }

    /**
     * Tester of processJumpItems for not equal symbol implementation
     *
     * @return void
     * @covers ::processJumpItems
     */
    public function testProcessJumpItemsNotEqualSymbol(): void
    {
        $currentAfterDot = $this->createSymbolMock(['isEqual']);
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())->method('afterDot')->willReturn([$currentAfterDot]);
        $items = $this->createSymbolCollectionMock(['add']);
        $jump = $this->createSymbolCollection(Item::class);
        $stateItems = $this->createSymbolCollection(Item::class)->add($item);
        $symbol = $this->createSymbolMock();
        $processor = $this->createStatesProcessorMock(['createCollection', 'processClosureItems']);
        $processor->expects(self::once())->method('createCollection')->willReturn($items);
        $currentAfterDot->expects(self::once())->method('isEqual')->willReturn(false);
        $items->expects(self::never())->method('add');
        $processor->expects(self::once())->method('processClosureItems')->willReturn($jump);
        $result = $this->callNonPublicMethod($processor, 'processJumpItems', [$stateItems, $symbol]);
        self::assertSame($jump, $result);
    }

    /**
     * Tester of processJumpItems for empty items implementation
     *
     * @return void
     * @covers ::processJumpItems
     */
    public function testProcessJumpItemsEmpty(): void
    {
        $items = $this->createSymbolCollection(Item::class);
        $jump = $this->createSymbolCollection(Item::class);
        $stateItems = $this->createSymbolCollection(Item::class);
        $symbol = $this->createSymbolMock();
        $processor = $this->createStatesProcessorMock(['createCollection', 'processClosureItems']);
        $processor->expects(self::once())
            ->method('createCollection')
            ->with(self::equalTo(Item::class))
            ->willReturn($items);
        $processor->expects(self::once())
            ->method('processClosureItems')
            ->with(self::equalTo($items))
            ->willReturn($jump);
        $result = $this->callNonPublicMethod($processor, 'processJumpItems', [$stateItems, $symbol]);
        self::assertSame($jump, $result);
    }

    /**
     * Tester of processJump for not empty jump implementation
     *
     * @return void
     * @covers ::processJump
     */
    public function testProcessJumpNotEmptyJump(): void
    {
        $index = 1;
        $jump = $this->createSymbolCollectionMock(['count']);
        $jump->expects(self::once())->method('count')->willReturn(1);
        $stateItems = $this->createSymbolCollection(Item::class);
        $symbol = $this->createSymbolMock();
        $processor = $this->createStatesProcessorMock(['getStateItems', 'processJumpItems', 'processJumpAdd']);
        $processor->expects(self::once())
            ->method('getStateItems')
            ->with(self::equalTo($index))
            ->willReturn($stateItems);
        $processor->expects(self::once())
            ->method('processJumpItems')
            ->with(self::equalTo($stateItems), self::equalTo($symbol))
            ->willReturn($jump);
        $processor->expects(self::once())
            ->method('processJumpAdd')
            ->with(self::equalTo($jump), self::equalTo($symbol), self::equalTo($index));
        $this->callNonPublicMethod($processor, 'processJump', [$symbol, $index]);
    }

    /**
     * Tester of processJumps implementation
     *
     * @return void
     * @covers ::processJumps
     */
    public function testProcessJumps(): void
    {
        $state = $this->createSymbolCollection(Item::class);
        $nonTerminal = $this->createNonTerminalMock();
        $states = [$state, $state];
        $terminal = $this->createTerminalMock();
        $processor = $this->createStatesProcessorMock(['getNonTerminals', 'getTerminals', 'getStates', 'processJump']);
        $processor->expects(self::once())
            ->method('getNonTerminals')
            ->willReturn($this->createSymbolCollection(NonTerminal::class)->add($nonTerminal));
        $processor->expects(self::once())
            ->method('getTerminals')
            ->willReturn($this->createSymbolCollection(Terminal::class)->add($terminal));
        $processor->expects(self::exactly(2))
            ->method('getStates')
            ->willReturn($states);
        $consecutive = static fn(Symbol $symbol, int $index) => match ([$symbol, $index]) { // @phpstan-ignore-line
                [$nonTerminal, 0], [$terminal, 0], [$nonTerminal, 1], [$terminal, 1] => $processor
        };
        $processor->expects(self::exactly(4))->method('processJump')->willReturnCallback($consecutive);
        $this->callNonPublicMethod($processor, 'processJumps');
    }

    /**
     * Tester of processNewItems for empty New implementation
     *
     * @return void
     * @covers ::processNewItems
     */
    public function testProcessNewItemsEmpty(): void
    {
        $items = $this->createSymbolCollection(Item::class);
        $new = $this->createSymbolCollection(Item::class);
        $done = true;
        $processor = $this->createStatesProcessorMock();
        self::assertTrue($this->callNonPublicMethod($processor, 'processNewItems', [$items, $new, $done]));
    }

    /**
     * Tester of processNewItems implementation
     *
     * @return void
     * @covers ::processNewItems
     */
    public function testProcessNewItems(): void
    {
        $data = [1, 2, 3];
        $items = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER);
        $new = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER);
        $this->setNonPublicPropertyValue($new, 'data', $data);
        $processor = $this->createStatesProcessorMock();
        self::assertEmpty($this->getNonPublicPropertyValue($items, 'data'));
        self::assertFalse($this->callNonPublicMethod($processor, 'processNewItems', [$items, $new, true]));
        self::assertSame($data, $this->getNonPublicPropertyValue($items, 'data'));
    }

    /**
     * Tester of processPrepareStates implementation
     *
     * @return void
     * @covers ::processPrepareStates
     */
    public function testProcessPrepareStates(): void
    {
        $end = $this->createTerminalMock(['getIndex']);
        $end->expects(self::once())->method('getIndex')->willReturn(1);
        $item = $this->createItemMock();
        $items = $this->createSymbolCollectionMock(['add']);
        $production = $this->createProductionMock();
        $processor = $this->createStatesProcessorMock(
            ['getEnd', 'getStartProduction', 'createItem', 'createCollection']
        );
        $processor->expects(self::once())->method('getStartProduction')->willReturn($production);
        $processor->expects(self::once())->method('getEnd')->willReturn($end);
        $processor->expects(self::once())
            ->method('createItem')
            ->with(self::equalTo($production), self::equalTo(0), self::equalTo(1))
            ->willReturn($item);
        $processor->expects(self::once())
            ->method('createCollection')
            ->with(self::equalTo(Item::class))
            ->willReturn($items);
        $items->expects(self::once())->method('add')->with(self::equalTo($item));
        self::assertSame($items, $this->callNonPublicMethod($processor, 'processPrepareStates'));
    }

    /**
     * Tester of processProduction implementation
     *
     * @return void
     * @covers ::processProduction
     */
    public function testProcessProduction(): void
    {
        $terminalIndex = 1;
        $firsts = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER);
        $this->setNonPublicPropertyValue($firsts, 'data', [$terminalIndex]);
        $item = $this->createItemMock();
        $new = $this->createSymbolCollectionMock(['add']);
        $new->expects(self::once())
            ->method('add')
            ->with(self::equalTo($item));
        $production = $this->createProductionMock();
        $processor = $this->createStatesProcessorMock(['createItem']);
        $processor->expects(self::once())
            ->method('createItem')
            ->with(self::equalTo($production), self::equalTo(0), self::equalTo($terminalIndex))
            ->willReturn($item);
        $this->callNonPublicMethod($processor, 'processProduction', [$firsts, $new, $production]);
    }

    /**
     * Tester of processProductions implementation
     *
     * @return void
     * @covers ::processProductions
     */
    public function testProcessProductions(): void
    {
        $currentAfterDot = $this->createSymbolMock(['isEqual']);
        $firsts = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER);
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())->method('afterDot')->willReturn([$currentAfterDot]);
        $left = $this->createNonTerminalMock();
        $new = $this->createSymbolCollectionMock(['add']);
        $production = $this->createProductionMock(['getLeft']);
        $production->expects(self::once())->method('getLeft')->willReturn($left);
        $productions = $this->createSymbolCollection(Production::class)->add($production);
        $processor = $this->createStatesProcessorMock(['getProductions', 'processItemFirsts', 'processProduction']);
        $processor->expects(self::once())->method('getProductions')->willReturn($productions);
        $currentAfterDot->expects(self::once())->method('isEqual')->with(self::equalTo($left))->willReturn(true);
        $processor->expects(self::once())
            ->method('processItemFirsts')
            ->with(self::equalTo($item))
            ->willReturn($firsts);
        $processor->expects(self::once())
            ->method('processProduction')
            ->with(self::equalTo($firsts), self::equalTo($new), self::equalTo($production));
        $this->callNonPublicMethod($processor, 'processProductions', [$new, $item]);
    }

    /**
     * Tester of processProductions for not equal production implementation
     *
     * @return void
     * @covers ::processProductions
     */
    public function testProcessProductionsNotEqual(): void
    {
        $currentAfterDot = $this->createSymbolMock(['isEqual']);
        $firsts = $this->createSymbolCollection(StatesProcessor::COLLECTION_INTEGER);
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())->method('afterDot')->willReturn([$currentAfterDot]);
        $left = $this->createNonTerminalMock();
        $new = $this->createSymbolCollectionMock(['add']);
        $production = $this->createProductionMock(['getLeft']);
        $production->expects(self::once())->method('getLeft')->willReturn($left);
        $productions = $this->createSymbolCollection(Production::class)->add($production);
        $processor = $this->createStatesProcessorMock(['getProductions', 'processItemFirsts', 'processProduction']);
        $processor->expects(self::once())->method('getProductions')->willReturn($productions);
        $currentAfterDot->expects(self::once())
            ->method('isEqual')
            ->with(self::equalTo($left))
            ->willReturn(false);
        $processor->expects(self::once())->method('processItemFirsts')
            ->with(self::equalTo($item))
            ->willReturn($firsts);
        $processor->expects(self::never())->method('processProduction');
        $this->callNonPublicMethod($processor, 'processProductions', [$new, $item]);
    }

    /**
     * Tester of processStates implementation
     *
     * @return void
     * @covers ::processStates
     */
    public function testProcessStates(): void
    {
        $closureItems = $this->createSymbolCollection(Item::class);
        $items = $this->createSymbolCollection(Item::class);
        $processor = $this->createStatesProcessorMock(['processPrepareStates', 'processClosureItems', 'setStates']);
        $processor->expects(self::once())
            ->method('processPrepareStates')
            ->willReturn($items);
        $processor->expects(self::once())
            ->method('processClosureItems')
            ->with(self::equalTo($items))
            ->willReturn($closureItems);
        $processor->expects(self::once())
            ->method('setStates')
            ->with(self::equalTo([$closureItems]));
        $this->callNonPublicMethod($processor, 'processStates');
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Item mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Item&MockObject
     */
    protected function createItemMock(array $methods = []): Item&MockObject
    {
        $mock = $this->createPartialMock(Item::class, $methods);
        return $mock;
    }

    /**
     * Jump mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Jump&MockObject
     */
    protected function createJumpMock(array $methods = []): Jump&MockObject
    {
        $mock = $this->createPartialMock(Jump::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * StatesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StatesProcessor&MockObject
     */
    protected function createStatesProcessorMock(array $methods = []): StatesProcessor&MockObject
    {
        $mock = $this->createPartialMock(StatesProcessor::class, $methods);
        return $mock;
    }

    /**
     * Symbol mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Symbol&MockObject
     */
    protected function createSymbolMock(array $methods = []): Symbol&MockObject
    {
        $mock = $this->createPartialMock(Symbol::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    /**
     * SymbolCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return SymbolCollection<mixed>&MockObject
     */
    protected function createSymbolCollectionMock(array $methods = []): SymbolCollection&MockObject
    {
        $mock = $this->createPartialMock(SymbolCollection::class, $methods);
        return $mock;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
