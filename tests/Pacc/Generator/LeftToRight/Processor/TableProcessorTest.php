<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\GotosProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\ReduceProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\ShiftProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\Table\TableCollection;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\TableProcessor
 */
class TableProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of Constructor implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getIndexesProcessor
     * @covers ::setIndexesProcessor
     * @covers ::getStatesProcessor
     * @covers ::setStatesProcessor
     */
    public function testConstructor(): void
    {
        $grammar = $this->createGrammarMock();
        $indexesProcessor = $this->createIndexesProcessorMock();
        $statesProcessor = $this->createStatesProcessorMock();
        $tableProcessor = $this->createTableProcessorMock();
        $tableProcessor->__construct($grammar, $indexesProcessor, $statesProcessor);
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($tableProcessor, 'getIndexesProcessor'));
        self::assertSame($statesProcessor, $this->callNonPublicMethod($tableProcessor, 'getStatesProcessor'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of createGotosProcessor implementation
     *
     * @return void
     * @covers ::createGotosProcessor
     */
    public function testCreateGotosProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $indexesProcessor = $this->createIndexesProcessorMock();
        $statesProcessor = $this->createStatesProcessorMock();
        $tableCollection = $this->createTableCollectionMock();
        $tableProcessor = $this->createTableProcessorMock();
        $this->setNonPublicPropertyValue($tableProcessor, 'grammar', $grammar);
        $this->setNonPublicPropertyValue($tableProcessor, 'indexesProcessor', $indexesProcessor);
        $this->setNonPublicPropertyValue($tableProcessor, 'statesProcessor', $statesProcessor);
        $this->setNonPublicPropertyValue($tableProcessor, 'tableCollection', $tableCollection);
        $gotosProcessor = $this->callNonPublicMethod($tableProcessor, 'createGotosProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($gotosProcessor, 'getGrammar'));
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($gotosProcessor, 'getIndexesProcessor'));
        self::assertSame($statesProcessor, $this->callNonPublicMethod($gotosProcessor, 'getStatesProcessor'));
        self::assertSame($tableCollection, $this->callNonPublicMethod($gotosProcessor, 'getTableCollection'));
    }

    /**
     * Tester of createReduceProcessor implementation
     *
     * @return void
     * @covers ::createReduceProcessor
     */
    public function testCreateReduceProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $indexesProcessor = $this->createIndexesProcessorMock();
        $statesProcessor = $this->createStatesProcessorMock();
        $tableCollection = $this->createTableCollectionMock();
        $tableProcessor = $this->createTableProcessorMock();
        $this->setNonPublicPropertyValue($tableProcessor, 'grammar', $grammar);
        $this->setNonPublicPropertyValue($tableProcessor, 'indexesProcessor', $indexesProcessor);
        $this->setNonPublicPropertyValue($tableProcessor, 'statesProcessor', $statesProcessor);
        $this->setNonPublicPropertyValue($tableProcessor, 'tableCollection', $tableCollection);
        $reduceProcessor = $this->callNonPublicMethod($tableProcessor, 'createReduceProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($reduceProcessor, 'getGrammar'));
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($reduceProcessor, 'getIndexesProcessor'));
        self::assertSame($statesProcessor, $this->callNonPublicMethod($reduceProcessor, 'getStatesProcessor'));
        self::assertSame($tableCollection, $this->callNonPublicMethod($reduceProcessor, 'getTableCollection'));
    }

    /**
     * Tester of createShiftProcessor implementation
     *
     * @return void
     * @covers ::createShiftProcessor
     */
    public function testCreateShiftProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $indexesProcessor = $this->createIndexesProcessorMock();
        $statesProcessor = $this->createStatesProcessorMock();
        $tableCollection = $this->createTableCollectionMock();
        $tableProcessor = $this->createTableProcessorMock();
        $this->setNonPublicPropertyValue($tableProcessor, 'grammar', $grammar);
        $this->setNonPublicPropertyValue($tableProcessor, 'indexesProcessor', $indexesProcessor);
        $this->setNonPublicPropertyValue($tableProcessor, 'statesProcessor', $statesProcessor);
        $this->setNonPublicPropertyValue($tableProcessor, 'tableCollection', $tableCollection);
        $shiftProcessor = $this->callNonPublicMethod($tableProcessor, 'createShiftProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($shiftProcessor, 'getGrammar'));
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($shiftProcessor, 'getIndexesProcessor'));
        self::assertSame($statesProcessor, $this->callNonPublicMethod($shiftProcessor, 'getStatesProcessor'));
        self::assertSame($tableCollection, $this->callNonPublicMethod($shiftProcessor, 'getTableCollection'));
    }

    /**
     * Tester of createTableCollection implementation
     *
     * @return void
     * @covers ::createTableCollection
     */
    public function testCreateTableCollection(): void
    {
        $tableProcessor = $this->createTableProcessorMock();
        $tableCollection = $this->callNonPublicMethod($tableProcessor, 'createTableCollection');
        self::assertEmpty($tableCollection->getTable());
    }

    /**
     * Tester of getGotosProcessor and setGotosProcessor implementation
     *
     * @return void
     * @covers ::getGotosProcessor
     * @covers ::setGotosProcessor
     */
    public function testGetSetGotosProcessor(): void
    {
        $gotosProcessor = $this->createGotosProcessorMock();
        $this->processTestGetSet(TableProcessor::class, 'gotosProcessor', $gotosProcessor);
    }

    /**
     * Tester of getGotosProcessor factory implementation
     *
     * @return void
     * @covers ::getGotosProcessor
     */
    public function testGetGotosProcessorFactory(): void
    {
        $gotosProcessor = $this->createGotosProcessorMock();
        $tableProcessor = $this->createTableProcessorMock(['createGotosProcessor']);
        $tableProcessor->expects(self::once())
            ->method('createGotosProcessor')
            ->willReturn($gotosProcessor);
        self::assertSame($gotosProcessor, $this->callNonPublicMethod($tableProcessor, 'getGotosProcessor'));
    }

    /**
     * Tester of getReduceProcessor and setReduceProcessor implementation
     *
     * @return void
     * @covers ::getReduceProcessor
     * @covers ::setReduceProcessor
     */
    public function testGetSetReduceProcessor(): void
    {
        $reduceProcessor = $this->createReduceProcessorMock();
        $this->processTestGetSet(TableProcessor::class, 'reduceProcessor', $reduceProcessor);
    }

    /**
     * Tester of getReduceProcessor factory implementation
     *
     * @return void
     * @covers ::getReduceProcessor
     */
    public function testGetReduceProcessorFactory(): void
    {
        $reduceProcessor = $this->createReduceProcessorMock();
        $tableProcessor = $this->createTableProcessorMock(['createReduceProcessor']);
        $tableProcessor->expects(self::once())
            ->method('createReduceProcessor')
            ->willReturn($reduceProcessor);
        self::assertSame($reduceProcessor, $this->callNonPublicMethod($tableProcessor, 'getReduceProcessor'));
    }

    /**
     * Tester of getShiftProcessor and setShiftProcessor implementation
     *
     * @return void
     * @covers ::getShiftProcessor
     * @covers ::setShiftProcessor
     */
    public function testGetSetShiftProcessor(): void
    {
        $shiftProcessor = $this->createShiftProcessorMock();
        $this->processTestGetSet(TableProcessor::class, 'shiftProcessor', $shiftProcessor);
    }

    /**
     * Tester of getShiftProcessor factory implementation
     *
     * @return void
     * @covers ::getShiftProcessor
     */
    public function testGetShiftProcessorFactory(): void
    {
        $shiftProcessor = $this->createShiftProcessorMock();
        $tableProcessor = $this->createTableProcessorMock(['createShiftProcessor']);
        $tableProcessor->expects(self::once())
            ->method('createShiftProcessor')
            ->willReturn($shiftProcessor);
        self::assertSame($shiftProcessor, $this->callNonPublicMethod($tableProcessor, 'getShiftProcessor'));
    }

    /**
     * Tester of getStates implementation
     *
     * @return void
     * @covers ::getStates
     */
    public function testGetStates(): void
    {
        $states = [$this->createSymbolCollection(Item::class)];
        $statesProcessor = $this->createStatesProcessorMock(['getStates']);
        $statesProcessor->expects(self::once())
            ->method('getStates')
            ->willReturn($states);
        $tableProcessor = $this->createTableProcessorMock(['getStatesProcessor']);
        $tableProcessor->expects(self::once())
            ->method('getStatesProcessor')
            ->willReturn($statesProcessor);
        self::assertSame($states, $tableProcessor->getStates());
    }

    /**
     * Tester of getTable implementation
     *
     * @return void
     * @covers ::getTable
     */
    public function testGetTable(): void
    {
        $table = [1, 2, 3];
        $tableCollection = $this->createTableCollectionMock(['getTable']);
        $tableCollection->expects(self::once())
            ->method('getTable')
            ->willReturn($table);
        $tableProcessor = $this->createTableProcessorMock(['getTableCollection']);
        $tableProcessor->expects(self::once())
            ->method('getTableCollection')
            ->willReturn($tableCollection);
        self::assertSame($table, $tableProcessor->getTable());
    }

    /**
     * Tester of getTableCollection and setTableCollection implementation
     *
     * @return void
     * @covers ::getTableCollection
     * @covers ::setTableCollection
     */
    public function testGetSetTableCollection(): void
    {
        $tableCollection = $this->createTableCollectionMock();
        $this->processTestGetSet(TableProcessor::class, 'tableCollection', $tableCollection);
    }

    /**
     * Tester of getTableCollection factory implementation
     *
     * @return void
     * @covers ::getTableCollection
     */
    public function testGetTableCollectionFactory(): void
    {
        $tableCollection = $this->createTableCollectionMock();
        $tableProcessor = $this->createTableProcessorMock(['createTableCollection']);
        $tableProcessor->expects(self::once())
            ->method('createTableCollection')
            ->willReturn($tableCollection);
        self::assertSame($tableCollection, $this->callNonPublicMethod($tableProcessor, 'getTableCollection'));
    }

    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $items = $this->createSymbolCollection(Item::class);
        $states = [
            0 => $items,
        ];
        $gotosProcessor = $this->createGotosProcessorMock(['process']);
        $gotosProcessor->expects(self::once())->method('process')->with(self::equalTo($items), self::equalTo(0));
        $reduceProcessor = $this->createReduceProcessorMock(['process']);
        $reduceProcessor->expects(self::once())->method('process')->with(self::equalTo($items), self::equalTo(0));
        $shiftProcessor = $this->createShiftProcessorMock(['process']);
        $shiftProcessor->expects(self::once())->method('process')->with(self::equalTo($items), self::equalTo(0));
        $processor = $this->createTableProcessorMock(
            ['getStates', 'getShiftProcessor', 'getReduceProcessor', 'getGotosProcessor']
        );
        $processor->expects(self::once())->method('getGotosProcessor')->willReturn($gotosProcessor);
        $processor->expects(self::once())->method('getReduceProcessor')->willReturn($reduceProcessor);
        $processor->expects(self::once())->method('getShiftProcessor')->willReturn($shiftProcessor);
        $processor->expects(self::once())->method('getStates')->willReturn($states);
        $processor->process();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * GotosProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return GotosProcessor&MockObject
     */
    protected function createGotosProcessorMock(array $methods = []): GotosProcessor&MockObject
    {
        $mock = $this->createPartialMock(GotosProcessor::class, $methods);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * IndexesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return IndexesProcessor&MockObject
     */
    protected function createIndexesProcessorMock(array $methods = []): IndexesProcessor&MockObject
    {
        $mock = $this->createPartialMock(IndexesProcessor::class, $methods);
        return $mock;
    }

    /**
     * ReduceProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ReduceProcessor&MockObject
     */
    protected function createReduceProcessorMock(array $methods = []): ReduceProcessor&MockObject
    {
        $mock = $this->createPartialMock(ReduceProcessor::class, $methods);
        return $mock;
    }

    /**
     * ShiftProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ShiftProcessor&MockObject
     */
    protected function createShiftProcessorMock(array $methods = []): ShiftProcessor&MockObject
    {
        $mock = $this->createPartialMock(ShiftProcessor::class, $methods);
        return $mock;
    }

    /**
     * StatesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StatesProcessor&MockObject
     */
    protected function createStatesProcessorMock(array $methods = []): StatesProcessor&MockObject
    {
        $mock = $this->createPartialMock(StatesProcessor::class, $methods);
        return $mock;
    }

    /**
     * Symbol collection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * TableCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableCollection&MockObject
     */
    protected function createTableCollectionMock(array $methods = []): TableCollection&MockObject
    {
        $mock = $this->createPartialMock(TableCollection::class, $methods);
        return $mock;
    }

    /**
     * TableProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableProcessor&MockObject
     */
    protected function createTableProcessorMock(array $methods = []): TableProcessor&MockObject
    {
        $mock = $this->createPartialMock(TableProcessor::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
