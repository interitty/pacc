<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Exceptions\BadItemException;
use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\Table\ShiftProcessor
 */
class ShiftProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of processShifts exception implementation
     *
     * @return void
     * @group integration
     * @covers ::processShifts
     */
    public function testProcessShifts(): void
    {
        $state = 1;
        $nextState = 2;
        $terminalIndex = 3;
        $tablePitch = 4;
        $tableIndex = $state * $tablePitch + $terminalIndex;
        $tableCollection = $this->createTableCollectionMock();
        $terminal = $this->createTerminalMock(['isEqual', 'getIndex']);
        $terminal->expects(self::once())->method('isEqual')->willReturn(true);
        $terminal->expects(self::once())->method('getIndex')->willReturn($terminalIndex);
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())->method('afterDot')->willReturn([$terminal]);
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class)->add($item);

        $shiftProcessor = $this->createShiftProcessorMock(['processNextState', 'getTablePitch']);
        $this->callNonPublicMethod($shiftProcessor, 'setTableCollection', [$tableCollection]);
        $shiftProcessor->expects(self::once())->method('processNextState')->willReturn($nextState);
        $shiftProcessor->expects(self::once())->method('getTablePitch')->willReturn($tablePitch);

        $this->callNonPublicMethod($shiftProcessor, 'processShifts', [$items, $terminal, $state]);
        self::assertTrue($tableCollection->hasTableItem($tableIndex));
        self::assertSame($nextState, $tableCollection->getTableItem($tableIndex));
    }

    /**
     * Tester of processShifts exception implementation
     *
     * @return void
     * @group negative
     * @covers ::processShifts
     */
    public function testProcessShiftsException(): void
    {
        $terminal = $this->createTerminalMock(['isEqual']);
        $terminal->expects(self::once())->method('isEqual')->willReturn(true);
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())
            ->method('afterDot')
            ->willReturn([$terminal]);
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $this->setNonPublicPropertyValue($items, 'data', [$item]);

        $shiftProcessor = $this->createShiftProcessorMock(['processNextState']);
        $shiftProcessor->expects(self::once())
            ->method('processNextState')
            ->willReturn(null);

        $this->expectException(BadItemException::class);
        $this->expectExceptionMessage('Cannot get next state for shift');
        $this->expectExceptionData();
        $this->callNonPublicMethod($shiftProcessor, 'processShifts', [$items, $terminal, 1]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of getTerminals implementation
     *
     * @return void
     * @covers ::getTerminals
     */
    public function testGetTerminals(): void
    {
        /** @var SymbolCollection<Terminal> $terminals */
        $terminals = $this->createSymbolCollection(Terminal::class);
        $grammar = $this->createGrammarMock(['getTerminals']);
        $grammar->expects(self::once())
            ->method('getTerminals')
            ->willReturn($terminals);
        $shiftProcessor = $this->createShiftProcessorMock(['getGrammar']);
        $shiftProcessor->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        self::assertSame($terminals, $this->callNonPublicMethod($shiftProcessor, 'getTerminals'));
    }

    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $state = 1;
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $terminal = $this->createTerminalMock();
        /** @var SymbolCollection<Terminal> $terminals */
        $terminals = $this->createSymbolCollection(Terminal::class);
        $terminalsData = [$terminal];
        $this->setNonPublicPropertyValue($terminals, 'data', $terminalsData);
        $gotosProcessor = $this->createShiftProcessorMock(['getTerminals', 'processShifts']);
        $gotosProcessor->expects(self::once())
            ->method('getTerminals')
            ->willReturn($terminals);
        $gotosProcessor->expects(self::once())
            ->method('processShifts')
            ->with(self::equalTo($items), self::equalTo($terminal), self::equalTo($state));
        $gotosProcessor->process($items, $state);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * Item mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Item&MockObject
     */
    protected function createItemMock(array $methods = []): Item&MockObject
    {
        $mock = $this->createPartialMock(Item::class, $methods);
        return $mock;
    }

    /**
     * ShiftProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ShiftProcessor&MockObject
     */
    protected function createShiftProcessorMock(array $methods = []): ShiftProcessor&MockObject
    {
        $mock = $this->createPartialMock(ShiftProcessor::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    /**
     * TableCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableCollection&MockObject
     */
    protected function createTableCollectionMock(array $methods = []): TableCollection&MockObject
    {
        $mock = $this->createPartialMock(TableCollection::class, $methods);
        return $mock;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
