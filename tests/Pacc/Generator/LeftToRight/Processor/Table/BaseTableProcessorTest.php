<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Generator\LeftToRight\Jump;
use Interitty\Pacc\Generator\LeftToRight\Processor\BaseProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\IndexesProcessor;
use Interitty\Pacc\Generator\LeftToRight\Processor\StatesProcessor;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

use function array_merge;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\Table\BaseTableProcessor
 */
class BaseTableProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of constructor implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getGrammar
     * @covers ::setGrammar
     * @covers ::getIndexesProcessor
     * @covers ::setIndexesProcessor
     * @covers ::getStatesProcessor
     * @covers ::setStatesProcessor
     * @covers ::getTableCollection
     * @covers ::setTableCollection
     */
    public function testConstructor(): void
    {
        $grammar = $this->createGrammarMock();
        $indexesProcessor = $this->createIndexesProcessorMock();
        $statesProcesor = $this->createStatesProcessorMock();
        $tableCollection = $this->createTableCollectionMock();
        $baseTableProcessor = $this->createBaseTableProcessorMock();
        $baseTableProcessor->__construct($grammar, $indexesProcessor, $statesProcesor, $tableCollection);
        self::assertSame($grammar, $this->callNonPublicMethod($baseTableProcessor, 'getGrammar'));
        self::assertSame($indexesProcessor, $this->callNonPublicMethod($baseTableProcessor, 'getIndexesProcessor'));
        self::assertSame($statesProcesor, $this->callNonPublicMethod($baseTableProcessor, 'getStatesProcessor'));
        self::assertSame($tableCollection, $this->callNonPublicMethod($baseTableProcessor, 'getTableCollection'));
    }

    /**
     * Tester of processNextState implementation
     *
     * @return void
     * @group integration
     * @covers ::processNextState
     */
    public function testProcessNextState(): void
    {
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $jump = $this->createJumpMock();
        $nextState = 1;
        $symbol = $this->createSymbolMock();
        $baseTableProcessor = $this->createBaseTableProcessorMock(['getJumps', 'processNextStateJump']);
        $baseTableProcessor->expects(self::once())->method('getJumps')->willReturn([$jump]);
        $baseTableProcessor->expects(self::once())
            ->method('processNextStateJump')
            ->with(self::equalTo($items), self::equalTo($jump), self::equalTo($symbol))
            ->willReturn($nextState);
        $result = $this->callNonPublicMethod($baseTableProcessor, 'processNextState', [$items, $symbol]);
        self::assertSame($nextState, $result);
    }

    /**
     * Tester of processNextState empty jumps implementation
     *
     * @return void
     * @group integration
     * @covers ::processNextState
     */
    public function testProcessNextStateEmpty(): void
    {
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $symbol = $this->createSymbolMock();
        $baseTableProcessor = $this->createBaseTableProcessorMock(['getJumps']);
        $baseTableProcessor->expects(self::once())->method('getJumps')->willReturn([]);
        self::assertNull($this->callNonPublicMethod($baseTableProcessor, 'processNextState', [$items, $symbol]));
    }

    /**
     * Tester of processNextStateJump implementation
     *
     * @return void
     * @group integration
     * @covers ::processNextStateJump
     */
    public function testProcessNextStateJump(): void
    {
        $items = $this->createSymbolCollection(Item::class);
        /** @var SymbolCollection<Item> $jumpItemFrom */
        $jumpItemFrom = $this->createSymbolCollection(Item::class);
        /** @var SymbolCollection<Item> $jumpItemTo */
        $jumpItemTo = $this->createSymbolCollection(Item::class);
        $expectedNextState = 1;
        $states = [$expectedNextState => $this->createSymbolCollection(BaseProcessor::COLLECTION_INTEGER)];
        $symbol = $this->createSymbolMock();
        $this->setNonPublicPropertyValue($symbol, 'name', 'symbol');
        $jump = $this->createJump($jumpItemFrom, $symbol, $jumpItemTo);
        $baseTableProcessor = $this->createBaseTableProcessorMock(['getStates']);
        $baseTableProcessor->expects(self::once())->method('getStates')->willReturn($states);
        $nextState = $this->callNonPublicMethod($baseTableProcessor, 'processNextStateJump', [$items, $jump, $symbol]);
        self::assertSame($expectedNextState, $nextState);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of Jumps getter implementation
     *
     * @return void
     * @covers ::getJumps
     */
    public function testGetJumps(): void
    {
        $jumps = [$this->createJumpMock()];
        $statesProcessor = $this->createStatesProcessorMock(['getJumps']);
        $baseTableProcessor = $this->createBaseTableProcessorMock(['getStatesProcessor']);
        $statesProcessor->expects(self::once())->method('getJumps')->willReturn($jumps);
        $baseTableProcessor->expects(self::once())->method('getStatesProcessor')->willReturn($statesProcessor);
        self::assertSame($jumps, $this->callNonPublicMethod($baseTableProcessor, 'getJumps'));
    }

    /**
     * Tester of States getter implementation
     *
     * @return void
     * @covers ::getStates
     */
    public function testGetStates(): void
    {
        $states = [$this->createSymbolCollection(Item::class)];
        $statesProcessor = $this->createStatesProcessorMock(['getStates']);
        $baseTableProcessor = $this->createBaseTableProcessorMock(['getStatesProcessor']);
        $statesProcessor->expects(self::once())->method('getStates')->willReturn($states);
        $baseTableProcessor->expects(self::once())->method('getStatesProcessor')->willReturn($statesProcessor);
        self::assertSame($states, $this->callNonPublicMethod($baseTableProcessor, 'getStates'));
    }

    /**
     * Tester of TablePitch getter implementation
     *
     * @return void
     * @covers ::getTablePitch
     */
    public function testGetTablePitch(): void
    {
        $tablePitch = 1;
        $indexesProcessor = $this->createIndexesProcessorMock(['getTablePitch']);
        $baseTableProcessor = $this->createBaseTableProcessorMock(['getIndexesProcessor']);
        $indexesProcessor->expects(self::once())->method('getTablePitch')->willReturn($tablePitch);
        $baseTableProcessor->expects(self::once())->method('getIndexesProcessor')->willReturn($indexesProcessor);
        self::assertSame($tablePitch, $this->callNonPublicMethod($baseTableProcessor, 'getTablePitch'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseTableProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return BaseTableProcessor&MockObject
     */
    protected function createBaseTableProcessorMock(array $methods = []): BaseTableProcessor&MockObject
    {
        $mockMethods = array_merge($methods, ['process']);
        $mock = $this->createPartialMock(BaseTableProcessor::class, $mockMethods);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * IndexesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return IndexesProcessor&MockObject
     */
    protected function createIndexesProcessorMock(array $methods = []): IndexesProcessor&MockObject
    {
        $mock = $this->createPartialMock(IndexesProcessor::class, $methods);
        return $mock;
    }

    /**
     * Jump factory
     *
     * @param SymbolCollection<Item> $itemFrom
     * @param Symbol $symbol
     * @param SymbolCollection<Item> $itemTo
     * @return Jump
     */
    protected function createJump(SymbolCollection $itemFrom, Symbol $symbol, SymbolCollection $itemTo): Jump
    {
        $jump = new Jump($itemFrom, $symbol, $itemTo);
        return $jump;
    }

    /**
     * Jump mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Jump&MockObject
     */
    protected function createJumpMock(array $methods = []): Jump&MockObject
    {
        $mock = $this->createPartialMock(Jump::class, $methods);
        return $mock;
    }

    /**
     * StatesProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StatesProcessor&MockObject
     */
    protected function createStatesProcessorMock(array $methods = []): StatesProcessor&MockObject
    {
        $mock = $this->createPartialMock(StatesProcessor::class, $methods);
        return $mock;
    }

    /**
     * Symbol mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Symbol&MockObject
     */
    protected function createSymbolMock(array $methods = []): Symbol&MockObject
    {
        $mock = $this->createPartialMock(Symbol::class, $methods);
        return $mock;
    }

    /**
     * Symbol collection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * TableCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableCollection&MockObject
     */
    protected function createTableCollectionMock(array $methods = []): TableCollection&MockObject
    {
        $mock = $this->createPartialMock(TableCollection::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
