<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\Table\TableCollection
 */
class TableCollectionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of TableCollection implementation
     *
     * @return void
     * @covers ::getTable
     * @covers ::getTableItem
     * @covers ::hasTableItem
     * @covers ::setTableItem
     * @group integration
     */
    public function testTableCollection(): void
    {
        $index = 1;
        $item = 2;
        $table = [$index => $item];
        $tableCollection = $this->createTableCollection();
        self::assertEmpty($tableCollection->getTable());
        self::assertFalse($tableCollection->hasTableItem($index));
        self::assertSame($tableCollection, $tableCollection->setTableItem($index, $item));
        self::assertTrue($tableCollection->hasTableItem($index));
        self::assertSame($item, $tableCollection->getTableItem($index));
        self::assertSame($table, $tableCollection->getTable());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * TableCollection factory
     *
     * @return TableCollection
     */
    protected function createTableCollection(): TableCollection
    {
        $tableCollection = new TableCollection();
        return $tableCollection;
    }

    // </editor-fold>
}
