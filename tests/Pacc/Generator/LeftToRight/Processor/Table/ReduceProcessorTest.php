<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Generator;
use Interitty\Pacc\Exceptions\BadItemException;
use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\Table\ReduceProcessor
 */
class ReduceProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of getStartProduction implementation
     *
     * @return void
     * @covers ::getStartProduction
     */
    public function testGetStartProduction(): void
    {
        $startProduction = $this->createProductionMock();
        $grammar = $this->createGrammarMock(['getStartProduction']);
        $grammar->expects(self::once())
            ->method('getStartProduction')
            ->willReturn($startProduction);
        $gotosProcessor = $this->createReduceProcessorMock(['getGrammar']);
        $gotosProcessor->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        self::assertSame($startProduction, $this->callNonPublicMethod($gotosProcessor, 'getStartProduction'));
    }

    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $state = 1;
        $item = $this->createItemMock(['afterDot']);
        $item->expects(self::once())
            ->method('afterDot')
            ->willReturn([]);
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $this->setNonPublicPropertyValue($items, 'data', [$item]);
        $startProduction = $this->createProductionMock();
        $gotosProcessor = $this->createReduceProcessorMock(['getStartProduction', 'processReduces']);
        $gotosProcessor->expects(self::once())
            ->method('getStartProduction')
            ->willReturn($startProduction);
        $gotosProcessor->expects(self::once())
            ->method('processReduces')
            ->with(self::equalTo($item), self::equalTo($startProduction), self::equalTo($state));
        $gotosProcessor->process($items, $state);
    }

    /**
     * Tester of processReduces implementation
     *
     * @return void
     * @covers ::processReduces
     */
    public function testProcessReduces(): void
    {
        $item = $this->createItemMock();
        $production = $this->createProductionMock(['isEqual', 'getIndex']);
        $reduceProcessor = $this->createReduceProcessorMock(['getTablePitch', 'processReducesConflict']);
        $startProduction = $this->createProductionMock();
        $tableCollection = $this->createTableCollectionMock(['setTableItem']);

        $this->setNonPublicPropertyValue($item, 'production', $production);
        $this->setNonPublicPropertyValue($item, 'terminalIndex', 3);
        $this->setNonPublicPropertyValue($reduceProcessor, 'tableCollection', $tableCollection);
        $production->expects(self::once())->method('isEqual')->willReturn(false);
        $production->expects(self::once())->method('getIndex')->willReturn(5);
        $reduceProcessor->expects(self::once())->method('getTablePitch')->willReturn(4);
        $reduceProcessor->expects(self::once())
            ->method('processReducesConflict')
            ->with(self::equalTo($item), self::equalTo(7));
        $tableCollection->expects(self::once())
            ->method('setTableItem')
            ->with(self::equalTo(7), self::equalTo(-5));

        $this->callNonPublicMethod($reduceProcessor, 'processReduces', [$item, $startProduction, 1]);
    }

    /**
     * Tester of processReducesConflict implementation
     *
     * @return void
     * @covers ::processReducesConflict
     */
    public function testProcessReducesConflict(): void
    {
        $item = $this->createItemMock();
        $tableCollection = $this->createTableCollectionMock(['hasTableItem']);
        $tableCollection->expects(self::once())
            ->method('hasTableItem')
            ->willReturn(false);
        $this->setNonPublicPropertyValue($tableCollection, 'table', []);
        $reduceProcessor = $this->createReduceProcessorMock();
        $this->setNonPublicPropertyValue($reduceProcessor, 'tableCollection', $tableCollection);

        $this->callNonPublicMethod($reduceProcessor, 'processReducesConflict', [$item, 1]);
    }

    /**
     * DataProvider for processReducesConflictException test
     *
     * @phpstan-return Generator<string, array{0: int[], 1: Item, 2: string, 3: array<string, mixed>}>
     */
    public function processReducesConflictExceptionDataProvider(): Generator
    {
        $item = $this->createItemMock(['__toString']);
        $item->expects(self::any())->method('__toString')->willReturn('[itemString]');
        // <editor-fold defaultstate="collapsed" desc="Shift-reduce conflict">
        yield 'Shift-reduce conflict' => [[1 => 1], $item, 'Shift-reduce conflict', []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Reduce-reduce conflict">
        yield 'Reduce-reduce conflict' => [[1 => -1], $item, 'Reduce-reduce conflict: :item', ['item' => $item]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Accept-reduce conflict">
        yield 'Accept-reduce conflict' => [[1 => 0], $item, 'Accept-reduce conflict: :item', ['item' => $item]];
        // </editor-fold>
    }

    /**
     * Tester of processReducesConflict exception implementation
     *
     * @param int[] $table
     * @param Item $item
     * @param string $message
     * @param array<string, mixed> $data
     * @return void
     * @dataProvider processReducesConflictExceptionDataProvider
     * @covers ::processReducesConflict
     */
    public function testProcessReducesConflictException(array $table, Item $item, string $message, array $data): void
    {
        $tableCollection = $this->createTableCollectionMock();
        $this->setNonPublicPropertyValue($tableCollection, 'table', $table);
        $reduceProcessor = $this->createReduceProcessorMock();
        $this->setNonPublicPropertyValue($reduceProcessor, 'tableCollection', $tableCollection);

        $this->expectException(BadItemException::class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionData($data);
        $this->callNonPublicMethod($reduceProcessor, 'processReducesConflict', [$item, 1]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * Item mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Item&MockObject
     */
    protected function createItemMock(array $methods = []): Item&MockObject
    {
        $mock = $this->createPartialMock(Item::class, $methods);
        return $mock;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * ReduceProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ReduceProcessor&MockObject
     */
    protected function createReduceProcessorMock(array $methods = []): ReduceProcessor&MockObject
    {
        $mock = $this->createPartialMock(ReduceProcessor::class, $methods);
        return $mock;
    }

    /**
     * Symbol collection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * TableCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableCollection&MockObject
     */
    protected function createTableCollectionMock(array $methods = []): TableCollection&MockObject
    {
        $mock = $this->createPartialMock(TableCollection::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
