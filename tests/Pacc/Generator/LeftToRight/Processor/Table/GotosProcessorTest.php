<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight\Processor\Table;

use Interitty\Pacc\Generator\LeftToRight\Item;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Processor\Table\GotosProcessor
 */
class GotosProcessorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of getNonTerminals implementation
     *
     * @return void
     * @covers ::getNonTerminals
     */
    public function testGetNonTerminals(): void
    {
        /** @var SymbolCollection<NonTerminal> $nonTerminals */
        $nonTerminals = $this->createSymbolCollection(NonTerminal::class);
        $grammar = $this->createGrammarMock(['getNonTerminals']);
        $grammar->expects(self::once())
            ->method('getNonTerminals')
            ->willReturn($nonTerminals);
        $gotosProcessor = $this->createGotosProcessorMock(['getGrammar']);
        $gotosProcessor->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        self::assertSame($nonTerminals, $this->callNonPublicMethod($gotosProcessor, 'getNonTerminals'));
    }

    /**
     * Tester of process implementation
     *
     * @return void
     * @covers ::process
     */
    public function testProcess(): void
    {
        $state = 1;
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $nonTerminal = $this->createNonTerminalMock();
        /** @var SymbolCollection<NonTerminal> $nonTerminals */
        $nonTerminals = $this->createSymbolCollection(NonTerminal::class);
        $nonTerminalsData = [$nonTerminal];
        $this->setNonPublicPropertyValue($nonTerminals, 'data', $nonTerminalsData);
        $gotosProcessor = $this->createGotosProcessorMock(['getNonTerminals', 'processGotos']);
        $gotosProcessor->expects(self::once())
            ->method('getNonTerminals')
            ->willReturn($nonTerminals);
        $gotosProcessor->expects(self::once())
            ->method('processGotos')
            ->with(self::equalTo($items), self::equalTo($nonTerminal), self::equalTo($state));
        $gotosProcessor->process($items, $state);
    }

    /**
     * Tester of processGotos implementation
     *
     * @return void
     * @covers ::processGotos
     */
    public function testProcessGotos(): void
    {
        $items = $this->createSymbolCollection(Item::class);
        $state = 1;
        $nextState = 2;
        $tablePitch = 3;
        $index = 4;
        $tableIndex = $state * $tablePitch + $index;

        $nonTerminal = $this->createNonTerminalMock(['getIndex']);
        $nonTerminal->expects(self::once())->method('getIndex')->willReturn($index);
        $tableCollection = $this->createTableCollectionMock(['setTableItem']);
        $tableCollection->expects(self::once())
            ->method('setTableItem')
            ->with(self::equalTo($tableIndex), self::equalTo($nextState));
        $gotosProcessor = $this->createGotosProcessorMock(['processNextState', 'getTableCollection', 'getTablePitch']);
        $gotosProcessor->expects(self::once())
            ->method('processNextState')
            ->with(self::equalTo($items), self::equalTo($nonTerminal))
            ->willReturn($nextState);
        $gotosProcessor->expects(self::once())->method('getTablePitch')->willReturn($tablePitch);
        $gotosProcessor->expects(self::once())->method('getTableCollection')->willReturn($tableCollection);

        $this->callNonPublicMethod($gotosProcessor, 'processGotos', [$items, $nonTerminal, $state]);
    }

    /**
     * Tester of processGotos for null nextState implementation
     *
     * @return void
     * @covers ::processGotos
     */
    public function testProcessGotosNullNextState(): void
    {
        /** @var SymbolCollection<Item> $items */
        $items = $this->createSymbolCollection(Item::class);
        $state = 1;
        $nextState = null;

        $nonTerminal = $this->createNonTerminalMock();
        $tableCollection = $this->createTableCollectionMock(['setTableItem']);
        $tableCollection->expects(self::never())
            ->method('setTableItem');
        $gotosProcessor = $this->createGotosProcessorMock(['processNextState', 'getTableCollection']);
        $gotosProcessor->expects(self::once())
            ->method('processNextState')
            ->with(self::equalTo($items), self::equalTo($nonTerminal))
            ->willReturn($nextState);
        $gotosProcessor->expects(self::once())
            ->method('getTableCollection')
            ->willReturn($tableCollection);

        $this->callNonPublicMethod($gotosProcessor, 'processGotos', [$items, $nonTerminal, $state]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * GotosProcessor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return GotosProcessor&MockObject
     */
    protected function createGotosProcessorMock(array $methods = []): GotosProcessor&MockObject
    {
        $mock = $this->createPartialMock(GotosProcessor::class, $methods);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * Symbol collection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        $collection = new SymbolCollection($type);
        return $collection;
    }

    /**
     * TableCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TableCollection&MockObject
     */
    protected function createTableCollectionMock(array $methods = []): TableCollection&MockObject
    {
        $mock = $this->createPartialMock(TableCollection::class, $methods);
        return $mock;
    }

    /**
     * Terminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Terminal&MockObject
     */
    protected function createTerminalMock(array $methods = []): Terminal&MockObject
    {
        $mock = $this->createPartialMock(Terminal::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
