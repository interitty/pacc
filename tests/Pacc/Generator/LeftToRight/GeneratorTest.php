<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight;

use Interitty\Pacc\Generator\LeftToRight\Generator as LRGenerator;
use Interitty\Pacc\Generator\LeftToRight\Processor\Processor;
use Interitty\Pacc\Generator\Printer;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\BaseParser;
use Interitty\Tokenizer\BaseTokenizerParser;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Dumper;
use Nette\PhpGenerator\PhpNamespace;
use PHPUnit\Framework\MockObject\MockObject;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Generator
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class GeneratorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of createLiteral implementation
     *
     * @return void
     * @group integration
     * @covers ::createLiteral
     */
    public function testCreateLiteral(): void
    {
        $value = '$a = 1;';
        $generator = $this->createGeneratorMock();
        $phpLiteral = $this->callNonPublicMethod($generator, 'createLiteral', [$value]);
        self::assertSame($value, (string) $phpLiteral);
    }

    /**
     * Tester of createProcessor implementation
     *
     * @return void
     * @group integration
     * @covers ::createProcessor
     */
    public function testCreateProcessor(): void
    {
        $grammar = $this->createGrammarMock();
        $generator = $this->createGeneratorMock(['getGrammar']);
        $generator->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        $processor = $this->callNonPublicMethod($generator, 'createProcessor');
        self::assertSame($grammar, $this->callNonPublicMethod($processor, 'getGrammar'));
    }

    /**
     * Tester of processParseMethod implementation
     *
     * @return void
     * @covers ::processParseMethod
     * @group integration
     */
    public function testProcessParseMethod(): void
    {
        $code = '{' . PHP_EOL
            . '    /**' . PHP_EOL
            . '     * Alias of processParse()' . PHP_EOL
            . '     *' . PHP_EOL
            . '     * @return mixed' . PHP_EOL
            . '     */' . PHP_EOL
            . '    protected function processDoParse(): mixed' . PHP_EOL
            . '    {' . PHP_EOL
            . '        $result = $this->processParse();' . PHP_EOL
            . '        return $result;' . PHP_EOL
            . '    }' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $generator = $this->createGeneratorMock();
        $this->callNonPublicMethod($generator, 'processParseMethod', [$class, 'processDoParse']);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processProductions implementation
     *
     * @return void
     * @covers ::processProductions
     * @group integration
     */
    public function testProcessProductions(): void
    {
        $code = '{' . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsEqual = [];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsLefts = [2 => 1];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsLengths = [2 => 0];' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $productions = $this->createSymbolCollection(Production::class);
        $productions->add($production = $this->createProduction('test1', '$$ = false;')->setIndex(2));
        $generator = $this->createGeneratorMock(['getProductions', 'processProductionMethod']);
        $generator->expects(self::once())->method('getProductions')->willReturn($productions);
        $generator->expects(self::once())
            ->method('processProductionMethod')
            ->with(self::equalTo($class), self::equalTo($production));
        $this->callNonPublicMethod($generator, 'processProductions', [$class]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processProductions for equal Production implementation
     *
     * @return void
     * @covers ::processProductions
     * @group integration
     */
    public function testProcessProductionsEqual(): void
    {
        $code = '{' . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsEqual = [4 => 2];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsLefts = [2 => 1, 4 => 1];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsLengths = [2 => 0, 4 => 0];' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $productions = $this->createSymbolCollection(Production::class);
        $productions->add($this->createProduction('test1', '$$ = false;')->setIndex(2));
        $productions->add($this->createProduction('test2', '$$ = false;')->setIndex(4));
        $generator = $this->createGeneratorMock(['getProductions', 'processProductionMethod']);
        $generator->expects(self::once())->method('getProductions')->willReturn($productions);
        $generator->expects(self::once())->method('processProductionMethod');
        $this->callNonPublicMethod($generator, 'processProductions', [$class]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processProductions for empty Productions implementation
     *
     * @return void
     * @covers ::processProductions
     * @group integration
     */
    public function testProcessProductionsEmpty(): void
    {
        $code = '{' . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsEqual = [];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsLefts = [];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $productionsLengths = [];' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $productions = $this->createSymbolCollection(Production::class);
        $generator = $this->createGeneratorMock(['getProductions']);
        $generator->expects(self::once())->method('getProductions')->willReturn($productions);
        $this->callNonPublicMethod($generator, 'processProductions', [$class]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processProductionMethod implementation
     *
     * @return void
     * @covers ::processProductionMethod
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers
     * @group integration
     */
    public function testProcessProductionMethod(): void
    {
        $code = '{' . PHP_EOL
            . '    /**' . PHP_EOL
            . '     * Reduce 2 processor' . PHP_EOL
            . '     *' . PHP_EOL
            . '     * @param array{1: mixed[]|null} $arg' . PHP_EOL
            . '     * @return mixed[]|null' . PHP_EOL
            . '     */' . PHP_EOL
            . '    protected function processReduce2(array $arg): ?array' . PHP_EOL
            . '    {' . PHP_EOL
            . '        $reduced = $arg[1];' . PHP_EOL
            . '        return $reduced;' . PHP_EOL
            . '    }' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $production = $this->createProduction('test1', '$<array|null>$ = $<?array>1;')->setIndex(2);
        $generator = $this->createGeneratorMock(['getProductions', 'getNamespace']);
        $this->callNonPublicMethod($generator, 'processProductionMethod', [$class, $production]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processProductionMethod idefault code mplementation
     *
     * @return void
     * @covers ::processProductionMethod
     * @covers Interitty\Pacc\Generator\PhpGeneratorHelpers
     * @group integration
     */
    public function testProcessProductionMethodDefaultCode(): void
    {
        $code = '{' . PHP_EOL
            . '    /**' . PHP_EOL
            . '     * Reduce 3 processor' . PHP_EOL
            . '     *' . PHP_EOL
            . '     * @param array{1: mixed} $arg' . PHP_EOL
            . '     * @return mixed' . PHP_EOL
            . '     */' . PHP_EOL
            . '    protected function processReduce3(array $arg): mixed' . PHP_EOL
            . '    {' . PHP_EOL
            . '        $reduced = $arg[1];' . PHP_EOL
            . '        return $reduced;' . PHP_EOL
            . '    }' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $production = $this->createProduction('testDefault')->setIndex(3);
        $generator = $this->createGeneratorMock(['getProductions', 'getNamespace']);
        $this->callNonPublicMethod($generator, 'processProductionMethod', [$class, $production]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processTables implementation
     *
     * @return void
     * @covers ::processTables
     * @covers ::processProtectedProperty
     * @group integration
     */
    public function testProcessTables(): void
    {
        $code = '{' . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $table = [1, 2, 3];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int */' . PHP_EOL
            . '    protected int $tablePitch = 2;' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $processor = $this->createProcessorMock(['getTable', 'getTablePitch']);
        $processor->expects(self::once())->method('getTable')->willReturn([1, 2, 3]);
        $processor->expects(self::once())->method('getTablePitch')->willReturn(2);
        $generator = $this->createGeneratorMock(['getProcessor']);
        $generator->expects(self::once())->method('getProcessor')->willReturn($processor);

        $this->callNonPublicMethod($generator, 'processTables', [$class]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processTerminals implementation
     *
     * @return void
     * @covers ::processTerminals
     * @group integration
     */
    public function testProcessTerminals(): void
    {
        $code = '{' . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $terminalsTypes = [self::TYPE => 1];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $terminalsValues = [\'value\' => 2];' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $terminals = $this->createSymbolCollection(Terminal::class);
        $terminals->add($this->createTerminal('$name', 'TYPE', null)->setIndex(1));
        $terminals->add($this->createTerminal('$name', null, 'value')->setIndex(2));
        $generator = $this->createGeneratorMock(['getTerminals']);
        $generator->expects(self::once())->method('getTerminals')->willReturn($terminals);
        $this->callNonPublicMethod($generator, 'processTerminals', [$class]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processTerminals for empty Terminals implementation
     *
     * @return void
     * @covers ::processTerminals
     * @group integration
     */
    public function testProcessTerminalsEmpty(): void
    {
        $code = '{' . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $terminalsTypes = [];' . PHP_EOL
            . PHP_EOL
            . '    /** @var int[] */' . PHP_EOL
            . '    protected array $terminalsValues = [];' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $terminals = $this->createSymbolCollection(Terminal::class);
        $generator = $this->createGeneratorMock(['getTerminals']);
        $generator->expects(self::once())->method('getTerminals')->willReturn($terminals);
        $this->callNonPublicMethod($generator, 'processTerminals', [$class]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor implementation
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $grammar = $this->createGrammarMock();
        $processor = $this->createProcessorMock(['process']);
        $processor->expects(self::once())
            ->method('process');
        $generator = $this->createGeneratorMock(['getProcessor']);
        $generator->expects(self::once())
            ->method('getProcessor')
            ->willReturn($processor);
        $generator->__construct($grammar);
        self::assertSame($grammar, $this->callNonPublicMethod($generator, 'getGrammar'));
    }

    /**
     * Tester of Namespace getter/setter implementation
     *
     * @return void
     * @covers ::getNamespace
     * @covers ::setNamespace
     */
    public function testGetSetNamespace(): void
    {
        $namespace = $this->createNamespace('Test');
        $this->processTestGetSet(LRGenerator::class, 'namespace', $namespace);
    }

    /**
     * Tester of getOption implementation
     *
     * @return void
     * @covers ::getOption
     */
    public function testGetOption(): void
    {
        $optionsCollection = $this->createOptionsCollectionMock();
        $optionsCollection->setOption(OptionsCollection::OPTION_EXTENDS, BaseTokenizerParser::class);
        $grammar = $this->createGrammarMock(['getOptions']);
        $grammar->expects(self::once())
            ->method('getOptions')
            ->willReturn($optionsCollection);
        $generator = $this->createGeneratorMock(['getGrammar']);
        $generator->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        $extends = $this->callNonPublicMethod($generator, 'getOption', [OptionsCollection::OPTION_EXTENDS]);
        self::assertSame(BaseTokenizerParser::class, $extends);
    }

    /**
     * Tester of getOption for default value implementation
     *
     * @return void
     * @covers ::getOption
     */
    public function testGetOptionDefault(): void
    {
        $optionsCollection = $this->createOptionsCollectionMock();
        $grammar = $this->createGrammarMock(['getOptions']);
        $grammar->expects(self::once())
            ->method('getOptions')
            ->willReturn($optionsCollection);
        $generator = $this->createGeneratorMock(['getGrammar']);
        $generator->expects(self::once())
            ->method('getGrammar')
            ->willReturn($grammar);
        $defaultExtends = $this->callNonPublicMethod($generator, 'getOption', [OptionsCollection::OPTION_EXTENDS]);
        self::assertSame(BaseParser::class, $defaultExtends);
    }

    /**
     * Tester of Processor getter/setter implementation
     *
     * @return void
     * @covers ::getProcessor
     * @covers ::setProcessor
     */
    public function testGetSetProcessor(): void
    {
        $processor = $this->createProcessorMock();
        $this->processTestGetSet(LRGenerator::class, 'processor', $processor);
    }

    /**
     * Tester of getProcessor factory implementation
     *
     * @return void
     * @covers ::getProcessor
     */
    public function testGetProcessorFactory(): void
    {
        $processor = $this->createProcessorMock();
        $generator = $this->createGeneratorMock(['createProcessor']);
        $generator->expects(self::once())
            ->method('createProcessor')
            ->willReturn($processor);
        self::assertSame($processor, $this->callNonPublicMethod($generator, 'getProcessor'));
    }

    /**
     * Tester of processInner implementation
     *
     * @return void
     * @covers ::processInner
     */
    public function testProcessInner(): void
    {
        $parseMethod = 'processDoParse';
        $class = $this->createClassType();
        $generator = $this->createGeneratorMock(
            ['processParseMethod', 'processProductions', 'processTables', 'processTerminals', 'getOption']
        );
        $generator->expects(self::once())->method('processProductions')->with(self::equalTo($class));
        $generator->expects(self::once())->method('processTables')->with(self::equalTo($class));
        $generator->expects(self::once())->method('processTerminals')->with(self::equalTo($class));

        $generator->expects(self::once())->method('getOption')
            ->with(self::equalTo(OptionsCollection::OPTION_PARSE), self::equalTo($generator::DEFAULT_OPTION_PARSE))
            ->willReturn($parseMethod);
        $generator->expects(self::once())
            ->method('processParseMethod')
            ->with(self::equalTo($class), self::equalTo($parseMethod));
        $this->callNonPublicMethod($generator, 'processInner', [$class]);
    }

    /**
     * Tester of processInner for default parseMethod implementation
     *
     * @return void
     * @covers ::processInner
     */
    public function testProcessInnerDefault(): void
    {
        $class = $this->createClassType();
        $generator = $this->createGeneratorMock(
            ['processParseMethod', 'processProductions', 'processTables', 'processTerminals', 'getOption']
        );
        $generator->expects(self::once())->method('processProductions')->with(self::equalTo($class));
        $generator->expects(self::once())->method('processTables')->with(self::equalTo($class));
        $generator->expects(self::once())->method('processTerminals')->with(self::equalTo($class));

        $generator->expects(self::once())->method('getOption')
            ->with(self::equalTo(OptionsCollection::OPTION_PARSE), self::equalTo($generator::DEFAULT_OPTION_PARSE))
            ->willReturn($generator::DEFAULT_OPTION_PARSE);
        $generator->expects(self::never())
            ->method('processParseMethod');
        $this->callNonPublicMethod($generator, 'processInner', [$class]);
    }

    /**
     * Tester of processProductionHash implementation
     *
     * @return void
     * @covers ::processProductionHash
     */
    public function testProcessProductionHash(): void
    {
        $generator = $this->createGeneratorMock();

        $code = 'return null;';
        $codeProduction = $this->createProductionMock(['getCode']);
        $codeProduction->expects(self::exactly(2))->method('getCode')->willReturn($code);
        $defaultProduction = $this->createProductionMock(['getCode']);
        $defaultProduction->expects(self::exactly(2))->method('getCode')
            ->willReturn($generator::DEFAULT_PRODUCTION_CODE);
        $noCodeProduction = $this->createProductionMock(['getCode']);
        $noCodeProduction->expects(self::exactly(2))->method('getCode')->willReturn(null);
        $sameCodeProduction = $this->createProductionMock(['getCode']);
        $sameCodeProduction->expects(self::exactly(2))->method('getCode')->willReturn($code);

        $codeHash = $this->callNonPublicMethod($generator, 'processProductionHash', [$codeProduction]);
        $defaultHash = $this->callNonPublicMethod($generator, 'processProductionHash', [$defaultProduction]);
        $noCodeHash = $this->callNonPublicMethod($generator, 'processProductionHash', [$noCodeProduction]);
        $sameCodeHash = $this->callNonPublicMethod($generator, 'processProductionHash', [$sameCodeProduction]);

        self::assertNotSame($codeHash, $defaultHash);
        self::assertSame($codeHash, $sameCodeHash);
        self::assertSame($noCodeHash, $defaultHash);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * ClassType factory
     *
     * @param string|null $name [OPTIONAL]
     * @param PhpNamespace|null $namespace [OPTIONAL]
     * @return ClassType
     */
    protected function createClassType(string $name = null, PhpNamespace $namespace = null): ClassType
    {
        $classType = new ClassType($name, $namespace);
        return $classType;
    }

    /**
     * Dumper factory
     *
     * @return Dumper
     */
    protected function createDumper(): Dumper
    {
        $dumper = new Dumper();
        return $dumper;
    }

    /**
     * Generator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return LRGenerator&MockObject
     */
    protected function createGeneratorMock(array $methods = []): LRGenerator&MockObject
    {
        $grammar = $this->createGrammarMock();
        $printer = $this->createPrinterMock();
        $mock = $this->createPartialMock(LRGenerator::class, $methods);
        $this->callNonPublicMethod($mock, 'setPrinter', [$printer]);
        $this->callNonPublicMethod($mock, 'setGrammar', [$grammar]);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * Namespace factory
     *
     * @param string $name
     * @return PhpNamespace
     */
    protected function createNamespace(string $name): PhpNamespace
    {
        $namespace = new PhpNamespace($name);
        return $namespace;
    }

    /**
     * NonTerminal mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return NonTerminal&MockObject
     */
    protected function createNonTerminalMock(array $methods = []): NonTerminal&MockObject
    {
        $mock = $this->createPartialMock(NonTerminal::class, $methods);
        return $mock;
    }

    /**
     * OptionsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OptionsCollection&MockObject
     */
    protected function createOptionsCollectionMock(array $methods = []): OptionsCollection&MockObject
    {
        $mock = $this->createPartialMock(OptionsCollection::class, $methods);
        return $mock;
    }

    /**
     * Printer mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Printer&MockObject
     */
    protected function createPrinterMock(array $methods = []): Printer&MockObject
    {
        $dumper = $this->createDumper();
        $mock = $this->createPartialMock(Printer::class, $methods);
        $this->setNonPublicPropertyValue($mock, 'dumper', $dumper);
        return $mock;
    }

    /**
     * Production factory
     *
     * @param string $name
     * @param string|null $code [OPTIONAL]
     * @param array<'return'|int, array<'doc'|'type', string[]>>|null $types [OPTIONAL]
     * @return Production
     */
    protected function createProduction(string $name, ?string $code = null, array $types = null): Production
    {
        $left = $this->createNonTerminalMock();
        $this->callNonPublicMethod($left, 'setName', [$name]);
        $left->setIndex(1);
        $right = [];
        $production = new Production($left, $right, $code, $types);
        return $production;
    }

    /**
     * Production mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Production&MockObject
     */
    protected function createProductionMock(array $methods = []): Production&MockObject
    {
        $mock = $this->createPartialMock(Production::class, $methods);
        return $mock;
    }

    /**
     * Processor mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Processor&MockObject
     */
    protected function createProcessorMock(array $methods = []): Processor&MockObject
    {
        $mock = $this->createPartialMock(Processor::class, $methods);
        return $mock;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     * @template CollectionType of object
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        /** @var SymbolCollection<CollectionType> $symbolCollection */
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    protected function createTerminal(string $name, ?string $type = null, ?string $value = null): Terminal
    {
        $terminal = new Terminal($name, $type, $value);
        return $terminal;
    }

    // </editor-fold>
}
