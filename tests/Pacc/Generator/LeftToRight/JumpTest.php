<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\LeftToRight;

use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\LeftToRight\Jump
 */
class JumpTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of jump implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getItemFrom
     * @covers ::setItemFrom
     * @covers ::getSymbol
     * @covers ::setSymbol
     * @covers ::getItemTo
     * @covers ::setItemTo
     */
    public function testJump(): void
    {
        $itemFrom = $this->createSymbolCollection(Item::class);
        $symbol = $this->createNonTerminal('symbol');
        $itemTo = $this->createSymbolCollection(Item::class);
        $jump = $this->createJump($itemFrom, $symbol, $itemTo);
        self::assertSame($itemFrom, $jump->getItemFrom());
        self::assertSame($symbol, $jump->getSymbol());
        self::assertSame($itemTo, $jump->getItemTo());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Jump factory
     *
     * @param SymbolCollection<Item> $itemFrom
     * @param Symbol $symbol
     * @param SymbolCollection<Item> $itmeTo
     * @return Jump
     */
    protected function createJump(SymbolCollection $itemFrom, Symbol $symbol, SymbolCollection $itmeTo)
    {
        $jump = new Jump($itemFrom, $symbol, $itmeTo);
        return $jump;
    }

    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    /**
     * SymbolCollection factory
     *
     * @param string $type
     * @return SymbolCollection<mixed>
     * @template CollectionType of object
     * @phpstan-param class-string<CollectionType> $type
     * @phpstan-return SymbolCollection<CollectionType>
     */
    protected function createSymbolCollection(string $type): SymbolCollection
    {
        /** @var SymbolCollection<CollectionType> $symbolCollection */
        $symbolCollection = new SymbolCollection($type);
        return $symbolCollection;
    }

    // </editor-fold>
}
