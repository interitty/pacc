<?php

declare(strict_types=1);

namespace Interitty\Pacc\Generator\RecursiveDescent;

use Generator;
use Interitty\Pacc\Generator\Printer;
use Interitty\Pacc\Generator\RecursiveDescent\Generator as PaccGenerator;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Symbol\NonTerminal;
use Interitty\Pacc\Symbol\Production;
use Interitty\Pacc\Symbol\Symbol;
use Interitty\Pacc\Symbol\SymbolCollection;
use Interitty\Pacc\Symbol\Terminal;
use Interitty\PhpUnit\BaseTestCase;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use PHPUnit\Framework\MockObject\MockObject;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Pacc\Generator\RecursiveDescent\Generator
 */
class GeneratorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of phpizeRuleTree for string tree implementation
     *
     * @return void
     * @covers ::phpizeRuleTree
     * @group integration
     */
    public function testPhpizeRuleTreeString(): void
    {
        $generator = $this->createGeneratorMock();
        $expectedResult = '        $reduced = true;' . PHP_EOL;
        $result = $this->callNonPublicMethod($generator, 'phpizeRuleTree', ['$$ = true;', 1, 2, PHP_EOL]);
        self::assertSame($expectedResult, $result);
    }

    /**
     * Tester of processParse implementation
     *
     * @return void
     * @covers ::processParse
     * @group integration
     */
    public function testProcessParse(): void
    {
        $code = '{' . PHP_EOL
            . '    /**' . PHP_EOL
            . '     * Parse processor' . PHP_EOL
            . '     *' . PHP_EOL
            . '     * @return mixed' . PHP_EOL
            . '     */' . PHP_EOL
            . '    protected function processParse(): mixed' . PHP_EOL
            . '    {' . PHP_EOL
            . '        $status = $this->processTest();' . PHP_EOL
            . '        if ($status !== true) {' . PHP_EOL
            . '            throw new ParseException(\'Parse error\');' . PHP_EOL
            . '        }' . PHP_EOL
            . '        return null;' . PHP_EOL
            . '    }' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $generator = $this->createGeneratorMock();
        $this->callNonPublicMethod($generator, 'processParse', [$class, 'test']);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of processRules implementation
     *
     * @return void
     * @covers ::processRules
     * @group integration
     */
    public function testProcessRules(): void
    {
        $code = '{' . PHP_EOL
            . '    /**' . PHP_EOL
            . '     * Test processor' . PHP_EOL
            . '     *' . PHP_EOL
            . '     * @return bool' . PHP_EOL
            . '     */' . PHP_EOL
            . '    protected function processTest(): bool' . PHP_EOL
            . '    {' . PHP_EOL
            . '        $reduced = false;' . PHP_EOL
            . '        //$reduced = true;' . PHP_EOL
            . '        return $reduced;' . PHP_EOL
            . '    }' . PHP_EOL
            . '}' . PHP_EOL;
        $class = $this->createClassType();
        $rulesTree = [
            'test' => '//$$ = true;',
        ];
        $generator = $this->createGeneratorMock();
        $this->callNonPublicMethod($generator, 'processRules', [$class, $rulesTree]);
        self::assertSame($code, $this->createPrinterMock()->printClass($class));
    }

    /**
     * Tester of phpizeRuleTreeArray implementation
     *
     * @return void
     * @covers ::phpizeRuleTreeArray
     * @group integration
     */
    public function testPhpizeRuleTreeArray(): void
    {
        $code = ''
            . '        if ($this->processRightSymbol() === true) {' . PHP_EOL
            . '            $reduced = false;' . PHP_EOL
            . '        } else {' . PHP_EOL
            . '            $reduced = true;' . PHP_EOL
            . '        }' . PHP_EOL;
        $rulesTree = [
            'N:RightSymbol' => '$reduced = false;',
            '$' => '$$ = true;',
        ];
        $generator = $this->createGeneratorMock();
        $result = $this->callNonPublicMethod($generator, 'phpizeRuleTreeArray', [$rulesTree, 1, 2, PHP_EOL]);
        self::assertSame($code, $result);
    }

    /**
     * Tester of phpizeRuleTreeItem implementation
     *
     * @return void
     * @covers ::phpizeRuleTreeItem
     * @group integration
     */
    public function testPhpizeRuleTreeItem(): void
    {
        $code = ''
            . '        if ($this->processTest() === true) {' . PHP_EOL
            . '            if ($this->currentTokenLexeme() === \'Symbol\') {' . PHP_EOL
            . '                $this->next();' . PHP_EOL
            . '                $reduced = false;' . PHP_EOL
            . '            } elseif ($this->currentTokenType() === self::TERMINAL) {' . PHP_EOL
            . '                $this->next();' . PHP_EOL
            . '                $reduced = false;' . PHP_EOL
            . '            } else {' . PHP_EOL
            . '                $reduced = true;' . PHP_EOL
            . '            }' . PHP_EOL
            . '        }';
        $generator = $this->createGeneratorMock();
        $value = [
            'S:Symbol' => '$reduced = false;',
            'T:TERMINAL' => '$reduced = false;',
            '$' => '$reduced = true;',
        ];
        $result = $this->callNonPublicMethod($generator, 'phpizeRuleTreeItem', ['N:Test', $value, 1, 2, PHP_EOL, true]);
        self::assertSame($code, $result);
    }

    /**
     * Tester of processRulesTreeProduction implementation
     *
     * @return void
     * @covers ::processRulesTreeProduction
     * @group integration
     */
    public function testProcessRulesTreeProduction(): void
    {
        $expectedResult = [
            'Production1' => [
                '$' => '$reduced = true;',
            ],
            'Production2' => [
                'N:RightSymbol' => [
                    '$' => '$reduced = true;',
                ],
            ],
        ];
        $production = $this->createProduction('Production2');
        $rightSymbol = $this->createNonTerminal('RightSymbol');
        $right = [$rightSymbol];
        $this->callNonPublicMethod($production, 'setRight', [$right]);
        $generator = $this->createGeneratorMock();
        $return = ['Production1' => ['$' => '$reduced = true;']];
        $result = $this->callNonPublicMethod($generator, 'processRulesTreeProduction', [$production, $return]);
        self::assertSame($expectedResult, $result);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of processInner implementation
     *
     * @return void
     * @covers ::processInner
     */
    public function testProcessInner(): void
    {
        $class = $this->createClassType();
        $firstRuleKey = 'test';
        $grammar = $this->createGrammarMock();
        $rulesTree = [
            $firstRuleKey => 'blah',
        ];
        $generator = $this->createGeneratorMock(['processRulesTree', 'processRules', 'processParse']);
        $this->setNonPublicPropertyValue($generator, 'grammar', $grammar);
        $generator->expects(self::once())
            ->method('processRulesTree')
            ->willReturn($rulesTree);
        $generator->expects(self::once())
            ->method('processRules')
            ->with(self::equalTo($class), self::equalTo($rulesTree));
        $generator->expects(self::once())
            ->method('processParse')
            ->with(self::equalTo($class), self::equalTo($firstRuleKey));
        $this->callNonPublicMethod($generator, 'processInner', [$class]);
    }

    /**
     * Tester of processRulesTree implementation
     *
     * @return void
     * @covers ::processRulesTree
     */
    public function testProcessRulesTree(): void
    {
        $expectedResult = ['production1' => '$reduced = true;', 'production2' => '$reduced = false;'];

        $productions = $this->createSymbolCollectionProduction()
            ->add($productionOne = $this->createProduction('Production1'))
            ->add($productionTwo = $this->createProduction('Production2'));
        $generator = $this->createGeneratorMock(['getProductions', 'processRulesTreeProduction', 'treeLifting']);
        $generator->expects(self::once())->method('getProductions')->willReturn($productions);
        $consecutive = static fn(Production $production, array $return)
            => match ([$production, $return]) { // @phpstan-ignore-line
                [$productionOne, []] => [
                    'production1' => ['$' => '$reduced = true;'],
                ],
                [$productionTwo, ['production1' => ['$' => '$reduced = true;']]] => [
                    'production1' => ['$' => '$reduced = true;'],
                    'production2' => ['$' => '$reduced = false;'],
                ]
            };
        $generator->expects(self::exactly(2))->method('processRulesTreeProduction')->willReturnCallback($consecutive);
        $treeLifting = static fn($tree)=> match ([$tree]) { // @phpstan-ignore-line
                [['$' => '$reduced = true;']] => '$reduced = true;',
                [['$' => '$reduced = false;']] => '$reduced = false;',
        };
        $generator->expects(self::exactly(2))->method('treeLifting')->willReturnCallback($treeLifting);
        self::assertSame($expectedResult, $this->callNonPublicMethod($generator, 'processRulesTree'));
    }

    /**
     * DataProvider for processRulesTreeRight test
     *
     * @phpstan-return Generator<string, array{0: Symbol, 1: string}>
     */
    public function processRulesTreeRightDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="NonTerminal">
        yield 'NonTerminal' => [$this->createNonTerminal('NonTerminal'), 'N:NonTerminal'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Terminal with type">
        yield 'Terminal with type' => [$this->createTerminal('Terminal', 'integer'), 'T:integer'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Terminal with value">
        yield 'Terminal with value' => [$this->createTerminal('Terminal', null, '123'), 'S:123'];
        // </editor-fold>
    }

    /**
     * Tester of processRulesTreeRight implementation
     *
     * @param Symbol $right
     * @param string $expectedKey
     * @return void
     * @dataProvider processRulesTreeRightDataProvider
     * @covers ::processRulesTreeRight
     */
    public function testProcessRulesTreeRight(Symbol $right, string $expectedKey): void
    {
        $generator = $this->createGeneratorMock();
        self::assertSame($expectedKey, $this->callNonPublicMethod($generator, 'processRulesTreeRight', [$right]));
    }

    /**
     * Tester of phpizeRuleTree for array tree implementation
     *
     * @return void
     * @covers ::phpizeRuleTree
     */
    public function testPhpizeRuleTreeForArray(): void
    {
        $tree = [];
        $index = 1;
        $level = 2;
        $eol = PHP_EOL;
        $expectedResult = 'result';
        $generator = $this->createGeneratorMock(['phpizeRuleTreeArray']);
        $generator->expects(self::once())
            ->method('phpizeRuleTreeArray')
            ->with(self::equalTo($tree), self::equalTo($index), self::equalTo($level), self::equalTo($eol))
            ->willReturn($expectedResult);
        $result = $this->callNonPublicMethod($generator, 'phpizeRuleTree', [$tree, $index, $level, $eol]);
        self::assertSame($expectedResult, $result);
    }

    /**
     * Tester of treeLifting implementation
     *
     * @return void
     * @covers ::treeLifting
     */
    public function testTreeLifting(): void
    {
        $expectedResult = [
            'Production1' => [
                'N:RightSymbol' => '$reduced = true;',
                '$' => '$reduced = true;',
            ],
        ];
        $tree = [
            'Production1' => [
                'N:RightSymbol' => [
                    '$' => '$reduced = true;',
                ],
                '$' => '$reduced = true;',
            ],
        ];
        $generator = $this->createGeneratorMock();
        self::assertSame($expectedResult, $this->callNonPublicMethod($generator, 'treeLifting', [$tree]));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * ClassType factory
     *
     * @param string|null $name [OPTIONAL]
     * @param PhpNamespace|null $namespace [OPTIONAL]
     * @return ClassType
     */
    protected function createClassType(string $name = null, PhpNamespace $namespace = null): ClassType
    {
        $classType = new ClassType($name, $namespace);
        return $classType;
    }

    /**
     * Generator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return PaccGenerator&MockObject
     */
    protected function createGeneratorMock(array $methods = []): PaccGenerator&MockObject
    {
        $grammar = $this->createGrammarMock();
        $printer = $this->createPrinterMock();
        $mock = $this->createPartialMock(PaccGenerator::class, $methods);
        $this->callNonPublicMethod($mock, 'setPrinter', [$printer]);
        $this->callNonPublicMethod($mock, 'setGrammar', [$grammar]);
        return $mock;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * NonTerminal factory
     *
     * @param string $name
     * @return NonTerminal
     */
    protected function createNonTerminal(string $name): NonTerminal
    {
        $nonTerminal = new NonTerminal($name);
        return $nonTerminal;
    }

    /**
     * Printer mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Printer&MockObject
     */
    protected function createPrinterMock(array $methods = []): Printer&MockObject
    {
        $mock = $this->createPartialMock(Printer::class, $methods);
        return $mock;
    }

    /**
     * Production factory
     *
     * @param string $name
     * @param string|null $code [OPTIONAL]
     * @param array<'return'|int, array<'doc'|'type', string[]>> $types [OPTIONAL]
     * @return Production
     */
    protected function createProduction(string $name, ?string $code = null, array $types = []): Production
    {
        $left = $this->createNonTerminal($name);
        $left->setIndex(1);
        $right = [];
        $production = new Production($left, $right, $code, $types);
        return $production;
    }

    /**
     * SymbolCollection of Productions factory
     *
     * @return SymbolCollection<Production>
     */
    protected function createSymbolCollectionProduction(): SymbolCollection
    {
        /** @var SymbolCollection<Production> $symbolCollection */
        $symbolCollection = new SymbolCollection(Production::class);
        return $symbolCollection;
    }

    /**
     * Terminal factory
     *
     * @param string $name
     * @param string|null $type
     * @param string|null $value
     * @return Terminal
     */
    protected function createTerminal(string $name, ?string $type = null, ?string $value = null): Terminal
    {
        $terminal = new Terminal($name, $type, $value);
        return $terminal;
    }

    // </editor-fold>
}
