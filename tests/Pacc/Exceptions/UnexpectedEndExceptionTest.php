<?php

declare(strict_types=1);

namespace Interitty\Pacc\Exceptions;

use Exception;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Exceptions\UnexpectedEndException
 */
class UnexpectedEndExceptionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Constructor implementation
     *
     * @return void
     * @covers ::__construct
     * @group integration
     */
    public function testConstructor(): void
    {
        $previous = $this->createUnexpectedEndException();
        $exception = $this->createUnexpectedEndException($previous);
        self::assertSame('Unexpected end.', $exception->getMessage());
        self::assertSame(0, $exception->getCode());
        self::assertSame($previous, $exception->getPrevious());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * UnexpectedEndException factory
     *
     * @param Exception $previous [OPTIONAL]
     * @return UnexpectedEndException
     */
    protected function createUnexpectedEndException(Exception $previous = null): UnexpectedEndException
    {
        $exception = new UnexpectedEndException($previous);
        return $exception;
    }

    // </editor-fold>
}
