<?php

declare(strict_types=1);

namespace Interitty\Pacc\Exceptions;

use Exception;
use Interitty\Pacc\Tokens\Token;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\Exceptions\BadIdentifierException
 */
class BadIdentifierExceptionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Constructor implementation
     *
     * @return void
     * @group integration
     * @covers ::__construct
     * @covers ::getToken
     * @covers ::setToken
     */
    public function testConstructor(): void
    {
        $data = [
            'identifier' => 'test',
            'line' => 1,
            'position' => 2,
        ];
        $messageTemplate = 'Bad identifier ":identifier" on line :line at position :position';
        $token = $this->createToken(Token::TOKEN_STRING, $data['identifier'], $data['line'], $data['position']);
        $previous = $this->createBadIdentifierException($token);
        $exception = $this->createBadIdentifierException($token, $previous);
        self::assertSame($messageTemplate, $exception->getMessageTemplate());
        self::assertSame(0, $exception->getCode());
        self::assertSame($previous, $exception->getPrevious());
        self::assertSame($data, $exception->getData());
        self::assertSame($token, $exception->getToken());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BadIdentifierException factory
     *
     * @param Token $token
     * @param Exception $previous [OPTIONAL]
     * @return BadIdentifierException
     */
    protected function createBadIdentifierException(Token $token, Exception $previous = null): BadIdentifierException
    {
        $exception = new BadIdentifierException($token, $previous);
        return $exception;
    }

    /**
     * Token factory
     *
     * @param string $type
     * @param string $lexeme
     * @param int $line
     * @param int $position
     * @return Token
     */
    protected function createToken(string $type, string $lexeme, int $line, int $position): Token
    {
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    // </editor-fold>
}
