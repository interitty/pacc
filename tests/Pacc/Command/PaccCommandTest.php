<?php

declare(strict_types=1);

namespace Interitty\Pacc\Command;

use Exception;
use Interitty\Pacc\Generator\LeftToRight\Generator as LRGenerator;
use Interitty\Pacc\Generator\RecursiveDescent\Generator as RDGenerator;
use Interitty\Pacc\Grammar;
use Interitty\Pacc\Parser\OptionsCollection;
use Interitty\Pacc\Parser\TokenizerParser;
use Interitty\Pacc\Tokens\Token;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use Interitty\Utils\FileSystem;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use function array_merge;

/**
 * @coversDefaultClass Interitty\Pacc\Command\PaccCommand
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class PaccCommandTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration test">
    /**
     * Tester of LR generator implementation
     *
     * @return void
     * @covers ::createLRGenerator
     * @covers ::createTokenizerParser
     * @group integration
     */
    public function testProcessLR(): void
    {
        $outputContent = FileSystem::read(__DIR__ . '/../../../examples/Json.php');
        $inputFile = __DIR__ . '/../../../examples/Json.y';
        $outputFile = $this->createTempFile();
        FileSystem::delete($outputFile); // File not exists
        $input = $this->createInputMock();
        $input->expects(self::exactly(3))
            ->method('getOption')
            ->willReturnCallback(static fn(string $name) => match ([$name]) { // @phpstan-ignore-line
                    [PaccCommand::OPTION_INPUT] => $inputFile,
                    [PaccCommand::OPTION_ALGORITHM] => PaccCommand::ALGORITHM_LR,
                    [PaccCommand::OPTION_OUTPUT] => $outputFile,
            });
        $command = $this->createPaccCommandMock(['getInput', 'getOutput']);
        $command->expects(self::exactly(3))->method('getInput')->willReturn($input);
        $command->expects(self::once())->method('getOutput')->willReturn($this->createOutputMock());
        self::assertSame(PaccCommand::SUCCESS, $this->callNonPublicMethod($command, 'processExecute'));
        self::assertSame($outputContent, FileSystem::read($outputFile));
    }

    /**
     * Tester of RD generator implementation
     *
     * @return void
     * @covers ::createRDGenerator
     * @covers ::createTokenizerParser
     * @group integration
     */
    public function testProcessRD(): void
    {
        $outputContent = FileSystem::read(__DIR__ . '/../../../examples/GrammarParser.php');
        $inputFile = __DIR__ . '/../../../examples/GrammarParser.y';
        $outputFile = $this->createTempFile();
        FileSystem::delete($outputFile); // File not exists
        $input = $this->createInputMock();
        $input->expects(self::exactly(3))
            ->method('getOption')
            ->willReturnCallback(static fn(string $name) => match ([$name]) { // @phpstan-ignore-line
                    [PaccCommand::OPTION_INPUT] => $inputFile,
                    [PaccCommand::OPTION_ALGORITHM] => PaccCommand::ALGORITHM_RD,
                    [PaccCommand::OPTION_OUTPUT] => $outputFile,
            });
        $command = $this->createPaccCommandMock(['getInput', 'getOutput']);
        $command->expects(self::exactly(3))->method('getInput')->willReturn($input);
        $command->expects(self::once())->method('getOutput')->willReturn($this->createOutputMock());
        self::assertSame(PaccCommand::SUCCESS, $this->callNonPublicMethod($command, 'processExecute'));
        self::assertSame($outputContent, FileSystem::read($outputFile));
    }

    /**
     * Tester of processInputContent implementation
     *
     * @return void
     * @covers ::processInputContent
     * @group integration
     */
    public function testProcessInputContent(): void
    {
        $inputContent = 'inputContent';
        $inputFile = $this->createTempFile($inputContent);
        $command = $this->createPaccCommandMock(['processInputFile']);
        $command->expects(self::once())->method('processInputFile')->willReturn($inputFile);
        self::assertSame($inputContent, $this->callNonPublicMethod($command, 'processInputContent'));
    }

    /**
     * Tester of processOutput implementation
     *
     * @return void
     * @covers ::processOutput
     * @group integration
     */
    public function testProcessOutput(): void
    {
        $generated = 'generated';
        $outputFile = $this->createTempFile();
        FileSystem::delete($outputFile); // File not exists
        $generator = $this->createLRGeneratorMock(['getGenerated']);
        $generator->expects(self::once())->method('getGenerated')->willReturn($generated);
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')->with(self::equalTo(PaccCommand::OPTION_OUTPUT))->willReturn($outputFile);
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        $this->callNonPublicMethod($command, 'processOutput', [$generator]);
        self::assertSame($generated, FileSystem::read($outputFile));
    }

    /**
     * Tester of processOutput force implementation
     *
     * @return void
     * @covers ::processOutput
     * @group integration
     */
    public function testProcessOutputForce(): void
    {
        $generated = 'generated';
        $outputFile = $this->createTempFile(); // File exists
        $generator = $this->createLRGeneratorMock(['getGenerated']);
        $generator->expects(self::once())->method('getGenerated')->willReturn($generated);
        $input = $this->createInputMock();
        $input->expects(self::exactly(2))
            ->method('getOption')
            ->willReturnCallback(static fn(string $name) => match ([$name]) { // @phpstan-ignore-line
                    [PaccCommand::OPTION_OUTPUT] => $outputFile,
                    [PaccCommand::OPTION_FORCE] => true,
            });
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        $this->callNonPublicMethod($command, 'processOutput', [$generator]);
        self::assertSame($generated, FileSystem::read($outputFile));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of configure implementation
     *
     * @return void
     * @covers ::configure
     */
    public function testConfigure(): void
    {
        $text = 'Parser generator (currently generates recursive descent parser and canonical LR(1) parser) for PHP.';
        $command = $this->createPaccCommandMock(['setDescription', 'addOption']);
        $option = static fn(string $name, string|array $shortcut = null, int $mode = null, string $description = '')
                => match ([$name, $shortcut, $mode, $description]) { // @phpstan-ignore-line
                    [$command::OPTION_ALGORITHM, 'a', InputOption::VALUE_OPTIONAL, 'algorithm for generated parser'],
                    [$command::OPTION_FORCE, 'f', InputOption::VALUE_NONE, 'force output file overwrite if exists'],
                    [$command::OPTION_INPUT, 'i', InputOption::VALUE_OPTIONAL, 'input file'],
                    [$command::OPTION_OUTPUT, 'o', InputOption::VALUE_OPTIONAL, 'output file'] => $command,
                };
        $command->expects(self::once())->method('setDescription')->with(self::equalTo($text));
        $command->expects(self::exactly(4))->method('addOption')->willReturnCallback($option);
        $this->callNonPublicMethod($command, 'configure');
    }

    /**
     * Tester of createGenerator for exception implementation
     *
     * @return void
     * @covers ::createGenerator
     * @group negative
     */
    public function testCreateGeneratorException(): void
    {
        $algorithm = 'error';
        $input = 'inputContent';
        $grammar = $this->createGrammarMock();
        $command = $this->createPaccCommandMock(['processAlgorithm', 'processGrammar']);
        $command->expects(self::once())->method('processGrammar')
            ->with(self::equalTo($input))->willReturn($grammar);
        $command->expects(self::once())->method('processAlgorithm')
            ->with(self::equalTo($grammar))->willReturn($algorithm);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Unknown algorithm ":algorithm"');
        $this->expectExceptionData(['algorithm' => $algorithm]);
        $this->callNonPublicMethod($command, 'createGenerator', [$input]);
    }

    /**
     * Tester of createGenerator for LR implementation
     *
     * @return void
     * @covers ::createGenerator
     */
    public function testCreateGeneratorLR(): void
    {
        $algorithm = PaccCommand::ALGORITHM_LR;
        $input = 'inputContent';
        $generator = $this->createLRGeneratorMock();
        $grammar = $this->createGrammarMock();
        $command = $this->createPaccCommandMock(['createLRGenerator', 'processAlgorithm', 'processGrammar']);
        $command->expects(self::once())->method('processGrammar')
            ->with(self::equalTo($input))->willReturn($grammar);
        $command->expects(self::once())->method('processAlgorithm')
            ->with(self::equalTo($grammar))->willReturn($algorithm);
        $command->expects(self::once())->method('createLRGenerator')
            ->with(self::equalTo($grammar))->willReturn($generator);
        self::assertSame($generator, $this->callNonPublicMethod($command, 'createGenerator', [$input]));
    }

    /**
     * Tester of createGenerator for RD implementation
     *
     * @return void
     * @covers ::createGenerator
     */
    public function testCreateGeneratorRD(): void
    {
        $algorithm = PaccCommand::ALGORITHM_RD;
        $input = 'inputContent';
        $generator = $this->createRDGeneratorMock();
        $grammar = $this->createGrammarMock();
        $command = $this->createPaccCommandMock(['createRDGenerator', 'processAlgorithm', 'processGrammar']);
        $command->expects(self::once())->method('processGrammar')
            ->with(self::equalTo($input))->willReturn($grammar);
        $command->expects(self::once())->method('processAlgorithm')
            ->with(self::equalTo($grammar))->willReturn($algorithm);
        $command->expects(self::once())->method('createRDGenerator')
            ->with(self::equalTo($grammar))->willReturn($generator);
        self::assertSame($generator, $this->callNonPublicMethod($command, 'createGenerator', [$input]));
    }

    /**
     * Tester of processAlgorithm implementation
     *
     * @return void
     * @covers ::processAlgorithm
     */
    public function testProcessAlgorithm(): void
    {
        $algorithm = PaccCommand::ALGORITHM_LR;
        $grammar = $this->createGrammarMock();
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo(PaccCommand::OPTION_ALGORITHM))
            ->willReturn($algorithm);
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        self::assertSame($algorithm, $this->callNonPublicMethod($command, 'processAlgorithm', [$grammar]));
    }

    /**
     * Tester of processAlgorithm for default implementation
     *
     * @return void
     * @covers ::processAlgorithm
     */
    public function testProcessAlgorithmDefault(): void
    {
        $algorithm = PaccCommand::ALGORITHM_RD;
        $options = $this->createOptionsCollection(['hasOption', 'getOption']);
        $options->expects(self::once())
            ->method('hasOption')
            ->with(self::equalTo(OptionsCollection::OPTION_ALGORITHM))
            ->willReturn(false);
        $grammar = $this->createGrammarMock(['getOptions']);
        $grammar->expects(self::once())->method('getOptions')->willReturn($options);
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo(PaccCommand::OPTION_ALGORITHM))
            ->willReturn(null);
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        self::assertSame($algorithm, $this->callNonPublicMethod($command, 'processAlgorithm', [$grammar]));
    }

    /**
     * Tester of processAlgorithm for no option implementation
     *
     * @return void
     * @covers ::processAlgorithm
     */
    public function testProcessAlgorithmNoOption(): void
    {
        $algorithm = PaccCommand::ALGORITHM_LR;
        $options = $this->createOptionsCollection(['hasOption', 'getOption']);
        $options->expects(self::once())
            ->method('hasOption')
            ->with(self::equalTo(OptionsCollection::OPTION_ALGORITHM))
            ->willReturn(true);
        $options->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo(OptionsCollection::OPTION_ALGORITHM))
            ->willReturn($algorithm);
        $grammar = $this->createGrammarMock(['getOptions']);
        $grammar->expects(self::once())->method('getOptions')->willReturn($options);
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo(PaccCommand::OPTION_ALGORITHM))
            ->willReturn(null);
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        self::assertSame($algorithm, $this->callNonPublicMethod($command, 'processAlgorithm', [$grammar]));
    }

    /**
     * Tester of processException handler
     *
     * @return void
     * @covers ::processException
     */
    public function testProcessException(): void
    {
        $input = 'input content';
        $exception = $this->createException('Exception message');
        $output = $this->createOutputMock();
        $error = [
            $exception->getMessage(),
        ];
        $output->expects(self::once())->method('isVerbose')->willReturn(false);
        $output->expects(self::once())->method('isDebug')->willReturn(false);
        $command = $this->createPaccCommandMock(['getErrorOutput', 'writeError']);
        $command->expects(self::once())->method('getErrorOutput')->willReturn($output);
        $command->expects(self::once())->method('writeError')->with(self::equalTo($error));
        $result = $this->callNonPublicMethod($command, 'processException', [$exception, $input]);
        self::assertSame(PaccCommand::FAILURE, $result);
    }

    /**
     * Tester of processException debug handler
     *
     * @return void
     * @covers ::processException
     */
    public function testProcessExceptionDebug(): void
    {
        $input = 'input content';
        $token = $this->createToken(Token::TOKEN_STRING, 'content', 1, 7);
        $previous = $this->createException('Exception message');
        $exception = $this->createUnexpectedTokenException($token, $previous);
        $output = $this->createOutputMock();
        $error = [
            $exception->getMessage(),
            '1: input content',
            '         ^',
            '',
            $exception->getTraceAsString(),
        ];
        $output->expects(self::once())->method('isVerbose')->willReturn(true);
        $output->expects(self::once())->method('isDebug')->willReturn(true);
        $command = $this->createPaccCommandMock(['getErrorOutput', 'writeError']);
        $command->expects(self::once())->method('getErrorOutput')->willReturn($output);
        $command->expects(self::once())->method('writeError')->with(self::equalTo($error));
        $result = $this->callNonPublicMethod($command, 'processException', [$exception, $input]);
        self::assertSame(PaccCommand::FAILURE, $result);
    }

    /**
     * Tester of processException verbose handler
     *
     * @return void
     * @covers ::processException
     */
    public function testProcessExceptionVerbose(): void
    {
        $input = 'input content';
        $token = $this->createToken(Token::TOKEN_STRING, 'content', 1, 7);
        $previous = $this->createException('Exception message');
        $exception = $this->createUnexpectedTokenException($token, $previous);
        $output = $this->createOutputMock();
        $error = [
            $exception->getMessage(),
            '1: input content',
            '         ^',
        ];
        $output->expects(self::once())->method('isVerbose')->willReturn(true);
        $output->expects(self::once())->method('isDebug')->willReturn(false);
        $command = $this->createPaccCommandMock(['getErrorOutput', 'writeError']);
        $command->expects(self::once())->method('getErrorOutput')->willReturn($output);
        $command->expects(self::once())->method('writeError')->with(self::equalTo($error));
        $result = $this->callNonPublicMethod($command, 'processException', [$exception, $input]);
        self::assertSame(PaccCommand::FAILURE, $result);
    }

    /**
     * Tester of processExecute implementation
     *
     * @return void
     * @covers ::processExecute
     */
    public function testProcessExecute(): void
    {
        $generator = $this->createLRGeneratorMock();
        $input = 'inputContent';
        $command = $this->createPaccCommandMock(['createGenerator', 'processInputContent', 'processOutput']);
        $command->expects(self::once())
            ->method('processInputContent')
            ->willReturn($input);
        $command->expects(self::once())
            ->method('createGenerator')
            ->with(self::equalTo($input))
            ->willReturn($generator);
        $command->expects(self::once())
            ->method('processOutput')
            ->with(self::equalTo($generator));
        self::assertSame(PaccCommand::SUCCESS, $this->callNonPublicMethod($command, 'processExecute'));
    }

    /**
     * Tester of processGrammar implementation
     *
     * @return void
     * @covers ::processGrammar
     */
    public function testProcessGrammar(): void
    {
        $input = 'inputContent';
        $output = $this->createOutputMock();
        $grammar = $this->createGrammarMock(['hasName', 'setOutput']);
        $grammar->expects(self::once())->method('hasName')->willReturn(true);
        $grammar->expects(self::once())->method('setOutput')->with(self::equalTo($output));
        $parser = $this->createTokenizerParserMock(['getGrammar', 'parse']);
        $parser->expects(self::once())->method('parse')->with(self::equalTo($input));
        $parser->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $command = $this->createPaccCommandMock(['createTokenizerParser', 'getOutput']);
        $command->expects(self::once())->method('createTokenizerParser')->willReturn($parser);
        $command->expects(self::once())->method('getOutput')->willReturn($output);
        self::assertSame($grammar, $this->callNonPublicMethod($command, 'processGrammar', [$input]));
    }

    /**
     * Tester of processGrammar for exception implementation
     *
     * @return void
     * @covers ::processGrammar
     * @group negative
     */
    public function testProcessGrammarException(): void
    {
        $input = 'inputContent';
        $grammar = $this->createGrammarMock(['hasName', 'setOutput']);
        $grammar->expects(self::once())->method('hasName')->willReturn(false);
        $parser = $this->createTokenizerParserMock(['getGrammar', 'parse']);
        $parser->expects(self::once())->method('parse')->with(self::equalTo($input));
        $parser->expects(self::once())->method('getGrammar')->willReturn($grammar);
        $command = $this->createPaccCommandMock(['createTokenizerParser', 'getOutput']);
        $command->expects(self::once())->method('createTokenizerParser')->willReturn($parser);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('The "grammar" statement omitted');
        $this->expectExceptionData();
        $this->callNonPublicMethod($command, 'processGrammar', [$input]);
    }

    /**
     * Tester of processInputFile implementation
     *
     * @return void
     * @covers ::processInputFile
     */
    public function testProcessInputFile(): void
    {
        $inputFile = 'inputFile';
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo(PaccCommand::OPTION_INPUT))
            ->willReturn($inputFile);
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        self::assertSame($inputFile, $this->callNonPublicMethod($command, 'processInputFile'));
    }

    /**
     * Tester of processInputFile default implementation
     *
     * @return void
     * @covers ::processInputFile
     */
    public function testProcessInputFileDefault(): void
    {
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')
            ->with(self::equalTo(PaccCommand::OPTION_INPUT))
            ->willReturn(null);
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        self::assertSame('php://stdin', $this->callNonPublicMethod($command, 'processInputFile'));
    }

    /**
     * Tester of processOutput default implementation
     *
     * @return void
     * @covers ::processOutput
     */
    public function testProcessOutputDefault(): void
    {
        $generated = 'generated';
        $generator = $this->createLRGeneratorMock(['getGenerated']);
        $generator->expects(self::once())->method('getGenerated')->willReturn($generated);
        $input = $this->createInputMock();
        $input->expects(self::once())
            ->method('getOption')->with(self::equalTo(PaccCommand::OPTION_OUTPUT))->willReturn(null);
        $output = $this->createOutputMock();
        $output->expects(self::once())->method('write')->with(self::equalTo($generated));
        $command = $this->createPaccCommandMock(['getInput', 'getOutput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        $command->expects(self::once())->method('getOutput')->willReturn($output);
        $this->callNonPublicMethod($command, 'processOutput', [$generator]);
    }

    /**
     * Tester of processOutput exception implementation
     *
     * @return void
     * @covers ::processOutput
     * @group negative
     */
    public function testProcessOutputException(): void
    {
        $outputFile = $this->createTempFile(); // File exists
        $generator = $this->createLRGeneratorMock(['getGenerated']);
        $generator->expects(self::never())->method('getGenerated');
        $input = $this->createInputMock();
        $input->expects(self::exactly(2))
            ->method('getOption')
            ->willReturnCallback(static fn(string $name) => match ([$name]) { // @phpstan-ignore-line
                    [PaccCommand::OPTION_OUTPUT] => $outputFile,
                    [PaccCommand::OPTION_FORCE] => false,
            });
        $command = $this->createPaccCommandMock(['getInput']);
        $command->expects(self::once())->method('getInput')->willReturn($input);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Output file already exists. Use -f to force overwrite');
        $this->expectExceptionData();
        $this->callNonPublicMethod($command, 'processOutput', [$generator]);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Exception factory
     *
     * @param string $message
     * @param int $code [OPTIONAL]
     * @param Exception|null $previous [OPTIONAL]
     * @return Exception
     */
    protected function createException(string $message, int $code = 0, Exception $previous = null): Exception
    {
        $exception = new Exception($message, $code, $previous);
        return $exception;
    }

    /**
     * Grammar mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Grammar&MockObject
     */
    protected function createGrammarMock(array $methods = []): Grammar&MockObject
    {
        $mock = $this->createPartialMock(Grammar::class, $methods);
        return $mock;
    }

    /**
     * Input mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return InputInterface&MockObject
     */
    protected function createInputMock(array $methods = []): InputInterface&MockObject
    {
        $mockMethods = array_merge($methods, [
            'getFirstArgument',
            'hasParameterOption',
            'getParameterOption',
            'bind',
            'validate',
            'getArguments',
            'getArgument',
            'setArgument',
            'hasArgument',
            'getOptions',
            'getOption',
            'setOption',
            'hasOption',
            'isInteractive',
            'setInteractive',
            '__toString',
        ]);
        $mock = $this->createPartialMock(InputInterface::class, $mockMethods);
        return $mock;
    }

    /**
     * LRGenerator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return LRGenerator&MockObject
     */
    protected function createLRGeneratorMock(array $methods = []): LRGenerator&MockObject
    {
        $mock = $this->createPartialMock(LRGenerator::class, $methods);
        return $mock;
    }

    /**
     * OptionsCollection mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OptionsCollection&MockObject
     */
    protected function createOptionsCollection(array $methods = []): OptionsCollection&MockObject
    {
        $mock = $this->createPartialMock(OptionsCollection::class, $methods);
        return $mock;
    }

    /**
     * Output mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OutputInterface&MockObject
     */
    protected function createOutputMock(array $methods = []): OutputInterface&MockObject
    {
        $mockMethods = array_merge($methods, [
            'write',
            'writeln',
            'setVerbosity',
            'getVerbosity',
            'isQuiet',
            'isVerbose',
            'isVeryVerbose',
            'isDebug',
            'setDecorated',
            'isDecorated',
            'setFormatter',
            'getFormatter',
        ]);
        $mock = $this->createPartialMock(OutputInterface::class, $mockMethods);
        return $mock;
    }

    /**
     * PaccCommand mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return PaccCommand&MockObject
     */
    protected function createPaccCommandMock(array $methods = []): PaccCommand&MockObject
    {
        $mock = $this->createPartialMock(PaccCommand::class, $methods);
        return $mock;
    }

    /**
     * RDGenerator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return RDGenerator&MockObject
     */
    protected function createRDGeneratorMock(array $methods = []): RDGenerator&MockObject
    {
        $mock = $this->createPartialMock(RDGenerator::class, $methods);
        return $mock;
    }

    /**
     * TokenizerParser mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return TokenizerParser&MockObject
     */
    protected function createTokenizerParserMock(array $methods = []): TokenizerParser&MockObject
    {
        $mock = $this->createPartialMock(TokenizerParser::class, $methods);
        return $mock;
    }

    /**
     * Token factory
     *
     * @param string $type
     * @param string $lexeme
     * @param int $line
     * @param int $position
     * @return Token
     */
    protected function createToken(string $type, string $lexeme, int $line, int $position): Token
    {
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    /**
     * UnexpectedTokenException factory
     *
     * @param Token $token
     * @param Exception $previous
     * @return UnexpectedTokenException
     */
    protected function createUnexpectedTokenException(Token $token, Exception $previous): UnexpectedTokenException
    {
        $exception = new UnexpectedTokenException($token, $previous);
        return $exception;
    }

    // </editor-fold>
}
