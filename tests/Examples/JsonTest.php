<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Generator;
use Interitty\Pacc\Exceptions\ParseException;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Nette\Utils\Json as NetteJson;
use Throwable;

/**
 * @coversDefaultClass Interitty\Pacc\Json
 */
class JsonTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Dataprovider for parseInput
     *
     * @phpstan-return Generator<string, array{0: string, 1: mixed}>
     */
    public function parseDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="{}">
        yield '{}' => ['{}', Json::decode('{}')];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="[]">
        yield '[]' => ['[]', Json::decode('[]')];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="true">
        yield 'true' => ['true', Json::decode('true')];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="{"int":1,"float":1.1}">
        yield '{"int":1,"float":1.1}' => ['{"int":1,"float":1.1}', Json::decode('{"int":1,"float":1.1}')];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="{"test":true}">
        yield '{"test":true}' => ['{"test":true}', NetteJson::decode('{"test":true}')];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="{"object": {"array": [{"key": "value"}]}}">
        yield '{"object":{"array":[{"key":"value"}]}}' => [
            '{"object":{"array":[{"key":"value"}]}}',
            NetteJson::decode('{"object":{"array":[{"key":"value"}]}}'),
        ];
        // </editor-fold>
    }

    /**
     * Tester of parseInput implementation
     *
     * @param string $string
     * @param mixed $expected
     * @return void
     * @dataProvider parseDataProvider
     */
    public function testParseInput(string $string, $expected): void
    {
        $decoded = Json::parseInput($string);
        $json = NetteJson::encode($decoded);
        self::assertEquals($expected, $decoded);
        self::assertSame($string, $json);
    }

    /**
     * Dataprovider for parseInputError
     *
     * @phpstan-return Generator<string, array{
     *      0: string,
     *      1: class-string<Throwable>,
     *      2: string,
     *      3: array<string, string>,
     *  }>
     */
    public function parseErrorDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="{">
        yield '{' => ['{', IllegalActionException::class, 'Given text is not valid JSON string', []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="{\"}">
        yield '{"}' => ['{"}', ParseException::class, 'Unexpected character ":character"', ['character' => '"}']];
        // </editor-fold>
    }

    /**
     * Tester of parseInput for error handling implementation
     *
     * param string $string
     * @phpstan-param class-string<Throwable> $class
     * @param string $message
     * @phpstan-param array<string, string> $data
     * @return void
     * @group negative
     * @dataProvider parseErrorDataProvider
     */
    public function testParseInputError(string $string, string $class, string $message, array $data): void
    {
        $this->expectException($class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionData($data);
        Json::parseInput($string);
    }

    // </editor-fold>
}
