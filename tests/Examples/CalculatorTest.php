<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Generator;
use Interitty\Pacc\Exceptions\ParseException;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Throwable;

/**
 * @coversDefaultClass Interitty\Pacc\Calculator
 */
class CalculatorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Dataprovider for parseInput
     *
     * @phpstan-return Generator<string, array{0: string, 1: mixed}>
     */
    public function parseDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="1+1">
        yield '1+1' => ['1+1', 2];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="2 * 3">
        yield '2 * 3' => ['2 * 3', 6];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="(1 + 2) * 3">
        yield '(1 + 2) * 3' => ['(1 + 2) * 3', 9];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="((1 + 2) * (1 + 2))">
        yield '((1 + 2) * (1 + 2))' => ['((1 + 2) * (1 + 2))', 9];
        // </editor-fold>
    }

    /**
     * Tester of parseInput implementation
     *
     * @param string $string
     * @param mixed $expected
     * @return void
     * @dataProvider parseDataProvider
     */
    public function testParseInput(string $string, $expected): void
    {
        $decoded = Calculator::parseInput($string);
        self::assertEquals($expected, $decoded);
    }

    /**
     * Dataprovider for parseInputError
     *
     * @phpstan-return Generator<string, array{
     *      0: string,
     *      1: class-string<Throwable>,
     *      2: string,
     *      3: array<string, string>,
     *  }>
     */
    public function parseErrorDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="1 ++ 2">
        yield '1 ++ 2' => ['1 ++ 2', IllegalActionException::class, 'Given text is not valid countable string', []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="{">
        yield '{' => ['{', ParseException::class, 'Unexpected character ":character"', ['character' => '{']];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="1 / 0">
        yield '1 / 0' => ['1 / 0', ParseException::class, 'Division by zero', []];
        // </editor-fold>
    }

    /**
     * Tester of parseInput for error handling implementation
     *
     * @param string $string
     * @phpstan-param class-string<Throwable> $class
     * @param string $message
     * @phpstan-param array<string, string> $data
     * @return void
     * @group negative
     * @dataProvider parseErrorDataProvider
     */
    public function testParseInputError(string $string, string $class, string $message, array $data): void
    {
        $this->expectException($class);
        $this->expectExceptionMessage($message);
        $this->expectExceptionData($data);
        Calculator::parseInput($string);
    }

    // </editor-fold>
}
