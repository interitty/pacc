<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Generator;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Pacc\SimpleParser
 */
class SimpleParserTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Dataprovider for parseInput
     *
     * @phpstan-return Generator<string, array{0: string, 1: string}>
     */
    public function parseDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="string">
        yield 'string' => ['"string"', 'string'];
        // </editor-fold>
    }

    /**
     * Tester of parseInput implementation
     *
     * @param string $string
     * @param mixed $expected
     * @return void
     * @dataProvider parseDataProvider
     */
    public function testParseInput(string $string, $expected): void
    {
        $decoded = SimpleParser::parseInput($string);
        self::assertEquals($expected, $decoded);
    }

    // </editor-fold>
}
