<?php

declare(strict_types=1);

namespace Interitty\Pacc;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Utils\FileSystem;

/**
 * @coversDefaultClass Interitty\Pacc\GrammarParser
 */
class GrammarParserTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Dataprovider for grammarParser
     *
     * @phpstan-return Generator<string, array<string>>
     */
    public function grammarParserDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Calculator.y">
        yield 'Calculator.y' => [__DIR__ . '/../../examples/Calculator.y'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GrammarParser.y">
        yield 'GrammarParser.y' => [__DIR__ . '/../../examples/GrammarParser.y'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Json.y">
        yield 'Json.y' => [__DIR__ . '/../../examples/Json.y'];
        // </editor-fold>
    }

    /**
     * Tester of GrammarParser implementation
     *
     * @param string $grammarFile
     * @return void
     * @dataProvider grammarParserDataProvider
     */
    public function testGrammarParser(string $grammarFile): void
    {
        self::assertFileExists($grammarFile);
        $string = FileSystem::read($grammarFile);

        $grammarParser = new GrammarParser();
        $result = $grammarParser->parse($string);
        self::assertNull($result);
    }

    // </editor-fold>
}
